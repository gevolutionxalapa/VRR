<!--template-->
@extends('template.main2')
<!--menu-->

<!-- Contenido -->
@section('content')
<div class="container">
	<div class="row">
    <div class="col-md-12 text-center">
      <h4>Reportes</h4>
    </div>
		<div class="col-md-12">

			<!-- menu interno -->
      		@include('estadisticas.submenus.submenu')
			<!-- Contenido -->


			@include('estadisticas.map.map')
					<!-- Fin pestañas -->

			<!-- Pies -->


		</div>
	</div>
</div>


<script type="text/javascript">
	window.onload = function(){
		var ruta = "{{route('registros-robo')}}";
		var datos = null;
		var respuesta = peticionInfomacion('get',ruta ,datos);
		pintarron(respuesta);
	};
</script>





@endsection
