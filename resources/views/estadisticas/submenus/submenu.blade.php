@section('title')
Reportes
@endsection

 
<div class="row">
  <div class="col-md-12">
    <ul class="nav nav-pills flex-column flex-sm-row" id="myTab" role="tablist">
      <li class="nav-item2" id="a_report">
        <a class="nav-link active btn-outline-dark" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Robados</a>
      </li>
      <li class="nav-item2" id="b_report">
        <a class="nav-link btn-outline-dark" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Recuperados</a>
      </li>
    </ul>
    <div class="tab-content text-center" id="myTabContent">
      <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
        <br>
        <h5>Incidencia de vehículo robados</h5>
      </div>
      <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
        <br>
        <h5>Incidencia de vehículo recuperados</h5>
      </div>
    </div>

  </div>
</div>

<!-- librerias -->
<script src="{{ asset('js/denuncia/peticiones.js') }}"></script>

<!-- Acciones del apartado -->
<script type="text/javascript">
  $('#a_report').click(function(){
    var ruta = "{{route('registros-robo')}}";
    var datos = null;
    var respuesta = peticionInfomacion('get',ruta ,datos);
    pintarron(respuesta);

  });

  $('#b_report').click(function(){
    var ruta = "{{route('registros-recuperados')}}";
    var datos = null;
    var respuesta = peticionInfomacion('get',ruta,datos);
    pintarron(respuesta);

  });

  function pintarron(respuesta){
    var locations = [];
    for(i=0; i< respuesta.length; i++){
      var puntoMap = [respuesta[i]['latitud'],respuesta[i]['longitud']];
      locations.push(puntoMap);
    }
    
    var map = new google.maps.Map(document.getElementById('map_canvas'), {
      zoom: 8,
      center: new google.maps.LatLng(19.976312,-96.7279684),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var marker, i;
    for (i = 0; i < locations.length; i++) {
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][0], locations[i][1]),
        map: map
      });
      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  }



</script>
