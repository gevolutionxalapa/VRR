@section('title')
Panel de validación
@endsection


 <link rel="stylesheet" type="text/css" href="{{ asset('css/PreVehiculo/theme.css') }}">
 <link rel="stylesheet" type="text/css" href="{{ asset('css/PreVehiculo/fileinput.css') }}">
 <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatable/bootstrap-table.css') }}">


<div class="container">

	<div class="card">
		<!--<form id="formPredenuncia" enctype="multipart/form-data" method="POST" action="" accept-charset="UTF-8">-->
		{!! Form::open(['method' => 'POST','files' => true, 'id' => 'formPanel'])  !!}
			{!!Form::hidden('latMap',0,['id' => 'latMap'])!!}
			{!!Form::hidden('lonMap',0,['id' => 'lonMap'])!!}

			<div class="card-header">
		    	<h5 align="center">Registro robo de vehículo</h5>
		  	</div>
		  	<div class="card text-white bg-dark mb-3">
		  		<h5 align="justify">B&uacute;squeda Pre-Registro por Folio</h5>
		  	</div>
            <div class="card-body">
            	<div class="row">
	                <div class="col-3">
	                	{!!Form::label('token', 'Escriba c&oacute;digo de preregistro:',['class'=>'sr-only'])!!}
	                    {!!Form::text('token',null,['class'=>'form-control', 'id' => 'token', 'placeholder' => 'Ej. DKHJMS','style'=>'text-transform: uppercase;'])!!}

	                </div>
	                <div class="col-2">
	                    <button type="button" class="btn btn-dark" name="buscar" id="buscar">Buscar</button>
	                </div>
	                <div class="col-4">
	                </div>
	                <div class="col-2">
	                    <button type="button" class="btn btn-dark right" name="verificacion" id="verificacion" >Verificación</button>
	                </div>
	            </div>
	            <!-- Modal -->
					<div class="modal fade bd-example-modal-lg" id="verificacionmodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
						<div class="modal-dialog modal-lg" role="document">
						<div class="modal-content">
						<div class="modal-header" align="center">
							<h5 class="modal-title" id="exampleModalLabel" align="text-center">Coincidencias</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>

						<div class="modal-body">
              <div class="card-header">
                Carpetas
              </div>
              <div class="card-body">
                <table id="tablecarpetas" class="table table-bordered table-hover table-responsive-sm"></table>
              </div>

              <div class="card-header">
                Registros de robo de vehículos
              </div>
              <div class="card-body">
                <table id="tableregistros" class="table table-bordered table-hover table-responsive-sm"></table>
              </div>
              <div class="card-header">
                Registros de vehículos recuperados
              </div>
              <div class="card-body">
                <table id="tablerecuperados" class="table table-bordered table-hover table-responsive-sm"></table>
              </div>
						</div>

						<div class="modal-footer">
							<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
						</div>
						</div>
					</div>
				</div>
				<script type="text/javascript">

				</script>
			</div>


        	<div class="card-body"></div>
            <div class="card text-white bg-dark mb-3">
		  		<h5 align="justify">Datos generales del vehículo</h5>
		  	</div>                                   {{--  </form> --}}


		  	{{-- <div class="form-row align-items-center">
		  		<div class="row">
		  			<div class="col-lg-3 col-md-6">
		  				<div class="form">
		  					{!!Form::label('token', 'Escriba c&oacute;digo de preregistro:')!!}
		  					{!!Form::text('token',null,['class'=>'form-control form-control-sm', 'id' => 'token', 'placeholder' => 'Ej. DKHJMS','onkeyup'=>'mayus(this);'])!!}
		  				</div>
		  			</div>
		  		</div>
		  	</div> --}}
		  	<div class="card-body">
                  <div class="row">
                        <div class="col-md-4 col-lg-4">
                          <div class="form">
                              {!!Form::label('permiso', 'Documento de circulación')!!}
                          </div>
                          <div class="input-group">


                           <select class="templatingSelect10 form-control form-control-sm"  id="idDocumentoCirculacion" name="idDocumentoCirculacion" >
                               <option selected="selected" value=""> </option>
                               @foreach($documentosCirculacion as $documento)
                               	<option value="{{$documento->id}}" @if($documento->id == 1) selected @endif>{{$documento->nombre}}</option>
                               @endforeach
                           </select>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 ">
                    	<div id="divPlaca">
                    		<div class="form">
                    			{!!Form::label('placa', 'Placa')!!}
                    		</div>
                    		<div class="input-group">
                    			{!!Form::text('placa',null,['class'=>'form-control form-control-sm', 'null' ,'id' => 'placa', 'placeholder' => 'HMZ2398 - Ejemplo' , 'maxlength'=>'7',  'style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'alphanumeric'])!!}
                    			{{-- 'data-validation-regexp'=>'^((([A-ZÑ]|[\d])+)){5,7}$' --}}
                    		</div>
                    	</div>
                    	<div id="divFolio" style = 'display: none'>
                    		<div class="form">
                    			{!!Form::label('folioDoctoCirc', 'Folio')!!}
                    		</div>
                    		<div class="input-group">
                    			{!!Form::text('folioDoctoCirc',null,['class'=>'form-control form-control-sm', 'id' => 'folioDoctoCirc', 'maxlength'=>'11', 'style'=>'text-transform: uppercase;'])!!}
                    		</div>
                    	</div>

                    </div>
					<div class="col-lg-4 col-md-4">
						<div class="form-group">
							{!!Form::label('idEstado', 'Entidad federativa')!!}
							<select  class="templatingSelect2 form-control form-control-sm"  id="idEstado" name="idEstado" required>
								<option value="" selected="selected" > </option>
								@foreach($estados as $estado)
							  <option  value="{{$estado->id}}" >{{$estado->nombre}}</option>
							@endforeach
							</select>
						</div>
					</div>
					</div>
					<div class="row">
            <div class="col-lg-4 col-md-4">
              <div class="form-group">
                {!!Form::label('idMunicipio', 'Municipio')!!}
                <input type="text" id="text_municipio" name="text_municipio" hidden>
                <select class="templatingSelect11 form-control form-control-sm" required id="idMunicipio" name="idMunicipio" >
                  <option value="" selected="selected"></option>
                </select>
              </div>
            </div>
					<div class="col-lg-4 col-md-4">
						<div class="form-group">
							{!!Form::label('idMarca', 'Marca')!!}
							<select  class="templatingSelect3 form-control form-control-sm"  id="idMarca" name="idMarca",  required>
								<option value="" selected="selected" > </option>
							@foreach($marcas as $marca)
								<option  value="{{$marca->id}}">{{$marca->nombre}}</option>
							@endforeach
							</select>
						</div>
					</div>



		  			<div class="col">
						<div class="form-group">
							{!!Form::label('idSubmarca', 'Submarca')!!}
							<select  class="idSubmarca form-control form-control-sm"  id="idSubmarca" name="idSubmarca" required>
								<option value="" required selected="selected"></option>
							</select>
						</div>
					</div>
        </div>
        <div class="row">
					<div class="col">
						<div class="form-group">
							{!!Form::label('modelo', 'Modelo')!!}
							<input class="form-control form-control-sm" value="{{date('Y')}}" min="1885" max="{{date('Y')+1}}" name="modelo" type="number" required id="modelo">
						</div>
					</div>
					<div class="col">
						<div class="form-group">
							{!!Form::label('idColor','Color')!!}
							<select  class="templatingSelect5 form-control form-control-sm"  id="idColor" name="idColor" required>
								<option selected="selected" value=""> </option>
							@foreach($colores as $color)
								<option value="{{$color->id}}">{{$color->nombre}}</option>
							@endforeach
							</select>
						</div>
					</div>


					<div class="col">
						<div class="form-group">
							{!!Form::label('numSerie','Número de serie')!!}

							{!!Form::text('numSerie',null, ['class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el número de serie', 'required','minlegth'=>'10','maxlength'=>'17','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'alphanumeric'] )!!}

							{{-- {!!Form::text('numSerie',null, ['class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el número de serie', 'required','minlegth'=>'10','maxlength'=>'17','onkeyup'=>'mayus(this);', 'data-validation'=>'custom', 'data-validation-regexp'=>'^([A-Z\d]{3}([A-Z\d]{5})([A-Z\d]{3})([A-Z\d]{6}))$'] )!!} --}}
						</div>
					</div>

        </div>

        <div class="row">

		  		<div class="col">
						<div class="form-group">
							{!!Form::label('motor','Número de motor')!!}

							{!!Form::text('motor',null, ['class' => 'form-control form-control-sm','maxlength'=>'10',  'placeholder' => 'Ingrese el número de motor','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'alphanumeric' ] )!!}
							{{-- 'data-validation-regexp'=>'^((([A-Z]|[\d])+)+){10}$' --}}
						</div>
					</div>
		  			<div class="col">
						<div class="form-group">
							{!!Form::label('nrpv','NRPV')!!}

							{!!Form::text('nrpv',null, ['class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el NRPV', 'required','style'=>'text-transform: uppercase;'] )!!}
						</div>
					</div>

					<div class="col">
						<div class="form-group">
							{!!Form::label('idClaseVehiculo','Tipo de vehículo')!!}

							<select  class="templatingSelect7 form-control form-control-sm"  id="idClaseVehiculo" name="idClaseVehiculo" required>
								<option selected="selected" value=""> </option>
							@foreach($clasevehiculo as $clasevehiculo)
								<option value="{{$clasevehiculo->id}}">{{$clasevehiculo->nombre}}</option>
							@endforeach

							</select>
						</div>

					</div>
        </div>

        <div class="row">

					<div class="col-lg-4 col-md-4">
						<div class="form-group">
							{!!Form::label('idSubclase','Subtipo de vehículo')!!}

							<select  class="templatingSelect1 form-control form-control-sm"  id="idSubclase" name="idSubclase" required>
								<option selected="selected" value=""> </option>

							</select>
						</div>
					</div>


		  			<div class="col-lg-4 col-md-4">
						<div class="form-group">
							{!!Form::label('idUso','Tipo de uso')!!}

							<select  class="templatingSelect8 form-control form-control-sm"  id="idUso" name="idUso" required>
								<option selected="selected" value=""> </option>
							@foreach($tipouso as $tipouso)
								<option value="{{$tipouso->id}}">{{$tipouso->nombre}}</option>
							@endforeach
							</select>
						</div>
					</div>
		  			<div class="col-lg-4 col-md-4">
						<div class="form-group">
							{!!Form::label('idProcedencia','Procedencia')!!}
							<select class="templatingSelect9 form-control form-control-sm" id="idProcedencia" name="idProcedencia" required>
								<option value="" selected>Seleccione</option>
								<option value="2" >EXTRANJERO</option>
								<option value="1">NACIONAL</option>
							</select>
						</div>
					</div>
        </div>
        <div class="row">

		  			<div class="col">
						<div class="form-group">
							{!!Form::label('idAseguradora','Aseguradora')!!}
							<select class="templatingSelect6 form-control form-control-sm" required id="idAseguradora" name="idAseguradora" >
								<option selected="selected" value=""> </option>@foreach($aseguradoras as $aseguradora)
							<option value="{{$aseguradora->id}}">{{$aseguradora->nombre}}</option>@endforeach
							</select>
						</div>
					</div>
					<div class="col-lg-8 col-md-8 ">
						<div class="form-group">
							{!!Form::label('senasPartic','Señas particulares')!!}
							<textarea class="form-control form-control-sm" placeholder="Ingrese las señas particulares" rows="3" name="senasPartic" cols="50" style="text-transform: uppercase;" required id="senasPartic"></textarea>
						</div>
					</div>
					{!!Form::text('idSubCatIdenti',null, ['hidden','id'=>'idSubCatIdenti'] )!!}
					{!!Form::text('idSubCatFact',null, ['hidden','id'=>'idSubCatFact'] )!!}

</div>
		  	</div>
			<div class="card text-white bg-dark mb-3">
		  		<h5 align="justify">Cargar documentos solicitados</h5>
		  	</div>
		  	<!-- <div class="card-body">
					<button type="button" class="btn btn-outline-info">Cargar documentos</button>
				</div>
		  	<div class="card-header">
		    	<h5 align="center">Ubicación de los hechos</h5>
		  	</div> -->
			<div id="doctos" align="center" class="row">


		    </div>
			<hr>
		        <div align="center">
		          <div class="btn-group btn-group-toggle" data-toggle="buttons" id="selector">
		            <label class="btn btn-secondary form-check-label" onclick="sectionShow()">
		              <input type="radio" name="img" id="option1" autocomplete="off" value="archivo">
		               <i class="fa fa-folder-open"></i>
		            Carga de archivos
		            </label>
		            <label id="divBtnCamara" class="btn btn-secondary form-check-label " onclick="sectionShow()">
		              <input type="radio" name="img" id="option1" autocomplete="off" value="camara" >
		              <i class="fa fa-camera-retro"></i>
		            Enviar a camara

		            </label>
		          </div>
		        </div>
<hr>
        <div class="row" id="complemento_asc" style="display:none;">
          <div class="col-md-6">
            <div class="card-footer text-muted">
              <div id="iden" >
                <select class="custom-select" id="listaiden_op1" name="listaiden1" class="listaiden">
                  <option selected value="" >--Seleccione una opción--</option>
                  @foreach($identificacionoficial as $identificacionoficial)
                    <option  value="{{$identificacionoficial->id}}" >{{$identificacionoficial->nombre}}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card-footer text-muted">
              <div id="iden" >
                <select class="custom-select" id="listaiden_op2" name="listaiden3" class="listaiden">
                  <option selected value="" >--Seleccione una opción--</option>
                  @foreach($factura as $factura)
                  <option  value="{{$factura->id}}" >{{$factura->nombre}}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
          <input type="hidden" id="tipe_load" name="optCaptura" value="loadFileType">
        </div>
        <br>
        <div id="div1" style="display:none;" >
          <br>
          <div class="row" align="center">
            <div class="col  col-md">
            </div>

            <!--busqueda de archivos identificacion oficial-->
            <div class="col-4  col-md-4">
              <div class="card text-center">
                <div class="card-header">
                  <label style="font-size:20px; text-align:center" size=18 type="text">Identificacion oficial</label>
                </div>
                <div class="card-body">
                  <div class="file-loading" >
                    <input id="DOC1" class="btn_ide" name="ID_OFICIAL" type="file" accept="image/*" style="display: none;">
                  </div>
                </div>

              </div>
            </div>
            <div class="col-4  col-md-4">
              <div class="card text-center">
                <div class="card-header">
                  <label style="font-size:20px; text-align:center" size=18 type="text">Factura del vehículo</label>
                </div>
                <div class="card-body">
                  <div class="file-loading">
                    <input id="DOC5" class="btn-fac" name="ID_FACTURA" type="file" accept="image/*" style="display: none;">
                  </div>
                </div>

              </div>
            </div>

            <div class="col">
            </div>
          </div>
        </div>


        <div id="div2" style="display:none;">
          <br>
          <div class="row">
            <div class="col col-md">
            </div>
            <br>
            <div class="col-4  col-md-5">
              <div class="card text-center">
                <div class="card-header">
                  <label style="font-size:20px; text-align:center" size=18 type="text">Identificacion oficial</label>
                </div>
                <div class="card-body">
                  <div class="" id="ife_mini"></div>
                  <input type="hidden" id="mini_ife" name="cam_ident" value="">
                  <button id="cam_ide" type="button" class="btn btn-dark" data-toggle="modal" data-target="#profile-photo-camera-modal" onclick="setup('IFE')" disabled="disabled">
                    <i class="fa fa-camera"></i> Capturar imagen
                  </button>
                  <button type="button" id="borrar2"  class="btn btn-secondary" :disabled="form.busy" ata-target="#profile-photo-camera-modal"  disabled="disabled">
                    <i class="fa fa-trash"></i>
                  </button>
                </div>

              </div>
            </div>
            <div class="col-4  col-md-5">
              <div class="card text-center">
                <div class="card-header">
                  <label style="font-size:20px; text-align:center" size=18 type="text">Factura del vehículo</label>
                </div>
                <div class="card-body">
                  <div class="" id="fact_mini"></div>
                  <input type="hidden" id="mini_fac" name="cam_fact" value="">
                  <button id="cam_fac" type="button" class="btn btn-dark" data-toggle="modal" data-target="#profile-photo-camera-modal" onclick="setup('FACT')" disabled="disabled">
                    <i class="fa fa-camera"></i> Capturar imagen
                  </button>
                  <button type="button" id="borrar"  class="btn btn-secondary" :disabled="form.busy" ata-target="#profile-photo-camera-modal"  disabled="disabled">
                    <i class="fa fa-trash"></i>
                  </button>
                </div>

              </div>
            </div>

            <div class="col">

            </div>

          </div>

				</div>
				{{--  Codigo para el mapa --}}
				<link rel="stylesheet" type="text/css" href="{{ asset('css/styleBarSearchMap.css') }}">

				<script type="text/javascript">var centreGot = false;</script>
				<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyADLKCVRyl7I3LK5INMWsrqWhQgTkdCdVY&libraries=places&callback=initAutocomplete" async defer></script>
				<script src="{{ asset('js/geolocalizacion/geolocalizacion.js') }}"></script>
				<script src="{{ asset('js/geolocalizacion/barSearchMap.js') }}"></script>

				<div class="card text-white bg-dark mb-3">
		  		<h5 align="justify">Ubicación de los hechos</h5>
				</div>
				<div class="row">
					<input id="pac-input" class="controls" type="text" placeholder="Buscar ubicación">

					<div class="col-md-12" id="mapa">
            {!!$map['html']!!}
					</div>
				</div>
				{{--  Fin del codigo del mapa  --}}

	        <!-- section of modal -->
	      <div id="profile-photo-camera-modal" class="modal fade" tabindex="-1">
	        <div class="modal-dialog">
	          <div class="modal-content">
	            <div class="modal-header">
	              <h5 class="modal-title">Captura de documentos</h5>
	              <button type="button" onClick="close_cam(true)" class="close" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	              </button>
	            </div>
	            <div class="modal-body" id="cuerpo">
	              <div id="web_cam_section" class="alert alert-danger" v-if="form.errors.has('photo')">
	                <div id="my_camera"></div>
	              </div>
	              <div id="profile-photo-camera-preview">
	                <div id="results">Your captured image will appear here...</div>
	              </div>
	            </div>
	            <div class="modal-footer">
	              <button type="button" di="take_p" class="btn btn-primary" :disabled="form.busy" onClick="take_snapshot()">Capturar</button>
	            </div>
	          </div>
	        </div>
	      </div>
	      <!-- end section of modal-->

	        <div class="card-body">
	          <div align="right">
	            <button type="submit" class="btn btn-primary" id="btnEmpty">Guardar</button>
	          </div>
	        </div>

	        {!!Form::text('idCarpeta',null, ['hidden','id'=>'idCarpeta'] )!!}
	        {!!Form::text('numCarpeta',null, ['hidden','id'=>'numCarpeta'] )!!}

	        <!--</form>-->
	        {!! Form::close()!!}

	        {!! Form::open(['route' => 'generar-reporteCoincidencias','id'=>'ReporPdf','target'=>'_blank']) !!}
				{!!Form::hidden('sendPlaca',null, ['id'=>'sendPlaca'] )!!}
				{!!Form::hidden('sendSerie',null, ['id'=>'sendSerie'] )!!}
				{!!Form::hidden('sendMotor',null, ['id'=>'sendMotor'] )!!}
				{!!Form::hidden('sendToken',null, ['id'=>'sendToken'] )!!}

	        {!! Form::close()!!}
		</div>
	</div>







 <script src="{{ asset('js/sweetalert.min.js') }}"></script>
 <script src="{{ asset('js/denuncia/ordenar.js') }}"></script>
 <script src="{{ asset('plugins/datatable/bootstrap-table.js')}}"></script>
 <script src="{{ asset('js/denuncia/peticiones.js') }}"></script>
 <script type="text/javascript" src="{{ asset('js/webcam.min.js') }}"></script>
 <script type="text/javascript" src="{{ asset('js/camara/camara_validacion.js') }}"></script>



<style media="screen">
  #profile-photo-camera-preview,
  #profile-photo-camera-preview video {
    width: 100% !important;
    height: auto !important;
    min-width: 100px;
    min-height: 100px;
  }
  .spa_sec{
    padding: 0 1em;
  }
  #my_camera{
    display: block;
    margin: auto;
  }
  #web_cam_section{
    background-color: #717171;
    border-color: #424242;
  }
  #cuerpo{
    display: block;
    margin: auto;
  }
  #ife_mini,#fact_mini{
    padding-bottom: 1em;
  }
</style>

<script type="text/javascript">

	//eliminar fotografia de camara
    $(document).ready(function() {
      $("#borrar").click(function(event) {
      //$("#ife_mini").remove();
      $('#fact_mini').html('');
    });
      $("#borrar2").click(function(event) {
        $('#ife_mini').html('');
      });
    });
    //fin foto grafia de camara

	var statusMsj = "";
	var msj = "";
	function addImg (ruta,posicion,subcat,nombre) {
        //path = storage_path();
        var titDiv="";
        if(posicion==0){
        	titDiv="identidicacionCont";
        	$('#idSubCatIdenti').val(subcat);
        }else{
        	titDiv="facturaCont";
        	$('#idSubCatFact').val(subcat);
        }

        getImg = "{{route('getImg',['filename'=>':ruta'])}}";
        getImg = getImg.replace(':ruta', ruta);
		console.log(getImg);
        var element = '<div class="col center" id="'+titDiv+'"><img src="'+getImg+'" alt="" style="width:50%"><p>'+nombre+'</p></div>';
        return element;
        //console.log(element);
    }


function sectionShow(){
  $('.sec_upload').toggle('slow');
}

var cap="";
Webcam.set({
  width: 320,
  height: 240,
  image_format: 'jpeg',
  jpeg_quality: 90
});
function take_snapshot(tipo) {
  Webcam.snap( function(data_uri) {
    document.getElementById('results').innerHTML ='<img class="img-thumbnail" src="'+data_uri+'"/>';
    var img = $("#results").children("img").clone();
    var tipo = (cap == 'FACT')? '#fact_mini':'#ife_mini';
    console.log('Este es el tipo -> '+tipo+' esta es la imagen ->'+img);
    $(tipo).html(img);

    //-----2
    var tipo2 = (cap == 'FACT')? '#mini_fac':'#mini_ife';
    $(tipo2).val(data_uri);
    //------2
    setTimeout(function() {
      $('#profile-photo-camera-modal').modal('hide');
      $("#results").html('');
    }, 1000);

  } );
  close_cam(false);
  $('#web_cam_section').hide('slow');
}
function setup(tipo) {
  this.cap = tipo;
  Webcam.reset();
  Webcam.attach( '#my_camera' );
  $('#web_cam_section').show('slow');
  if(tipo=="FACT"){
  	$('#facturaCont').hide();
  }else{
  	$('#identidicacionCont').hide();
  }
}
function close_cam(act){
  if(act){
    $("#results").html('');
    $('#facturaCont').show();
    $('#identidicacionCont').show();
  }
  console.log('Calmando el evento');
  Webcam.reset();
}


$(document).ready(function(){

  $('button').keypress(function(e){
    tecla = (document.all) ? e.keyCode : e.which;
            return (tecla != 13);
            console.log('hola adios');
  });

	$('#verificacion').bind('click', function () {
		$('#verificacionmodal').modal('show');
		var placa = $('#placa').val();
		placa = (placa== '') ? 0 : placa;
		placa = placa.toUpperCase();
		var serie = $('#numSerie').val();
		serie = (serie== '') ? 0 : serie;
		//serie = serie.toUpperCase();
		var motor = $('#motor').val();
		motor = (motor== '') ? 0 : motor;
		//motor = motor.toUpperCase();
		var token = $('#token').val();
		//hiddenalert(token);
		var tipoCons = 2;
		//motor = (motor== '') ? 0 : motor;
		//vehiculos robados
		var ruta = "{{route('get_robados',['placa'=>':placa','serie'=>':serie','motor'=>':motor','token'=>':token'])}}";
		ruta = ruta.replace(':placa', placa);
		ruta = ruta.replace(':serie', serie);
		ruta = ruta.replace(':motor', motor);
		ruta = ruta.replace(':token', token);
		datos = null;
		console.log(ruta)
		vehRobados = peticionInfomacion('get',ruta,datos);
		console.log(vehRobados);

		//registros UIPJ

		var ruta2 = "{{route('get_involucrados',['placa'=>':placa','serie'=>':serie','motor'=>':motor','tipoCons'=>':tipoCons'])}}";
		ruta2 = ruta2.replace(':placa', placa);
		ruta2 = ruta2.replace(':serie', serie);
		ruta2 = ruta2.replace(':motor', motor);
		ruta2 = ruta2.replace(':tipoCons', tipoCons);
		datos = null;
		//console.log(ruta)
		registrosCarpeta = peticionInfomacion('get',ruta2,datos);
		console.log(registrosCarpeta);
    	$('#tablecarpetas').bootstrapTable('load', registrosCarpeta);

		//registros vehiculos involucrados

		var ruta3 = "{{route('getMockup','1')}}";
		datos = null;
			//console.log(ruta)
		carpetaInv = peticionInfomacion('get',ruta,datos);
		console.log(carpetaInv);
    	$('#tableregistros').bootstrapTable('load', vehRobados);
    	// Vehiculos Recuperados

    	var ruta4 = "{{route('get_recuperados',['placa'=>':placa','serie'=>':serie','motor'=>':motor','token'=>':token'])}}";
		ruta4 = ruta4.replace(':placa', placa);
		ruta4 = ruta4.replace(':serie', serie);
		ruta4 = ruta4.replace(':motor', motor);
		ruta4 = ruta4.replace(':token', token);
		datos = null;
		console.log(ruta4)
		vehRecuperados = peticionInfomacion('get',ruta4,datos);
		console.log(vehRecuperados);
		$('#tablerecuperados').bootstrapTable('load', vehRecuperados);


	});



    $('#buscar').click(function(){
		var identificador = $('#token').val();
		var respuesta;


		if(identificador!=''){
			var ruta = "{{route('getToken',':token')}}";
			ruta = ruta.replace(':token', identificador);
			datos = null;
			//console.log(ruta)
			respuesta = peticionInfomacion('get',ruta,datos);
			//console.log(respuesta);
			if (respuesta != null) {
				if (respuesta.codigo == 201) {
					var reg = respuesta.estado[0];
					swal({
				    	title: "Token encontrado",
				    	text: "Se han cargado los datos del preregistro",
				    	icon: "success",
				    })
					//console.log(respuesta);
					for (i = 0; i < respuesta.doctos.length ; i++) {
						//console.log(respuesta.doctos[i].nombre);
						$('#doctos').append(addImg(respuesta.doctos[i].destino,i,respuesta.doctos[i].idSubCategoriaDocumento,respuesta.doctos[i].nombre));
					}


					$('#placa').val(respuesta.estado[0].placas);
					$('#idEstado').val(reg.idEstado).trigger('change');
					$('#idMarca').val(reg.idMarca).trigger('change');
					$('#idSubmarca').val(reg.idSubmarca).trigger('change');
					$('#modelo').val(reg.modelo);
					$('#idColor').val(reg.idColor).trigger('change');
					$('#numSerie').val(reg.numSerie);
					$('#motor').val(reg.numMotor);
					$('#nrpv').val(reg.nrpv);
					$('#idClaseVehiculo').val(reg.idClaseVehiculo).trigger('change');
					$('#idSubclase').val(reg.idTipoVehiculo).trigger('change');
					$('#idUso').val(reg.idTipoUso).trigger('change');
					$('#idProcedencia').val(reg.idProcedencia).trigger('change');
					$('#idAseguradora').val(reg.idAseguradora).trigger('change');
					$('#senasPartic').val(reg.senasPartic);
					$("#idDocumentoCirculacion").val(reg.idDocumentoCirculacion).trigger('change');
					$('#folio').val(reg.folioDoctoCirc);

					var map = new google.maps.Map(document.getElementById('map_canvas'), {
						zoom: 8,
						center: new google.maps.LatLng(19.976312,-96.7279684),
						mapTypeId: google.maps.MapTypeId.ROADMAP
					});

					var marker = new google.maps.Marker({
						position: new google.maps.LatLng(reg.latitud, reg.longitud),
						draggable: true,
						map: map
					});

					$('#latMap').val(marker.getPosition().lat());
					$('#lonMap').val(marker.getPosition().lng());

					marker.addListener('dragend', function(){
						$('#latMap').val(marker.getPosition().lat());
						$('#lonMap').val(marker.getPosition().lng());
					});



				} else {
					$('#placa').val('');
					$('#idEstado').val('').trigger('change');
					$('#idMarca').val('').trigger('change');
					$('#idSubmarca').val('').trigger('change');
					$('#modelo').val('');
					$('#idColor').val('').trigger('change');
					$('#numSerie').val('');
					$('#motor').val('');
					$('#nrpv').val('');
					$('#idClaseVehiculo').val('').trigger('change');
					$('#idSubclase').val('').trigger('change');
					$('#idUso').val('').trigger('change');
					$('#idProcedencia').val('').trigger('change');
					$('#idAseguradora').val('').trigger('change');
					$('#senasPartic').val('');

					swal({
			      		title: respuesta.status,
			      		text: "",
			      		icon: "warning",
			      		buttons: true,
			      		dangerMode: true,
				    })
				}
			}else{
				alert('Error de servidor');
			}
		}else{
			//alert('no aplica');
			swal({
				title: "Debe introducir código token",
			    text: "",
			    icon: "warning",
			    buttons: true,
			    dangerMode: true,
		   	})
		}
	});

	$('.btn_ide').on('change', function(){
      	$('#identidicacionCont').hide();
   	});

   	$('.btn-fac').on('change', function(){
      	$('#facturaCont').hide();
   	});

	$('.btn_check').click(function(event){
		if($(this).val()==1){
			$('#placa').removeAttr('readonly');
			$('#permiso').attr('readonly',true);
		}else{
			$('#permiso').removeAttr('readonly');
			$('#placa').attr('readonly',true);
		}
	});

	$('#listaiden_op1').change(function(){
        var valor = $('#listaiden_op1').val();
        if(valor!=''){
          var tipoC = $('#tipe_load').val();
          if (tipoC==1){
            $('#DOC1').css('display','');
            $('#cam_ide').attr('disabled',true);
            $('#borrar2').attr('disabled',true);
          }else{
            $('#cam_ide').removeAttr('disabled');
            $('#DOC1').css('display','none');
            $('#borrar2').removeAttr('disabled');
          }
        }
    });

    $('#listaiden_op2').change(function(){
        var valor = $('#listaiden_op2').val();
        if(valor!=''){
          var tipoC = $('#tipe_load').val();
          if (tipoC==1){
            $('#DOC5').css('display','');
            $('#cam_fac').attr('disabled',true);
            $('#borrar').attr('disabled',true);
          }else{
            $('#cam_fac').removeAttr('disabled');
            $('#DOC5').css('display','none');
            $('#borrar').removeAttr('disabled');
          }
        }
    });

	$("#idMarca").change(function() {
		// alert('hola');
		var identificador = $(this).val();
		var respuesta;
		$('#idSubmarca').html('');
		if(identificador!=''){
			var ruta = "{{route('submarca',':marca')}}";
			ruta = ruta.replace(':marca', identificador);
			datos = null;
			respuesta = peticionInfomacion('get',ruta,datos);
			console.log(respuesta);
			if (respuesta != null) {
				for(i=0; i<respuesta.estado.length; i++){
					$("#idSubmarca").append("<option value='"+respuesta.estado[i].id+"'> "+respuesta.estado[i].nombre+"</option>");
				}
				if(respuesta.estado.length > 0 ) {
					localStorage.setItem("idSubmarca", respuesta.estado[0].id);
				}
        		console.log('cargando datos');
			}else{
				alert('Error de servidor');
			}
		}else{
			//alert('no aplica');
		}
	});

  $("#idEstado").change(function() {
    var identificador = $(this).val();
    var respuesta;
    $('#idMunicipio').html('');
    if(identificador!=''){
      var ruta = "{{route('municipio',':estado')}}";
      ruta = ruta.replace(':estado', identificador);
      datos = null;
      respuesta = peticionInfomacion('get',ruta,datos);
      if (respuesta != null) {
        for(i=0; i<respuesta.estado.length; i++){
          $("#idMunicipio").append("<option value='"+respuesta.estado[i].id+"'> "+respuesta.estado[i].nombre+"</option>");
        }
        if(respuesta.estado.length > 0 ){
          localStorage.setItem("idMunicipio", respuesta.estado[0].id);
        }
        console.log('cargando datos');
      }else{
        alert('Error de servidor');
      }
    }else{
    //alert('no aplica');
    }
  });

  $("#idMunicipio").change(function() {
    localStorage.setItem("idMunicipio", $('#idMunicipio').val());
  });


	$("#idClaseVehiculo").change(function() {
		// alert('hola');
		var identificador = $(this).val();
		var respuesta;
		$('#idSubclase').html('');
		if(identificador!=''){
			var ruta = "{{route('subclase',':clasevehiculo')}}";
			ruta = ruta.replace(':clasevehiculo', identificador);
			datos = null;
			respuesta = peticionInfomacion('get',ruta,datos);
			console.log(respuesta);
			if (respuesta != null) {
				for(i=0; i<respuesta.estado.length; i++){
					$("#idSubclase").append("<option value='"+respuesta.estado[i].id+"'> "+respuesta.estado[i].nombre+"</option>");
				}
				if (respuesta.estado.length > 0) {
					localStorage.setItem("idSubclase", respuesta.estado[0].id);
				}
        		console.log('cargando datos');
			}else{
				alert('Error de servidor');
			}
		}else{
			//alert('no aplica');

		}
	});

	$("#idSubmarca").change(function() {
		localStorage.setItem("idSubmarca", $('#idSubmarca').val());
	});

	$("#idSubclase").change(function() {
		localStorage.setItem("idSubclase", $('#idSubclase').val());
	});

	$('#formPanel').submit(function (e) {
		e.preventDefault();
		$('#sendPlaca').val($('#placa').val());
		$('#sendSerie').val($('#numSerie').val());
		$('#sendMotor').val($('#motor').val());
		$('#sendToken').val($('#token').val());
		var ruta ="{{route('getMockup','1')}}";
			//ruta = ruta.replace("num_serie",numSerie);
			//console.log(ruta);
		var carpeta = peticionInfomacion('get',ruta);
		//console.log(carpeta);
		$('#idCarpeta').val(carpeta.investigacion.id);
		$('#numCarpeta').val(carpeta.investigacion.numCarpeta);
		var clase = ""
		//var datos = $(this).serialize();
		var formData = new FormData(this);
		//Obtener coincidencias VRR
		//var placa = $('#placa').val();
		var placa = ($('#placa').val() == '') ? 0 : $('#placa').val();
		placa = placa.toUpperCase()
		var serie = $('#numSerie').val();
		serie = serie.toUpperCase()
		var motor = $('#motor').val();
		motor = motor.toUpperCase()
		var nrpv = $('#nrpv').val();
		nrpv = nrpv.toUpperCase()
		var token = $('#token').val();
		//console.log(token);
		var ruta = "{{route('get_countRobados',['placa' => ':placa', 'serie' => ':serie', 'motor' => ':motor','nrpv' => ':nrpv','token' => ':token'])}}";
		ruta = ruta.replace(':placa', placa);
		ruta = ruta.replace(':serie', serie);
		ruta = ruta.replace(':motor', motor);
		ruta = ruta.replace(':nrpv', nrpv);
		ruta = ruta.replace(':token', token);
		datos = null;
		robados = peticionInfomacion('get',ruta,datos);

		console.log(robados);
		        /*for(i=0; i<respuesta.estado.length; i++){
		          $("#idSubmarca").append("<option value='"+respuesta.estado[i].id+"'> "+respuesta.estado[i].nombre+"</option>");
		          //console.log(respuesta.estado[i].id);
		        }
		        $('#idSubmarca').val(localStorage.idSubmarca).trigger("change");*/


		var ruta = "{{route('get_countInvolucrados',['placa' => ':placa', 'serie' => ':serie', 'motor' => ':motor','nrpv' => ':nrpv'])}}";
		ruta = ruta.replace(':placa', placa);
		ruta = ruta.replace(':serie', serie);
		ruta = ruta.replace(':motor', motor);
		ruta = ruta.replace(':nrpv', nrpv);
		datos = null;
		respuestaUipj = peticionInfomacion('get',ruta,datos);

		if (respuestaUipj != null) {
		    	console.log(respuestaUipj);
		        /*for(i=0; i<respuesta.estado.length; i++){
		          $("#idSubmarca").append("<option value='"+respuesta.estado[i].id+"'> "+respuesta.estado[i].nombre+"</option>");
		          //console.log(respuesta.estado[i].id);
		        }
		        $('#idSubmarca').val(localStorage.idSubmarca).trigger("change");*/
		}
		var ruta3 = "{{route('get_countRecuperados',['placa' => ':placa', 'serie' => ':serie', 'motor' => ':motor','nrpv' => ':nrpv','token' => ':token'])}}";
		ruta3 = ruta3.replace(':placa', placa);
		ruta3 = ruta3.replace(':serie', serie);
		ruta3 = ruta3.replace(':motor', motor);
		ruta3 = ruta3.replace(':nrpv', nrpv);
		ruta3 = ruta3.replace(':token', token);
		datos = null;
		console.log(ruta3);
		recuperados = peticionInfomacion('get',ruta3,datos);
		console.log('Éstos son los recuperados');
		console.log(recuperados);
		// var numSerie = $('#numSerie').val();
		// if(numSerie!=""){
		// 	//console.log('Este es el numero de serie ->'+numSerie);
		// 	var ruta ="{{route('niv',['num_serie'])}}";
		// 	ruta = ruta.replace("num_serie",numSerie);
		// 	//console.log(ruta);
		// 	var respuestaNiv = peticionInfomacion('get',ruta);
		// 	console.log(respuestaNiv);
		// 	statusMsj = respuestaNiv.request.status;
		// 	msj = respuestaNiv.request.mensaje;
		// 	console.log(msj);
		// }
		// if(statusMsj=='correcto'){clase='alert alert-success';}else{clase='alert alert-danger';}
		//console.log(formData);

			// statusMsj = respuestaNiv.request.status;
			// msj = respuestaNiv.request.mensaje;
			// console.log(msj);
			// console.log('Este es el form serializado');
			// console.log($( this ).serialize());
			var coicidenciasPdf = '{{ route('generar-reporteCoincidencias') }}';
			console.log(coicidenciasPdf);
			var span = document.createElement('span');

		if(robados.estado.placas >0 || respuestaUipj.estado.placas > 0 || respuestaUipj.estado.numMotor > 0 || robados.estado.numMotor >0 || respuestaUipj.estado.numSerie > 0 || robados.estado.numSerie > 0 || respuestaUipj.estado.nrpv > 0 || robados.estado.nrpv > 0){

	      span.innerHTML=
	      '<p> Coincidencias con vehículos involucrados en alguna carpeta de investigación y/o con reporte de robo</p>'
	      +'<table class="table table-bordered" value="detalle"> <thead class="thead-light">'
	      +'<tr>'
	      +'<th scope="col"></th>'
	      +'<th scope="col">Carpetas</th>'
	      +'<th scope="col">Vehículos Robados</th>'
	      +'<th scope="col">Vehículos Recuperados</th>'
	      +'<tr>'
	      +'</thead>'
	      +'<tbody>'
	      +'<tr>'
	      +'<th scope="row">Placa</th>'+'<th scope="row">'+respuestaUipj.estado.placas+'</th>'+'<th scope="row">'+robados.estado.placas+'</th>'+'<th scope="row">'+recuperados.estado.placas+'</th>'+'</tr>'
	      +'<tr>'
	      +'<th scope="row">Numero de motor</th>'+'<th scope="row">'+respuestaUipj.estado.numMotor+'</th>'+'<th scope="row">'+robados.estado.numMotor+'</th>'+'<th scope="row">'+recuperados.estado.numMotor+'</th>'+'</tr>'
	      +'<tr>'
	      +'<th scope="row">Numero de serie</th>'+'<th scope="row">'+respuestaUipj.estado.numSerie+'</th>'+'<th scope="row">'+robados.estado.numSerie+'</th>'+'<th scope="row">'+recuperados.estado.numSerie+'</th>'+'</tr>'
	       +'<tr>'
	      +'<th scope="row">NRPV</th>'+'<th scope="row">'+respuestaUipj.estado.nrpv+'</th>'+'<th scope="row">'+robados.estado.nrpv+'</th>'+'<th scope="row">'+recuperados.estado.nrpv+'</th>'+'</tr>'
	      +'<th colspan="4">'+'<button class="btn btn-secondary btn-lg btn-block" id="generarPdf" onclick="enviarPDF()">'+'Ver detalle'+'</button>'+'</th>'+'</tr>'
	      +'</tbody>'
	      +'</table>'
	      +'<br>'
	      +'<div class="'+clase+'" role="alert">'+msj+'</div>'
	  } else {
	  	span.innerHTML=
	      '<p> No hay coincidencias con vehículos involucrados en alguna carpeta de investigación y/o con reporte de robo</p>'
	  }


		swal({
			title: "¿Esta seguro de que desea continuar?",
			text: "",
			icon: "warning",
			buttons: true,
			dangerMode: true,
			content: span,
		})
		.then((willDelete) => {
			if (willDelete) {
				swal("Su registro se ha registrado satisfactoriamente", {
				  icon: "success",
				});
				$.ajax({
					url : "{{route('saveDenuncia.vehiculo')}}",
					data : formData,
					type : 'POST',
					cache: false,
					contentType : false,
					processData : false,

					success : function(json) {
						console.log('Se agrego un vehiculo');
						console.log(json);

						swal({
							title: "Registro creado exitosamente",
							text: "Su registro se realizo correctamente",
							icon: "success",

						});
            $('#table').bootstrapTable('refresh');

						var rutaDeligencia ="{{route('formato-diligencia')}}";
						var diligencia = peticionInfomacion('post',rutaDeligencia, carpeta);
						location.href ="storage/"+diligencia;
						//console.log(diligencia);

						 $("#idEstado").val('').trigger('change');
             $("#idSubmarca").val('').trigger('change');
  					 $("#idColor").val('').trigger('change');
						 $("#idAseguradora").val('').trigger('change');
						 $("#idMarca").val('').trigger('change');
						 $("#permiso").val('').trigger('change');
             $("#numSerie").val('').trigger('change');
  					 $("#motor").val('').trigger('change');
						 $("#nrpv").val('').trigger('change');
						 $("#senasPartic").val('').trigger('change');
						 $("#placa").val('').trigger('change');
             $("#token").val('').trigger('change');
  					 $("#idClaseVehiculo").val('').trigger('change');
						 $("#idSubclase").val('').trigger('change');
						 $("#idUso").val('').trigger('change');
						 $("#idProcedencia").val('').trigger('change');
						 $("#identidicacionCont").html('');
						 $("#facturaCont").html('');
						 $('#DOC1').attr('value','');

					},

					error : function(xhr, status) {
						console.log('Disculpe, existió un problema');
						console.log(xhr);
						swal({
							title: "Error al introducir los datos",
							text: 'msj',
							icon: "error",
						});
					},
					complete : function(xhr, status) {
						console.log('Petición realizada');
					}
				});
			} else {
				swal("Su registro ha sido cancelado");
			}
		});
		console.log("{{route('saveDenuncia.vehiculo')}}");
	});

});

	function enviarPDF(){

		$('#ReporPdf').submit();
	}

$('#numSerie').focusout(function(){
	var numSerie = $('#numSerie').val();

	if(Number.parseInt($('#modelo').val())>=1980){
		if(numSerie!=""){
			//console.log('Este es el numero de serie ->'+numSerie);
			var ruta ="{{route('niv',['num_serie'])}}";
			ruta = ruta.replace("num_serie",numSerie);
			//console.log(ruta);
			var respuesta = peticionInfomacion('get',ruta);
			console.log(respuesta);
			//statusMsj = respuesta.request.status;
			//msj = respuesta.request.mensaje;
			console.log(msj);
	    (respuesta.request.status=='error')? toastr.error(respuesta.request.mensaje, "ERROR",{"preventDuplicates": true}):toastr.success(respuesta.request.mensaje, "CORRECTO",{"preventDuplicates": true});

		}
		//alert('aqui');
	}else{
		console.log('no verifica nada');
	}
});


$( document ).ready(function() {
		verificar_camara('divBtnCamara');

    console.log( "ready!" );
    var idmarca = $("#idMarca").val();

    if (idmarca > 0) {
      var ruta = "{{route('submarca',':marca')}}";
      ruta = ruta.replace(':marca', idmarca);
      datos = null;
      respuesta = peticionInfomacion('get',ruta,datos);

      if (respuesta != null) {
        for(i=0; i<respuesta.estado.length; i++){
          $("#idSubmarca").append("<option value='"+respuesta.estado[i].id+"'> "+respuesta.estado[i].nombre+"</option>");
          //console.log(respuesta.estado[i].id);
        }
        $('#idSubmarca').val(localStorage.idSubmarca).trigger("change");
      }
    }

    var idClaseVehiculo = $("#idClaseVehiculo").val();
    if (idClaseVehiculo > 0) {
    	var ruta = "{{route('subclase',':clasevehiculo')}}";
		ruta = ruta.replace(':clasevehiculo', idClaseVehiculo);
		datos = null;
		respuesta = peticionInfomacion('get',ruta,datos);
		if (respuesta != null) {
			for(i=0; i<respuesta.estado.length; i++){
				$("#idSubclase").append("<option value='"+respuesta.estado[i].id+"'> "+respuesta.estado[i].nombre+"</option>");
			}
		}
        $('#idSubclase').val(localStorage.idSubclase).trigger("change");
    }
});





$("#idDocumentoCirculacion").change(function(){
	var selec = $("#idDocumentoCirculacion").val();
	if(selec ==  1) {
		$('#divPlaca').css('display','');
		$('#divFolio').css('display','none');
	} else {
		$('#divPlaca').css('display','none');
		$('#divFolio').css('display','');
	}
});

 $('#tablecarpetas').bootstrapTable({
                        search:  true,
                        showColumns: true,
                        showRefresh: true,
                        //url: ruta,
                        buttonsAlign :'left',
                        searchAlign :'left',
                        idField: 'id',
                        iconsPrefix: 'fa',
                        pagination : true,
                        pageSize: 5,
                        columns: [{
                            field: 'id',
                            title: 'ID',
                            sortable: true,
                            visible: false
                        }, {
                            field: 'numCarpeta',
                            title: 'No. Carpeta',
                            sortable: true
                        },{
                            field: 'placas',
                            title: 'Placas',
                            sortable: true
                        }, {
                            field: 'nrpv',
                            title: 'NRPV',
                            sortable: true
                        }, {
                            field: 'numSerie',
                            title: 'No. serie',
                            sortable: true
                        }, {
                            field: 'numMotor',
                            title: 'No. Motor',
                            sortable: true
                        },
                            //field: 'operate',
                            //title: 'Acciones',
                            //align: 'center',
                           // events: operateEvents,
                            //formatter: operateFormatter
                        ],
                    });
 $('#tableregistros').bootstrapTable({
                        search:  true,
                        showColumns: true,
                        showRefresh: true,
                        //url: ruta,
                        buttonsAlign :'left',
                        searchAlign :'left',
                        idField: 'id',
                        iconsPrefix: 'fa',
                        pagination : true,
                        pageSize: 5,
                        columns: [{
                            field: 'id',
                            title: 'ID',
                            sortable: true,
                            visible: false
                        }, {
                            field: 'numCarpeta',
                            title: 'No. Carpeta',
                            sortable: true
                        }, {
                            field: 'placas',
                            title: 'Placas',
                            sortable: true
                        }, {
                            field: 'nrpv',
                            title: 'NRPV',
                            sortable: true
                        }, {
                            field: 'numSerie',
                            title: 'No. serie',
                            sortable: true
                        }, {
                            field: 'numMotor',
                            title: 'No. Motor',
                            sortable: true
                        },
                            //field: 'operate',
                            //title: 'Acciones',
                            //align: 'center',
                           // events: operateEvents,
                            //formatter: operateFormatter
                        ],
                    });

$('#tablerecuperados').bootstrapTable({
                       search:  true,
                       showColumns: true,
                       showRefresh: true,
                       //url: ruta,
                       buttonsAlign :'left',
                       searchAlign :'left',
                       idField: 'id',
                       iconsPrefix: 'fa',
                       pagination : true,
                       pageSize: 5,
                       columns: [{
                           field: 'id',
                           title: 'ID',
                           sortable: true,
                           visible: false
                       }, {
                           field: 'numCarpeta',
                           title: 'No. Carpeta',
                           sortable: true
                       }, {
                           field: 'placas',
                           title: 'Placas',
                           sortable: true
                       }, {
                           field: 'nrpv',
                           title: 'NRPV',
                           sortable: true
                       }, {
                           field: 'numSerie',
                           title: 'No. serie',
                           sortable: true
                       }, {
                           field: 'numMotor',
                           title: 'No. Motor',
                           sortable: true
                       },

                       ],
                   });
</script>
