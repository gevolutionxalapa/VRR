
@section('title')
Panel de validación
@endsection
<!--menu-->

<!-- Contenido -->
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatable/bootstrap-table.css') }}">
<link rel="stylesheet" href="{{ asset('css/viewer.css')}}">
<script src="{{ asset('plugins/datatable/bootstrap-table.js')}}"></script>



<style>
.pictures {
  margin: 0;
  padding: 0;
  list-style: none;
}
.pictures > li {
  float: left;
  width: 33.3%;
  height:33.3%;
  margin: 0 -1px -1px 0;
  border: 1px solid transparent;
  overflow: hidden;
}
.pictures > li > img {
  width: 100%;
  cursor: -webkit-zoom-in;
  cursor: zoom-in;
}

.switch {
  position: relative;
  display: inline-block;
  width: 45px;
  height: 24px;
}

.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 16px;
  width: 16px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(20px);
  -ms-transform: translateX(20px);
  transform: translateX(20px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 17px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>

<div class="container">
  <div class="row">
    <div class="col-md-12">

      <!-- menu interno -->

      <!-- Contenido -->

      <div >
        <div class="card-header">
          Vehículos robados
        </div>
        <div class="card-body">
          <table id="table" class="table table-bordered table-hover table-responsive-sm"></table>
        </div>
      </div>

      {{-- Diseño del modal para mostrar los detalles del vehiculo--}}
      <div class="row">

        <div id="modalDetalleVehiculo" class="modal modal-responsive-sm" tabindex="-1" role="dialog" data-backdrop="static">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Detalle del vehículo Robado</h5>
                <button type="button" class="close check-all btnClose" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <p> <strong>Instrucciones :</strong> Seleciona los campos que cumplen con la información correcta </p>
                <br>
                <form id="form_registro" >
                  {!!Form::text('id',null,['id' => 'id', 'hidden'])!!}
                <div class="input-group mb-3">
                  <select name="validaEntidad" class="custom-select" id="ValidadorEntidad" required>
                    <option selected>SELECCIONE TIPO DE ENTIDAD</option>
                    <option value="1">POLICIA MINISTERIAL</option>
                    <option value="2">ENLACE DE ESTADÍSTICA E INFORMÁTICA</option>
                  </select>
                  <div class="input-group-append">
                    <label class="input-group-text" for="inputGroupSelect02">Entidad que valida</label>
                  </div>
                </div>
                <br>

                  <div class="row">
                    <div class="col">
                      <div class="form">
                        {!!Form::label('permiso', 'Documento de circulación')!!}
                      </div>
                      <div class="input-group" >


                        <select class="templatingSelect10 form-control form-control-sm" readonly="readonly"  id="idDocCirculacion" style="width:100%" name="idDocCirculacion" readonly="readonly" >
                          <option selected="selected" value="" > </option>
                          @foreach($documentosCirculacion as $documento)
                          <option value="{{$documento->id}}" >{{$documento->nombre}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="col">
                      <div id="divPlaca">
                        <div class="form">
                          {!!Form::label('placa', 'Placa')!!}
                        </div>
                        <div class="input-group" style="width:100%">
                          <div class="input-group-prepend">
                            <div class="input-group-text ">
                              <input type="checkbox" value="placa" class="cmp_uno btncheckbox" id="checkplaca" aria-label="Checkbox for following text input">
                            </div>
                          </div>
                        {!!Form::text('placa',null,['class'=>'form-control form-control-sm', 'null' ,'id' => 'idplaca', 'placeholder' => 'HMZ2398 - Ejemplo' , 'maxlength'=>'7',  'style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'alphanumeric','readonly'=>'readonly;'])!!}
                        </div>
                      </div>
                      <div id="divFolio" style = 'display: none'>
                        <div class="form">
                          {!!Form::label('folio', 'Folio')!!}
                        </div>
                        <div class="input-group" style="width:100%">
                          <div class="input-group-prepend">
                            <div class="input-group-text">
                              <input type="checkbox" value="folio" class="cmp_uno " aria-label="Checkbox for following text input">
                            </div>
                          </div>
                          {!!Form::text('folio',null,['class'=>'form-control form-control-sm', 'id' => 'folio', 'style'=>'text-transform: uppercase;', 'readonly'=>'readonly;'])!!}
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      {!!Form::label('idEstado', 'Entidad federativa')!!}
                      <div class="row">
                        <div class="col-md-2">
                          <div class="input-group-prepend">
                            <div class="input-group-text">
                              <input type="checkbox" value="idEstados" class="cmp_uno btncheckbox" aria-label="Checkbox for following text input">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-10">
                          <select  class=" templatingSelect3 form-control form-control-sm " style="width:100%" id="idEstados" name="idEstados" required readonly="readonly">
                            <option value="" selected="selected"> </option>
                            @foreach($estados as $estado)
                            <option  value="{{$estado->id}}" >{{$estado->nombre}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                    </div>
                  </div>

                  <div class="row">
                    <div class="col">
                      <div class="form-group">
                        {!!Form::label('idMarca', 'Marca')!!} <br>
                        <div class="row">
                          <div class="col-md-2">
                            <div class="input-group-prepend">
                              <div class="input-group-text">
                                <input type="checkbox" value="idMarcas" class="cmp_uno btncheckbox" aria-label="Checkbox for following text input">
                              </div>
                            </div>
                          </div>
                          <div class="col-md-10">
                            <select  class="templatingSelect3 form-control form-control-sm" style="width:100%" id="idMarcas" name="idMarcas" required>
                              <option value="" selected="selected" > </option>
                              @foreach($marcas as $marca)
                              <option  value="{{$marca->id}}">{{$marca->nombre}}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col">
                      {!!Form::label('idSubmarca', 'Submarca')!!}
                      <div class="row">
                        <div class="col-md-2">
                          <div class="input-group-prepend">
                            <div class="input-group-text">
                              <input type="checkbox" value="idSubmarcas" class="cmp_uno btncheckbox" aria-label="Checkbox for following text input">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-10">
                          <select  class="idSubmarcas form-control form-control-sm" style="width:100%"  id="idSubmarcas" name="idSubmarcas" required>
                            <option value="" required selected="selected"></option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col">
                      {!!Form::label('modelo', 'Modelo')!!}
                      <div class="row">
                        <div class="col-md-2">
                          <div class="input-group-prepend">
                            <div class="input-group-text">
                              <input type="checkbox" value="modelos" class="cmp_uno btncheckbox" aria-label="Checkbox for following text input" readonly>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-10">
                          <input class="form-control form-control-sm" style="width:100%" value="{{date('Y')}}" min="1885" max="{{date('Y')+1}}" name="modelos" type="number" required id="modelos">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col">
                      {!!Form::label('idColor','Color')!!} <br>

                      <div class="input-group">
                        <!-- <div class="row"> -->
                        <div class="col-md-3">
                          <div class="input-group-prepend">
                            <div class="input-group-text">
                              <input type="checkbox" value="idColors" class="cmp_uno btncheckbox" aria-label="Checkbox for following text input">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-9" disabled>
                          <select  class=" form-control form-control-sm" style="width:100%" id="idColors" name="idColors" required disabled>
                            <option selected="selected" value="" disabled> </option>
                            @foreach($colores as $color)
                            <option value="{{$color->id}}">{{$color->nombre}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col">
                      {!!Form::label('numSeries','Número de serie')!!}
                      <div class="row">
                        <div class="col-md-3">
                          <div class="input-group-prepend">
                            <div class="input-group-text">
                              <input type="checkbox" value="numSeries" class="cmp_uno btncheckbox"  aria-label="Checkbox for following text input">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-9">
                          {!!Form::text('numSeries',null, ['class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el número de serie','maxlength'=>'17','style'=>'text-transform: uppercase;',  'data-validation'=>'custom', 'data-validation'=>'alphanumeric', 'readonly'] )!!}
                        </div>
                      </div>
                    </div>
                    <div class="col">
                      {!!Form::label('motors', 'Número de motor')!!}
                      <div class="row">
                        <div class="col-md-3">
                          <div class="input-group-prepend">
                            <div class="input-group-text">
                              <input type="checkbox" value="motors" class="cmp_uno btncheckbox" aria-label="Checkbox for following text input">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-9">
                          {!!Form::text('motors',null, ['class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el número de motor', 'style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'alphanumeric', 'readonly' ] )!!}
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col">
                      {!!Form::label('nrpv2','NRPV')!!}
                      <div class="row">
                        <div class="col-md-3">
                          <div class="input-group-prepend">
                            <div class="input-group-text">
                              <input type="checkbox" value="nrpv2" class="cmp_uno btncheckbox" aria-label="Checkbox for following text input">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-9">
                          {!!Form::text('nrpv2',null, ['class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el NRPV', 'style'=>'text-transform: uppercase;','required', 'readonly'] )!!}
                        </div>
                      </div>
                    </div>
                    <div class="col">
                      {!!Form::label('idClaseVehiculo','Tipo de vehículo')!!}
                      <div class="row">
                        <div class="col-md-3">
                          <div class="input-group-prepend">
                            <div class="input-group-text">
                              <input type="checkbox" value="idClaseVehiculo2" class="cmp_uno btncheckbox" aria-label="Checkbox for following text input">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-9">
                          <select  class="templatingSelect7 form-control form-control-sm" style="width:100%"  id="idClaseVehiculo2" name="idClaseVehiculo2" required>
                            @foreach($clasevehiculo as $clasevehiculo)
                            <option value="{{$clasevehiculo->id}}">{{$clasevehiculo->nombre}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col">
                      {!!Form::label('idSubclase','Subtipo de vehículo')!!}
                      <div class="row">
                        <div class="col-md-3">
                          <div class="input-group-prepend">
                            <div class="input-group-text">
                              <input type="checkbox" value="idSubclase2" class="cmp_uno btncheckbox" aria-label="Checkbox for following text input">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-9">
                          <select  class="templatingSelect1 form-control form-control-sm" style="width:100%" id="idSubclase2" name="idSubclase2" required>
                            <option selected="selected" value=""> </option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col">
                      {!!Form::label('idUso','Tipo de uso')!!}
                      <div class="row">
                        <div class="col-md-3">
                          <div class="input-group-prepend">
                            <div class="input-group-text">
                              <input type="checkbox" value="idUso2" class="cmp_uno btncheckbox" aria-label="Checkbox for following text input">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-9">
                          <select  class="templatingSelect8 form-control form-control-sm" style="width:100%" id="idUso2" name="idUso2" required>
                            <option selected="selected" value=""> </option>
                            @foreach($tipouso as $tipouso)
                            <option value="{{$tipouso->id}}">{{$tipouso->nombre}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col">
                      {!!Form::label('idProcedencia','Procedencia')!!}
                      <div class="row">
                        <div class="col-md-3">
                          <div class="input-group-prepend">
                            <div class="input-group-text">
                              <input type="checkbox" value="idProcedencia2" class="cmp_uno btncheckbox" aria-label="Checkbox for following text input">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-9">
                          <select style="width:100%" class="templatingSelect9 form-control form-control-sm"  id="idProcedencia2" name="idProcedencia2" required>
                            <option value="" selected>Seleccione</option>
                            <option value="2" >EXTRANJERO</option>
                            <option value="1">NACIONAL</option>
                          </select>
                        </div>
                      </div>

                    </div>
                    <div class="col">
                      {!!Form::label('idAseguradora','Aseguradora')!!}
                      <div class="row">
                        <div class="col-md-3">
                          <div class="input-group-prepend">
                            <div class="input-group-text">
                              <input type="checkbox" value="idAseguradora2" class="cmp_uno btncheckbox" aria-label="Checkbox for following text input">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-9">
                          <select class="templatingSelect6 form-control form-control-sm" style="width:100%" required id="idAseguradora2" name="idAseguradora2">
                            <option selected="selected" value=""> </option>
                            @foreach($aseguradoras as $aseguradora)
                            <option value="{{$aseguradora->id}}">{{$aseguradora->nombre}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col">
                      {!!Form::label('senasPartic','Señas particulares')!!}
                      <div class="row">
                        <div class="col-md-12">
                          <div class="input-group-prepend">
                            <div class="input-group-text">
                              <input type="checkbox" value="senasPartic2" class="cmp_uno btncheckbox" aria-label="Checkbox for following text input">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <textarea  class="form-control form-control-sm" placeholder="Ingrese las señas particulares" rows="3" name="senasPartic2" cols="50" style="text-transform: uppercase;" required id="senasPartic2" readonly></textarea>
                        </div>
                      </div>
                    </div>
                  </div>

                  <input type="hidden" id="invalidosDatos" name="elementosInvalidos" value="" style="display:none">


              </div>
              <div class="mensajes">
                <div class="row text-center">
                  <div class="col-md-6" id="ver_event1" style="display:none">
                    <label for="">Información</label>
                    <div class="alert alert-info" role="alert">
                      Continúe comprobando la información, en caso de existir alguna campo incorrecto añade algún comentario relativo para su corrección
                    </div>
                  </div>
                  <div class="col-md-6" id="editorComent" style="display:none">
                    <label for="comment">Comentario</label>
                    <textarea class="form-control" name="comentario_data" id="comentario_data"  aria-label="With textarea"></textarea>
                  </div>
                  <div class="col-md-6" id="ver_event2" style="display:none">
                    <div class="alert alert-success" role="alert">
                      La información mostrada se encuentra en su totalidad validada
                    </div>
                  </div>
                </div>
              </div>
              </form>
              <div class="modal-footer">
                <!-- <label>Editar</label>
                <label class="switch">
                  <input type="checkbox" id="btn_editar">
                  <span class="slider round"></span>
                </label> -->
                <button type="button" class="btn btn-secondary btnClose" id="guardar_cambios" >Guardar</button>
                <button type="button" class="btn btn-secondary check-all btnClose" data-dismiss="modal">Cerrar</button>
              </div>

            </div>
          </div>
        </div>
      </div>

        <script src="{{asset('js/viewer.js')}}"></script>
        <script src="{{ asset('js/denuncia/peticiones.js') }}"></script>
        <script type="text/javascript">


        // $("#detallevr").click(function(){
        //   // document.getElementById("modalDetalleVehiculo").reset();
        //    $("#checkplaca").html('');
        //   console.log("aqui se borra");
        // });
        // function detallevr(){
        //
        //   console.log("aqui se borra");
        // }

        var checked = false;

    $('.check-all').on('click',function(){

    if(checked == false) {
    $('.cmp_uno').prop('checked', false);
    checked = false;
    } else {
    $('.cmp_uno').prop('checked', true);
    checked = false;
    }

    });

        function elemento (nombre, valor) {
          var p = $('<p>').text(nombre+': '+valor);
          return p;
        }



        /* Creacion de los elementos extras a mostrar*/
        function operateFormatter(value, row, index) {
          var ruta = "{{route('existeDiligencia',':idVeh')}}";
          ruta = ruta.replace(':idVeh', row.id);
          respuesta = peticionInfomacion('get',ruta,null);
          if(respuesta) {
            return [

              '<a class="visualizacion"  href="javascript:void(0)" title="Visualización de documento">',
              '<i class="fa fa-eye" aria-hidden="true"></i>',
              '</a>  ',
              '<a class="validacion"  data-target="#modal" href="javascript:void(0)" title="Validación">',
              '<i class="fa fa-check-square-o" aria-hidden="true"></i>',
              '</a>  ',

            ].join('');
          } else {
            return [

              '<a class="visualizacion"  href="javascript:void(0)" title="Visualización de documento">',
              '<i class="fa fa-eye" aria-hidden="true"></i>',
              '</a>  ',
              '<a class="validacion"  data-target="#modal" href="javascript:void(0)" title="Validación" onclick="mostrar()">',
              '<i class="fa fa-check-square-o" aria-hidden="true"></i>',
              '</a>  ',
            ].join('');
          }
        }





        /* Asignacion de funciones a los elemento agreados anteriormente en operateFormatter*/
        window.operateEvents = {
          'click .validacion': function (e, value, row, index) {
            //alert('info del vehiculo, row: ' + JSON.stringify(row));
            console.log(row);
            $('#modalDetalleVehiculo').modal('show');
            $('#id').val(row.id);
            $('#idplaca').val(row.placas);
            $('#idEstados').val(row.idEstado).trigger('change');
            $('#idMarcas').val(row.idMarca).trigger('change');
            $('#idSubmarcas').val(row.idSubmarca).trigger('change');
            $('#modelos').val(row.modelo);
            $('#idColors').val(row.idColor).trigger('change');
            $('#numSeries').val(row.numSerie);
            $('#motors').val(row.numMotor);
            $('#nrpv2').val(row.nrpv);
            $('#idClaseVehiculo2').val(row.idClaseVehiculo).trigger('change');
            $('#idSubclase2').val(row.idTipoVehiculo).trigger('change');
            $('#idUso2').val(row.idTipoUso).trigger('change');
            $('#idProcedencia2').val(2).trigger('change');
            $('#idAseguradora2').val(row.idAseguradora).trigger('change');
            $('#senasPartic2').val(row.senasPartic);
            $("#idDocCirculacion").val(row.idDocumentoCirculacion).trigger('change');
            $('#folio').val(row.folioDoctoCirc);
            $('#invalidosDatos').val('placa,idEstados,idMarcas,idSubmarcas,modelos,idColors,numSeries,motors,nrpv2,idClaseVehiculo2,idSubclase,idAseguradora2,idUso2,senasPartic2,idProcedencia2');
            data = ['placa','idEstados','idMarcas','idSubmarcas',
            'modelos','idColors','numSeries','motors',
            'nrpv2','idClaseVehiculo2','idSubclase2',
            'idAseguradora2','idProcedencia2','idUso2','senasPartic2'];

          },


          //porcentanje en barra

          'click .formatoVdiligencia': function (e, value, row, index) {
              //alert('info del vehiculo, row: ' + JSON.stringify(row));
              // $('#modaldoctosdiligencia').modal('show');
              var identificador = row['id'];
              console.log(identificador);
              if(identificador!=''){
                var ruta = "{{route('getDilidencia',':idVeh')}}";
                ruta = ruta.replace(':idVeh', identificador);
                datos = null;
                respuesta = peticionInfomacion('get',ruta,datos);
                console.log(respuesta);
                var filename=respuesta.estado[0].destino;
                console.log(filename);
                getDoc = "{{route('getImg',['filename'=>':ruta'])}}";
                getDoc = getDoc.replace(':ruta', filename);
                //     $('#contenidoDil').append('')
                console.log(getDoc);
                window.open(getDoc, '_blank');

              }
              //alert("hola");

            },
            'click .visualizacion': function (e, value, row, index) {
                        //alert('info del vehiculo, row: ' + JSON.stringify(row));
                        // $('#modaldoctosdiligencia').modal('show');
                        var identificador = row['id'];
                        console.log(identificador);
                        if(identificador!=''){
                          var ruta = "{{route('getDilidencia',':idVeh')}}";
                          ruta = ruta.replace(':idVeh', identificador);
                          datos = null;
                          respuesta = peticionInfomacion('get',ruta,datos);
                          console.log(respuesta);
                          var filename=respuesta.estado[0].destino;
                          console.log(filename);
                          getDoc = "{{route('getImg',['filename'=>':ruta'])}}";
                          getDoc = getDoc.replace(':ruta', filename);
                        //     $('#contenidoDil').append('')
                        console.log(getDoc);
                        window.open(getDoc, '_blank');

                      }
                        //alert("hola");

                      },
                    };


          /* Configuracion de la tabla de registros de vehiculos */
          var ruta = "{{route('getregistros')}}";
          $('#table').bootstrapTable({
            search:  true,
            showColumns: true,
            showRefresh: true,
            url: ruta,
            buttonsAlign :'left',
            searchAlign :'left',
            idField: 'id',
            iconsPrefix: 'fa',
            pagination : true,
            pageSize: 5,
            columns: [{
              field: 'id',
              title: 'ID',
              sortable: true,
              visible: false
            }, {
              field: 'placas',
              title: 'Placas',
              sortable: true
            }, {
              field: 'modelo',
              title: 'Modelo',
              sortable: true
            }, {
              field: 'nrpv',
              title: 'NRPV',
              sortable: true
            }, {
              field: 'permiso',
              title: 'Permiso',
              sortable: true
            }, {
              field: 'numSerie',
              title: 'No. serie',
              sortable: true
            }, {
              field: 'numMotor',
              title: 'No. Motor',
              sortable: true
            }, {
              field: 'procedencia',
              title: 'Procedencia',
              sortable: true
            }, {
              field: 'marca',
              title: 'Marca',
              sortable: true
            }, {
              field: 'submarca',
              title: 'Submarca',
              sortable: true,
              visible: false
            }, {
              field: 'color',
              title: 'Color',
              sortable: true,
              visible: false
            }, {
              field: 'tipoVehiculo',
              title: 'Tipo',
              sortable: true,
              visible: false
            }, {
              field: 'claseVehiculo',
              title: 'Clase',
              sortable: true,
              visible: false
            }, {
              field: 'tipoUSo',
              title: 'Tipo de uso',
              sortable: true,
              visible: false
            }, {
              field: 'aseguradora',
              title: 'Aseguradora',
              sortable: true,
              visible: false
            }, {
              field: 'senasPartic',
              title: 'Señas Particulares',
              sortable: true,
              visible: false
            }, {
              field: 'operate',
              title: 'Acciones',
              align: 'center',
              events: operateEvents,
              formatter: operateFormatter
            }],
          });

          $( document ).ready(function() {
            var preguntas = 15;
            var data = ['placa','idEstados','idMarcas','idSubmarcas',
            'modelos','idColors','numSeries','motors',
            'nrpv2','idClaseVehiculo2','idSubclase2',
            'idAseguradora2','idProcedencia2','idUso2','senasPartic2'];

            $('.cmp_uno').change(function(){
              ($(this).prop( "checked" ))?  preguntas-- : preguntas++;
              console.log('Antes de');
              console.log(data);
              var elimina = data.indexOf( $(this).val());
              ($(this).prop( "checked" ))?  data.splice( elimina,1 ) : data.push($(this).val());
              console.log('Antes de');
              console.log(data);
              if(preguntas==0){
                $('#ver_event2').show('slow');
                $('#editorComent').hide('slow');
                $('#ver_event1').hide('slow');
              }else{
                $('#ver_event2').hide('slow');
                $('#editorComent').show('slow');
                $('#ver_event1').show('slow');
              }
              $('#invalidosDatos').val( data.toString());


            });

            $("#idMarcas").change(function() {
              // alert('hola');
              var identificador = $(this).val();
              var respuesta;
              $('#idSubmarcas').html('');
              if(identificador!=''){
                var ruta = "{{route('submarca',':marca')}}";
                ruta = ruta.replace(':marca', identificador);
                datos = null;
                respuesta = peticionInfomacion('get',ruta,datos);
                console.log(respuesta);
                if (respuesta != null) {
                  for(i=0; i<respuesta.estado.length; i++){
                    $("#idSubmarcas").append("<option value='"+respuesta.estado[i].id+"'> "+respuesta.estado[i].nombre+"</option>");
                  }
                  if(respuesta.estado.length > 0 ) {
                    localStorage.setItem("idSubmarcas", respuesta.estado[0].id);
                  }
                  console.log('cargando datos');
                }else{
                  alert('Error de servidor');
                }
              }else{
                //alert('no aplica');
              }
            });

            $("#idClaseVehiculo2").change(function() {
              // alert('hola');
              var identificador = $(this).val();
              var respuesta;
              $('#idSubclase2').html('');
              if(identificador!=''){
                var ruta = "{{route('subclase',':clasevehiculo')}}";
                ruta = ruta.replace(':clasevehiculo', identificador);
                datos = null;
                respuesta = peticionInfomacion('get',ruta,datos);
                console.log(respuesta);
                if (respuesta != null) {
                  for(i=0; i<respuesta.estado.length; i++){
                    $("#idSubclase2").append("<option value='"+respuesta.estado[i].id+"'> "+respuesta.estado[i].nombre+"</option>");
                  }
                  if (respuesta.estado.length > 0) {
                    localStorage.setItem("idSubclase2", respuesta.estado[0].id);
                  }
                  console.log('cargando datos');
                }else{
                  alert('Error de servidor');
                }
              }else{
                alert('no aplica');

              }
            });



            var idmarca = $("#idMarcas").val();

            if (idmarca > 0) {
              var ruta = "{{route('submarca',':marca')}}";
              ruta = ruta.replace(':marca', idmarca);
              datos = null;
              respuesta = peticionInfomacion('get',ruta,datos);

              if (respuesta != null) {
                for(i=0; i<respuesta.estado.length; i++){
                  $("#idSubmarcas").append("<option value='"+respuesta.estado[i].id+"'> "+respuesta.estado[i].nombre+"</option>");
                  //console.log(respuesta.estado[i].id);
                }
                $('#idSubmarcas').val(localStorage.idSubmarca).trigger("change");
              }
            }

            var idClaseVehiculo = $("#idClaseVehiculo2").val();
            if (idClaseVehiculo > 0) {
              var ruta = "{{route('subclase',':clasevehiculo')}}";
              ruta = ruta.replace(':clasevehiculo', idClaseVehiculo);
              datos = null;
              respuesta = peticionInfomacion('get',ruta,datos);
              if (respuesta != null) {
                for(i=0; i<respuesta.estado.length; i++){
                  $("#idSubclase2").append("<option value='"+respuesta.estado[i].id+"'> "+respuesta.estado[i].nombre+"</option>");
                }
              }
              $('#idSubclase2').val(localStorage.idSubclase).trigger("change");
            }
          });

          $('#form_registro').find('input, textarea, button, select').removeAttr('disabled','disabled');
          $('#guardar_cambios').removeAttr('disabled','disabled');
          $('#idDocCirculacion').attr('disabled','disabled');
          $('#idEstados').attr('disabled','disabled');
          $('#modelos').attr('disabled','disabled');
          $('#idMarcas').attr('disabled','disabled');
          $('#idSubmarcas').attr('disabled','disabled');
          $('#idColors').attr('disabled','disabled');
          $('#idClaseVehiculo2').attr('disabled','disabled');
          $('#idSubclase2').attr('disabled','disabled');
          $('#idUso2').attr('disabled','disabled');
          $('#idProcedencia2').attr('disabled','disabled');
          $('#idAseguradora2').attr('disabled','disabled');
          //bloquear modal detalle de vehiculo robado
          $('.btncheckbox').attr('disabled','disabled');
          $('#guardar_cambios').attr('disabled','disabled');
          $('.btnClose').removeAttr('disabled');
          $('#ValidadorEntidad').change(function(){
              var valor = $('#ValidadorEntidad').val();
              if(valor!=0){
                  $('.btncheckbox').removeAttr('disabled');
                  $('#guardar_cambios').removeAttr('disabled');
                }else{
                  $('.btncheckbox').attr('disabled','disabled');
                  $('#guardar_cambios').attr('disabled','disabled');
                }

          });
          $('.btnClose').click(function(){
            //alert('aqui');
            $('#ValidadorEntidad').removeAttr('disabled');
            $('#modalDetalleVehiculo').trigger("reset");
            $('.mensajes').html('');
          });

          $('#ValidadorEntidad').change(function(){
            //alert ($('#id').val());
            var idVeh = $('#id').val();
            var entidad = $(this).val();
              var ruta = "{{route('statusRevision',['idVeh'=>':idVeh','entidad'=>':entidad'])}}";
              ruta = ruta.replace(':idVeh', idVeh);
              ruta = ruta.replace(':entidad', entidad);
              datos = null;
              cambioStatus = peticionInfomacion('get',ruta,datos);
              console.log("Aqui se cambia el estatus");
              if (respuesta != null) {
                //ert("funciona");
              //   for(i=0; i<respuesta.estado.length; i++){
              //     $("#idSubmarcas").append("<option value='"+respuesta.estado[i].id+"'> "+respuesta.estado[i].nombre+"</option>");
              //     //console.log(respuesta.estado[i].id);
              //   }
              //   $('#idSubmarcas').val(localStorage.idSubmarca).trigger("change");
              }
          });

          // $('#guardar_cambios').attr('disabled','disabled');;
          // $('#ValidadorEntidad').bind('click', function () {
          //   if ($('#ValidadorEntidad').is(':checked')) {
          //     $('#modalDetalleVehiculo').find('input, textarea, button, select').attr('disabled','disabled');
          //     $('#guardar_cambios').attr('disabled','disabled');
          //   } else {
          //     $('#modalDetalleVehiculo').find('input, textarea, button, select').removeAttr('disabled','disabled');
          //     $('#guardar_cambios').removeAttr('disabled','disabled');
          //   }
          //});

          $('#numSeries').focusout(function(){
            var numSerie = $('#numSeries').val();
            if(numSerie!=""){
              console.log('Este es el numero de serie ->'+numSerie);
              var ruta ="{{route('niv',['num_serie'])}}";
              ruta = ruta.replace("num_serie",numSerie);
              console.log(ruta);
              var respuesta = peticionInfomacion('get',ruta);
              (respuesta.request.status=='error')? toastr.error(respuesta.request.mensaje, "ERROR",{"preventDuplicates": true}):toastr.success(respuesta.request.mensaje, "CORRECTO",{"preventDuplicates": true});

            }
          });

          $('#guardar_cambios').bind('click', function () {
            //alert($('#idDocCirculacion').val());
            $('#form_registro').find('input, textarea, button, select').removeAttr('disabled');
            $.ajax({
              url : "{{route('saveValidacion')}}",
              data : $('#form_registro').serialize(),
              type : 'POST',
              success : function(json) {
                console.log('Se agrego un vehiculo');
                console.log(json);

                swal({
                  title: "Revisión de registro exitosa",
                  icon: "success",

                });
                $('#table').bootstrapTable('refresh');
                $('#modalDetalleVehiculo').modal('toggle');
                $('#form_registro').find('input, textarea, button, select').attr('disabled','disabled');
                $('#guardar_cambios').attr('disabled','disabled');
                $("input[type='checkbox']").prop("checked", false).attr("checked", false).removeAttr("checked");
                console.log('actualizar');
                //$('#form_registro).find('input, textarea, button, select').attr('disabled','disabled');
                //$('#guardar_cambios').attr('disabled','disabled');

              },
              error : function(xhr, status) {
                console.log('Disculpe, existió un problema');
                console.log(xhr);
                swal({
                  title: "Error al guardar cambios",
                  icon: "error",
                });
              },
              complete : function(xhr, status) {
                console.log('Petición realizada');
              }
            });
          });

          $("#idDocumentoCirculacion").change(function(){
            var selec = $("#idDocumentoCirculacion").val();
            if(selec ==  1) {
              $('#divPlaca').css('display','');
              $('#divFolio').css('display','none');
            } else {
              $('#divPlaca').css('display','none');
              $('#divFolio').css('display','');
            }
          });

          function genRepoVehic(){
            var idmarca = 2123455;
            var ruta = "{{route('getMockup',':idCarpeta')}}";
            ruta = ruta.replace(':idCarpeta', idmarca);
            respuesta = peticionInfomacion('get',ruta,null);
            respuesta = JSON.stringify(respuesta);
            $('#beforeSendRepor').val(respuesta);
            if($('#beforeSendRepor').val()!='' && $('#beforeSendRepor').val()!= null){
              $( "#getSendReporPdf" ).submit();
            }

          }
          function mostrar(){
            $('#modalvalidación').modal('show');

          }



          function adjuntar(){
            $('#modalvalidación').modal('show');

          }
          function visualizardoc(){
            var id = 2123455;
            var ruta = "{{route('getDilidencia',':idVeh')}}";

            ruta = ruta.replace(':idVeh',id);
            console.log(ruta);
            console.log('Hola desde la funcion');
            respuesta = peticionInfomacion('get',ruta,null);
            respuesta = JSON.stringify(respuesta);
            $('#beforeSendRepor').val(respuesta);
            if($('#beforeSendRepor').val()!='' && $('#beforeSendRepor').val()!= null){
              $( "#getSendReporPdf" ).submit();
            }
          }


          </script>


        </div>
      </div>
      {{-- @endsection --}}
