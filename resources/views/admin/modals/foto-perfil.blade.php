<div class="modal fade" id="fotoPerfil">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                    <div class="stack text-left " >
                            <h5><strong>Actualiza tu foto de perfil:</strong></h5>
                            <h4 class="modal-title"></h4>
                    </div>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div id="cuerpomodal" class="modal-body">                 
                <div  class="form-group file-loading" id="subirArchivo">                    
                    <input type="file"  id="archivoFoto" name="archivoFoto">                   
                </div>
    
            <div class="modal-footer">                  
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            <button type="button" id="subirFoto" class="btn btn-secondary">Actualizar</button>
            </div>
            </div>
       </div>
    </div>
</div>


    <script src="{{ asset('plugins/fileinput/js/fileinput.min.js')}}" ></script>
    <script src="{{ asset('plugins/fileinput/themes/fa/theme.min.js')}}" ></script>
    <script src="{{ asset('plugins/fileinput/locales/es.js')}}" ></script>

<script type="text/javascript">
    $('#botonModalFoto').on('click',function(){
        $("#archivoFoto").fileinput('destroy');
        $("#archivoFoto").fileinput({
            language:'es',
            theme: 'fa',
            browseClass: "btn btn-primary btn-block",
            showCaption: false,
            {{-- maxImageWidth: 200,
            maxImageHeight: 200, --}}
            showRemove: false,
            showUpload: false,
            allowedFileExtensions: ['jpg','jpeg','png','gif']
        });
        $('.fileinput-cancel').hide();
       {{--   var canvas = document.getElementById('myCanvas');
       // console.log('canvas',canvas);  --}}
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });   
  
    $('#subirFoto').on('click',function(){
        var formData = new FormData();
        formData.append('file', $('#archivoFoto')[0].files[0]);
        $.ajax({
            url : "{{route('update.foto')}}",
            type : 'POST',
            data : formData,
            processData: false,
            contentType: false,
            success : function(data) {
                if(data.success==true){
                    $('#perfilImg').attr('src',"{{ asset("storage/fotoFiscal")}}"+"/"+data.ruta);
                    swal("Éxito", "Imagen actualizada con éxito.", "success");
                    $('#fotoPerfil').modal('toggle');
                }else{
                    swal("Error", "Inténtelo de nuevo.", "error");
                }
            },
            error: function(e){
                swal("Error", "Error interno, inténtelo más tarde.", "error");
               // console.log(e);
            }
        });
    });
</script>
