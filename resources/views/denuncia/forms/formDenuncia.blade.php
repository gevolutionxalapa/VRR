@section('title')
Vehículos Robados
@endsection


<link rel="stylesheet" type="text/css" href="{{ asset('css/PreVehiculo/theme.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/PreVehiculo/fileinput.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/modalsweetalert.css') }}">
<!-- Codigo para el mapa  -->

<div class="container">
	<br>
	{!! Form::open(['method' => 'POST','files' => true, 'id' => 'formDenuncia'])  !!}
	<div class="card">
		{!!Form::hidden('latMap',0,['id' => 'latMap'])!!}
		{!!Form::hidden('lonMap',0,['id' => 'lonMap'])!!}

		<div class="card-header">
			<div class="row">
				<div class="col-lg-6 col-md-6" >
					<h5 align="justify">Búsqueda Preregistro por Folio</h5>
				</div>
				<div class="col-lg-6 col-md-6" align="right">
					<a href="{{env('SSO_IP_UIPJ')}}carpeta/{{$idcarpeta}}" class="btn btn-secondary left" name="volver" id="volver" >Regresar </a>
					<button type="button" class="btn btn-secondary left" name="repuve" id="repuve" disabled>REPUVE</button>
					<button type="button" class="btn btn-secondary right" name="sat" id="sat">SAT</button>
					<button type="button" class="btn btn-secondary right" name="verificacion" id="verificacion" >Verificación</button>
					<button type="submit" class="btn btn-primary left" id="btnEmpty">Guardar</button>
				</div>
			</div>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="" id="registro">
					<div id="denunciante">
						<ul id="tdenunciante" class="nav nav-pills flex-column flex-sm-row">
							<li class= "nav-item2" id="registro">
								<a class="nav-link active btn-outline-dark" data-toggle="tab" href="#panel4" ><span id="personal" class="pestaña" >Registro robo de vehículo</span></a>
							</li>
							<li class= "nav-item2" id="registros">
								<a class="nav-link btn-outline-dark" data-toggle="tab" href="#panel11" ><span id="direccion" class="pestaña">Vehículos robados</span></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-3 col-md-3">
					{!!Form::label('token', 'Escriba c&oacute;digo de preregistro:',['class'=>'sr-only'])!!}
					{!!Form::text('token',null,['class'=>'form-control', 'id' => 'token', 'placeholder' => 'Ej. DKHJMS','style'=>'text-transform: uppercase;'])!!}
				</div>
				<div class="col-lg-2 col-md-2">
					<button type="button" class="btn btn-dark" name="buscar" id="buscar">Buscar</button>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-8 col-md-8 ">
			<div class="container">
				<div class="card">
					<div class="card-header">
						<h5 align="justify">Datos generales del vehículo</h5>
					</div>
					<br>
					<div class="card-body">
						<div class="row">
							<div class="col-lg-6 col-md-6">
								<div class="form-group">
									{!!Form::label('idProcedencia','Procedencia')!!}
									<select class="templatingSelect9 form-control form-control-sm" id="idProcedencia" name="idProcedencia" required>
										<option value="" selected>Seleccione</option>
										<option value="2" >EXTRANJERO</option>
										<option value="1">NACIONAL</option>
									</select>
								</div>
							</div>
							<div class="col-md-6 col-lg-6">
								<div class="form">
									{!!Form::label('permiso', 'Documento de circulación')!!}
								</div>
								<div class="input-group">
									<select class="templatingSelect10 form-control form-control-sm"  id="idDocumentoCirculacion" name="idDocumentoCirculacion" >
										<option selected="selected" value=""> </option>
										@foreach($documentosCirculacion as $documento)
										<option value="{{$documento->id}}" @if($documento->id == 5) selected @endif>{{$documento->nombre}}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-lg-6 ">
								<div id="divPlaca">
									<div class="form">
										{!!Form::label('placa', 'Placa')!!}
									</div>
									<div class="input-group">
										{!!Form::text('placa',null,['class'=>'form-control form-control-sm', 'null' ,'id' => 'placa', 'placeholder' => 'HMZ2398 - Ejemplo' , 'maxlength'=>'7',  'style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'alphanumeric'])!!}
									</div>
								</div>
								<div id="divFolio" style = 'display: none'>
									<div class="form">
										{!!Form::label('folioDoctoCirc', 'Folio')!!}
									</div>
									<div class="input-group">
										{!!Form::text('folioDoctoCirc',null,['class'=>'form-control form-control-sm', 'id' => 'folioDoctoCirc', 'maxlength'=>'10', 'style'=>'text-transform: uppercase;'])!!}
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6">
								<div class="form-group">
									{!!Form::label('idEstado', 'Entidad federativa de la placa')!!}
									<select  class="templatingSelect2 form-control form-control-sm"  id="idEstado" name="idEstado" required>
										<option value="" selected="selected"> </option>
										@foreach($estados as $estado)
										<option  value="{{$estado->id}}">{{$estado->nombre}}</option>
										{{-- @if ($estado->id == 31)
										<option selected value="{{ $estado->id }}">{{$estado->nombre}}</option>
										@else
										<option  value="{{$estado->id}}">{{$estado->nombre}}</option>
										@endif --}}
										@endforeach
									</select>
								</div>
							</div>
						</div>
						
						<div class="row">
							{{-- <div class="col-lg-6 col-md-6">
								<div class="form-group">
									{!!Form::label('idMunicipio', 'Municipio')!!}
									<input type="text" id="text_municipio" name="text_municipio" hidden>
									<select class="templatingSelect11 form-control form-control-sm" required id="idMunicipio" name="idMunicipio" disabled>
										<option value="" selected="selected"></option>
									</select>
								</div>
							</div> --}}
							<div class="col-lg-6 col-md-6">
								<div class="form-group">
									{!!Form::label('idMarca', 'Marca')!!}
									<select  class="templatingSelect3 form-control form-control-sm"  id="idMarca" name="idMarca",  required onchange="if(this.value!=0) {document.getElementById('idSubmarca').disabled = false}">
										<option value="" selected="selected" > </option>
										@foreach($marcas as $marca)
										<option  value="{{$marca->id}}">{{$marca->nombre}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-lg-6 col-md-6">
								<div class="form-group">
									{!!Form::label('idSubmarca', 'Submarca')!!}
									<select  class="idSubmarca form-control form-control-sm"  id="idSubmarca" name="idSubmarca" required disabled>
										<option value="" required selected="selected"></option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6 col-md-6">
								<div class="form-group">
									{!!Form::label('modelo', 'Modelo')!!}
									<input class="form-control form-control-sm" value="{{date('Y')}}" min="1885" max="{{date('Y')+1}}" name="modelo" type="number" required id="modelo">
								</div>
							</div>
							<div class="col-lg-6 col-md-6">
								<div class="form-group">
									{!!Form::label('idColor','Color')!!}
									<select  class="templatingSelect5 form-control form-control-sm"  id="idColor" name="idColor" required>
										<option selected="selected" value=""> </option>
										@foreach($colores as $color)
										<option value="{{$color->id}}">{{$color->nombre}}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6 col-md-6">
								<div class="form-group">
									{!!Form::label('numSerie','Número de serie')!!}
									{!!Form::text('numSerie',null, ['class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el número de serie', 'required','minlength'=>'10','maxlength'=>'20','style'=>'text-transform: uppercase;' , 'data-validation'=>'custom', 'data-validation'=>'alphanumeric'] )!!}
								</div>
							</div>
							<div class="col-lg-6 col-md-6">
								<div class="form-group">
									{!!Form::label('motor','Número de motor')!!}
									{!!Form::text('motor',null, ['class' => 'form-control form-control-sm','maxlength'=>'20',  'placeholder' => 'Ingrese el número de motor','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'alphanumeric' ] )!!}
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6 col-md-6">
								<div class="form-group">
									{!!Form::label('nrpv','NRPV')!!}
									{!!Form::text('nrpv',null, ['class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el NRPV', 'style'=>'text-transform: uppercase;', 'maxlength'=>'20'] )!!}
								</div>
							</div>
							<div class="col-lg-6 col-md-6">
								<div class="form-group">
									{!!Form::label('idClaseVehiculo','Tipo de vehículo')!!}
									<select  class="templatingSelect7 form-control form-control-sm"  id="idClaseVehiculo" name="idClaseVehiculo" required onchange="if(this.value!=0) {document.getElementById('idSubclase').disabled = false}">
										<option selected="selected" value=""> </option>
										@foreach($clasevehiculo as $clasevehiculo)
										<option value="{{$clasevehiculo->id}}">{{$clasevehiculo->nombre}}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6 col-md-6">
								<div class="form-group">
									{!!Form::label('idSubclase','Subtipo de vehículo')!!}
									<select  class="templatingSelect1 form-control form-control-sm"  id="idSubclase" name="idSubclase" required disabled>
										<option selected="selected" value=""> </option>
									</select>
								</div>
							</div>
							<div class="col-lg-6 col-md-6">
								<div class="form-group">
									{!!Form::label('idUso','Tipo de uso')!!}
									<select  class="templatingSelect8 form-control form-control-sm"  id="idUso" name="idUso" required>
										<option selected="selected" value=""> </option>
										@foreach($tipouso as $tipouso)
										<option value="{{$tipouso->id}}">{{$tipouso->nombre}}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6 col-md-6">
								<div class="form-group">
									{!!Form::label('idAseguradora','Aseguradora')!!}
									<select class="templatingSelect6 form-control form-control-sm" required id="idAseguradora" name="idAseguradora" >
										<option selected="selected" value=""> </option>@foreach($aseguradoras as $aseguradora)
										<option value="{{$aseguradora->id}}">{{$aseguradora->nombre}}</option>@endforeach
									</select>
								</div>
							</div>
							<div class="col-lg-6 col-md-6">
								<div class="form-group">
									{!!Form::label('idPropietario','Propietario')!!}
									<select class="templatingSelectPropietario form-control form-control-sm" required id="idPropietario" name="Propietario" >
										<option selected="selected" value="">Selecione</option>
										@foreach($propietarios as $key => $propietario)
											<option value="{{$propietario}}">{{$key}}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-md-12">
								<div class="form-group">
									{!!Form::label('numeroeconomico','Número económico')!!}
									{!!Form::number('numeroeconomico',null, ['class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el número económico', 'style'=>'text-transform: uppercase;','data-validation-regexp'=>'^([0-9 ]+)$', 'maxlength'=>'20'] )!!}
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-md-12 ">
								<div class="form-group">
									{!!Form::label('senasPartic','Señas particulares')!!}
									<textarea class="form-control form-control-sm" placeholder="Ingrese las señas particulares" rows="5" name="senasPartic" cols="90" maxlength="100" style="text-transform: uppercase;" id="senasPartic"></textarea>
								</div>
							</div>
							{!!Form::text('idSubCatIdenti',null, ['hidden','id'=>'idSubCatIdenti'] )!!}
							{!!Form::text('idSubCatFact',null, ['hidden','id'=>'idSubCatFact'] )!!}
							{!!Form::hidden('idSecurity',null, ['id'=>'idSecurity'] )!!}
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-4 col-md-4 " align="center">
			<div class="container-fluid" align="center">
				<div class="card" align="center">
					<div class="card-header">
						<h5 align="justify">Cargar documentos solicitados</h5>
					</div>
					<div class="card-body" align="center">
						<div id="doctos" clase="row" >
						</div>
						<hr>
						<div class="btn-group btn-group-toggle row" data-toggle="buttons" id="selector" align="center">
							<label class="btn btn-secondary form-check-label" onclick="sectionShow()">
								{{-- <input type="radio" name="img" id="option1" autocomplete="off" value="archivo"> --}}
								<input type="radio" name="img"  id="opcion1"  value="archivo" data-validation="required" required>
								<i class="fa fa-upload"></i>
								Archivos
							</label>
							<label id="divBtnCamara" class="btn btn-secondary form-check-label " onclick="sectionShow()">
								{{-- <input type="radio" name="img" id="option1" autocomplete="off" value="camara" > --}}
								<input align="center"  name="img" type="radio" id="opcion2" value="camara" data-validation="required">
								<i class="fa fa-camera"></i>
								Camara
							</label>
						</div>
						<hr>
						<div id="complemento_asc" style="display:none;">
							<div class="row">
								<div class="col-lg-12 col-md-12">
									<div >
										<div id="iden" >
											<div>
												<label>Identificacion oficial</label>
											</div>
											<select class="listaiden1 form-control form-control-sm listaiden" id="listaiden_op1" name="listaiden1" responsive required>
												<option selected value="" >--Seleccione una opción--</option>
												@foreach($identificacionoficial as $identificacionoficial)
												<option  value="{{$identificacionoficial->id}}" >{{$identificacionoficial->nombre}}</option>
												@endforeach
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="div1" style="display:none;" align="center">
								<br>
									{{-- <div class="row" align="center">
										<div class="col-lg-1 col-md-1"></div>
										<!--busqueda de archivos identificacion oficial-->
										<div class="col-lg-10 col-md-10" align="center">
											<div class="file-loading" >
												<input id="DOC1" class="btn_ide" name="ID_OFICIAL" type="file" accept="image/*" style="display: none;">
											</div>
										</div>
									</div> --}}

								<div class="row" align="center">
									<div class="col-lg-3 col-md-3"></div>
									<!--busqueda de archivos identificacion oficial-->
									<div class="col-lg-7 col-md-7">
										<div class="file-loading" >
											<input id="DOC1" class="btn_ide" name="ID_OFICIAL" type="file" accept="image/*" style="display: none;" required>
										</div>
									</div>
								</div>

							</div>
							<div class="div2" style="display:none;" align="center">
								<div class="row" align="center">
									<div class="col-lg-12 col-md-12" align="center">
										<div class="" id="ife_mini"></div>
										<input type="hidden" id="mini_ife" name="cam_ident" value="">
										<button id="cam_ide" type="button" class="btn btn-dark" data-toggle="modal" data-target="#profile-photo-camera-modal" onclick="setup('IFE')" disabled="disabled" data-toggle="popover" data-trigger="focus hover" title="ATENCIÓN:Capture una imagen" >
											<i class="fa fa-camera"></i> Capturar imagen
										</button>
										<button type="button" id="borrar2"  class="btn btn-secondary" :disabled="form.busy" ata-target="#profile-photo-camera-modal"  disabled="disabled">
											<i class="fa fa-trash"></i>
										</button>
									</div>
									<br>
								</div>
							</div>

							<div class="row">
								<div class="col-lg-12 col-md-12">
									<div >
										<div id="iden" >
											<div>
												<br>
												<label>Factura del vehículo</label>
											</div>
											<select class="listaiden3 form-control form-control-sm listaiden" id="listaiden_op2" name="listaiden3" responsive required>
												<option selected value="" >--Seleccione una opción--</option>
												@foreach($factura as $factura)
												<option  value="{{$factura->id}}" >{{$factura->nombre}}</option>
												@endforeach
											</select>
										</div>
									</div>
								</div>
							</div>
							<input type="hidden" id="tipe_load" name="optCaptura" value="loadFileType" align="center">
						</div>
						<div class="div1" style="display:none;" align="center">
							<br>
							<div class="row" align="center">
								<div class="col-lg-1 col-md-1"></div>
								<div class="col-lg-10 col-md-10" align="center">
									<div class="file-loading">
										<input id="DOC5" align="center" class="btn-fac" name="ID_FACTURA" type="file" accept="image/*" style="display: none;">
									</div>
								</div>
							</div>
						</div>

						<div class="div2" style="display:none;">
							<div class="row">
								<div class="col-lg-12 col-md-12" align="center">
									<div class="" id="fact_mini"></div>
									<input type="hidden" id="mini_fac" name="cam_fact" value="">
									<button id="cam_fac" type="button" class="btn btn-dark" data-toggle="modal" data-target="#profile-photo-camera-modal" onclick="setup('FACT')" disabled="disabled"  data-toggle="popover" data-trigger="focus hover" title="ATENCIÓN:Capture una imagen" required >
										<i class="fa fa-camera"></i> Capturar imagen
									</button>
									<button type="button" id="borrar"  class="btn btn-secondary" :disabled="form.busy" ata-target="#profile-photo-camera-modal"  disabled="disabled">
										<i class="fa fa-trash"></i>
									</button>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12 col-md-12">
			<h5 align="justify">Ubicación de los hechos</h5>
		</div>
	</div>
	<div class="row">
		<input id="pac-input" class="controls" type="text" placeholder="Buscar ubicación">
		<div class="col-lg-12 col-md-12" id="mapa">
			{!!$map['html']!!}
		</div>
	</div>
	<div class="card-body">
		<div align="right">
			<a href="#0" class="cd-top">Top</a>
		</div>
	</div>
	{!!Form::text('idCarpeta',null, ['hidden','id'=>'idCarpeta'] )!!}
	{!!Form::text('numCarpeta',null, ['hidden','id'=>'numCarpeta'] )!!}
	{!!Form::text('vinVal',null, ['hidden','id'=>'vinVal'] )!!}
	{!! Form::close()!!}

	{!! Form::open(['route' => 'generar-reporteCoincidencias','id'=>'ReporPdf','target'=>'_blank']) !!}
	{!!Form::hidden('sendPlaca',null, ['id'=>'sendPlaca'] )!!}
	{!!Form::hidden('sendSerie',null, ['id'=>'sendSerie'] )!!}
	{!!Form::hidden('sendMotor',null, ['id'=>'sendMotor'] )!!}
	{!!Form::hidden('sendToken',null, ['id'=>'sendToken'] )!!}
	{!! Form::close()!!}
</div>

<!-- section of modal -->
<div id="profile-photo-camera-modal" class="modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Captura de documentos</h5>
				<button type="button" onClick="close_cam(true)" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="cuerpo">
				<div id="web_cam_section" class="alert alert-danger" v-if="form.errors.has('photo')">
					<div id="my_camera"></div>
				</div>
				<div id="profile-photo-camera-preview">
					<div id="results">Your captured image will appear here...</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" di="take_p" class="btn btn-primary" :disabled="form.busy" onClick="take_snapshot()">Capturar</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade bd-example-modal-lg" id="verificacionmodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header" align="center">
				<h5 class="modal-title" id="exampleModalLabel" align="text-center">Coincidencias</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="modal-body">
				<div class="card-header">
					Carpetas
				</div>
				<div class="card-body">
					<table id="tablecarpetas" class="table table-bordered table-hover table-responsive-sm"></table>
				</div>
				<div class="card-header">
					Registros de robo de vehículos
				</div>
				<div class="card-body">
					<table id="tableregistros" class="table table-bordered table-hover table-responsive-sm"></table>
				</div>
				<div class="card-header">
					Registros de vehículos recuperados
				</div>
				<div class="card-body">
					<table id="tablerecuperados" class="table table-bordered table-hover table-responsive-sm"></table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" id="idcerrar" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade bd-repuve-modal-lg" id="repuvemodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header" align="center">
				<h5 class="modal-title" id="exampleModalLabel" align="text-center"> Registro Público Vehicular</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
					<table class="table" id="reporte_repube" style="display:none">
						<thead>
							<tr>
							<th scope="col">Campo</th>
							<th scope="col">Información</th>
							<th scope="col">Campo</th>
							<th scope="col">Información</th>
							</tr>
						</thead>
						<tbody>
							<tr>
							<th scope="col">Agente  M.P.</th><td id="mp"></td>
							<td scope="col">Color</td><td id="col"></td>
							</tr>
							<tr>
							<th scope="col">Fecha Robo</th>	<td id="fechRob"></td>
							<td scope="col">Fecha Averiguación</td><td id="fechAve"></td>
							</tr>
							<tr>
							<th scope="col">Marca</th><td id="marc"></td>
							<td scope="col">Modelo</td><td id="mod"></td>
							</tr>

							<tr>
							<th scope="col">Num. Averiguación </th><td id="averNum"></td>
							<td scope="col">Placa</td><td id="plac"></td>
							</tr>
							<tr>
							<th scope="col">SubMarca</th><td id="subMarc"></td>
							<td scope="col">VIN</td><td id="vim"></td>
							</tr>
						</tbody>
					</table>
					<h5 class="text-center" id="sin_registros" style="display:none"></h5>

				<div class="card-body">

				</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="satmodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header" align="center">
				<h5 class="modal-title" id="exampleModalLabel" align="text-center"> SAT</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form>
					<div class="form-row align-items-center">
						<div class="row">
							<div class="col-3">
								<label>RFC emisor</label>
								<input type="text" class="form-control mb-2" id="rfc_emisor" placeholder="RFC emisor">
							</div>
							<div class="col-3">
								<label>RFC receptor</label>
								<input type="text" class="form-control mb-2" id="rfc_receptor" placeholder="RFC receptor">
							</div>
							<div class="col-3">
								<label>Importe total</label>
								<input type="text" class="form-control mb-2" id="importe" placeholder="0.00" data-validation="number" data-validation-allowing="float"  data-validation-decimal-separator=".">
							</div>
							<div class="col-3">
								<label>Folio fiscal</label>
								<input type="text" class="form-control mb-2" id="uuid" placeholder="0000">
							</div>
						</div>

					</div>
				</form>
				<br>
				<div id="imageload" align="center"  >
				</div>
				<br>
				<div class="alert " role="alert" id="sat_resultado"  >
        </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="sat_consultar" >Consultar</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>
<!-- end section of modal-->
<script src="{{ asset('js/geolocalizacion/geolocalizacionToken.js') }}"></script>
<script src="{{ asset('js/denuncia/ordenar.js') }}"></script>
<script src="{{ asset('js/denuncia/peticiones.js') }}"></script>



<style media="screen">
#profile-photo-camera-preview,
#profile-photo-camera-preview video {
	width: 100% !important;
	height: auto !important;
	min-width: 100px;
	min-height: 100px;
}
.spa_sec{
	padding: 0 1em;
}
#my_camera{
	display: block;
	margin: auto;
}
#web_cam_section{
	background-color: #717171;
	border-color: #424242;
}
#cuerpo{
	display: block;
	margin: auto;
}
#ife_mini,#fact_mini{
	padding-bottom: 1em;
}
</style>
<script type="text/javascript">



function mostrarOpcionSeleccionada() {
		//var $option = $select.find('option:selected');

		var identificador = $('#idEstado').val();
		var respuesta;
		$('#idMunicipio').html('');
		if(identificador!=''){
			var ruta = "{{route('municipio',':estado')}}";
			ruta = ruta.replace(':estado', identificador);
			datos = null;
			respuesta = peticionInfomacion('get',ruta,datos);
			if (respuesta != null) {
				for(i=0; i<respuesta.estado.length; i++){
					$("#idMunicipio").append("<option value='"+respuesta.estado[i].idMunicipio+"'> "+respuesta.estado[i].nombre+"</option>");
				}
				if(respuesta.estado.length > 0 ){
					localStorage.setItem("idMunicipio", respuesta.estado[0].idMunicipio);
				}
			}else{
				alert('Error de servidor');
			}
		}else{
		}
	}
$(document).ready(function() {
	//Carga de paht de seguridad
	var ruta = window.location.pathname.split('/');
	//$('#idSecurity').val((ruta[4])?ruta[4]:"uipj-1");
	$('#idSecurity').val((ruta[4])?ruta[4]:"dWlwai0y");

	$('#idProcedencia').change(function(){
		($('#idProcedencia').val()==2)?$('#idDocumentoCirculacion').val(5).trigger('change'):$('#idDocumentoCirculacion').val(1).trigger('change')
	});
	map = new google.maps.Map(document.getElementById('map_canvas'), {
		center: { lat: 19.976312, lng: -96.7279684 },
		zoom: 7,
		mapTypeId: 'roadmap'
	});
	initAutocomplete();
	$('#idEstado').val(30).trigger('change');
	 mostrarOpcionSeleccionada();
	$("#borrar").click(function(event) {
		$('#fact_mini').html('');
	});
	$("#borrar2").click(function(event) {
		$('#ife_mini').html('');
	});
});

var statusMsj = "";
var msj = "";
function addImg (ruta,posicion,subcat,nombre) {
	var titDiv="";
	if(posicion==0){
		titDiv="identidicacionCont";
		$('#idSubCatIdenti').val(subcat);
	}else{
		titDiv="facturaCont";
		$('#idSubCatFact').val(subcat);
	}

	getImg = "{{route('getImg',['filename'=>':ruta'])}}";
	getImg = getImg.replace(':ruta', ruta);
	var element = '<div class="col center" id="'+titDiv+'"><img src="'+getImg+'" alt="" style="width:50%"><p>'+nombre+'</p></div>';
	return element;

}


function sectionShow(){
	$('.sec_upload').toggle('slow');
}

var cap="";
Webcam.set({
	width: 320,
	height: 240,
	image_format: 'jpeg',
	jpeg_quality: 90
});
function take_snapshot(tipo) {
	Webcam.snap( function(data_uri) {
		document.getElementById('results').innerHTML ='<img class="img-thumbnail" src="'+data_uri+'"/>';
		var img = $("#results").children("img").clone();
		var tipo = (cap == 'FACT')? '#fact_mini':'#ife_mini';
		console.log('Este es el tipo -> '+tipo+' esta es la imagen ->'+img);
		$(tipo).html(img);
		var tipo2 = (cap == 'FACT')? '#mini_fac':'#mini_ife';
		$(tipo2).val(data_uri);
		setTimeout(function() {
			$('#profile-photo-camera-modal').modal('hide');
			$("#results").html('');
		}, 1000);

	});
	close_cam(false);
	$('#web_cam_section').hide('slow');
}
function setup(tipo) {
	this.cap = tipo;
	Webcam.reset();
	Webcam.attach( '#my_camera' );
	$('#web_cam_section').show('slow');
	if(tipo=="FACT"){
		$('#facturaCont').hide();
	}else{
		$('#identidicacionCont').hide();
	}
}
function close_cam(act){
	if(act){
		$("#results").html('');
		$('#facturaCont').show();
		$('#identidicacionCont').show();
	}
	Webcam.reset();
}


$(document).ready(function(){

	$('button').keypress(function(e){
		tecla = (document.all) ? e.keyCode : e.which;
		return (tecla != 13);
	});

	$('#verificacion').bind('click', function () {
		$('#verificacionmodal').modal('show');
		var placa = $('#placa').val();
		placa = (placa== '') ? 0 : placa;
		//placa = placa.toUpperCase();
		var serie = $('#numSerie').val();
		serie = (serie== '') ? 0 : serie;
		var motor = $('#motor').val();
		motor = (motor== '') ? 0 : motor;
		var token = $('#token').val();
		var tipoCons = 2;
		var ruta = "{{route('get_robados',['placa'=>':placa','serie'=>':serie','motor'=>':motor','token'=>':token'])}}";
  	ruta = ruta.replace(':placa', placa);
		ruta = ruta.replace(':serie', serie);
		ruta = ruta.replace(':motor', motor);
		ruta = ruta.replace(':token', token);
		datos = null;
		vehRobados = peticionInfomacion('get',ruta,datos);
		console.log(ruta);
		var datosRobados = [];
			for(i = 0; i < vehRobados.registros.length; i++){
					var reg = {
						id: vehRobados.registros[i].id,
						numCarpeta: vehRobados.registros[i].numCarpeta,
						placas: vehRobados.registros[i].placas,
						nrpv: vehRobados.registros[i].nrpv,
						numSerie: vehRobados.registros[i].numSerie,
						numMotor: vehRobados.registros[i].numMotor,
						nomFiscal: vehRobados.nomFiscal[i].nomFiscal
					}
					datosRobados.push(reg);
			}
		$('#tableregistros').bootstrapTable('load', datosRobados);
			//registros UIPJ

		var ruta2 = "{{route('get_involucrados',['placa'=>':placa','serie'=>':serie','motor'=>':motor','tipoCons'=>':tipoCons'])}}";
		ruta2 = ruta2.replace(':placa', placa);
		ruta2 = ruta2.replace(':serie', serie);
		ruta2 = ruta2.replace(':motor', motor);
		ruta2 = ruta2.replace(':tipoCons', tipoCons);
		datos = null;
		registrosCarpeta = peticionInfomacion('get',ruta2,datos);
		console.log(registrosCarpeta);

		$('#tablecarpetas').bootstrapTable('load', registrosCarpeta);




		var ruta4 = "{{route('get_recuperados',['placa'=>':placa','serie'=>':serie','motor'=>':motor','token'=>':token'])}}";
		ruta4 = ruta4.replace(':placa', placa);
		ruta4 = ruta4.replace(':serie', serie);
		ruta4 = ruta4.replace(':motor', motor);
		ruta4 = ruta4.replace(':token', token);
		datos = null;
		vehRecuperados = peticionInfomacion('get',ruta4,datos);
	console.log(ruta4);
		var datosRecuperados = [];
			for(i = 0; i < vehRecuperados.registros.length; i++){
					var reg = {
						id: vehRecuperados.registros[i].id,
						numCarpeta: vehRecuperados.registros[i].numCarpeta,
						placas: vehRecuperados.registros[i].placas,
						nrpv: vehRecuperados.registros[i].nrpv,
						numSerie: vehRecuperados.registros[i].numSerie,
						numMotor: vehRecuperados.registros[i].numMotor,
						nomFiscal: vehRecuperados.nomFiscal[i].nomFiscal
					}
					datosRecuperados.push(reg);
			}


		$('#tablerecuperados').bootstrapTable('load', datosRecuperados);


	});


	$('#buscar').click(function(){
		// $("#DOC5").val('').trigger('change');
		// $("#DOC1").val('').trigger('change');
		// $("#ife_mini").html('');
		// $("#fact_mini").html('');
		$('#doctos').html('');
		//$("#map_canvas").html('');
		$("#identidicacionCont").html('');
		//initAutocomplete();
		var identificador = $('#token').val();
		var respuesta;


		if(identificador!=''){
			var ruta = "{{route('getToken',':token')}}";
			ruta = ruta.replace(':token', identificador);
			datos = null;
			respuesta = peticionInfomacion('get',ruta,datos);
			console.log("Datos del token----->");
			console.log(respuesta);
			if (respuesta != null) {
				if (respuesta.codigo == 201) {
					var reg = respuesta.estado[0];
					swal({
						title: "Folio encontrado",
						text: "Se han cargado los datos del preregistro",
						icon: "success",
					})
					for (i = 0; i < respuesta.doctos.length ; i++) {
						$('#doctos').append(addImg(respuesta.doctos[i].destino,i,respuesta.doctos[i].idSubCategoriaDocumento,respuesta.doctos[i].nombre));
					}

					$('#placa').val(respuesta.estado[0].placas);
					console.log(respuesta.estado[0].placas);
					$('#idEstado').val(reg.idEstado).trigger('change');
					$('#idMunicipio').val(reg.idMunicipio).trigger('change');
					$('#idMarca').val(reg.idMarca).trigger('change');
					$('#idSubmarca').val(reg.idSubmarca).trigger('change');
					$('#modelo').val(reg.modelo);
					$('#idColor').val(reg.idColor).trigger('change');
					$('#numSerie').val(reg.numSerie);
					$('#motor').val(reg.numMotor);
					$('#nrpv').val(reg.nrpv);
					$('#idClaseVehiculo').val(reg.idClaseVehiculo).trigger('change');
					$('#idSubclase').val(reg.idTipoVehiculo).trigger('change');
					$('#idUso').val(reg.idTipoUso).trigger('change');
					$('#idProcedencia').val(reg.idProcedencia).trigger('change');
					$('#idAseguradora').val(reg.idAseguradora).trigger('change');
					$('#senasPartic').val(reg.senasPartic);
					$("#idDocumentoCirculacion").val(reg.idDocumentoCirculacion).trigger('change');
					$('#folio').val(reg.folioDoctoCirc);
					$('#folioDoctoCirc').val(reg.folioDoctoCirc);

					if(reg.latitud!="0.000000000000000" && reg.longitud!="0.000000000000000"){
						var myLatlng = new google.maps.LatLng(reg.latitud,reg.longitud);
						marker.position = myLatlng;
						marker.setMap(map);
					}

				} else {
					$('#placa').val('');
					$('#idEstado').val('').trigger('change');
					$('#idMarca').val('').trigger('change');
					$('#idSubmarca').val('').trigger('change');
					$('#modelo').val('');
					$('#idColor').val('').trigger('change');
					$('#numSerie').val('');
					$('#motor').val('');
					$('#nrpv').val('');
					$('#idClaseVehiculo').val('').trigger('change');
					$('#idSubclase').val('').trigger('change');
					$('#idUso').val('').trigger('change');
					$('#idProcedencia').val('').trigger('change');
					$('#idAseguradora').val('').trigger('change');
					$('#senasPartic').val('');
					$('#latMap').val('');
					$('#lonMap').val('');
					initAutocomplete();

					swal({
						title: respuesta.status,
						text: "",
						icon: "warning",
						buttons: true,
						dangerMode: true,
					})
				}
			}else{
				alert('Error de servidor');
			}
		}else{
			swal({
				title: "Debe introducir número de folio",
				text: "",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
		}
	});

	$('.btn_ide').on('change', function(){
		$('#identidicacionCont').hide();
	});

	$('.btn-fac').on('change', function(){
		$('#facturaCont').hide();
	});

	$('.btn_check').click(function(event){
		if($(this).val()==1){
			$('#placa').removeAttr('readonly');
			$('#permiso').attr('readonly',true);
		}else{
			$('#permiso').removeAttr('readonly');
			$('#placa').attr('readonly',true);
		}
	});

	$('#listaiden_op1').change(function(){
		var valor = $('#listaiden_op1').val();
		if(valor!=''){
			var tipoC = $('#tipe_load').val();
			if (tipoC==1){
				$('#DOC1').css('display','');
				$('#cam_ide').attr('disabled',true);
				$('#borrar2').attr('disabled',true);
			}else{
				$('#cam_ide').removeAttr('disabled');
				$('#DOC1').css('display','none');
				$('#borrar2').removeAttr('disabled');
			}
		}
	});

	$('#listaiden_op2').change(function(){
		var valor = $('#listaiden_op2').val();
		if(valor!=''){
			var tipoC = $('#tipe_load').val();
			if (tipoC==1){
				$('#DOC5').css('display','');
				$('#cam_fac').attr('disabled',true);
				$('#borrar').attr('disabled',true);
			}else{
				$('#cam_fac').removeAttr('disabled');
				$('#DOC5').css('display','none');
				$('#borrar').removeAttr('disabled');
			}
		}
	});

	$("#idMarca").change(function() {
		var identificador = $(this).val();
		var respuesta;
		$('#idSubmarca').html('');
		if(identificador!=''){
			var ruta = "{{route('submarca',':marca')}}";
			ruta = ruta.replace(':marca', identificador);
			datos = null;
			respuesta = peticionInfomacion('get',ruta,datos);
			if (respuesta != null) {
				for(i=0; i<respuesta.estado.length; i++){
					$("#idSubmarca").append("<option value='"+respuesta.estado[i].id+"'> "+respuesta.estado[i].nombre+"</option>");
				}
				if(respuesta.estado.length > 0 ) {
					localStorage.setItem("idSubmarca", respuesta.estado[0].id);
				}
			}else{
				alert('Error de servidor');
			}
		}else{
		}
	});

	$("#idEstado").change(function() {
		var identificador = $(this).val();
		var respuesta;
		$('#idMunicipio').html('');
		if(identificador!=''){
			var ruta = "{{route('municipio',':estado')}}";
			ruta = ruta.replace(':estado', identificador);
			datos = null;
			respuesta = peticionInfomacion('get',ruta,datos);
			if (respuesta != null) {
				for(i=0; i<respuesta.estado.length; i++){
					$("#idMunicipio").append("<option value='"+respuesta.estado[i].idMunicipio+"'> "+respuesta.estado[i].nombre+"</option>");
				}
				if(respuesta.estado.length > 0 ){
					localStorage.setItem("idMunicipio", respuesta.estado[0].idMunicipio);
				}
			}else{
				alert('Error de servidor');
			}
		}else{
		}
	});

	$("#idMunicipio").change(function() {
		localStorage.setItem("idMunicipio", $('#idMunicipio').val());
	});


	$("#idClaseVehiculo").change(function() {
		var identificador = $(this).val();
		var respuesta;
		$('#idSubclase').html('');
		if(identificador!=''){
				//console.log(identificador);
				var ruta = "{{route('subclase',':clasevehiculo')}}";
				ruta = ruta.replace(':clasevehiculo', identificador);
				datos = null;
				respuesta = peticionInfomacion('get',ruta,datos);
				if (respuesta != null) {
					for(i=0; i<respuesta.estado.length; i++){
						$("#idSubclase").append("<option value='"+respuesta.estado[i].id+"'> "+respuesta.estado[i].nombre+"</option>");
					}
					if (identificador == 99) {
						$('#idSubclase').val(999).trigger('change');
					}
					if (respuesta.estado.length > 0) {
						localStorage.setItem("idSubclase", respuesta.estado[0].id);
					}
				}else{
					alert('Error de servidor');
				}
			}else{
			}
		});

	$("#idSubmarca").change(function() {
		localStorage.setItem("idSubmarca", $('#idSubmarca').val());
	});

	$("#idSubclase").change(function() {
		localStorage.setItem("idSubclase", $('#idSubclase').val());
	});

	$('#formDenuncia').submit(function (e) {
		e.preventDefault();

		var valor = $("#opcion2:checked").val();
		if(valor =='camara'){
		    if($('#mini_ife').val()=='' && (valor == "camara")){
		    	toastr.warning("Debe de capturar la identificación oficial y la factura con la camara",{"preventDuplicates":true
		    });
		    	$('.popover-dismiss').popover({
		    		trigger: 'hover'
		    	})

		    	return false;
		    }

		    if($('#mini_fac').val()=='' && (valor == "camara")){
		    	toastr.warning("Debe de capturar la identificación oficial y la factura con la camara",{"preventDuplicates":true
		    });

		    	$('.popover-dismiss').popover({
		    		trigger: 'hover'
		    	})


		    	return false;
		    }
		}


		$('#sendPlaca').val($('#placa').val());
		$('#sendSerie').val($('#numSerie').val());
		$('#sendMotor').val($('#motor').val());
		$('#sendToken').val($('#token').val());
		var ruta ="{{route('getMockup','1')}}";
		var carpeta = peticionInfomacion('get',ruta);
		$('#idCarpeta').val(carpeta.investigacion.id);
		$('#numCarpeta').val(carpeta.investigacion.numCarpeta);
		var clase = ""
		var formData = new FormData(this);
		var placa = ($('#placa').val() == '') ? '0' : $('#placa').val();
		placa.toUpperCase();
		var serie = $('#numSerie').val();
		serie = serie.toUpperCase();
		var motor = $('#motor').val();
		motor = motor.toUpperCase();
		//var nrpv = $('#nrpv').val();
		// nrpv = nrpv.toUpperCase()
		var token = $('#token').val();
		var ruta = "{{route('get_countRobados',['placa' => ':placa', 'serie' => ':serie', 'motor' => ':motor','token' => ':token'])}}";
		ruta = ruta.replace(':placa', placa);
		ruta = ruta.replace(':serie', serie);
		ruta = ruta.replace(':motor', motor);
		//ruta = ruta.replace(':nrpv', nrpv);
		ruta = ruta.replace(':token', token);
		datos = null;
		robados = peticionInfomacion('get',ruta,datos);


		var ruta = "{{route('get_countInvolucrados',['placa' => ':placa', 'serie' => ':serie', 'motor' => ':motor'])}}";
		ruta = ruta.replace(':placa', placa);
		ruta = ruta.replace(':serie', serie);
		ruta = ruta.replace(':motor', motor);
		// ruta = ruta.replace(':nrpv', nrpv);
		datos = null;
		respuestaUipj = peticionInfomacion('get',ruta,datos);

		if (respuestaUipj != null) {
		}
		var ruta3 = "{{route('get_countRecuperados',['placa' => ':placa', 'serie' => ':serie', 'motor' => ':motor','nrpv'=>':nrpv','token' => ':token'])}}";
		ruta3 = ruta3.replace(':placa', placa);
		ruta3 = ruta3.replace(':serie', serie);
		ruta3 = ruta3.replace(':motor', motor);
		ruta3 = ruta3.replace(':nrpv', nrpv);
		ruta3 = ruta3.replace(':token', token);
		datos = null;
		recuperados = peticionInfomacion('get',ruta3,datos);
		var coicidenciasPdf = '{{ route('generar-reporteCoincidencias') }}';
		var span = document.createElement('span');

		if(robados.estado.placas >0 || respuestaUipj.estado.placas > 0 || respuestaUipj.estado.numMotor > 0 || robados.estado.numMotor >0 || respuestaUipj.estado.numSerie > 0 || robados.estado.numSerie > 0 ){

			span.innerHTML=
			'<p> Coincidencias con vehículos involucrados en alguna carpeta de investigación y/o con reporte de robo</p>'
			+'<table class="table table-bordered" value="detalle"> <thead class="thead-light">'
			+'<tr>'
			+'<th scope="col"></th>'
			+'<th scope="col">Carpetas</th>'
			+'<th scope="col">Vehículos Robados</th>'
			+'<th scope="col">Vehículos Recuperados</th>'
			+'<tr>'
			+'</thead>'
			+'<tbody>'
			+'<tr>'
			+'<th scope="row">Placa</th>'+'<th scope="row">'+respuestaUipj.estado.placas+'</th>'+'<th scope="row">'+robados.estado.placas+'</th>'+'<th scope="row">'+recuperados.estado.placas+'</th>'+'</tr>'
			+'<tr>'
			+'<th scope="row">Numero de motor</th>'+'<th scope="row">'+respuestaUipj.estado.numMotor+'</th>'+'<th scope="row">'+robados.estado.numMotor+'</th>'+'<th scope="row">'+recuperados.estado.numMotor+'</th>'+'</tr>'
			+'<tr>'
			+'<th scope="row">Numero de serie</th>'+'<th scope="row">'+respuestaUipj.estado.numSerie+'</th>'+'<th scope="row">'+robados.estado.numSerie+'</th>'+'<th scope="row">'+recuperados.estado.numSerie+'</th>'+'</tr>'
			+'<tr>'
			+'<th colspan="4">'+'<button class="btn btn-secondary btn-lg btn-block" id="generarPdf" onclick="enviarPDF()">'+'Ver detalle'+'</button>'+'</th>'+'</tr>'
			+'</tbody>'
			+'</table>'

			+'<div class="'+clase+'" role="alert">'+msj+'</div>'
		} else {
			span.innerHTML=
			'<p> No hay coincidencias con vehículos involucrados en alguna carpeta de investigación y/o con reporte de robo</p>'
		}


		swal({
			title: "¿Está seguro de que desea continuar?",
			text: "",
			icon: "warning",
			buttons: true,
			dangerMode: true,
			content: span,
			customClass: 'swal-wide'
		})
		.then((willDelete) => {
			console.log('datos del formulario');
			console.log(formData);

			if (willDelete) {
				$.ajax({
					url : "{{route('saveDenuncia.vehiculo')}}",
					data : formData,
					type : 'POST',
					cache: false,
					contentType : false,
					processData : false,

					success : function(json) {
						console.log('Se agrego un vehiculo');
						console.log(json);
						if(json.status==419){
							swal({
								title: "Registro duplicado",
								text: "Los datos del vehiculo estan duplicados",
								icon: "warning",

							});
						}else{
							$('#table').bootstrapTable('refresh');

							swal({
								title: "Registro creado exitosamente",
								text: "Su registro se realizó correctamente",
								icon: "success",

							});

							var rutaDeligencia ="{{route('formato-diligencia')}}";
							var diligencia = peticionInfomacion('post',rutaDeligencia, carpeta);
							//-----------------------
							// limpiar formulario
							

							$("#idEstado").val('').trigger('change');
							$("#idSubmarca").val('').trigger('change');
							$("#idColor").val('').trigger('change');
							$("#idAseguradora").val('').trigger('change');
							$("#idMarca").val('').trigger('change');
							$("#permiso").val('').trigger('change');
							$("#numSerie").val('').trigger('change');
							$("#motor").val('').trigger('change');
							$("#nrpv").val('').trigger('change');
							$("#senasPartic").val('').trigger('change');
							$("#placa").val('').trigger('change');
							$("#token").val('').trigger('change');
							$("#idClaseVehiculo").val('').trigger('change');
							$("#idSubclase").val('').trigger('change');
							$("#idUso").val('').trigger('change');
							$("#idProcedencia").val('').trigger('change');
							$("#listaiden_op1").val('').trigger('change');
							$("#listaiden_op2").val('').trigger('change');
							$("#DOC5").val('').trigger('change');
							$("#DOC1").val('').trigger('change');
							$("#ife_mini").html('');
							$("#fact_mini").html('');
							//$("#map_canvas").html('');
							$("#identidicacionCont").html('');
							$("#formDenuncia")[0].reset();
							$("#latMap").val('');
							$("#lonMap").val('');

							var fecha = new Date();
							$("#modelo").val(fecha.getFullYear());
							

							//------------------------------------

							// map = new google.maps.Map(document.getElementById('map_canvas'), {
							// 	zoom: 8,
							// 	center: new google.maps.LatLng(19.976312,-96.7279684),
							// 	mapTypeId: google.maps.MapTypeId.ROADMAP
							// });

							// var marker = new google.maps.Marker({
							// 	position: new google.maps.LatLng(19.976312,-96.7279684),
							// 	draggable: true,
							// 	map: map
							// });

							// $('#latMap').val(marker.getPosition().lat());
							// $('#lonMap').val(marker.getPosition().lng());


							// marker.addListener('dragend', function(){
							// 	$('#latMap').val(marker.getPosition().lat());
							// 	$('#lonMap').val(marker.getPosition().lng());
							// });

							var myLatlng = new google.maps.LatLng(19.976312,-96.7279684);
								marker.position = myLatlng;
								marker.setMap(map);
						}
					},

					error : function(xhr, status) {
						console.log('Disculpe, existió un problema');
						console.log(xhr);
						swal({
							title: "Error al introducir los datos",
							text: 'msj',
							icon: "error",
						});
					},
					complete : function(xhr, status) {
						console.log('Petición realizada');
					}
				});
			} else {
				swal("Su registro ha sido cancelado");
			}
		});
	});


});

$('#formDenuncia').on('keyup keypress', function(e) {
  var keyCode = e.keyCode || e.which;
  if (keyCode === 13) {
    e.preventDefault();
    return false;
  }
});



function enviarPDF(){
	$('#ReporPdf').submit();
}

$('#numSerie').focusout(function(){
	var numSerie = $('#numSerie').val();

	if(Number.parseInt($('#modelo').val())>=1980){
		if(numSerie!=""){
			var ruta ="{{route('niv',['num_serie'])}}";
			ruta = ruta.replace("num_serie",numSerie);
			var respuesta = peticionInfomacion('get',ruta);
			console.log(respuesta);
			(respuesta.request.status=='error')? toastr.error(respuesta.request.mensaje, "ERROR",{"preventDuplicates": true}):toastr.success(respuesta.request.mensaje, "CORRECTO",{"preventDuplicates": true});
			(respuesta.request.status=='error')? $('#vinVal').val(0):$('#vinVal').val(1);

		}
	}else{
		console.log('no verifica nada');
	}
});


$(document).ready(function() {

	$("#placa").blur(function() {

		var prohibido =['SIN', 'SIN PLA', 'S.D.' ,'NULL','sin','sin pla','s.d','null' ];
		var cadenaUser = $("#placa").val();
		var idx = prohibido.indexOf(cadenaUser);
		if(idx > -1){
			$("#placa").val("0000000");
		}
	});

	$("#numSerie").blur(function() {

		var prohibido =['SE IGNORA', 'NULL','null','se ignora' ];
		var cadenaUser = $("#numSerie").val();
		var idx = prohibido.indexOf(cadenaUser);
		if(idx > -1){
			$("#numSerie").val("");
		}
	});


		$("#modelo").blur(function() {
			var prohibido =['S/D', 'SE I','SIN','s/d','se i','sin' ];
			var cadenaUser = $("#modelo").val();
			var idx = prohibido.indexOf(cadenaUser);
			if(idx > -1){
				$("#modelo").val("");
			}
		});
		verificar_camara('divBtnCamara');

		console.log( "ready!" );
		var idmarca = $("#idMarca").val();

		if (idmarca > 0) {
			var ruta = "{{route('submarca',':marca')}}";
			ruta = ruta.replace(':marca', idmarca);
			datos = null;
			respuesta = peticionInfomacion('get',ruta,datos);

			if (respuesta != null) {
				for(i=0; i<respuesta.estado.length; i++){
					$("#idSubmarca").append("<option value='"+respuesta.estado[i].id+"'> "+respuesta.estado[i].nombre+"</option>");
				}
				$('#idSubmarca').val(localStorage.idSubmarca).trigger("change");
			}
		}

		var idClaseVehiculo = $("#idClaseVehiculo").val();
		if (idClaseVehiculo > 0) {
			var ruta = "{{route('subclase',':clasevehiculo')}}";
			ruta = ruta.replace(':clasevehiculo', idClaseVehiculo);
			datos = null;
			respuesta = peticionInfomacion('get',ruta,datos);
			if (respuesta != null) {
				for(i=0; i<respuesta.estado.length; i++){
					$("#idSubclase").append("<option value='"+respuesta.estado[i].id+"'> "+respuesta.estado[i].nombre+"</option>");
				}
			}
			$('#idSubclase').val(localStorage.idSubclase).trigger("change");
		}
	});


	$('#tablecarpetas').bootstrapTable({
		search:  true,
		showColumns: true,
		showRefresh: true,
		buttonsAlign :'left',
		searchAlign :'left',
		idField: 'id',
		iconsPrefix: 'fa',
		pagination : true,
		pageSize: 5,
		columns: [{
			field: 'id',
			title: 'ID',
			sortable: true,
			visible: false
		}, {
			field: 'numCarpeta',
			title: 'No. Carpeta',
			sortable: true
		},{
			field: 'placas',
			title: 'Placas',
			sortable: true
		}, {
			field: 'nrpv',
			title: 'NRPV',
			sortable: true
		}, {
			field: 'numSerie',
			title: 'No. serie',
			sortable: true
		}, {
			field: 'numMotor',
			title: 'No. Motor',
			sortable: true
		},{
			field: 'nomFiscal',
			title: 'Nombre Fiscal',
			sortable: true
		}],
	});

	$('#tableregistros').bootstrapTable({
		search:  true,
		showColumns: true,
		showRefresh: true,
		buttonsAlign :'left',
		searchAlign :'left',
		idField: 'id',
		iconsPrefix: 'fa',
		pagination : true,
		pageSize: 5,
		columns: [{
			field: 'id',
			title: 'ID',
			sortable: true,
			visible: false
		}, {
			field: 'numCarpeta',
			title: 'No. Carpeta',
			sortable: true
		}, {
			field: 'placas',
			title: 'Placas',
			sortable: true
		}, {
			field: 'nrpv',
			title: 'NRPV',
			sortable: true
		}, {
			field: 'numSerie',
			title: 'No. serie',
			sortable: true
		}, {
			field: 'numMotor',
			title: 'No. Motor',
			sortable: true
		},{
			field: 'nomFiscal',
			title: 'Nombre Fiscal',
			sortable: true
		}],
	});

	$('#tablerecuperados').bootstrapTable({
		search:  true,
		showColumns: true,
		showRefresh: true,
		buttonsAlign :'left',
		searchAlign :'left',
		idField: 'id',
		iconsPrefix: 'fa',
		pagination : true,
		pageSize: 5,
		columns: [{
			field: 'id',
			title: 'ID',
			sortable: true,
			visible: false
		}, {
			field: 'numCarpeta',
			title: 'No. Carpeta',
			sortable: true
		}, {
			field: 'placas',
			title: 'Placas',
			sortable: true
		}, {
			field: 'nrpv',
			title: 'NRPV',
			sortable: true
		}, {
			field: 'numSerie',
			title: 'No. serie',
			sortable: true
		}, {
			field: 'numMotor',
			title: 'No. Motor',
			sortable: true
		},{
			field: 'nomFiscal',
			title: 'Nombre Fiscal',
			sortable: true
		}],
	});

	$('#placa, #numSerie').bind('keyup', function(){
		if($('#placa').val() != '' || $('#numSerie').val() != ''){
			$('#repuve').attr("disabled", false);
		} else {
			$('#repuve').attr("disabled", true);
		}
	});

	$('#repuve').bind('click', function () {
		$('#repuvemodal').modal('show');
		var placa = ($('#placa').val() == '') ? 0 : $('#placa').val().toUpperCase();
		var serie = ($('#numSerie').val() == '') ? 0 : $('#numSerie').val().toUpperCase();

		var ruta = "{{route('consult-soap-service',['placa'=>':placa','serie'=>':serie'])}}";
		ruta = ruta.replace(':placa', placa);
		ruta = ruta.replace(':serie', serie);

		//console.log(ruta
		vehRobados = peticionInfomacion('get',ruta,null);
		//console.log(vehRobados);
		if(vehRobados['IdEstatus']=="OK:1"){
			$('#reporte_repube').show();
			$('#sin_registros').hide();
			//(vehRobados['IdEstatus']=='"OK:1"')? $('#sin_registros').hide():$('#sin_registros').show();
			$('#mp').html(vehRobados['Agente de Ministerio Público']);
			$('#col').html(vehRobados['Color']);
			$('#fechRob').html(vehRobados['Fecha Robo']);
			$('#fechAve').html(vehRobados['Fecha de Averiguación']);
			$('#marc').html(vehRobados['Marca']);
			$('#mod').html(vehRobados['Modelo']);
			$('#averNum').html(vehRobados['Número de Averiguación']);
			$('#plac').html(vehRobados['Placa']);
			$('#subMarc').html(vehRobados['SubMarca']);
			$('#vim').html(vehRobados['VIN']);
		}else{
			 $('#reporte_repube').hide();
			 $('#sin_registros').show();
			 $('#sin_registros').text(vehRobados['msj']);
			//(vehRobados['IdEstatus']=='"OK:1"')? $('#reporte_repube').show():$('#sin_registros').hide();

		}
	});

	$('#sat').bind('click', function () {
		$('#satmodal').modal('show');
		$('#sat_resultado').html('');
	});

	$('#sat_consultar').bind('click', function () {
		$('#satmodal').modal('show');
		$("#imageload").html("<img src={{url('/img/espera.gif')}} width=380 height=240>");
		var rfc_emisor = ($('#rfc_emisor').val() == '') ? 0 : $('#rfc_emisor').val().toUpperCase();
		var rfc_receptor = ($('#rfc_receptor').val() == '') ? 0 : $('#rfc_receptor').val().toUpperCase();
		var importe = ($('#importe').val() == '') ? 0 : $('#importe').val().toUpperCase();
		var uuid = ($('#uuid').val() == '') ? 0 : $('#uuid').val().toUpperCase();

		var ruta = "{{route('consult-soap-sat',['rfc_emisor'=>':rfc_emisor','rfc_receptor'=>':rfc_receptor', 'importe'=>':importe', 'uuid'=>':uuid'])}}";
		ruta = ruta.replace(':rfc_emisor',rfc_emisor);
		ruta = ruta.replace(':rfc_receptor', rfc_receptor);
		ruta = ruta.replace(':importe', importe);
		ruta = ruta.replace(':uuid', uuid);

		console.log(ruta);

		var factura_sat;
		setTimeout(function(){
			factura_sat = peticionInfomacion('get',ruta,null);
			$("#imageload").html('');

			var texto = $('<h3 class="center">').text(factura_sat.codigo);
			$('#sat_resultado').html('');
			$('#sat_resultado').append('<h5 class="center"> Estado: '+factura_sat.estado+'</h5>');
			$('#sat_resultado').append('<p class="center">'+factura_sat.codigo+'</p>');

			if (factura_sat.estado=='Vigente'){
				$('#sat_resultado').css('background-color','#D0E8D0');
				$('#sat_resultado').css('color','#148C10');
			}
			else if (factura_sat.estado=='No Encontrado'){
				$('#sat_resultado').css('background-color','#E9D6E7');
				$('#sat_resultado').css('color','#A13D57');
			}

		}, 3000);

		$('#sat_resultado').html('');
		$('#sat_resultado').css('background-color','white');
		$('#sat_resultado').css('color','white');


	});
</script>
