  @section('title')
  Vehículos Recuperados
  @endsection

  <div class="container-fluid">
    <br>
    {!! Form::open(['method' => 'POST','files' => true, 'id' => 'formRecuperado'])  !!}
    <div class="card">
      <div class="card-header">
        <div class="row">
          <div class="col-lg-6 col-md-6" >
            <h5 align-text="left" id="titulo">B&uacute;squeda de vehículos recuperados</h5>
          </div>
          <div class="col-lg-6 col-md-6" align="right">
            <button type="button" class="btn btn-secondary " name="verificacion" id="verificacion" >Verificaci&oacute;n</button>
            <button type="submit" class="btn btn-primary" id="btnEmpty">Guardar</button>
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <ul class="nav nav-pills mb-3 " id="pills-tab" role="tablist">
            <li class="nav-item2">
              <a class="nav-link active btn-outline-dark " id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true"> Registro de vehículo recuperado </a>
            </li>
            <li class="nav-item2">
              <a class="nav-link btn-outline-dark" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Vehículos recuperados</a>
            </li>
          </ul>
        </div>
        <div class="row">
          <div class="col-md-4 col-lg-4">
            <div class="form">
              {!!Form::label('permiso', 'Documento de circulación')!!}
            </div>
            <div class="input-group">
              <select class="templatingSelect10 form-control form-control-sm"  id="idDocumentoCirculacion" name="idDocumentoCirculacion" >
                <option selected="selected" value=""> </option>
                @foreach($documentosCirculacion as $documento)
                <option value="{{$documento->id}}" @if ($documento->id == 5) selected @endif >{{$documento->nombre}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <!--

          <div class="col-md-8 col-lg-8">
            <div class="form">
              {!!Form::label('permiso', 'Consulta ')!!}
            </div>
            <div class="form-check form-check-inline col-md-2 col-lg-2 ">
              <input type="checkbox" class="form-check-input btn-verificar" id="consulta1">
              <label class="form-check-label" for="consulta1">Robados</label>
            </div>
            <div class="form-check form-check-inline col-md-2 col-lg-2 ">
              <input type="checkbox"  class="form-check-input btn-verificar" id="consulta2" value="1">
              <label class="form-check-label" for="consulta1">Involucrados</label>
            </div>
            <div class="form-check form-check-inline  col-md-2 col-lg-2">
              <input type="checkbox"  class="form-check-input btn-verificar" id="consulta1" >
              <label class="form-check-label" for="consulta1">Todos</label>
            </div>
          </div> -->
          <div class="modal fade bd-example-modal-lg" id="verificacionmodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header" align="center">
                  <h5 class="modal-title" id="exampleModalLabel" align="text-center">Coincidencias</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="row">
                  <div class="modal-body" style="width: 60%;">
                    <div class="card-header">
                      Vehículos
                    </div>
                    <div class="card-body">
                      <table id="tablevehiculos" style="text-transform: uppercase;" class="table table-bordered table-hover table-responsive-sm" ></table>
                    </div>
                  </div>
                  <div class="modal-body" style="width: 40%;">
                    <div class="card-header">
                      Datos complementarios
                    </div>
                    <div class="card-body" id='datosComplementarios'>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <p></p>
        <div class="row">
          <div class="col-md-4 col-lg-4 ">
            <div id="divPlaca">
              <div class="form">
                {!!Form::label('placa', 'Placa')!!}
              </div>
              <div class="input-group">
                {!!Form::text('placa',null,['class'=>'form-control form-control-sm', 'null' ,'id' => 'placa', 'placeholder' => 'HMZ2398 - Ejemplo' , 'maxlength'=>'7',  'style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'alphanumeric'])!!}
              </div>
            </div>
            <div id="divFolio" style = 'display: none'>
              <div class="form">
                {!!Form::label('folioDoctoCirc', 'Folio')!!}
              </div>
              <div class="input-group">
                {!!Form::text('folioDoctoCirc',null,['class'=>'form-control form-control-sm', 'id' => 'folioDoctoCirc', 'maxlength'=>'10', 'onkeyup'=>'mayus(this);'])!!}
              </div>
            </div>
          </div>
          <div class="col-md-4 col-lg-4">
            <div class="form-group">
              {!!Form::label('numSerie','Número de serie')!!}
              {!!Form::text('numSerie',null, ['id'=>'serie' ,'class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el número de serie', 'required','maxlength'=>'17','style'=>'text-transform: uppercase;'] )!!}
            </div>
          </div>
          <div class="col-md-4 col-lg-4">
            <div class="form-group">
              {!!Form::label('motor','Número de motor')!!}
              {!!Form::text('motor',null, ['id'=>'idmotor','class' => 'form-control form-control-sm','maxlength'=>'10',  'placeholder' => 'Ingrese el número de motor','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'alphanumeric'] )!!}
            </div>
          </div>
        </div>
  </div>
        <div class="card-header">
          <h5 align="left">Datos generales del vehículo robado</h5>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-lg-8 col-md-8">
              <div class="form-group">
                {!!Form::label('idEstado', 'Entidad federativa de placa')!!}

                <select  class="templatingSelect2 form-control form-control-sm"  id="idEstado" name="idEstado" required >
                  <option value="" selected="selected" > </option>
                  @foreach($estados as $estado)
                  <option  value="{{$estado->id}}" >{{$estado->nombre}}</option>
                  @endforeach
                </select>
              </div>
            </div>
           {{--  <div class="col-lg-4 col-md-4">
              <div class="form-group">
                {!!Form::label('idMunicipio', 'Municipio')!!}
                <input type="text" id="text_municipio" name="text_municipio" hidden>
                <select class="templatingSelect11 form-control form-control-sm" required id="idMunicipio" name="idMunicipio" disabled>
                  <option value="" selected="selected"></option>
                </select>
              </div>
            </div> --}}
            <div class="col-lg-4 col-md-4">
              <div class="form-group">
                {!!Form::label('idMarca', 'Marca')!!}
                <select  class="templatingSelect3 form-control form-control-sm"  id="idMarca" name="idMarca",  required onchange="if(this.value!=0) {document.getElementById('idSubmarca').disabled = false}">
                  <option value="" selected="selected" > </option>
                  @foreach($marcas as $marca)
                  <option  value="{{$marca->id}}">{{$marca->nombre}}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-4 col-md-4">
              <div class="form-group">
                {!!Form::label('idSubmarca', 'Submarca')!!}
                <select  class="idSubmarca form-control form-control-sm"  id="idSubmarca" name="idSubmarca" required disabled>
                  <option value="" required selected="selected"></option>
                </select>
              </div>
            </div>
            <div class="col-lg-4 col-md-4">
              <div class="form-group">
                {!!Form::label('modelo', 'Modelo')!!}
                <input class="form-control form-control-sm" value="{{date('Y')}}" min="1885" max="{{date('Y')+1}}" name="modelo" type="number" required id="modelo">
              </div>
            </div>
            <div class="col-lg-4 col-md-4">
              <div class="form-group">
                {!!Form::label('idColor','Color')!!}
                <select  class="templatingSelect5 form-control form-control-sm"  id="idColor" name="idColor" required>
                  <option selected="selected" value=""> </option>
                  @foreach($colores as $color)
                  <option value="{{$color->id}}">{{$color->nombre}}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-4 col-md-4">
              <div class="form-group">
                {!!Form::label('nrpv','NRPV')!!}
                {!!Form::text('nrpv',null, ['id'=>'idnrpv','class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el NRPV', 'style'=>'text-transform: uppercase;', 'maxlength'=>'20'] )!!}
              </div>
            </div>
            <div class="col-lg-4 col-md-4">
              <div class="form-group">
                {!!Form::label('idClaseVehiculo','Tipo de vehículo')!!}
                <select  class="templatingSelect7 form-control form-control-sm"  id="idClaseVehiculo" name="idClaseVehiculo" required onchange="if(this.value!=0) {document.getElementById('idSubclase').disabled = false}">
                  <option selected="selected" value=""> </option>
                  @foreach($clasevehiculos as $clasevehiculo)
                  <option value="{{$clasevehiculo->id}}">{{$clasevehiculo->nombre}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-lg-4 col-md-4">
              <div class="form-group">
                {!!Form::label('idSubclase','Subtipo de vehículo')!!}
                <select  class="templatingSelect1 form-control form-control-sm"  id="idSubclase" name="idSubclase" required disabled>
                  <option selected="selected" value=""> </option>
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-4 col-md-4">
              <div class="form-group">
                {!!Form::label('idUso','Tipo de uso')!!}
                <select  class="templatingSelect8 form-control form-control-sm"  id="idUso" name="idUso" required>
                  <option selected="selected" value=""> </option>
                  @foreach($tipousos as $tipouso)
                  <option value="{{$tipouso->id}}">{{$tipouso->nombre}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-lg-4 col-md-4">
              <div class="form-group">
                {!!Form::label('idProcedencia','Procedencia')!!}
                <select class="templatingSelect9 form-control form-control-sm" id="idProcedencia" name="idProcedencia" required>
                  <option value="" selected>Seleccione</option>
                  <option value="2" >EXTRANJERO</option>
                  <option value="1">NACIONAL</option>
                </select>
              </div>
            </div>
            <div class="col-lg-4 col-md-4">
              <div class="form-group">
                {!!Form::label('idAseguradora','Aseguradora')!!}
                <select class="templatingSelect6 form-control form-control-sm" required id="idAseguradora" name="idAseguradora" >
                  <option selected="selected" value=""> </option>@foreach($aseguradoras as $aseguradora)
                  <option value="{{$aseguradora->id}}">{{$aseguradora->nombre}}</option>@endforeach
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            </div>
            <div class="col-lg-12 col-md-12 ">
              <div class="form-group">
                {!!Form::label('senasPartic','Señas particulares')!!}
                <textarea class="form-control form-control-sm" style="text-transform: uppercase;" placeholder="Ingrese las señas particulares" rows="3" name="senasPartic" cols="50" id="senasPartic"></textarea>
              </div>
            </div>
            {!!Form::text('idSubCatIdenti',null, ['hidden','id'=>'idSubCatIdenti'] )!!}
            {!!Form::text('idSubCatFact',null, ['hidden','id'=>'idSubCatFact'] )!!}
          </div>

          <div class="card-header">
          <h5 align="left">Datos generales del vehículo recuperado</h5>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-lg-4 col-md-4">
              <div class="form-group">
                {!!Form::label('idEstado2', 'Entidad federativa')!!}
                <select  class="estado form-control form-control-sm"  id="idEstado2" name="idEstado2" required onchange="if(this.value!=0) {document.getElementById('idMunicipio2').disabled = false}">
                  <option value="" selected="selected" > </option>
                  @foreach($estados as $estado)
                  <option  value="{{$estado->id}}" >{{$estado->nombre}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-lg-4 col-md-4">
              <div class="form-group">
                {!!Form::label('idMunicipio2', 'Municipio')!!}
                <input type="text" id="text_municipio" name="text_municipio2" hidden>
                <select class="municipio form-control form-control-sm" required id="idMunicipio2" name="idMunicipio2" disabled>
                  <option value="" selected="selected"></option>
                </select>
              </div>
            </div>
            <div class="col-lg-4 col-md-4">
            <div class="form-group">
              {!!Form::label('idCP','Código Postal')!!}
              {!!Form::text('cp',null, ['id'=>'cp','class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el código postal', 'required','minlength'=>'1','minlength'=>'5','maxlength'=>'5','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'number'] )!!}
            </div>
          </div>
          </div>
          <div class="row">
            <div class="col-lg-4 col-md-4">
            <div class="form-group">
              {!!Form::label('idCalle','Calle')!!}
              {!!Form::text('calle',null, ['id'=>'calle','class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese la calle', 'required','style'=>'text-transform: uppercase;'] )!!}
            </div>
          </div>
          <div class="col-lg-4 col-md-4">
            <div class="form-group">
              {!!Form::label('idColonia','Colonia')!!}
              {!!Form::text('colonia',null, ['id'=>'colonia','class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese la colonia', 'required','style'=>'text-transform: uppercase;'] )!!}
            </div>
          </div>
           <div class="col-lg-2 col-md-2">
            <div class="form-group">
              {!!Form::label('numExt','Número Externo')!!}
              {!!Form::text('numExt',null, ['id'=>'numExt','class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el número externo', 'required','minlength'=>'1','maxlength'=>'10','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'alphanumeric'] )!!}
            </div>
          </div>
          <div class="col-lg-2 col-md-2">
            <div class="form-group">
              {!!Form::label('numInt','Número Interno')!!}
               {!!Form::text('numInt',null, ['id'=>'numInt' ,'class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el número interno', 'maxlength'=>'10','style'=>'text-transform: uppercase;'] )!!}
            </div>
          </div>
            {!!Form::text('idSubCatIdenti',null, ['hidden','id'=>'idSubCatIdenti'] )!!}
            {!!Form::text('idSubCatFact',null, ['hidden','id'=>'idSubCatFact'] )!!}
          </div>
          <div class="row">
            <div class="col-lg-4 col-md-4">
            <div class="form-group">
              {!!Form::label('idFechaEntrega','Fecha de recuperación')!!}
              <input class="form-control form-control-sm" type="date" value="hoy" id="idFechaEntrega" name="idFechaEntrega" required>
            </div>
          </div>
           <div class="col-lg-4 col-md-4">
            <div class="form-group">
              {!!Form::label('idHoraEntrega','Hora de recuperación')!!}
              <input class="form-control form-control-sm" type="time" value="13:45:00" id="idHoraEntrega" name="idHoraEntrega" required>
            </div>
          </div>
            <div class="col-lg-4 col-md-4">
              <div class="form-group">
                {!!Form::label('idResguardo','Depósito de resguardo')!!}
                <select class="templatingSelect12 form-control form-control-sm" required id="idResguardo" name="idResguardo">
                  <option  selected="selected" value=""> </option>
                  @foreach($depositos as $deposito)
                  <option value="{{$deposito->id}}">{{$deposito->nombre}}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12 col-md-12 ">
              <div class="form-group">
                {!!Form::label('observacion','Observaciones')!!}
                <textarea class="form-control form-control-sm" placeholder="Coloque las observaciones aquí" rows="3" name="observacion" cols="50" style="text-transform: uppercase;" required id="observacion"></textarea>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  <div>
    <h5 align="left">Ubicación de veh&iacute;culos recuperados</h5>
  </div>
  <div class="row">
    <input id="pac-input" class="controls" type="text" placeholder="Buscar ubicación">
    <div class="col-md-12" id="mapa">
      {!!$map['html']!!}
    </div>
  </div>
  <br>
  <div align="right">
   <a href="#0" class="cd-top">Top</a>
  </div>
  <br>

	{!!Form::text('idSecurity',null, ['id'=>'idSecurity', 'name'=>'idSecurity', 'hidden'] )!!}
  {!!Form::text('idCarpeta',null, ['hidden','id'=>'idCarpeta'] )!!}
  {!!Form::text('numCarpeta',null, ['hidden','id'=>'numCarpeta'] )!!}
  {!!Form::hidden('latMap',null,['class'=>'form-control form-control-sm', 'id' => 'latMap', 'onkeyup'=>'mayus(this);'])!!}
  {!!Form::hidden('lonMap',null,['class'=>'form-control form-control-sm', 'id' => 'lonMap', 'onkeyup'=>'mayus(this);'])!!}
  {!! Form::close()!!}


  {{-- <script src="{{ asset('js/sweetalert.min.js') }}"></script> --}}


  <script src="{{ asset('js/denuncia/peticiones.js') }}"></script>



  <style media="screen">
  #profile-photo-camera-preview,
  #profile-photo-camera-preview video {
    width: 100% !important;
    height: auto !important;
    min-width: 100px;
    min-height: 100px;
  }
  .spa_sec{
    padding: 0 1em;
  }
  #my_camera{
    display: block;
    margin: auto;
  }
  #web_cam_section{
    background-color: #717171;
    border-color: #424242;
  }
  #cuerpo{
    display: block;
    margin: auto;
  }
  #ife_mini,#fact_mini{
    padding-bottom: 1em;
  }
  #titulo, #pp {display: inline;}

  .flotante {
    display:scroll;
    bottom:320px;
    right:0px;
    transform:scale(1);

  }

  </style>

  <script type="text/javascript">
      function mostrarOpcionSeleccionada() {
        //var $option = $select.find('option:selected');

        var identificador = $('#idEstado2').val();
        var respuesta;
        $('#idMunicipio2').html('');
        if(identificador!=''){
          var ruta = "{{route('municipio',':estado')}}";
          ruta = ruta.replace(':estado', identificador);
          datos = null;
          respuesta = peticionInfomacion('get',ruta,datos);
          if (respuesta != null) {
            for(i=0; i<respuesta.estado.length; i++){
              $("#idMunicipio2").append("<option value='"+respuesta.estado[i].idMunicipio+"'> "+respuesta.estado[i].nombre+"</option>");
            }
            if(respuesta.estado.length > 0 ){
              localStorage.setItem("idMunicipio2", respuesta.estado[0].id);
            }
          }else{
            alert('Error de servidor');
          }
        }else{
        }
      }

    var statusMsj = "";
    var msj = "";
    function addImg (ruta,posicion,subcat,nombre) {
    //path = storage_path();
    var titDiv="";
    if(posicion==0){
      titDiv="identidicacionCont";
      $('#idSubCatIdenti').val(subcat);
    }else{
      titDiv="facturaCont";
      $('#idSubCatFact').val(subcat);
    }

    getImg = "{{route('getImg',['filename'=>':ruta'])}}";
    getImg = getImg.replace(':ruta', ruta);
    //console.log(getImg);
    var element = '<div class="col center" id="'+titDiv+'"><img src="'+getImg+'" alt="" style="width:50%"><p>'+nombre+'</p></div>';
    return element;
    //console.log(element);
  }

  $(document).ready(function(){
    var ruta = window.location.pathname.split('/');
    $('#idSecurity').val((ruta[4])?ruta[4]:"uipj-1");
    
    window.onload = function(){
    	  fecha = new Date();
    	  texto = document.getElementById("hoy");
    	  texto.innerHTML = fecha;
      }

    map = new google.maps.Map(document.getElementById('map_canvas'), {
      center: { lat: 19.976312, lng: -96.7279684 },
      zoom: 7,
      mapTypeId: 'roadmap'
    });
    initAutocomplete();

    $('#idEstado2').val(30).trigger('change');
       mostrarOpcionSeleccionada();
    $('button').keypress(function(e){
      tecla = (document.all) ? e.keyCode : e.which;
      return (tecla != 13);
    });

    $('#verificacion').bind('click', function () {
      $('#datosComplementarios').empty();
      $('#verificacionmodal').modal('show');
      var placa = $('#placa').val();
      placa = (placa== '') ? 0 : placa;
      //placa = placa.toUpperCase();
      var serie = $('#serie').val();
      serie = (serie== '') ? 0 : serie;
      var motor = $('#idmotor').val();
      motor = (motor== '') ? 0 : motor;

      //registros VRR
      var ruta = "{{route('get_robados',['placa'=>':placa','serie'=>':serie','motor'=>':motor'])}}";
      ruta = ruta.replace(':placa', placa);
      ruta = ruta.replace(':serie', serie);
      ruta = ruta.replace(':motor', motor);
      respuesta = peticionInfomacion('get',ruta,null);

      console.log(respuesta);
      //registros UIPJ
      var ruta2 = "{{route('get_involucrados',['placa'=>':placa','serie'=>':serie','motor'=>':motor'])}}";
      ruta2 = ruta2.replace(':placa', placa);
      ruta2 = ruta2.replace(':serie', serie);
      ruta2 = ruta2.replace(':motor', motor);
      registrosCarpeta = peticionInfomacion('get',ruta2,null);


      //registros vehiculos involucrados
      var ruta3 = "{{route('get_recuperados',['placa'=>':placa','serie'=>':serie','motor'=>':motor'])}}";
      ruta3 = ruta3.replace(':placa', placa);
      ruta3 = ruta3.replace(':serie', serie);
      ruta3 = ruta3.replace(':motor', motor);
      recuperados = peticionInfomacion('get',ruta3,null);

      console.log('Recuperados');
      console.log(recuperados);
      //Codigo para unificar los arreglos de UIPJ y LOCAL
      var datosCompletos = [];
      for(i = 0; i < registrosCarpeta.length; i++){
        var reg = {
          tipo: 1,
          status:0,
          nrpv:registrosCarpeta[i].nrpv,
          numCarpeta:registrosCarpeta[i].numCarpeta,
          numMotor:registrosCarpeta[i].numMotor,
          numSerie:registrosCarpeta[i].numSerie,
          placas:registrosCarpeta[i].placas,
          nomFiscal:registrosCarpeta[i].nomFiscal

        }
        datosCompletos.push(reg);
      }
      for(i = 0; i < respuesta.registros.length; i++){
        var reg = {
          tipo: 2,
          status:respuesta.registros[i].status,
          nrpv: (respuesta.registros[i].nrpv == '') ? "SIN INFORMACION" : respuesta.registros[i].nrpv,
          numCarpeta:respuesta.registros[i].numCarpeta,
          numMotor:respuesta.registros[i].numMotor,
          numSerie:respuesta.registros[i].numSerie,
          placas:respuesta.registros[i].placas,
          folioDoctoCirc:respuesta.registros[i].folioDoctoCirc,
          id:respuesta.registros[i].id,
          idAseguradora:respuesta.registros[i].idAseguradora,
          idColor:respuesta.registros[i].idColor,
          idDocumentoCirculacion:respuesta.registros[i].idDocumentoCirculacion,
          idEstado:respuesta.registros[i].idEstado,
          idLocalidad:respuesta.registros[i].idLocalidad,
          //idMunicipio:respuesta.registros[i].idMunicipio,
          idProcedencia:respuesta.registros[i].idProcedencia,
          idMarca:respuesta.registros[i].idMarca,
          idSubmarca:respuesta.registros[i].idSubmarca,
          idTipoUso:respuesta.registros[i].idTipoUso,
          idTipoVehiculo:respuesta.registros[i].idClaseVehiculo,
          idSubTipoVehiculo:respuesta.registros[i].idTipoVehiculo,
          modelo:respuesta.registros[i].modelo,
          permiso:respuesta.registros[i].permiso,
          senasPartic: (respuesta.registros[i].senasPartic == '') ? "SIN INFORMACION" : respuesta.registros[i].senasPartic,
          tipoDenuncia:respuesta.registros[i].tipoDenuncia,
          nomFiscal:respuesta.nomFiscal[i].nomFiscal
        }
        datosCompletos.push(reg);
      }
      for(i = 0; i < recuperados.registros.length; i++){
        var reg = {
          tipo: 3,
          status:recuperados.registros[i].status,
          nrpv:recuperados.registros[i].nrpv,
          numCarpeta:recuperados.registros[i].numCarpeta,
          numMotor:recuperados.registros[i].numMotor,
          numSerie:recuperados.registros[i].numSerie,
          placas:recuperados.registros[i].placas,
          folioDoctoCirc:recuperados.registros[i].folioDoctoCirc,
          id:recuperados.registros[i].id,
          idAseguradora:recuperados.registros[i].idAseguradora,
          idColor:recuperados.registros[i].idColor,
          idDocumentoCirculacion:recuperados.registros[i].idDocumentoCirculacion,
          idEstado:recuperados.registros[i].idEstado,
          idLocalidad:recuperados.registros[i].idLocalidad,
          //idMunicipio:recuperados.registros[i].idMunicipio,
          idProcedencia:recuperados.registros[i].idProcedencia,
          idMarca:recuperados.registros[i].idMarca,
          idSubmarca:recuperados.registros[i].idSubmarca,
          idTipoUso:recuperados.registros[i].idTipoUso,
          idTipoVehiculo:recuperados.registros[i].idClaseVehiculo,
          idSubTipoVehiculo:recuperados.registros[i].idTipoVehiculo,
          modelo:recuperados.registros[i].modelo,
          permiso:recuperados.registros[i].permiso,
          senasPartic:recuperados.registros[i].senasPartic,
          tipoDenuncia:recuperados.registros[i].tipoDenuncia,
          nomFiscal:recuperados.nomFiscal[i].nomFiscal
        }
        datosCompletos.push(reg);
      }
      $('#tablevehiculos').bootstrapTable('load',datosCompletos);

    });




  $('.btn_ide').on('change', function(){
    $('#identidicacionCont').hide();
  });

  $('.btn-fac').on('change', function(){
    $('#facturaCont').hide();
  });

  $('.btn_check').click(function(event){
    if($(this).val()==1){
      $('#placa').removeAttr('readonly');
      $('#permiso').attr('readonly',true);
    }else{
      $('#permiso').removeAttr('readonly');
      $('#placa').attr('readonly',true);
    }
  });

  $("#idMarca").change(function() {
    var identificador = $(this).val();
    var respuesta;
    $('#idSubmarca').html('');
    if(identificador!=''){
      var ruta = "{{route('submarca',':marca')}}";
      ruta = ruta.replace(':marca', identificador);
      datos = null;
      respuesta = peticionInfomacion('get',ruta,datos);
      if (respuesta != null) {
        for(i=0; i<respuesta.estado.length; i++){
          $("#idSubmarca").append("<option value='"+respuesta.estado[i].id+"'> "+respuesta.estado[i].nombre+"</option>");
        }
        if(respuesta.estado.length > 0 ) {
          localStorage.setItem("idSubmarca", respuesta.estado[0].id);
        }
      }else{
        alert('Error de servidor');
      }
    }else{
    }
  });

  // $("#idEstado").change(function() {
  //   var identificador = $(this).val();
  //   var respuesta;
  //   $('#idMunicipio').html('');
  //   if(identificador!=''){
  //     var ruta = "{{route('municipio',':estado')}}";
  //     ruta = ruta.replace(':estado', identificador);
  //     datos = null;
  //     respuesta = peticionInfomacion('get',ruta,datos);
  //     if (respuesta != null) {
  //       for(i=0; i<respuesta.estado.length; i++){
  //         $("#idMunicipio").append("<option value='"+respuesta.estado[i].idMunicipio+"'> "+respuesta.estado[i].nombre+"</option>");
  //       }
  //       if(respuesta.estado.length > 0 ){
  //         localStorage.setItem("idMunicipio", respuesta.estado[0].id);
  //       }
  //     }else{
  //       alert('Error de servidor');
  //     }
  //   }else{
  //   }
  // });

  $("#idMunicipio").change(function() {
    localStorage.setItem("idMunicipio", $('#idMunicipio').val());
  });
  $("#idEstado2").change(function() {
    var identificador = $(this).val();
    var respuesta;
    $('#idMunicipio2').html('');
    if(identificador!=''){
      var ruta = "{{route('municipio',':estado')}}";
      ruta = ruta.replace(':estado', identificador);
      datos = null;
      respuesta = peticionInfomacion('get',ruta,datos);
      if (respuesta != null) {
        for(i=0; i<respuesta.estado.length; i++){
          $("#idMunicipio2").append("<option value='"+respuesta.estado[i].idMunicipio+"'> "+respuesta.estado[i].nombre+"</option>");
        }
        if(respuesta.estado.length > 0 ){
          localStorage.setItem("idMunicipio2", respuesta.estado[0].id);
        }
      }else{
        alert('Error de servidor');
      }
    }else{
    }
  });
  $("#idMunicipio2").change(function() {
    localStorage.setItem("idMunicipio2", $('#idMunicipio2').val());
  });

  $("#idClaseVehiculo").change(function() {
    var identificador = $(this).val();
    var respuesta;
    $('#idSubclase').html('');
    if(identificador!=''){
      var ruta = "{{route('subclase',':clasevehiculo')}}";
      ruta = ruta.replace(':clasevehiculo', identificador);
      datos = null;
      respuesta = peticionInfomacion('get',ruta,datos);
      if (respuesta != null) {
        for(i=0; i<respuesta.estado.length; i++){
          $("#idSubclase").append("<option value='"+respuesta.estado[i].id+"'> "+respuesta.estado[i].nombre+"</option>");
        }
        if (identificador == 99) {
          $('#idSubclase').val(999).trigger('change');
        }
        if (respuesta.estado.length > 0) {
          localStorage.setItem("idSubclase", respuesta.estado[0].id);
        }
      }else{
        alert('Error de servidor');
      }
    }else{
    }
  });

  $("#idSubmarca").change(function() {
    localStorage.setItem("idSubmarca", $('#idSubmarca').val());
  });

  $("#idSubclase").change(function() {
    localStorage.setItem("idSubclase", $('#idSubclase').val());
  });

  $('#formRecuperado').submit(function (e) {
    e.preventDefault();
    var ruta ="{{route('getMockup','1')}}";
    var carpeta = peticionInfomacion('get',ruta);
    $('#idCarpeta').val(carpeta.investigacion.id);
    $('#numCarpeta').val(carpeta.investigacion.numCarpeta);
    var clase = ""
    var formData = new FormData(this);
    var placa = ($('#placa').val() == '') ? 0 : $('#placa').val();
    var serie = $('#numSerie').val();
    var motor = $('#motor').val();
    var nrpv = $('#nrpv').val();
    var token = $('#token').val();
    var ruta = "{{route('get_robados',['placa' => ':placa', 'serie' => ':serie', 'motor' => ':motor','token' => ':token'])}}";
    ruta = ruta.replace(':placa', placa);
    ruta = ruta.replace(':serie', serie);
    ruta = ruta.replace(':motor', motor);
    ruta = ruta.replace(':token', token);
    datos = null;
    respuesta = peticionInfomacion('get',ruta,datos);

    var ruta = "{{route('get_involucrados',['placa' => ':placa', 'serie' => ':serie', 'motor' => ':motor'])}}";
    ruta = ruta.replace(':placa', placa);
    ruta = ruta.replace(':serie', serie);
    ruta = ruta.replace(':motor', motor);
    ruta = ruta.replace(':token', token);
    datos = null;
    respuestaUipj = peticionInfomacion('get',ruta,datos);

    var numSerie = $('#serieB').val();
    if(numSerie!=""){
        //console.log('Este es el numero de serie ->'+numSerie);
        var ruta ="{{route('niv',['num_serie'])}}";
        ruta = ruta.replace("num_serie",numSerie);
        //console.log(ruta);
        var respuestaNiv = peticionInfomacion('get',ruta);
        statusMsj = respuestaNiv.request.status;
        msj = respuestaNiv.request.mensaje;
      }
      if(statusMsj=='correcto'){clase='alert alert-success';}else{clase='alert alert-danger';}

      swal({
        title: "¿Esta seguro que desea continuar?",
        text: "",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        //content: span,
      })
      .then((willDelete) => {
        if (willDelete) {
          swal("Su registro se ha registrado satisfactoriamente", {
            icon: "success",
          });
          $.ajax({
            url : "{{route('saveRecuperados')}}",
            data : formData,
            type : 'POST',
            cache: false,
            contentType : false,
            processData : false,
            success : function(json) {
              console.log('Se agrego un vehiculo');
              $('#table').bootstrapTable('refresh');
              swal({
                title: "Registro creado exitosamente",
                text: "Su registro se realizo correctamente",
                icon: "success",
              });
              /*
              $('#table').bootstrapTable('refresh');
              $("#idEstado").val('').trigger('change');
              $("#idMunicipio").val('').trigger('change');
              $("#idSubmarca").val('').trigger('change');
              $("#idColor").val('').trigger('change');
              $("#idAseguradora").val('').trigger('change');
              $("#idMarca").val('').trigger('change');
              $("#permiso").val('').trigger('change');
              $("#serie").val('').trigger('change');
              $("#idmotor").val('').trigger('change');
              $("#nrpv").val('').trigger('change');
              $("#senasPartic").val('').trigger('change');
              $("#placa").val('').trigger('change');
              $("#folioDoctoCirc").val('').trigger('change');
              $("#token").val('').trigger('change');
              $("#idClaseVehiculo").val('').trigger('change');
              $("#idSubclase").val('').trigger('change');
              $("#idUso").val('').trigger('change');
              $("#idProcedencia").val('').trigger('change');
              $("#idEstado2").val('').trigger('change');
              $("#idMunicipio2").val('').trigger('change');
              $("#formRecuperado")[0].reset();
              $("#idResguardo").val('').trigger('change');
              $("#identidicacionCont").html('');
              $("#facturaCont").html('');
              ("#latMap").html('');
              ("#lonMap").html('');
              $('#DOC1').attr('value','');

              $("#cp").val('').trigger('change');
              $("#calle").val('').trigger('change');
              $("#colonia").val('').trigger('change');
              $("#numExt").val('').trigger('change');
              $("#numInt").val('').trigger('change');
              $("#idFechaEntrega").val('').trigger('change');
              $("#idHoraEntrega").val('').trigger('change');
              */

            },

            error : function(xhr, status) {
              console.log('Disculpe, existió un problema');
              swal({
                title: "Error al introducir los datos",
                text: 'msj',
                icon: "error",
              });
            },
            complete : function(xhr, status) {
              //console.log('Petición realizada');
            }
          });
        } else {
          swal("Su registro ha sido cancelado");
        }
      });
    });

  });


  $('#serie').focusout(function(){
    var numSerie = $('#serie').val();
    if(Number.parseInt($('#modelo').val())>=1980){
      if(numSerie!=""){
        var ruta ="{{route('niv',['num_serie'])}}";
        ruta = ruta.replace("num_serie",numSerie);
        var respuesta = peticionInfomacion('get',ruta);
        console.log(respuesta);
        (respuesta.request.status=='error')? toastr.error(respuesta.request.mensaje, "ERROR",{"preventDuplicates": true}):toastr.success(respuesta.request.mensaje, "CORRECTO",{"preventDuplicates": true});
      }
    }else{
      //console.log('no verifica nada');
    }
  });


  $( document ).ready(function() {
    verificar_camara('divBtnCamara');
    var idmarca = $("#idMarca").val();
    if (idmarca > 0) {
      var ruta = "{{route('submarca',':marca')}}";
      ruta = ruta.replace(':marca', idmarca);
      datos = null;
      respuesta = peticionInfomacion('get',ruta,datos);
      if (respuesta != null) {
        for(i=0; i<respuesta.estado.length; i++){
          $("#idSubmarca").append("<option value='"+respuesta.estado[i].id+"'> "+respuesta.estado[i].nombre+"</option>");
        }
        $('#idSubmarca').val(localStorage.idSubmarca).trigger("change");
      }
    }

    var idClaseVehiculo = $("#idClaseVehiculo").val();
    if (idClaseVehiculo > 0) {
      var ruta = "{{route('subclase',':clasevehiculo')}}";
      ruta = ruta.replace(':clasevehiculo', idClaseVehiculo);
      datos = null;
      respuesta = peticionInfomacion('get',ruta,datos);
      if (respuesta != null) {
        for(i=0; i<respuesta.estado.length; i++){
          $("#idSubclase").append("<option value='"+respuesta.estado[i].id+"'> "+respuesta.estado[i].nombre+"</option>");
        }
      }
      $('#idSubclase').val(localStorage.idSubclase).trigger("change");
    }
  });


  function deshabilitar (){
    $('#idUso').attr('disabled',true);
    $('#idEstado').attr('disabled',true);
    $('#idMunicipio').attr('disabled',true);
    $('#idMarca').attr('disabled',true);
    $('#idSubmarca').attr('disabled',true);
    $('#modelo').attr('disabled',true);
    $('#idColor').attr('disabled',true);
    $('#idnrpv').attr('disabled',true);
    $('#idClaseVehiculo').attr('disabled',true);
    $('#idSubclase').attr('disabled',true);
    $('#iduso').attr('disabled',true);
    $('#idProcedencia').attr('disabled',true);
    $('#idAseguradora').attr('disabled',true);
    //$('#idResguardo').attr('disabled',true);
    $('#senasPartic').attr('disabled',true);
  }

  function operateFormatter(value, row, index){
    var texto ="";
    if(row.status==0){
      texto = "Involucrado";
    }else if(row.status==1){
      texto = "Robado"
    }else if(row.status==2){
      texto = "Recuperado"
    }
    return [
    texto,
    ].join('');
  }

  $('#tablevehiculos').bootstrapTable({
    search:  true,
    showColumns: true,
    showRefresh: true,
    //url: ruta,
    buttonsAlign :'left',
    searchAlign :'left',
    idField: 'id',
    iconsPrefix: 'fa',
    pagination : true,
    pageSize: 5,
    columns: [{
      field: 'id',
      title: 'ID',
      sortable: true,
      visible: false
    }, {
      field: 'status',
      title: 'Estatus',
      sortable: true,
      formatter: operateFormatter
    }, {
      field: 'numCarpeta',
      title: 'No. Carpeta',
      sortable: true
    },{
      field: 'placas',
      title: 'Placas',
      sortable: true
    }, {
      field: 'nrpv',
      title: 'NRPV',
      sortable: true,
      visible: false
    }, {
      field: 'numSerie',
      title: 'No. serie',
      sortable: true
    }, {
      field: 'numMotor',
      title: 'No. Motor',
      sortable: true
    },{
      field: 'nomFiscal',
      title: 'Nombre Fiscal',
      sortable: true
    }],

  });

  $('#tablevehiculos').on('click-row.bs.table', function(tr, row){
    console.log('Este es el row')
    console.log(row);
    $('#id').val(row.id);
    $('#placa').val(row.placas);
    $('#idEstado').val(row.idEstado).trigger('change');
    $('#idMunicipio').val(row.idMunicipio).trigger('change');
    $('#idMarca').val(row.idMarca).trigger('change');
    $('#idSubmarca').val(row.idSubmarca).trigger('change');
    $('#modelo').val(row.modelo);
    $('#idColor').val(row.idColor).trigger('change');
    $('#serie').val(row.numSerie);
    $('#idmotor').val(row.numMotor);
    $('#idnrpv').val(row.nrpv);
    $('#idClaseVehiculo').val(row.idTipoVehiculo).trigger('change');
    $('#idSubclase').val(row.idSubTipoVehiculo).trigger('change');
    $('#idUso').val(row.idTipoUso).trigger('change');
    $('#idProcedencia').val(row.idProcedencia).trigger('change');
    $('#idAseguradora').val(row.idAseguradora).trigger('change');
    $('#senasPartic').val(row.senasPartic);
    $('#folioDoctoCirc').val(row.folioDoctoCirc);
    $('#datosComplementarios').empty();
    $('#datosComplementarios').append('<p><b>Carpeta:</b> '+row.numCarpeta+'</p>');
    $('#datosComplementarios').append('<p><b>Nombre Fiscal:</b> '+row.nomFiscal+'</p>');
    $('#datosComplementarios').append('<p><b>Placa:</b> '+row.placas+'</p>');
    $('#datosComplementarios').append('<p><b>No. Serie:</b> '+row.numSerie+'</p>');
    $('#datosComplementarios').append('<p><b>No. Motor:</b> '+row.numMotor+'</p>');
    $('#datosComplementarios').append('<p><b>NRPV:</b> '+row.nrpv+'</p>');
    if(row.tipo == 2 || row.tipo == 3){
      $('#datosComplementarios').append('<p><b>Modelo:</b> '+row.modelo+'</p>');
      $('#datosComplementarios').append('<p><b>Permiso:</b> '+row.permiso+'</p>');
      $('#datosComplementarios').append('<p><b>Señas particulares:</b> '+row.senasPartic+'</p>');
    }
    deshabilitar();
  })

  $('.btn-verificar').change(function(){
    if ($('.btn-verificar:checked').val()){
      $('#verificacion').removeAttr('disabled');;
    }else{
      $('#verificacion').attr('disabled',true);
    }
  });

  $( "#serie" ).blur(function() {
    var prohibido =['SE IGNORA', 'NULL','null','se ignora' ];
    var cadenaUser = $("#serie").val();
    var idx = prohibido.indexOf(cadenaUser);
    if(idx > -1){
      $("#serie").val("");
    }
  });

  $( "#placa" ).blur(function() {
    var prohibido =['SIN', 'SIN PLA', 'S.D.' ,'S.D','NULL','sin','sin pla','s.d','null' ];
    var cadenaUser = $("#placa").val();
    var idx = prohibido.indexOf(cadenaUser);
    if(idx > -1){
      $("#placa").val("0000000");
    }
  });

  $('#placa').keyup(function(){
      ($('#placa').val())?$('#verificacion').removeAttr('disabled'):'';

  });
  $('#serie').keyup(function(){
      ($('#serie').val())?$('#verificacion').removeAttr('disabled'):'';

  });
  $('#idmotor').keyup(function(){
      ($('#idmotor').val())?$('#verificacion').removeAttr('disabled'):'';
  });




  </script>
