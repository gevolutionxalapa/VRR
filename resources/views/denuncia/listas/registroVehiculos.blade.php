    @section('title')
    Registros
    @endsection
    <!-- Contenido -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/modalsweetalert2.css') }}">

    <style>
    .pictures {
      margin: 0;
      padding: 0;
      list-style: none;
    }
    .pictures > li {
      float: left;
      width: 33.3%;
      height:33.3%;
      margin: 0 -1px -1px 0;
      border: 0.5px solid transparent;
      overflow: hidden;
    }
    .pictures > li > img {
      width: 100%;
      cursor: -webkit-zoom-in;
      cursor: zoom-in;
    }

    .switch {
      position: relative;
      display: inline-block;
      width: 45px;
      height: 24px;
    }

    .switch input {display:none;}

    .slider {
      position: absolute;
      cursor: pointer;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      background-color: #ccc;
      -webkit-transition: .4s;
      transition: .4s;
    }

    .slider:before {
      position: absolute;
      content: "";
      height: 16px;
      width: 16px;
      left: 4px;
      bottom: 4px;
      background-color: white;
      -webkit-transition: .4s;
      transition: .4s;
    }

    input:checked + .slider {
      background-color: #2196F3;
    }

    input:focus + .slider {
      box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
      -webkit-transform: translateX(20px);
      -ms-transform: translateX(20px);
      transform: translateX(20px);
    }

    .slider.round {
      border-radius: 17px;
    }

    .slider.round:before {
      border-radius: 50%;
    }
    </style>
    <div class="card">
      <div class="card-header">
        <h5 align="justify">Registros de vehiculos robados</h5>

      </div>
      <div class="card-body">
        <ul class="nav nav-pills">
         <li style="padding-right: 10px"><a id="idReversa" href="{{route('showDenuncia')}}" class="nav-link  btn-outline-dark " ><span class="glyphicon glyphicon-home"></span> Registro robo de vehículo</a></li>
         <li class="active"><a href="#" class="nav-link active btn-outline-dark " ><span class="glyphicon glyphicon-user"></span> Vehículos robados</a></li>
       </ul>
       <table id="table" class="table table-bordered  table-responsive-sm"></table>
     </div>
    </div>

    <div id="galley" style="display:none">
      <ul class="pictures" id="listaDoc"></ul>
    </div>

    <!--Modal adjuntar diligencia -->
    <div id="modaladjuntardiligencia" class="modal" tabindex="-1" role="dialog" >
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Carga de archivos</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="alert alert-success alert-dismissible fade show" role="alert" id="resp_load_1" style="display:none;">
              <p>Se anexo correctamente</p>
              <button type="button" class="close" id="anexo2" data-dismiss="modal" aria-label="Close" onclick="alerta()">
                <span aria-hidden="true" id="anexo">&times;</span>
              </button>
            </div>
            <script type="text/javascript">

            </script>
            <div class="alert alert-danger alert-dismissible fade show" role="alert" id="resp_load_2" style="display:none;">
              <p>No ha seleccionado ningún archivo</p>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true" id="anexo">&times;</span>
              </button>
            </div>
            {!! Form::open(['route' => 'docUpDiligencia','id'=>'docDiligenciaUp']) !!}
            <div class="form-group">
              <label for="exampleInputEmail1">Selecciona la diligencia asociada</label>
              {{ Form::file('thefile', ['class' => 'field form-control','id' => 'diligenciaFormaro', 'name' => 'diligenciaFormaro']) }}
            </div>
            <input id="iddiligencia" name="iddiligencia" type="text" value="iddiligencia" hidden >
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary custom-close" id="custom-close">Guardar</button>
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>

    <div id="modalValidacion" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Detalle vehículo</h5>
            <button type="button" class="close btn_cerrar" data-dismiss="modal" id="cerrar" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="form_validacion">
              {!!Form::text('id1',null,['id' => 'id1', 'hidden'])!!}
              {!!Form::text('llaveExterna',null,['id' => 'llaveExterna', 'hidden'])!!}
              <div class="row">
                <div class="col-lg col-md">
                  <p>FAVOR DE VERIFICAR LOS DATOS DE ACUERDO A LAS SIGUIENTES OBSERVACIONES</p><hr>
                  <div id="error" style="color:red;border-color: red;border-radius: 3px;">

                  </div>
                  {{-- {!!Form::label('Comentario Entidad','Comentario relacionado a los datos erróneos')!!}
                  <textarea class="form-control form-control-sm" placeholder="Comentario" rows="3" name="comentario" cols="50" style="text-transform: uppercase;" required id="comentario" readonly="readonly"></textarea> --}}
                </div>
              </div>
              <div class="row">
                <div class="col-lg col-md">
                  <div class="form">
                    {!!Form::label('permiso', 'Documento de circulación')!!}
                  </div>
                  <div class="input-group">
                    <select class="templatingSelect10 form-control form-control-sm"  id="idDocCirculacion1" style="width:100%" name="idDocCirculacion1" >
                      <option selected="selected" value=""> </option>
                      @foreach($documentosCirculacion as $documento)
                      <option value="{{$documento->id}}" >{{$documento->nombre}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-lg col-md">
                  <div id="divPlaca">
                    <div class="form">
                      {!!Form::label('idplaca1', 'Placa')!!}
                    </div>
                    <div class="input-group" style="width:100%">
                      {!!Form::text('idplaca1',null,['class'=>'form-control form-control-sm', 'null' ,'id' => 'idplaca1', 'placeholder' => 'HMZ2398 - Ejemplo' , 'maxlength'=>'7',  'style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'alphanumeric'])!!}
                    </div>
                  </div>
                  <div id="divFolio" style = 'display: none'>
                    <div class="form">
                      {!!Form::label('folio', 'Folio')!!}
                    </div>
                    <div class="input-group" style="width:100%">
                      {!!Form::text('folio1',null,['class'=>'form-control form-control-sm', 'id' => 'folio1', 'style'=>'text-transform: uppercase;','maxlength'=>'10'])!!}
                    </div>
                  </div>
                </div>
                <div class="col-lg col-md">
                  <div class="form-group">
                    {!!Form::label('idEstado', 'Entidad federativa')!!}
                    <select  class="templatingSelect2 form-control form-control-sm" style="width:100%" id="idEstados1" name="idEstados1" required>
                      <option value="" selected="selected" > </option>
                      @foreach($estados as $estado)
                      <option  value="{{$estado->id}}" >{{$estado->nombre}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg col-md">
                  <div class="form-group">
                    {!!Form::label('idMarca', 'Marca')!!} <br>
                    <select  class="templatingSelect3 form-control form-control-sm" style="width:100%" id="idMarcas1" name="idMarcas1" required>
                      <option value="" selected="selected" > </option>
                      @foreach($marcas as $marca)
                      <option  value="{{$marca->id}}">{{$marca->nombre}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-lg col-md">
                  {!!Form::label('idSubmarca', 'Submarca')!!}<br>
                  <select  class="idSubmarcas form-control form-control-sm" style="width:100%"  id="idSubmarcas1" name="idSubmarca1" required>
                    <option value="" required selected="selected"></option>
                  </select>
                </div>
                <div class="col-lg col-md">
                  {!!Form::label('modelo', 'Modelo')!!}
                  <input class="form-control form-control-sm" style="width:100%" value="{{date('Y')}}" min="1885" max="{{date('Y')+1}}" name="modelo1" type="number" required id="modelos1">
                </div>
              </div>
              <div class="row">
                <div class="col-lg col-md">
                  {!!Form::label('idColo1','Color')!!} <br>
                  <select  class="templatingSelect5 form-control form-control-sm" style="width:100%" id="idColors1" name="idColor1" required>
                    <option selected="selected" value=""> </option>
                    @foreach($colores as $color)
                    <option value="{{$color->id}}">{{$color->nombre}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-lg col-md">
                  {!!Form::label('numSerie1','Número de serie')!!}
                  {!!Form::text('numSerie1',null, ['id'=> 'numSeries1','class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el número de serie','maxlength'=>'17','style'=>'text-transform: uppercase;',  'data-validation'=>'custom', 'data-validation'=>'alphanumeric'] )!!}
                </div>
                <div class="col-lg col-md">
                  {!!Form::label('motor1','Número de motor')!!}
                  {!!Form::text('motor1',null, ['id'=> 'motors1','class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el número de motor', 'maxlength'=>'10', 'style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'alphanumeric' ] )!!}
                </div>
              </div>
              <div class="row">
                <div class="col-lg col-md">
                  {!!Form::label('nrpv1','NRPV')!!}
                  {!!Form::text('nrpv1',null, ['id'=> 'nrpv1','class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el NRPV', 'style'=>'text-transform: uppercase;','required', 'maxlength'=>'20'] )!!}
                </div>
                <div class="col-lg col-md">
                  {!!Form::label('idClaseVehiculo','Tipo de vehículo')!!}
                  <select  class="templatingSelect7 form-control form-control-sm" style="width:100%"  id="idClaseVehiculo1" name="idClaseVehiculo1" required>
                    @foreach($clasevehiculo as $clasevehiculo)
                    <option value="{{$clasevehiculo->id}}">{{$clasevehiculo->nombre}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-lg col-md">
                  {!!Form::label('idSubclase','Subtipo de vehículo')!!}
                  <select  class="templatingSelect1 form-control form-control-sm" style="width:100%" id="idSubclase1" name="idSubclase1" required>
                    <option selected="selected" value=""> </option>
                  </select>
                </div>
              </div>
              <div class="row">
                <div class="col-lg col-md">
                  {!!Form::label('idUso','Tipo de uso')!!}
                  <select  class="templatingSelect8 form-control form-control-sm" style="width:100%" id="idUso1" name="idUso1" required>
                    <option selected="selected" value=""> </option>
                    @foreach($tipouso as $tipouso)
                    <option value="{{$tipouso->id}}">{{$tipouso->nombre}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-lg col-md">
                  {!!Form::label('idProcedencia','Procedencia')!!} <br>
                  <select style="width:100%" class="templatingSelect9 form-control form-control-sm"  id="idProcedencia1" name="idProcedencia1" required>
                    <option value="" selected>Seleccione</option>
                    <option value="2" >EXTRANJERO</option>
                    <option value="1">NACIONAL</option>
                  </select>
                </div>
                <div class="col-lg col-md">
                  {!!Form::label('idAseguradora','Aseguradora')!!}
                  <select class="templatingSelect6 form-control form-control-sm" style="width:100%" required id="idAseguradora1" name="idAseguradora1" >
                    <option selected="selected" value=""> </option>
                    @foreach($aseguradoras as $aseguradora)
                    <option value="{{$aseguradora->id}}">{{$aseguradora->nombre}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="row">
                <div class="col-lg col-md">
                  {!!Form::label('senasPartic','Señas particulares')!!}
                  <textarea class="form-control form-control-sm" placeholder="Ingrese las señas particulares" rows="3" name="senasPartic1" cols="50" style="text-transform: uppercase;" required id="senasPartic1"></textarea>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" id="guardar_validacion" >Guardar</button>
            <button type="button" class="btn btn-secondary btn_cerrar" id="cerrar" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </div>
    </div>


      <script type="text/javascript">
      function alerta(){
        $('#table').bootstrapTable('refresh');
      }

      function limpiar(){
        $('#form_validacion').trigger("reset");
      }


      function elemento (nombre, valor) {
        var p = $('<p>').text(nombre+': '+valor);
        return p;
      }

      function addImg2 (ruta,nombre) {
        getImg = "{{route('getImg',['filename'=>':ruta'])}}";
        getImg = getImg.replace(':ruta', ruta);
        var element = '<li><img data-original="'+getImg+'" src="'+getImg+'" alt="'+nombre+'"><p>'+nombre+'</p></i>';
        return element;
      }

      function addImg3 (ruta,nombre,posicion,subcat) {
        var titDiv="";
        //var posicion="";
        //var subcat="";
        console.log('Aqui--- la posicion');
        console.log(subcat);
         if(posicion==0){
           titDiv="identidicacionCont1";
           $('#idSubCatIdenti1').val(subcat);
         }else{
           titDiv="facturaCont1";
           $('#idSubCatFact1').val(subcat);
         }

        getImg = "{{route('getImg',['filename'=>':ruta'])}}";
        console.log(getImg);
        getImg = getImg.replace(':ruta', ruta);
        console.log("Imagen Aqui");
        console.log(getImg);
        var element = '<div class="col center" align="center" id="'+titDiv+'"><img src="'+getImg+'" alt="" style="width:50%"><p>'+nombre+'</p></div>';
        return element;
      }

      function operateFormatter(value, row, index) {
        if(row.status_diligencia) {
          return [
          '<p style="width: 75px;">',
          '<a class="detalleVehiculo btn btn-link" href="javascript:void(0)" title="Detalle del vehiculo">',
          '<i class="fa fa-info" aria-hidden="true"></i>',
          '</a>  ',
          '<a class="doctosVehiculo btn btn-link" data-target="#modal" href="javascript:void(0)" title="Documentos del vehiculo">',
          '<i class="fa fa-picture-o" aria-hidden="true"></i>',
          '</a>  ',
          '</p>',
          '<p style="width: 75px;">',
          '<a class="reporteVehiculo btn btn-link" data-target="#modal" href="javascript:void(0)" title="Reporte de vehiculos">',
          '<i class="fa fa-print" aria-hidden="true"></i>',
          '</a>  ',
          '<a class="formatodiligencia btn btn-link"  data-target="#modal" href="javascript:void(0)" title="Diligencia" onclick="adjuntar('+row.id+')">',
          '<i class="fa fa-file" aria-hidden="true"></i>',
          '</a>  ',
          '</p>',
          '<p style="width: 75px;">',
          '<a class="formatoVdiligencia btn btn-link" href="javascript:void(0)" title="Visualizar Diligencia" target="_blank"  style="disabled:true">',
          '<i class="fa fa-eye" aria-hidden="true" id="vDiligencia"></i>',
          '</a>  ',
          '</p>',
          ].join('');
        } else {
          return [
          '<p style="width: 75px;">',
          '<a class="detalleVehiculo btn btn-link" href="javascript:void(0)" title="Detalle del vehiculo">',
          '<i class="fa fa-info" aria-hidden="true"></i>',
          '</a>  ',
          '<a class="doctosVehiculo btn btn-link" data-target="#modal" href="javascript:void(0)" title="Documentos del vehiculo">',
          '<i class="fa fa-picture-o" aria-hidden="true"></i>',
          '</a>  ',
          '</p>',
          '<p style="width: 75px;">',
          '<a class="reporteVehiculo btn btn-link" data-target="#modal" href="javascript:void(0)" title="Reporte de vehiculos">',
          '<i class="fa fa-print" aria-hidden="true"></i>',
          '</a>  ',
          '<a class="formatodiligencia btn btn-link"  data-target="#modal" href="javascript:void(0)" title="Diligencia" onclick="adjuntar('+row.id+')">',
          '<i class="fa fa-file" aria-hidden="true"></i>',
          '</a>  ',
          '</p>',
          ].join('');
        }
      }
      function operateFormatter2(value, row, index) {

        var datos = JSON.stringify(row);
        var campos = ['placas' || 'permiso','modelo','nrpv','numSerie','numMotor','procedencia','marca','submarca','color','tipoVehiculo','claseVehiculo','tipoUSo','aseguradora','senasPartic'];
        var acierto = 0;
        for(var i = 0; i < campos.length; i++)
        {
          if (row[campos[i]]!=""){
            acierto= acierto + 1;
          }
        }
        var p = (acierto * 100)/campos.length;
        p = Math.round(p);


        $('#pro-bar-'+row.id).css('width', p+'%').attr('aria-valuenow', p);
        $('#pro-bar-'+row.id).text(Math.round(p)+'%');
        if(p>=0 && p<=49){
          color = 'red';
        }
        else if(p>=50 && p<=99){
            color = 'orange';
          }
          else{
            color = 'green';
          }
       //guardar porcentaje


        // var rutaP = "";
        // rutaP = rutaP.replace(':idVeh', row.id);
        // rutaP = rutaP.replace(':porcentanje', p);
        // //ruta = ruta.replace(':tipoVal', 1);
        // console.log(rutaP);
        // statusP = peticionInfomacion('get',ruta,null);

        return [
        '<div class="progress" id="porcentajeVehiculo">',
        '<div class="progress-bar" id="pro-bar-'+row.id+'" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; background-color:'+color+'; width: '+p+'%;">'+p+'%</div>',
        '</div>',
      ].join('');
    }

    function operateFormatter3(value, row, index){
      var color = "";
      var icono = "";
      var ruta = "{{route('validacionRobados',':idVeh')}}";
      ruta = ruta.replace(':idVeh', row.id);
      //ruta = ruta.replace(':tipoVal', 1);
      console.log(ruta);
      statusBus = peticionInfomacion('get',ruta,null);
      console.log('Datos BUS');
      console.log(statusBus[0].busCNS);
      if(statusBus[0].busCNS==2){
         icono = "fa fa-upload";
         color = "#138C13";
      }
      if(statusBus[0].busCNS==3){
          //alert('entra aqui');
         icono = "fa fa-download";
         color = "#8C1333";
      }
      if(statusBus[0].busFGE==0 && statusBus[0].busCNS ==0){
        color = "#BDBDBD";
        icono = "fa fa-circle-o";
      }
      if(statusBus[0].busFGE==1 && statusBus[0].busCNS ==0){
        color = "#DDA61D";
        icono = "fa fa-exclamation-circle";
      }
      if(statusBus[0].busFGE==1 && statusBus[0].busCNS==1){
         color = "#DDA61D";
         icono = "fa fa-upload";
      }

      if(statusBus[0].busCNS==0 && statusBus[0].busCNSKey==3){
         icono = "fa fa-download";
         color = "#8C1333";
      }



      return [
        '<a class="statusRegistro btn_policia"  href="javascript:void(0)" title="Validación de Registro" align="center" onclick="limpiar();">',
        '<i class="'+icono+'" aria-hidden="true" style="color:'+color+';"></i>',
        '</a>  ',
      ].join('');
    }

    function operateFormatterRepuve(value, row, index){
      if(row.status_repuve == 0){
         icono = "fa fa-circle";
         color = "#BDBDBD";
      }
      if(row.status_repuve == 1){
         icono = "fa fa-check-circle";
         color = "#138C13";
      }
      if(row.status_repuve == 2){
        color = "#138C13";
        icono = "fa fa-check-circle";
      }
      if(row.status_repuve == 3){
        color = "#8C1333";
        icono = "fa fa-times-circle";
      }
      if(row.status_repuve == 4){
        color = "#8C1333";
        icono = "fa fa-times-circle";
      }

      return [
        '<a class="statusRepuve popover_repuve"  href="javascript:void(0)" title="Validación de Registro" align="center">',
        '<i class="'+icono+'" aria-hidden="true" style="color:'+color+';"></i>',
        '</a>  ',
      ].join('');
    }



    /* Asignacion de funciones a los elemento agreados anteriormente en operateFormatter*/
    window.operateEvents = {
      'click .detalleVehiculo': function (e, value, row, index) {
        $('#doctosModal').html('');
        // $("#identidicacionCont1").html('');
        console.log(row);
        if(row.busFGE==0 && row.busCNS==0 && row.busCNSKey==0){

          var ruta = "{{route('getDoctosVeh',':idVeh')}}";
          ruta = ruta.replace(':idVeh', row.id);
          datos = null;
          doctosVeh = peticionInfomacion('get',ruta,datos);
          console.log(doctosVeh);
          $('#modalDetalleVehiculo').modal('show');
          $('#id').val(row.id);
          $('#id2').val(row.id);
          $('#idplaca2').val(row.placas2);
          $('#folio').val(row.placas);
          $('#idEstados').val(row.idEstado).trigger('change');
          $('#idMarcas').val(row.idMarca).trigger('change');
          $('#idSubmarcas').val(row.idSubmarca).trigger('change');
          $('#modelos').val(row.modelo);
          $('#idColors').val(row.idColor).trigger('change');
          $('#numSeries').val(row.numSerie);
          $('#motors').val(row.numMotor);
          $('#carpeIdenty').html(row.llave_externa);
          $('#nrpv2').val(row.nrpv);
          $('#numeroeconomico2').val(row.numeroeconomico);
          $('#idClaseVehiculo2').val(row.idClaseVehiculo).trigger('change');
          $('#idSubclase2').val(row.idTipoVehiculo).trigger('change');
          $('#idUso2').val(row.idTipoUso).trigger('change');
          $('#idProcedencia2').val(2).trigger('change');
          $('#idAseguradora2').val(row.idAseguradora).trigger('change');
          $('#senasPartic2').val(row.senasPartic);
          $("#idDocCirculacion").val(row.idDocumentoCirculacion).trigger('change');
          $('#folio').val(row.folioDoctoCirc);

          if(doctosVeh.estado.length>0){
            //alert("aqui");
            for(i=0; i<doctosVeh.estado.length; i++){
              console.log("Valor de i");
               console.log(i);
            //   for (i = 0; i < respuesta.doctos.length ; i++) {
            // $('#doctos').append(addImg(respuesta.doctos[i].destino,i,respuesta.doctos[i].idSubCategoriaDocumento,respuesta.doctos[i].nombre));
              // console.log(doctosVeh.estado[i].destino);
              $('#doctosModal').append(addImg3(doctosVeh.estado[i].destino,doctosVeh.estado[i].nombre,i,doctosVeh.estado[i].idDocto));
            }
          }
        }else{
          swal({
                title: "Validación en proceso",
                text: "Los datos del vehiculo no están disponibles para edición en este momento, ya que se encuentran en proceso de validación por parte de la Consejo Estatal de Información(CNS).",
                icon: "warning",

          });
        }



      },
      'click .doctosVehiculo': function (e, value, row, index) {
        var identificador = row['id'];
        var respuesta;
        var totalDoctos = 0;
        var tempDoctos=0;
        $('ul#listaDoc').html('');
        //$("#progress").css("background-color", "");
        //$('#progress').html('');
        //$("#pro-bar").css("background-color", "");
        if(identificador!=''){
          var ruta = "{{route('getDoctosVeh',':idVeh')}}";
          ruta = ruta.replace(':idVeh', identificador);
          datos = null;
          respuesta = peticionInfomacion('get',ruta,datos);
          if (respuesta.estado.length > 0) {
            for(i=0; i<respuesta.estado.length; i++){
              //alert(i);
              if(respuesta.estado[i].idDocto!=tempDoctos){
                totalDoctos = totalDoctos+1;
                tempDoctos = respuesta.estado[i].idDocto;
              }else{
                totalDoctos = totalDoctos;
                tempDoctos = respuesta.estado[i].idDocto;
              }
              $('ul#listaDoc').append(addImg2(respuesta.estado[i].destino,respuesta.estado[i].nombre));
            }
            var galley = document.getElementById('galley');
            var viewer;
            viewer = new Viewer(galley, {
              url: 'data-original',
              hidden: function () {
                viewer.destroy();
              },
            });
            viewer.show();
            totalDoctos = Math.round(totalDoctos*100/2);
            $('#progress').css('width',totalDoctos+'%');
            $('#progress').append('Documentos adjuntos:  '+totalDoctos+'%');
            if(totalDoctos>=50 && totalDoctos<=99){
              $('#progress').css('background','orange');
            }else if(totalDoctos>=0 && totalDoctos<=49){
              $('#progress').css('background','red');
              $('#progress').css('width','100%').attr('aria-valuenow','100%');

            }else{
              $('#progress').css('background','green');
            }
          }else{
            confirm('Este registro no cuenta con imagenes asociadas');
          }
        }else{
          alert('no aplica');

        }

      },

      'click .reporteVehiculo': function (e, value, row, index) {
        var ruta ="{{route('generar-reportevehiculos',['idVeh'])}}";
        ruta = ruta.replace("idVeh",row.id);
        window.open(ruta, '_blank');
      },
      //porcentanje en barra
      'click .progress': function (e, value, row, index) {
        $('#porcentajeVehiculo');
        var datos = JSON.stringify(row);
        $("#pro-bar").css("background-color", "");
        $("#pro-bar").html('');
        var campos = ['placas','modelo','nrpv','numSerie','numMotor','procedencia','marca','submarca','color','tipoVehiculo','claseVehiculo','tipoUSo','aseguradora','senasPartic', 'placasExtranjera'];
        var acierto = 0;
        $('#pro-bar-'+row.id).html('');
        for(var i = 0; i < campos.length; i++)
        {
          if (row[campos[i]]!=""){
            acierto= acierto + 1;
          }
        }
        var p = (acierto * 100)/campos.length;


        $('#pro-bar-'+row.id).css('width', p+'%').attr('aria-valuenow', p);
        $('#pro-bar-'+row.id).text(Math.round(p)+'%');
        if(p>=0 && p<=49){
          $('#pro-bar-'+row.id).css('background','red');}
          else if(p>=50 && p<=99){
            $('#pro-bar-'+row.id).css('background','orange');
          }
          else{
            $('#pro-bar-'+row.id).css('background','green');
          }




        },
      'click .formatoVdiligencia': function (e, value, row, index) {
        var identificador = row['id'];
        if(identificador!=''){
          var ruta = "{{route('getDilidencia',':idVeh')}}";
          ruta = ruta.replace(':idVeh', identificador);
          datos = null;
          respuesta = peticionInfomacion('get',ruta,datos);
          var filename=respuesta.estado[0].destino;
          getDoc = "{{route('getImg',['filename'=>':ruta'])}}";
          getDoc = getDoc.replace(':ruta', filename);
          window.open(getDoc, '_blank');

        }
      },

      'click .statusRegistro': function (e, value, row, index) {
        var ruta = "{{route('validacionRobados',':idVeh')}}";
        ruta = ruta.replace(':idVeh', row.id);
        //ruta = ruta.replace(':tipoVal', 1);
        console.log(row);
        statusBus = peticionInfomacion('get',ruta,null);
        console.log('Datos BUS');
        console.log(statusBus[0].busFGE);

          if(statusBus[0].busCNS==2){
            $(this).popover({title: "Validación", content: "El registro se encuentra en PLATAFORMA NACIONAL", placement: "right"});
            $(this).popover('show');
          }
          if((statusBus[0].busCNS==3) || (statusBus[0].busCNS==0 && statusBus[0].busCNSKey==3)){
            $('#error').html('');
            var error = "";
            var ruta1 = "{{route('validacionRobadosData',':idVeh')}}";
            ruta1 = ruta1.replace(':idVeh', row.id);
            //ruta = ruta.replace(':tipoVal', 1);
            dataError = peticionInfomacion('get',ruta1,null);
            console.log(dataError);
            if (dataError.length > 0) {
              for(i=0; i<dataError.length; i++){
                //alert(i);
                error = error + '<p>'+dataError[i].DESC_ERROR+'</p>';

              }
              console.log(error);
              $('#error').append(error);
            }



            //Creal modal con errores
            $(this).popover('destroy');
            $('#modalValidacion').modal('show');
            $('#llaveExterna').val(dataError[0].LLAVE);
            //$('#comentario').val(dataPolicia.contenido.observacion);
            $('#id1').val(row.id);
            $("#idDocCirculacion1").attr('disabled','disabled');
            (row.placas == 0)?$('#idplaca1').attr('disabled','disabled'):'';
            $('#idplaca1').val(row.placas2);
            (row.idEstado == 0)?$('#idEstados1').attr('disabled','disabled'):'';
            $('#idEstados1').val(row.idEstado).trigger('change');
            (row.idSubmarca == 0)?$('#idMarcas1,#idSubmarcas1').attr('disabled','disabled'):'';
            $('#idMarcas1').val(row.idMarca).trigger('change');
            $('#idSubmarcas1').val(row.idSubmarca).trigger('change');
            (row.modelo == 0 )?$('#modelos1').attr('disabled','disabled'):'';
            $('#modelos1').val(row.modelo);
            (row.idColor == 0 )?$('#idColors1').attr('disabled','disabled'):'';
            $('#idColors1').val(row.idColor).trigger('change');
            (row.numSerie == 0 )?$('#numSeries1').attr('disabled','disabled'):'';
            $('#numSeries1').val(row.numSerie);
            (row.numMotor == 0 )?$('#motors1').attr('disabled','disabled'):'';
            $('#motors1').val(row.numMotor);
            (row.nrpv == 0 )?$('#nrpv1').attr('disabled','disabled'):'';
            $('#nrpv1').val(row.nrpv);
            (row.idTipoVehiculo == 0 )?$('#idClaseVehiculo1,#idSubclase1').attr('disabled','disabled'):'';
            $('#idClaseVehiculo1').val(row.idClaseVehiculo).trigger('change');
            $('#idSubclase1').val(row.idTipoVehiculo).trigger('change');
            (row.idTipoUso == 0 )?$('#idUso1').attr('disabled','disabled'):'';
            $('#idUso1').val(row.idTipoUso).trigger('change');
            (row.idProcedencia == 0 )?$('#idProcedencia1').attr('disabled','disabled'):'';
            $('#idProcedencia1').val(2).trigger('change');
            (row.idAseguradora == 0 )?$('#idAseguradora1').attr('disabled','disabled'):'';
            $('#idAseguradora1').val(row.idAseguradora).trigger('change');
            (row.senasPartic == 0 )?$('#senasPartic1').attr('disabled','disabled'):'';
            $('#senasPartic1').val(row.senasPartic);
            $('#numeroeconomico').val(row.numeroeconomico);

            $("#idDocCirculacion1").val(row.idDocumentoCirculacion).trigger('change');
            (row.folioDoctoCirc == 'NA' )?$('#folio1').attr('disabled','disabled'):'';
            $('#folio1').val(row.folioDoctoCirc);

          }
          if(statusBus[0].busFGE==1 && statusBus[0].busCNS ==0 && statusBus[0].busCNSKey ==0){
            $(this).popover({title: "Validación", content: "El registro se ha enviado al BUS", placement: "right"});
            $(this).popover('show');
          }
          if(statusBus[0].busFGE==1 && statusBus[0].busCNS==1){
            $(this).popover({title: "Validación", content: "El registro está en validación en la CNS", placement: "right"});
            $(this).popover('show');
          }
          // if(statusBus[0].busCNS==0 && statusBus[0].busCNSKey==3){
          //  icono = "fa fa-download";
          //  color = "#8C1333";
          // }

        setTimeout(function () {
          $('.btn_policia').popover('hide');
        }, 7000);
      },

      'click .statusRepuve': function (e, value, row, index) {

        if(row.status_repuve == 0){
          $(this).popover({title: "Validación", content: "El registro aun no se ha verificado en el Registro Público Vehicular", placement: "right"});
          $(this).popover('show');
        }
        if(row.status_repuve == 1){
          $(this).popover({title: "Validación", content: "El registro EXISTE en el padrón vehicular y tiene reporte de ROBO", placement: "right"});
          $(this).popover('show');
        }
        if(row.status_repuve == 2){
          $(this).popover({title: "Validación", content: "El registro EXISTE en el padrón vehicular y NO tiene reporte de ROBO", placement: "right"});
          $(this).popover('show');
        }
        if(row.status_repuve == 3){
          $(this).popover({title: "Validación", content: "El registro NO EXISTE en el padrón vehicular y NO tiene reporte de ROBO", placement: "right"});
          $(this).popover('show');
        }
        if(row.status_repuve == 4){
          $(this).popover({title: "Validación", content: "El registro NO EXISTE en el padrón vehicular y tiene reporte de ROBO", placement: "right"});
          $(this).popover('show');
        }

        setTimeout(function () {
          $('.popover_repuve').popover('hide');
        }, 7000);
      },

    };


      $( document ).ready(function() {
        var ruta = window.location.pathname.split('/');
        var rutaDefinida = $('#idReversa').attr('href');
      	var identyCarp=(ruta[4])?'/'+ruta[4]:"/dWlwai0y";
        $('#idReversa').attr('href',rutaDefinida+identyCarp);



        /* Configuracion de la tabla de registros de vehiculos */
        var ruta = "{{route('getregistros')}}/"+$('#idSecurity').val();
        // $('#idReversa').attr('href',ruta);
        //ruta =  $('#idSecurity').val();
        console.log('ruta --->');
        console.log(ruta);
        $('#table').bootstrapTable({
          search:  true,
          showColumns: true,
          showRefresh: true,
          url: ruta,
          buttonsAlign :'right',
          searchAlign :'right',
          idField: 'id',
          iconsPrefix: 'fa',
          pagination : true,
          pageSize: 5,
          columns: [{
            field: 'id',
            title: 'ID',
            sortable: true,
            visible: false
          }, {
            field: 'operate3',
            title: 'P.NACIONAL',
            events: operateEvents,
            formatter: operateFormatter3
          }, {
            field: 'status_repuve',
            title: 'REPUVE',
            formatter: operateFormatterRepuve,
              events: operateEvents,
          }, {
            field: 'entidadPlaca',
            title: 'Entidad de la placa',
            sortable: true
          }, {
            field: 'placas',
            title: 'Placas',
            sortable: true
          },{
            field: 'modelo',
            title: 'Modelo',
            sortable: true
          }, {
            field: 'nrpv',
            title: 'NRPV',
            sortable: true
          }, {
            field: 'numSerie',
            title: 'No. serie',
            sortable: true
          }, {
            field: 'numMotor',
            title: 'No. Motor',
            sortable: true
          }, {
            field: 'procedencia',
            title: 'Procedencia',
            sortable: true
          }, {
            field: 'marca',
            title: 'Marca',
            sortable: true
          }, {
            field: 'submarca',
            title: 'Submarca',
            sortable: true,
            visible: false
          }, {
            field: 'color',
            title: 'Color',
            sortable: true,
            visible: false
          }, {
            field: 'tipoVehiculo',
            title: 'Tipo',
            sortable: true,
            visible: false
          }, {
            field: 'claseVehiculo',
            title: 'Clase',
            sortable: true,
            visible: false
          }, {
            field: 'tipoUSo',
            title: 'Tipo de uso',
            sortable: true,
            visible: false
          }, {
            field: 'aseguradora',
            title: 'Aseguradora',
            sortable: true,
            visible: false
          }, {
            field: 'senasPartic',
            title: 'Señas Particulares',
            sortable: true,
            visible: false
          },{
            field: 'numeroeconomico',
            title: 'Número economico',
            sortable: true,
            visible: false
          }, {
            field: 'operate',
            title: 'Acciones',
            align: 'center',
            events: operateEvents,
            formatter: operateFormatter
          },
          {
            field: 'operate2',
            title: 'Porcentaje total',
            align: 'center',
            events: operateEvents,
            formatter: operateFormatter2
          }],
        });

        $('.btn_cerrar').click(function(){
          $('#form_validacion').find('input, textarea, button, select').removeAttr('disabled');
          $('#form_validacion').trigger("reset");
        });


        $("#idMarcas").change(function() {
          var identificador = $(this).val();
          var respuesta;
          $('#idSubmarcas').html('');
          if(identificador!=''){
            var ruta = "{{route('submarca',':marca')}}";
            ruta = ruta.replace(':marca', identificador);
            datos = null;
            respuesta = peticionInfomacion('get',ruta,datos);
            if (respuesta != null) {
              for(i=0; i<respuesta.estado.length; i++){
                $("#idSubmarcas").append("<option value='"+respuesta.estado[i].id+"'> "+respuesta.estado[i].nombre+"</option>");
              }
              if(respuesta.estado.length > 0 ) {
                localStorage.setItem("idSubmarcas", respuesta.estado[0].id);
              }
            }else{
              alert('Error de servidor');
            }
          }else{
          }
        });

        $("#idMarcas1").change(function() {
          var identificador = $(this).val();
          var respuesta;
          $('#idSubmarcas1').html('');
          if(identificador!=''){
            var ruta = "{{route('submarca',':marca')}}";
            ruta = ruta.replace(':marca', identificador);
            datos = null;
            respuesta = peticionInfomacion('get',ruta,datos);
            console.log(respuesta);
            if (respuesta != null) {
              for(i=0; i<respuesta.estado.length; i++){
                $("#idSubmarcas1").append("<option value='"+respuesta.estado[i].id+"'> "+respuesta.estado[i].nombre+"</option>");
              }
              if(respuesta.estado.length > 0 ) {
                localStorage.setItem("idSubmarcas1", respuesta.estado[0].id);
              }
            }else{
            }
          }else{
          }
        });

        $("#idClaseVehiculo2").change(function() {
          var identificador = $(this).val();
          var respuesta;
          $('#idSubclase2').html('');
          if(identificador!=''){
            var ruta = "{{route('subclase',':clasevehiculo')}}";
            ruta = ruta.replace(':clasevehiculo', identificador);
            datos = null;
            respuesta = peticionInfomacion('get',ruta,datos);
            if (respuesta != null) {
              for(i=0; i<respuesta.estado.length; i++){
                $("#idSubclase2").append("<option value='"+respuesta.estado[i].id+"'> "+respuesta.estado[i].nombre+"</option>");
              }
              if (identificador == 99) {
                $('#idSubclase2').val(999).trigger('change');
              }
              if (respuesta.estado.length > 0) {
                localStorage.setItem("idSubclase2", respuesta.estado[0].id);
              }
            }else{
              alert('Error de servidor');
            }
          }else{
            alert('no aplica');

          }
        });

        $("#idClaseVehiculo1").change(function() {
          var identificador = $(this).val();
          var respuesta;
          $('#idSubclase1').html('');
          if(identificador!=''){
            var ruta = "{{route('subclase',':clasevehiculo')}}";
            ruta = ruta.replace(':clasevehiculo', identificador);
            datos = null;
            respuesta = peticionInfomacion('get',ruta,datos);
            if (respuesta != null) {
              for(i=0; i<respuesta.estado.length; i++){
                $("#idSubclase1").append("<option value='"+respuesta.estado[i].id+"'> "+respuesta.estado[i].nombre+"</option>");
              }
              if (respuesta.estado.length > 0) {
                localStorage.setItem("idSubclase1", respuesta.estado[0].id);
              }
            }else{
              alert('Error de servidor');
            }
          }else{
            alert('no aplica');

          }
        });



        var idmarca = $("#idMarcas").val();

        if (idmarca > 0) {
          var ruta = "{{route('submarca',':marca')}}";
          ruta = ruta.replace(':marca', idmarca);
          datos = null;
          respuesta = peticionInfomacion('get',ruta,datos);

          if (respuesta != null) {
            for(i=0; i<respuesta.estado.length; i++){
              $("#idSubmarcas").append("<option value='"+respuesta.estado[i].id+"'> "+respuesta.estado[i].nombre+"</option>");
            }
            $('#idSubmarcas').val(localStorage.idSubmarca).trigger("change");
          }
        }

        var idClaseVehiculo = $("#idClaseVehiculo2").val();
        if (idClaseVehiculo > 0) {
          var ruta = "{{route('subclase',':clasevehiculo')}}";
          ruta = ruta.replace(':clasevehiculo', idClaseVehiculo);
          datos = null;
          respuesta = peticionInfomacion('get',ruta,datos);
          if (respuesta != null) {
            for(i=0; i<respuesta.estado.length; i++){
              $("#idSubclase2").append("<option value='"+respuesta.estado[i].id+"'> "+respuesta.estado[i].nombre+"</option>");
            }
          }
          $('#idSubclase2').val(localStorage.idSubclase).trigger("change");
        }
      });

    $('#form_registro').find('input, textarea, button, select').attr('disabled','disabled');
    $('#guardar_cambios').attr('disabled','disabled');;
    $('#btn_editar').bind('click', function () {
      if ($('#btn_editar').is(':checked')) {
        $('#form_registro').find('input, textarea, button, select').removeAttr('disabled','disabled');
        $('#guardar_cambios').removeAttr('disabled','disabled');
      } else {
        $('#form_registro').find('input, textarea, button, select').attr('disabled','disabled');
        $('#guardar_cambios').attr('disabled','disabled');
      }
    });

    $('#numSeries').focusout(function(){
      var numSerie = $('#numSeries').val();
      if(numSerie!=""){
        var ruta ="{{route('niv',['num_serie'])}}";
        ruta = ruta.replace("num_serie",numSerie);
        var respuesta = peticionInfomacion('get',ruta);
        (respuesta.request.status=='error')? toastr.error(respuesta.request.mensaje, "ERROR",{"preventDuplicates": true}):toastr.success(respuesta.request.mensaje, "CORRECTO",{"preventDuplicates": true});

      }
    });

    $('#guardar_cambios').bind('click', function () {
      $.ajax({
        url : "{{route('updateRegistro')}}",
        data : $('#form_registro').serialize(),
        type : 'POST',
        success : function(json) {

          swal({
            title: "Datos actualizados correctamente",
            icon: "success",

          });
          $('#table').bootstrapTable('refresh');
          $('#modalDetalleVehiculo').modal('toggle');
          $('#form_registro').find('input, textarea, button, select').attr('disabled','disabled');
          $('#guardar_cambios').attr('disabled','disabled');
          $("input[type='checkbox']").prop("checked", false).attr("checked", false).removeAttr("checked");
          $('#guardar_cambios').attr('disabled','disabled');
        },


        error : function(xhr, status) {
          swal({
            title: "Error al guardar cambios",
            icon: "error",
          });
        },
        complete : function(xhr, status) {
        }
      });
    });

    $('#guardar_validacion').bind('click', function () {
      $('#form_validacion').find('input, textarea, button, select').removeAttr('disabled');
      $.ajax({
        url : "{{route('actualizarValidacion')}}",
        data : $('#form_validacion').serialize(),
        type : 'POST',
        success : function(json) {
          swal({
            title: "Datos actualizados correctamente",
            icon: "success",

          });
          $('#table').bootstrapTable('refresh');
          $('#modalValidacion').modal('toggle');
          $('#guardar_validacion').attr('disabled','disabled');
        },


        error : function(xhr, status) {
          swal({
            title: "Error al guardar cambios",
            icon: "error",
          });
        },
        complete : function(xhr, status) {
        }
      });
    });

    $("#idDocumentoCirculacion").change(function(){
      $('#placas').val('');
      $('#folioDoctoCirc').val('');
      var selec = $("#idDocumentoCirculacion").val();
      if(selec ==  1 || selec ==5) {
        if(selec == 1) {
          $("#placa").attr('maxlength','7');
        } else {
          $("#placa").attr('maxlength','10');
        }
        $('#divPlaca').css('display','');
        $('#divFolio').css('display','none');
      } else {
        $('#divPlaca').css('display','none');
        $('#divFolio').css('display','');
      }
    });


    function adjuntar(id){
      $('#iddiligencia').val(id);
      $('#resp_load_1').hide();
      $('#resp_load_2').hide();
      $('#modaladjuntardiligencia').modal('show');

    }

    $('#docDiligenciaUp').submit(function(event){
      event.preventDefault();

      var formData = new FormData(this);
      $.ajax({
        url : "{{route('docUpDiligencia')}}",
        data : formData,
        type : 'POST',
        cache: false,
        contentType : false,
        processData : false,
        success : function(json) {

          $('#resp_load_1').show();
          $('#resp_load_2').hide();
        },

        error : function(xhr, status) {
          console.log(xhr);
          $('#resp_load_2').show();
          $('#resp_load_1').hide();
        },
        complete : function(xhr, status) {
        }
      });

      $("#diligenciaFormaro").val('').trigger('change');
      $("#anexo").on('click', function() {
        $('#modaladjuntardiligencia').modal('hide');
      });
    });

    </script>
