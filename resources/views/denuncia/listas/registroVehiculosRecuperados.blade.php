
@section('title')
Registros
@endsection
<!--menu-->

<!-- Contenido -->


<link rel="stylesheet" href="{{ asset('css/denuncia/imagen/fileinput.min.css')}}">
<link rel="stylesheet" href="{{ asset('css/denuncia/imagen/fileinput.css')}}">
<link rel="stylesheet" href="{{ asset('themes/explorer/theme.css')}}">


<style>
.pictures {
  margin: 0;
  padding: 0;
  list-style: none;
}
.pictures > li {
  float: left;
  width: 33.3%;
  height:33.3%;
  margin: 0 -1px -1px 0;
  border: 1px solid transparent;
  overflow: hidden;
}
.pictures > li > img {
  width: 100%;
  cursor: -webkit-zoom-in;
  cursor: zoom-in;
}

.switch {
  position: relative;
  display: inline-block;
  width: 45px;
  height: 24px;
}

.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 16px;
  width: 16px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(20px);
  -ms-transform: translateX(20px);
  transform: translateX(20px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 17px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>

<div class="card">
  <div class="card-header">
    <h5>  Registros de vehiculos recuperados</h5>
  </div>
  <div class="card-body">
    <ul class="nav nav-pills">
     <li style="padding-right: 10px"><a href="{{route('showVehRecuperados')}}" class="nav-link  btn-outline-dark " ><span class="glyphicon glyphicon-home"></span> Registro de vehiculo recuperado </a></li>
     <li class="active"><a href="#" class="nav-link active btn-outline-dark " ><span class="glyphicon glyphicon-user"></span>Vehiculos recuperados</a></li>
   </ul>
   <table id="table" class="table table-bordered table-hover table-responsive-sm"></table>
 </div>
</div>
<div class="row">
  {{-- Diseño del modal para mostrar los detalles del vehiculo--}}
  <div id="modalDetalleVehiculo" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Detalle del vehículo recuperado</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="form_registro">
            {!!Form::text('id',null,['id' => 'id', 'hidden'])!!}
            <div class="row">
              <div class="col">
                <div class="form">
                  {!!Form::label('permiso', 'Documento de circulación')!!}
                </div>
                <div class="input-group" style="width:100%">
                  <select class="templatingSelect10 form-control form-control-sm"  id="idDocCirculacion2"  name="idDocCirculacion2" >
                    <option selected="selected" value=""> </option>
                    @foreach($documentosCirculacion as $documento)
                    <option value="{{$documento->id}}" >{{$documento->nombre}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col">
                <div id="divPlaca">
                  <div class="form">
                    {!!Form::label('placa', 'Placa')!!}
                  </div>
                  <div class="input-group" style="width:100%">
                    {!!Form::text('placa',null,['class'=>'form-control form-control-sm', 'null' ,'id' => 'idplaca', 'placeholder' => 'HMZ2398 - Ejemplo' , 'maxlength'=>'7',  'style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation-regexp'=>'^((([A-ZÑ]|[\d])+)){5,7}$'])!!}
                  </div>
                </div>
                <div id="divFolio" style = 'display: none'>
                  <div class="form">
                    {!!Form::label('folio', 'Folio')!!}
                  </div>
                  <div class="input-group" style="width:100%">
                    {!!Form::text('folio',null,['class'=>'form-control form-control-sm', 'id' => 'folio', 'onkeyup'=>'mayus(this);', 'maxlength'=>'10'])!!}
                  </div>
                </div>
              </div>
              <div class="col">
                <div class="form-group" >
                  {!!Form::label('idEstado', 'Entidad federativa')!!}
                  <select  class="templatingSelect2 form-control form-control-sm " style="width:75%"  id="idEstados" name="idEstados" required>
                    <option value="" selected="selected" > </option>
                    @foreach($estados as $estado)
                    <option  value="{{$estado->id}}" >{{$estado->nombre}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col">
                <div class="form-group">
                  {!!Form::label('idMarca', 'Marca')!!} <br>
                  <select  class="templatingSelect3 form-control form-control-sm" style="width:100%" id="idMarcas" name="idMarcas" required>
                    <option value="" selected="selected" > </option>
                    @foreach($marcas as $marca)
                    <option  value="{{$marca->id}}">{{$marca->nombre}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col">
                {!!Form::label('idSubmarca', 'Submarca')!!}<br>
                <select  class="idSubmarcas form-control form-control-sm" style="width:100%"  id="idSubmarcas" name="idSubmarcas" required>
                  <option value="" required selected="selected"></option>
                </select>
              </div>
              <div class="col">
                {!!Form::label('modelo', 'Modelo')!!}
                <input class="form-control form-control-sm" style="width:100%" value="{{date('Y')}}" min="1885" max="{{date('Y')+1}}" name="modelos" type="number" required id="modelos">
              </div>
            </div>
            <div class="row">
              <div class="col">
                {!!Form::label('idColor','Color')!!} <br>
                <select  class="templatingSelect5 form-control form-control-sm" style="width:100%" id="idColors" name="idColors" required>
                  <option selected="selected" value=""> </option>
                  @foreach($colores as $color)
                  <option value="{{$color->id}}">{{$color->nombre}}</option>
                  @endforeach
                </select>
              </div>
              <div class="col">
                {!!Form::label('numSeries','Número de serie')!!}
                {!!Form::text('numSeries',null, ['class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el número de serie','maxlength'=>'17','style'=>'text-transform: uppercase;',  'data-validation'=>'custom', 'data-validation-regexp'=>'^([A-Z\d]{3}([A-Z\d]{5})([A-Z\d]{3})([0-9]{6}))$'] )!!}
              </div>
              <div class="col">
                {!!Form::label('motors','Número de motor')!!}
                {!!Form::text('motors',null, ['class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el número de motor', 'style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation-regexp'=>'^((([A-Z]|[\d])+)+){10}$' ] )!!}
              </div>
            </div>
            <div class="row">
              <div class="col">
                {!!Form::label('nrpv2','NRPV')!!}
                {!!Form::text('nrpv2',null, ['class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el NRPV', 'style'=>'text-transform: uppercase;','required', 'maxlength'=>'20'] )!!}
              </div>
              <div class="col">
                {!!Form::label('idClaseVehiculo','Tipo de vehículo')!!}
                <select  class="templatingSelect7 form-control form-control-sm" style="width:100%"  id="idClaseVehiculo2" name="idClaseVehiculo2" required>
                  @foreach($clasevehiculos as $clasevehiculo)
                  <option value="{{$clasevehiculo->id}}">{{$clasevehiculo->nombre}}</option>
                  @endforeach
                </select>
              </div>
              <div class="col">
                {!!Form::label('idSubclase','Subtipo de vehículo')!!}
                <select  class="templatingSelect1 form-control form-control-sm" style="width:100%" id="idSubclase2" name="idSubclase2" required>
                  <option selected="selected" value=""> </option>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="col">
                {!!Form::label('idUso','Tipo de uso')!!}
                <select  class="templatingSelect8 form-control form-control-sm" style="width:100%" id="idUso2" name="idUso2" required>
                  <option selected="selected" value=""> </option>
                  @foreach($tipousos as $tipouso)
                  <option value="{{$tipouso->id}}">{{$tipouso->nombre}}</option>
                  @endforeach
                </select>
              </div>
              <div class="col">
                {!!Form::label('idProcedencia','Procedencia')!!} <br>
                <select style="width:100%" class="templatingSelect9 form-control form-control-sm"  id="idProcedencia2" name="idProcedencia2" required>
                  <option value="" selected>Seleccione</option>
                  <option value="2" >EXTRANJERO</option>
                  <option value="1">NACIONAL</option>
                </select>
              </div>
              <div class="col">
                {!!Form::label('idAseguradora','Aseguradora')!!}
                <select class="templatingSelect6 form-control form-control-sm" style="width:100%" required id="idAseguradora2" name="idAseguradora2" >
                  <option selected="selected" value=""> </option>
                  @foreach($aseguradoras as $aseguradora)
                  <option value="{{$aseguradora->id}}">{{$aseguradora->nombre}}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-lg-8 col-md-8">
                {!!Form::label('idResguardo','Depósito de resguardo')!!}
                <select class="templatingSelect12 form-control form-control-sm" required id="idResguardo2" name="idResguardo2">
                  <option  selected="selected" value=""> </option>
                  @foreach($depositos as $deposito)
                  <option value="{{$deposito->id}}">{{$deposito->nombre}}</option>
                  @endforeach
                </select>
              </div>

            </div>
            <div class="row">
              <div class="col">
                {!!Form::label('senasPartic','Señas particulares')!!}
                <textarea class="form-control form-control-sm" placeholder="Ingrese las señas particulares" rows="3" name="senasPartic2" cols="50" style="text-transform: uppercase;" required id="senasPartic2"></textarea>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12 ">
                <div class="form-group">
                  {!!Form::label('observacion2','Observaciones')!!}
                  <textarea class="form-control form-control-sm" placeholder="Coloque las observaciones aquí" rows="3" name="observacion" cols="50" style="text-transform: uppercase;" required id="observacion2"></textarea>
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>

  {{-- Diseño del modal para mostrar los documentos cargados del vehiculo--}}
  <div class="modal fade bd-example-modal-lg" id="modaldoctosVehiculo2" role="dialog" aria-labelledby="modalLabel" tabindex="-1" style="scroll; max-height:85%;">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modalLabel">Documentos adjuntos del veh&iacute;culo</h5>
          <button type="button" id="btnCerrar" onclick="limpiar()" class="btn btn-default" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div align="center">
            <div class="btn-group btn-group-toggle" data-toggle="buttons" id="selector2">
              <label class="btn btn-secondary form-check-label" onclick="sectionShow2()">
                <input type="radio" name="img" id="option1" autocomplete="off" value="archivo2">
                <i class="fa fa-folder-open"></i>
                Archivos
              </label>
              <label id="divBtnCamara" class="btn btn-secondary form-check-label " onclick="sectionShow2()">
                <input type="radio" name="img" id="option2" autocomplete="off" value="camara2" >
                <i class="fa fa-camera-retro"></i>
                C&aacute;mara
              </label>
            </div>
            <br>
            <div id="div3" style="display:none;" >
              <br>
              <div class="file-loading">
                <input id="cargarImagen" type="file"  multiple name="cargarImagen[]">
                <input type="text" name="loquesea" value="1" id="loquesea">
              </div>
            </div>

            <div id="div4" style="display:none;" >
              <div class="card-body">
                <div class="" id="capturaimg_mini"></div>
                <div id="web_cam_section" class="alert alert-danger" v-if="form.errors.has('photo')">
                  <div id="my_camera">HOla</div>
                </div>
                <button type="button" id="take_p" class="btn btn-primary" :disabled="form.busy" onClick="take_snapshot()">Capturar</button>
                <div id="profile-photo-camera-preview">
                  <div id="results" class="row">
                    <!--Carga de array de imagenes-->
                  </div>
                </div>
                {!!form::open(['id'=>'pictures_cam_load', 'method' => 'POST']) !!}
                {{ Form::hidden('identyCar', null , array('id' => 'invisible_id')) }}
                <div id="elementosForm_WEB">
                  <!--Array de imagenes-->
                </div>
                <div id="mensaje_carga">
                </div>
                <div>
                  {!!form::submit('Cargar Todas las imagenes', array('id'=>'carga','class' => 'btn btn-secondary')) !!}
                </div>
                {!! Form::close() !!}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="galley" style="display:none">
    <ul class="pictures" id="listaImagenes">
    </ul>
  </div>


{{-- MODAL VALIDACION DE BUS --}}

<div id="modalValidacion" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Detalle del vehículo recuperado</h5>
        <button type="button" class="close btn_cerrar" data-dismiss="modal" id="cerrar" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form_validacion">
          {!!Form::text('id1',null,['id' => 'id1', 'hidden'])!!}
          {!!Form::text('llaveExterna',null,['id' => 'llaveExterna', 'hidden'])!!}
          <div class="row">
            <div class="col-lg col-md">
              <p>FAVOR DE VERIFICAR LOS DATOS DE ACUERDO A LAS SIGUIENTES OBSERVACIONES</p><hr>
              <div id="error" style="color:red;border-color: red;border-radius: 3px;">

              </div>
              {{-- {!!Form::label('Comentario Entidad','Comentario relacionado a los datos erróneos')!!}
              <textarea class="form-control form-control-sm" placeholder="Comentario" rows="3" name="comentario" cols="50" style="text-transform: uppercase;" required id="comentario" readonly="readonly"></textarea> --}}
            </div>
          </div>
        <div class="row">
            <div class="col-lg-4 col-md-4">
              <div class="form-group">
                {!!Form::label('idEstado1', 'Entidad federativa')!!}
                <select  class="estado form-control form-control-sm"  id="idEstado1" name="idEstado1" required onchange="if(this.value!=0) {document.getElementById('idMunicipio1').disabled = false}">
                  <option value="" selected="selected" > </option>
                  @foreach($estados as $estado1)
                  <option  value="{{$estado1->id}}" >{{$estado1->nombre}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-lg-4 col-md-4">
              <div class="form-group">
                {!!Form::label('idMunicipio1', 'Municipio')!!}
                <input type="text" id="text_municipio" name="text_municipio2" hidden>
                <select class="municipio form-control form-control-sm" required id="idMunicipio1" name="idMunicipio1" disabled>
                  <option value="" selected="selected"></option>
                </select>
              </div>
            </div>
            <div class="col-lg-4 col-md-4">
            <div class="form-group">
              {!!Form::label('idCP','Codigo Postal')!!}
              {!!Form::text('cp1',null, ['id'=>'cp1','class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el código postal', 'required','minlength'=>'1','minlength'=>'5','maxlength'=>'5','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'number'] )!!}
            </div>
          </div>
          </div>
          <div class="row">
            <div class="col-lg-4 col-md-4">
              <div class="form-group">
                {!!Form::label('idCalle','Calle')!!}
                {!!Form::text('calle1',null, ['id'=>'calle1','class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese la calle', 'required','style'=>'text-transform: uppercase;'] )!!}
              </div>
            </div>
            <div class="col-lg-4 col-md-4">
              <div class="form-group">
                {!!Form::label('idColonia','Colonia')!!}
                {!!Form::text('colonia1',null, ['id'=>'colonia1','class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese la colonia', 'required','style'=>'text-transform: uppercase;'] )!!}
              </div>
            </div>
            <div class="col-lg-2 col-md-2">
              <div class="form-group">
                {!!Form::label('numExt','Número Externo')!!}
                {!!Form::text('numExt1',null, ['id'=>'numExt1','class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el número externo', 'required','minlength'=>'1','maxlength'=>'10','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'alphanumeric'] )!!}
              </div>
            </div>
            <div class="col-lg-2 col-md-2">
              <div class="form-group">
                {!!Form::label('numInt','Número Interno')!!}
                {!!Form::text('numInt1',null, ['id'=>'numInt1' ,'class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el número interno', 'minlength'=>'1','maxlength'=>'10','data-validation'=>'custom','style'=>'text-transform: uppercase;'] )!!}
              </div>
            </div>
            {!!Form::text('idSubCatIdenti',null, ['hidden','id'=>'idSubCatIdenti'] )!!}
            {!!Form::text('idSubCatFact',null, ['hidden','id'=>'idSubCatFact'] )!!}
          </div>
          <div class="row">
            <div class="col-lg-4 col-md-4">
            <div class="form-group">
              {!!Form::label('idFechaEntrega1','Fecha de entrega')!!}
              <input class="form-control form-control-sm" type="date" value="2011-08-19" id="idFechaEntrega1" name="idFechaEntrega1" required>
            </div>
          </div>
           <div class="col-lg-4 col-md-4">
            <div class="form-group">
              {!!Form::label('idHoraEntrega1','Hora de entrega')!!}
              <input class="form-control form-control-sm" type="time" value="13:45:00" id="idHoraEntrega1" name="idHoraEntrega1" required>
            </div>
          </div>
            <div class="col-lg-4 col-md-4">
              <div class="form-group">
                {!!Form::label('idResguardo1','Depósito de resguardo')!!}
                <select class="templatingSelect12 form-control form-control-sm" required id="idResguardo1" name="idResguardo1">
                  <option  selected="selected" value=""> </option>
                  @foreach($depositos as $deposito1)
                  <option value="{{$deposito1->id}}">{{$deposito1->nombre}}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12 col-md-12 ">
              <div class="form-group">
                {!!Form::label('observacion1','Observaciones')!!}
                <textarea class="form-control form-control-sm" placeholder="Coloque las observaciones aquí" rows="3" name="observacion1" cols="50" style="text-transform: uppercase;" required id="observacion1"></textarea>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" id="guardar_validacion" >Guardar</button>
        <button type="button" class="btn btn-secondary btn_cerrar" id="cerrar" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>


  <script src="{{asset('js/viewer.js')}}"></script>
  <script src="{{ asset('js/denuncia/carga2.js') }}"></script>
  <script src="{{ asset('js/denuncia/plugins/sortable.js')}}" ></script>
  <script src="{{ asset('js/denuncia/fileinput.js')}}"></script>
  <script src="{{ asset('js/denuncia/locales/fr.js')}}" ></script>
  <script src="{{ asset('js/denuncia/locales/es.js')}}" ></script>
  <script src="{{ asset('themes/explorer-fa/theme.js')}}"></script>
  <script src="{{ asset('themes/explorer/theme.js')}}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" type="text/javascript"></script>
{{--   <script type="text/javascript" src="{{ asset('js/camara/camara_validacion.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/webcam.min.js') }}"></script>
 --}}
  <style media="screen">
  #profile-photo-camera-preview,
  #profile-photo-camera-preview video {
    width: 100% !important;
    height: auto !important;
    min-width: 100px;
    min-height: 100px;
  }
  .spa_sec{
    padding: 0 1em;
  }
  #my_camera{
    display: block;
    margin: auto;
  }
  #web_cam_section{
    background-color: #717171;
    border-color: #424242;
  }
  #cuerpo{
    display: block;
    margin: auto;
  }
  #ife_mini,#fact_mini{
    padding-bottom: 1em;
  }
</style>
<!--Modal adjuntar diligencia -->
<div id="modaladjuntardiligencia" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Carga de archivos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="alert alert-success" role="alert" id="resp_load_1" style="display:none;">
          <p>Se anexo correctamente</p>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="alert alert-danger" role="alert" id="resp_load_2" style="display:none;">
          <p>No se ha seleccionado ningun archivo</p>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        {!! Form::open(['route' => 'docUpDiligencia','id'=>'docDiligenciaUp']) !!}
        <div class="form-group">
          <label for="exampleInputEmail1">Selecciona la diligencia asociada</label>
          {{ Form::file('thefile', ['class' => 'field form-control','id' => 'diligenciaFormaro', 'name' => 'diligenciaFormaro']) }}
        </div>
        <input id="iddiligencia" name="iddiligencia" type="text" hidden>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar</button>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
{{-- Diseño del modal para mostrar el porcentaje--}}
<div id="modalporcentajeVehiculo" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Porcentaje de carga de documentos del vehículo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="contenido">
          <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width: 2%;" id="progress2">
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
{!! Form::text('idVeh', null, ['class' => 'form-control','id'=>'idVeh','hidden']) !!}


<script type="text/javascript">



  Webcam.set({
    width: 320,
    height: 240,
    image_format: 'jpeg',
    jpeg_quality: 90
  });

  function sectionShow2(){
    $('.sec_upload').toggle('slow');
  }

  function setupCam() {
    Webcam.reset();
    Webcam.attach( '#my_camera');
  }

  $('#pictures_cam_load').submit(function(event){
    event.preventDefault();
    var ruta = "{{route('webcam-recuperados-load')}}";
    var formData = new FormData(this);
    $.ajax({
      url : "{{route('webcam-recuperados-load')}}",
      data : formData,
      type : 'POST',
      cache: false,
      contentType : false,
      processData : false,
      success : function(msj) {
        $('#mensaje_carga').html('<a class="badge badge-success">'+msj+'</a>');
        location.reload();
      },
      error : function(xhr, status) {
      },
      complete : function(xhr, status) {
      }
    });
  });
  var cap="";
  function take_snapshot() {
    Webcam.snap( function(data_uri) {
      document.getElementById('results').innerHTML +='<div class="col-md-3"><img class="img-thumbnail" name="pictureWEBcam[]" src="'+data_uri+'"/></div>';
      $('#elementosForm_WEB').prepend('<input class="img-thumbnail" type="hidden" name="pictureWEBcam[]" value="'+data_uri+'"/>');
    } );
  }
  function close_cam(act){
    if(act){
      $("#results").html('');
      $('#facturaCont').show();
      $('#identidicacionCont').show();
    }
    Webcam.reset();
  }
  function limpiar (){
    location.reload();
  }
  function elemento (nombre, valor) {
    var p = $('<p>').text(nombre+': '+valor);
    return p;
  }
  function addImg2 (ruta,nombre) {
    getImg = "{{route('getImg',['filename'=>':ruta'])}}";
    getImg = getImg.replace(':ruta', ruta);
    var element = '<li><img data-original="'+getImg+'" src="'+getImg+'" alt="'+nombre+'"><p>'+nombre+'</p></i>';
    return element;
  }
  window.addEventListener('DOMContentLoaded', function () {
    var galley = document.getElementById('galley');
    var viewer;
    $('#modaldoctosVehiculo').on('shown.bs.modal', function (e) {
      // WARNING: should ignore Viewer's `shown` event here.
      if(e.namespace === 'bs.modal') {
        viewer = new Viewer(galley, {
          url: 'data-original',
        });
      }
    }).on('hidden.bs.modal', function (e) {
      // WARNING: should ignore Viewer's `hidden` event here.
      if(e.namespace === 'bs.modal') {
        viewer.destroy();
      }
    });
  });
  /* Creacion de los elementos extras a mostrar*/
  function operateFormatter(value, row, index) {
    var ruta = "{{route('existenImagenes',':idVeh')}}";
    ruta = ruta.replace(':idVeh', row.id);
    respuesta = peticionInfomacion('get',ruta,null);
    if(respuesta) {
      return [
      '<p style="width:75px;">',
      '<a class="detalleVehiculo btn btn-link" href="javascript:void(0)" title="Detalle del vehiculo">',
      '<i class="fa fa-info" aria-hidden="true"></i>',
      '</a>  ',
      '<a class="cargaImgVehiculo btn  btn-link" data-target="#modal" href="javascript:void(0)" title="Documentos del vehiculo">',
      '<i class="fa fa-file" aria-hidden="true"></i>',
      '</a>  ',
      '</p>',
      '<p style="width:77px;">',
      '<a class="altaVehiculo btn btn-link" data-target="#modal" href="javascript:void(0)" title="Reporte alta de vehiculos">',
      '<i class="fa fa-print" aria-hidden="true"></i>',
      '</a>  ',
      '<a class="imagenesrecu btn btn-link"  data-target="#modal" href="javascript:void(0)" title="Imagenes de vehiculos recuperados">',
      '<i class="fa fa-eye" aria-hidden="true"></i>',
      '</a>  ',
      '</p>',
      ].join('');
    } else {
      return [
      '<p style="width:75px;">',
      '<a class="detalleVehiculo btn btn-link" href="javascript:void(0)" title="Detalle del vehiculo">',
      '<i class="fa fa-info" aria-hidden="true"></i>',
      '</a> ',
      '<a class="cargaImgVehiculo btn btn-link" data-target="#modal" href="javascript:void(0)" title="Documentos del vehiculo">',
      '<i class="fa fa-file" aria-hidden="true"></i>',
      '</a> ',
      '</p>',
      '<p >',
      '<a class="altaVehiculo btn btn-link" data-target="#modal" href="javascript:void(0)" title="Reporte alta de vehiculos">',
      '<i class="fa fa-print" aria-hidden="true"></i>',
      '</a>  ',
      '</p>',

      ].join('');
    }
  }

  function operateFormatter2(value, row, index) {
     var datos = JSON.stringify(row);
        var campos = ['placas' || 'permiso','modelo','nrpv','numSerie','numMotor','procedencia','marca','submarca','color','tipoVehiculo','claseVehiculo','tipoUSo','aseguradora','senasPartic'];
        var acierto = 0;
        for(var i = 0; i < campos.length; i++)
        {
          if (row[campos[i]]!=""){
            acierto= acierto + 1;
          }
        }
        var p = (acierto * 100)/campos.length;
        p = Math.round(p);


        $('#pro-bar-'+row.id).css('width', p+'%').attr('aria-valuenow', p);
        $('#pro-bar-'+row.id).text(Math.round(p)+'%');
        if(p>=0 && p<=49){
          color = 'red';
        }
        else if(p>=50 && p<=99){
            color = 'orange';
          }
          else{
            color = 'green';
          }
        return [
        '<div class="progress" id="porcentajeVehiculo">',
        '<div class="progress-bar" id="pro-bar-'+row.id+'" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; background-color:'+color+'; width: '+p+'%;">'+p+'%</div>',
        '</div>',
      ].join('');
  }

  function operateFormatter3(value, row, index) {
    var color = "";
    var icono = "";
    var ruta = "{{route('validacionRecuperados',':idVeh')}}";
    ruta = ruta.replace(':idVeh', row.id);
    //ruta = ruta.replace(':tipoVal', 1);
    console.log(ruta);
    statusBus = peticionInfomacion('get',ruta,null);
    console.log('Datos BUS');
    console.log(statusBus[0].busCNS);
    if(statusBus[0].busCNS==2){
       icono = "fa fa-upload";
       color = "#138C13";
    }
    if(statusBus[0].busCNS==3){
        //alert('entra aqui');
       icono = "fa fa-download";
       color = "#8C1333";
    }
    if(statusBus[0].busFGE==0 && statusBus[0].busCNS ==0){
      color = "#BDBDBD";
      icono = "fa fa-circle-o";
    }
    if(statusBus[0].busFGE==1 && statusBus[0].busCNS ==0){
      color = "#DDA61D";
      icono = "fa fa-exclamation-circle";
    }
    if(statusBus[0].busFGE==1 && statusBus[0].busCNS==1){
       color = "#DDA61D";
       icono = "fa fa-upload";
    }

    if(statusBus[0].busCNS==0 && statusBus[0].busCNSKey==3){
       icono = "fa fa-download";
       color = "#8C1333";
    }

    return [
    '<a class="statusRegistro btn_status"  href="javascript:void(0)" title="Validación de Registro" align="center">',
    '<i class="'+icono+'" aria-hidden="true" style="color:'+color+';"></i>',
    '</a>  ',
    ].join('');
  }
  /* Asignacion de funciones a los elemento agreados anteriormente en operateFormatter*/
  window.operateEvents = {
    'click .detalleVehiculo': function (e, value, row, index) {
      $('#modalDetalleVehiculo').modal('show');
      $('#id').val(row.id);
      $('#idplaca').val(row.placas2);
      $('#folio').val(row.placas2);
      $('#idEstados').val(row.idEstado).trigger('change');
      $('#idMarcas').val(row.idMarca).trigger('change');
      $('#idSubmarcas').val(row.idSubmarca).trigger('change');
      $('#modelos').val(row.modelo);
      $('#idColors').val(row.idColor).trigger('change');
      $('#numSeries').val(row.numSerie);
      $('#motors').val(row.numMotor);
      $('#nrpv2').val(row.nrpv);
      $('#idClaseVehiculo2').val(row.idClaseVehiculo).trigger('change');
      $('#idSubclase2').val(row.idTipoVehiculo).trigger('change');
      $('#idUso2').val(row.idTipoUso).trigger('change');
      $('#idProcedencia2').val(2).trigger('change');
      $('#idAseguradora2').val(row.idAseguradora).trigger('change');
      $('#senasPartic2').val(row.senasPartic);
      $('#observacion2').val(row.observacion);
      $("#idDocCirculacion2").val(row.idDocumentoCirculacion).trigger('change');
      $('#folio').val(row.folioDoctoCirc);
      $('#idResguardo2').val(row.idDeposito).trigger('change');
    },

    'click .cargaImgVehiculo': function (e, value, row, index) {
      $('#modaldoctosVehiculo2').modal('show');
      idVeh = row['id'];
      $('#invisible_id').val(idVeh);
      var ruta ="{{ route('saveImage',['idVeh'=>'x'])}}";
      ruta = ruta.replace('x',idVeh);
      $("#cargarImagen").fileinput({
        'theme': 'explorer-fa',
        language:'es',
        uploadUrl: {{"ruta"}},
        error: 'No hay archivos que subir',
        uploadAsync: true,
        overwriteInitial: false,
        initialPreviewAsData: true,
        minFileCount:1,
        showUpload: false,
        showRemove: false,
        initialPreview: [
        ]
      });
    },
    'click .altaVehiculo': function (e, value, row, index) {
      var ruta ="{{route('generar-reportealtavehiculo',['idVeh'])}}";
      ruta = ruta.replace("idVeh",row.id);
      window.open(ruta, '_blank');
    },
    'click .imagenesrecu': function (e, value, row, index){
      var identificador = row['id'];
      var respuesta;
      $('ul#listaImagenes').html('');
      if(identificador!=''){
        var ruta = "{{route('getImagenRec',':idVeh')}}";
        ruta = ruta.replace(':idVeh', identificador);
        datos = null;
        respuesta = peticionInfomacion('get',ruta,datos);
        if (respuesta != null && respuesta.estado.length > 0) {
          for(i=0; i<respuesta.estado.length; i++){
            $('ul#listaImagenes').append(addImg2(respuesta.estado[i].destino,respuesta.estado[i].nombre));
          }
          var galley = document.getElementById('galley');
          var viewer;
          viewer = new Viewer(galley, {
            url: 'data-original',
            hidden: function () {
              viewer.destroy();
            },
          });
          viewer.show();
        }else{
          alert('Error de servidor');
        }
      }
    },
    //porcentanje en barra
    'click .progress': function (e, value, row, index) {
      $('#porcentajeVehiculo');
      var datos = JSON.stringify(row);
      $("#pro-bar").css("background-color", "");
      $("#pro-bar").html('');
      var campos = ['placas','modelo','nrpv','numSerie','permiso','numMotor','procedencia','marca','submarca','color','tipoVehiculo','claseVehiculo','tipoUSo','aseguradora','senasPartic'];
      var acierto = 0;
      $('#pro-bar-'+row.id).html('');
      for(var i = 0; i < campos.length; i++)
      {
        if (row[campos[i]]!=""){
          acierto= acierto + 1;
        }
      }
      var p = (acierto * 100)/campos.length;
      $('#pro-bar-'+row.id).css('width', p+'%').attr('aria-valuenow', p);
      $('#pro-bar-'+row.id).text(Math.round(p)+'%');
      if(p>=0 && p<=49){
        $('#pro-bar-'+row.id).css('background','red');}
        else if(p>=50 && p<=99){
          $('#pro-bar-'+row.id).css('background','orange');
        }
        else{
          $('#pro-bar-'+row.id).css('background','green');
        }
      },
      'click .formatoVdiligencia': function (e, value, row, index) {
        var identificador = row['id'];
        if(identificador!=''){
          var ruta = "{{route('getDilidencia',':idVeh')}}";
          ruta = ruta.replace(':idVeh', identificador);
          datos = null;
          respuesta = peticionInfomacion('get',ruta,datos);
          var filename=respuesta.estado[0].destino;
          getDoc = "{{route('getImg',['filename'=>':ruta'])}}";
          getDoc = getDoc.replace(':ruta', filename);
          window.open(getDoc, '_blank');
        }
      },

      'click .statusRegistro': function (e, value, row, index) {
        var ruta = "{{route('validacionRecuperados',':idVeh')}}";
        ruta = ruta.replace(':idVeh', row.id);
        //ruta = ruta.replace(':tipoVal', 1);
        console.log(row);
        statusBus = peticionInfomacion('get',ruta,null);
        console.log('Datos BUS');
        console.log(statusBus[0].busFGE);

        if(statusBus[0].busCNS==2){
         $(this).popover({title: "Validación", content: "El registro se encuentra en PLATAFORMA NACIONAL", placement: "right"});
          $(this).popover('show');
        }
        if((statusBus[0].busCNS==3) || (statusBus[0].busCNS==0 && statusBus[0].busCNSKey==3)){
          $('#error').html('');
          var error = "";
          var ruta1 = "{{route('validacionRecuperadosData',':idVeh')}}";
          ruta1 = ruta1.replace(':idVeh', row.id);
          //ruta = ruta.replace(':tipoVal', 1);
          dataRecuperado = peticionInfomacion('get',ruta1,null);
          console.log(dataRecuperado.detalle[0].idDeposito);
          if (dataRecuperado.error.length > 0) {
            for(i=0; i<dataRecuperado.error.length; i++){
              //alert(i);
              error = error + '<p>'+dataRecuperado.error[i].DESC_ERROR+'</p>';
            }
            console.log(error);
            $('#error').append(error);




          // Creal modal con errores
          $(this).popover('destroy');
          $('#modalValidacion').modal('show');
          $('#llaveExterna').val(dataRecuperado.error[0].LLAVE);
          $('#id1').val(row.id);

          $('#idEstado1').val(dataRecuperado.detalle[0].id_entidad_rec).trigger('change');
          $('#idMunicipio1').val(dataRecuperado.detalle[0].id_municipio_rec).trigger('change');
          $('#cp1').val(dataRecuperado.detalle[0].cp_rec);
          $('#calle1').val(dataRecuperado.detalle[0].calle_rec);
          $('#colonia1').val(dataRecuperado.detalle[0].colonia_rec);
          $('#numExt1').val(dataRecuperado.detalle[0].numext_rec);
          $('#numInt1').val(dataRecuperado.detalle[0].numint_rec);
          $('#idFechaEntrega1').val(dataRecuperado.detalle[0].fecha_rec);
          $('#idHoraEntrega1').val(dataRecuperado.detalle[0].hora_rec);
          $('#idResguardo1').val(dataRecuperado.detalle[0].idDeposito).trigger('change');
          $('#observacion1').val(dataRecuperado.detalle[0].observacion);



          (row.idSubmarca == 0)?$('#idMarcas1,#idSubmarcas1').attr('disabled','disabled'):'';




          $('#idMarcas1').val(row.idMarca).trigger('change');
          $('#idSubmarcas1').val(row.idSubmarca).trigger('change');
          (row.modelo == 0 )?$('#modelos1').attr('disabled','disabled'):'';
          $('#modelos1').val(row.modelo);
          (row.idColor == 0 )?$('#idColors1').attr('disabled','disabled'):'';
          $('#idColors1').val(row.idColor).trigger('change');
          (row.numSerie == 0 )?$('#numSeries1').attr('disabled','disabled'):'';
          $('#numSeries1').val(row.numSerie);
          (row.numMotor == 0 )?$('#motors1').attr('disabled','disabled'):'';
          $('#motors1').val(row.numMotor);
          (row.nrpv == 0 )?$('#nrpv1').attr('disabled','disabled'):'';
          $('#nrpv1').val(row.nrpv);
          (row.idTipoVehiculo == 0 )?$('#idClaseVehiculo1,#idSubclase1').attr('disabled','disabled'):'';
          $('#idClaseVehiculo1').val(row.idClaseVehiculo).trigger('change');
          $('#idSubclase1').val(row.idTipoVehiculo).trigger('change');
          (row.idTipoUso == 0 )?$('#idUso1').attr('disabled','disabled'):'';
          $('#idUso1').val(row.idTipoUso).trigger('change');
          (row.idProcedencia == 0 )?$('#idProcedencia1').attr('disabled','disabled'):'';
          $('#idProcedencia1').val(2).trigger('change');
          (row.idAseguradora == 0 )?$('#idAseguradora1').attr('disabled','disabled'):'';
          $('#idAseguradora1').val(row.idAseguradora).trigger('change');
          (row.senasPartic == 0 )?$('#senasPartic1').attr('disabled','disabled'):'';
          $('#senasPartic1').val(row.senasPartic);
          $("#idDocCirculacion1").val(row.idDocumentoCirculacion).trigger('change');
          (row.folioDoctoCirc == 'NA' )?$('#folio1').attr('disabled','disabled'):'';
          $('#folio1').val(row.folioDoctoCirc);
        }

        }
        if(statusBus[0].busFGE==1 && statusBus[0].busCNS ==0 && statusBus[0].busCNSKey ==0){
          $(this).popover({title: "Validación", content: "El registro se ha enviado al BUS", placement: "right"});
          $(this).popover('show');
        }
        if(statusBus[0].busFGE==1 && statusBus[0].busCNS==1){
          $(this).popover({title: "Validación", content: "El registro está en validación en la CNS", placement: "right"});
          $(this).popover('show');
        }
        // if(statusBus[0].busCNS==0 && statusBus[0].busCNSKey==3){
        //  icono = "fa fa-download";
        //  color = "#8C1333";
        // }

      setTimeout(function () {
        $('.btn_status').popover('hide');
      }, 7000);
    },
    };

    /* Configuracion de la tabla de registros de vehiculos */
    var ruta = "{{route('getVehRecuperados')}}/"+$('#idSecurity').val();
    $('#table').bootstrapTable({
      search:  true,
      showColumns: true,
      showRefresh: true,
      url: ruta,
      buttonsAlign :'right',
      searchAlign :'right',
      idField: 'id',
      iconsPrefix: 'fa',
      pagination : true,
      pageSize: 5,
      columns: [{
        field: 'id',
        title: 'ID',
        sortable: true,
        visible: false
      },{
        field: 'operate3',
        title: 'P.NACIONAL',
        events: operateEvents,
        formatter: operateFormatter3
       }, {
        field: 'entidadPlaca',
        title: 'Entidad de la placa',
        sortable: true
      }, {
        field: 'placas',
        title: 'Placas',
        sortable: true
      },{
        field: 'modelo',
        title: 'Modelo',
        sortable: true
      }, {
        field: 'nrpv',
        title: 'NRPV',
        sortable: true
      }, {
        field: 'permiso',
        title: 'Permiso',
        sortable: true
      }, {
        field: 'numSerie',
        title: 'No. serie',
        sortable: true
      }, {
        field: 'numMotor',
        title: 'No. Motor',
        sortable: true
      }, {
        field: 'procedencia',
        title: 'Procedencia',
        sortable: true
      }, {
        field: 'marca',
        title: 'Marca',
        sortable: true
      }, {
        field: 'submarca',
        title: 'Submarca',
        sortable: true,
        visible: false
      }, {
        field: 'color',
        title: 'Color',
        sortable: true,
        visible: false
      }, {
        field: 'tipoVehiculo',
        title: 'Tipo',
        sortable: true,
        visible: false
      }, {
        field: 'claseVehiculo',
        title: 'Clase',
        sortable: true,
        visible: false
      }, {
        field: 'tipoUSo',
        title: 'Tipo de uso',
        sortable: true,
        visible: false
      }, {
        field: 'aseguradora',
        title: 'Aseguradora',
        sortable: true,
        visible: false
      }, {
        field: 'senasPartic',
        title: 'Señas Particulares',
        sortable: true,
        visible: false
      }, {
        field: 'deposito',
        title: 'Deposito',
        sortable: true,
        visible: false
      }, {
        field: 'operate',
        title: 'Acciones',
        align: 'center',
        events: operateEvents,
        formatter: operateFormatter
      },
      {
        field: 'operate2',
        title: 'Porcentaje total',
        align: 'center',
        events: operateEvents,
        formatter: operateFormatter2
      }],
    });

    $( document ).ready(function() {
      $("#idMarcas").change(function() {
        // alert('hola');
        var identificador = $(this).val();
        var respuesta;
        $('#idSubmarcas').html('');
        if(identificador!=''){
          var ruta = "{{route('submarca',':marca')}}";
          ruta = ruta.replace(':marca', identificador);
          datos = null;
          respuesta = peticionInfomacion('get',ruta,datos);
          if (respuesta != null) {
            for(i=0; i<respuesta.estado.length; i++){
              $("#idSubmarcas").append("<option value='"+respuesta.estado[i].id+"'> "+respuesta.estado[i].nombre+"</option>");
            }
            if(respuesta.estado.length > 0 ) {
              localStorage.setItem("idSubmarcas", respuesta.estado[0].id);
            }
          }else{
            alert('Error de servidor');
          }
        }else{
          //alert('no aplica');
        }
      });

      $("#idClaseVehiculo2").change(function() {
        // alert('hola');
        var identificador = $(this).val();
        var respuesta;
        $('#idSubclase2').html('');
        if(identificador!=''){
          var ruta = "{{route('subclase',':clasevehiculo')}}";
          ruta = ruta.replace(':clasevehiculo', identificador);
          datos = null;
          respuesta = peticionInfomacion('get',ruta,datos);
          if (respuesta != null) {
            for(i=0; i<respuesta.estado.length; i++){
              $("#idSubclase2").append("<option value='"+respuesta.estado[i].id+"'> "+respuesta.estado[i].nombre+"</option>");
            }
            if (respuesta.estado.length > 0) {
              localStorage.setItem("idSubclase2", respuesta.estado[0].id);
            }
          }else{
            alert('Error de servidor');
          }
        }else{
          alert('no aplica');

        }
      });



      var idmarca = $("#idMarcas").val();

      if (idmarca > 0) {
        var ruta = "{{route('submarca',':marca')}}";
        ruta = ruta.replace(':marca', idmarca);
        datos = null;
        respuesta = peticionInfomacion('get',ruta,datos);

        if (respuesta != null) {
          for(i=0; i<respuesta.estado.length; i++){
            $("#idSubmarcas").append("<option value='"+respuesta.estado[i].id+"'> "+respuesta.estado[i].nombre+"</option>");
          }
          $('#idSubmarcas').val(localStorage.idSubmarca).trigger("change");
        }
      }

      var idClaseVehiculo = $("#idClaseVehiculo2").val();
      if (idClaseVehiculo > 0) {
        var ruta = "{{route('subclase',':clasevehiculo')}}";
        ruta = ruta.replace(':clasevehiculo', idClaseVehiculo);
        datos = null;
        respuesta = peticionInfomacion('get',ruta,datos);
        if (respuesta != null) {
          for(i=0; i<respuesta.estado.length; i++){
            $("#idSubclase2").append("<option value='"+respuesta.estado[i].id+"'> "+respuesta.estado[i].nombre+"</option>");
          }
        }
        $('#idSubclase2').val(localStorage.idSubclase).trigger("change");
      }
    });

    //MODAL VALIDACION
    $("#idEstado1").change(function() {
    var identificador = $(this).val();
    var respuesta;
    $('#idMunicipio1').html('');
    if(identificador!=''){
      var ruta = "{{route('municipio',':estado')}}";
      ruta = ruta.replace(':estado', identificador);
      datos = null;
      respuesta = peticionInfomacion('get',ruta,datos);
      if (respuesta != null) {
        for(i=0; i<respuesta.estado.length; i++){
          $("#idMunicipio1").append("<option value='"+respuesta.estado[i].idMunicipio+"'> "+respuesta.estado[i].nombre+"</option>");
        }
        if(respuesta.estado.length > 0 ){
          localStorage.setItem("idMunicipio1", respuesta.estado[0].ididMunicipio);
        }
      }else{
        alert('Error de servidor');
      }
    }else{
    }
  });
  $("#idMunicipio1").change(function() {
    localStorage.setItem("idMunicipio1", $('#idMunicipio1').val());
  });


    $('#form_registro').find('input, textarea, button, select').attr('disabled','disabled');
    $('#guardar_cambios').attr('disabled','disabled');;
    $('#btn_editar').bind('click', function () {
      if ($('#btn_editar').is(':checked')) {
        $('#form_registro').find('input, textarea, button, select').removeAttr('disabled','disabled');
        $('#guardar_cambios').removeAttr('disabled','disabled');
      } else {
        $('#form_registro').find('input, textarea, button, select').attr('disabled','disabled');
        $('#guardar_cambios').attr('disabled','disabled');
      }
    });

    $('#numSerie').focusout(function(){
      var numSerie = $('#numSerie').val();
      if(numSerie!=""){
        var ruta ="{{route('niv',['num_serie'])}}";
        ruta = ruta.replace("num_serie",numSerie);
        var respuesta = peticionInfomacion('get',ruta);
        (respuesta.request.status=='error')? toastr.error(respuesta.request.mensaje, "ERROR",{"preventDuplicates": true}):toastr.success(respuesta.request.mensaje, "CORRECTO",{"preventDuplicates": true});

      }
    });

    $('#guardar_cambios').bind('click', function () {
      $.ajax({
        url : "{{route('updateRegistro')}}",
        data : $('#form_registro').serialize(),
        type : 'POST',
        success : function(json) {
          swal({
            title: "Se guardaron las imagenes correctamente",
            icon: "success",
          });
          $('#table').bootstrapTable('refresh');
        },
        error : function(xhr, status) {
          swal({
            title: "Error al guardar cambios",
            icon: "error",
          });
        },
        complete : function(xhr, status) {
        }
      });
    });

    $("#idDocumentoCirculacion").change(function(){
      var selec = $("#idDocumentoCirculacion").val();
      if(selec ==  1||selec==5) {
        if(selec == 1) {
          $("#placa").attr('maxlength','7');
        } else {
          $("#placa").attr('maxlength','10');
        }
        $('#divPlaca').css('display','');
        $('#divFolio').css('display','none');
      } else {
        $('#divPlaca').css('display','none');
        $('#divFolio').css('display','');
      }
    });

    function adjuntar(){
      $('#modaladjuntardiligencia').modal('show');

    }

    $('#docDiligenciaUp').submit(function(event){
      event.preventDefault();
      var formData = new FormData(this);
      $.ajax({
        url : "{{route('docUpDiligencia')}}",
        data : formData,
        type : 'POST',
        cache: false,
        contentType : false,
        processData : false,
        success : function(json) {
          $('#resp_load_1').show();
        },
        error : function(xhr, status) {
          $('#resp_load_2').show();
        },
        complete : function(xhr, status) {
        }
      });
    });

    //GUARDAR VALIDACION BUS

    $('#guardar_validacion').bind('click', function () {
      $('#form_validacion').find('input, textarea, button, select').removeAttr('disabled');
      $.ajax({
        url : "{{route('validacionRecuperadosBus')}}",
        data : $('#form_validacion').serialize(),
        type : 'POST',
        success : function(json) {
          swal({
            title: "Datos actualizados correctamente",
            icon: "success",

          });
          $('#table').bootstrapTable('refresh');
          $('#modalValidacion').modal('toggle');
          $('#guardar_validacion').attr('disabled','disabled');
        },


        error : function(xhr, status) {
          swal({
            title: "Error al guardar cambios",
            icon: "error",
          });
        },
        complete : function(xhr, status) {
        }
      });
    });

    $(document).ready(function() {

      $("#btnCerrar").click(function(event) {
        $('#cargarImagen').fileinput('clear');
      });
    });
  </script>
</div>
</div>
