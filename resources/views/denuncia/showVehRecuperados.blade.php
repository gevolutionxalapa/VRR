<!--template-->
@extends('template.main2')
<!--menu-->

@section('content')

			<!-- Contenido -->
			
			<div class="tab-content" id="pills-tabContent">
				<div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
					@include('denuncia.forms.formVehRecuperados')
				</div>
				<div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
					@include('denuncia.listas.registroVehiculosRecuperados')
				</div>
			</div>


@endsection
