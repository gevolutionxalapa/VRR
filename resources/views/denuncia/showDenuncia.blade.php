<!--template-->
@extends('template.main2')
<!--menu-->

<!-- Contenido -->
@section('content')
<!-- menu interno -->

<!-- Contenido -->

<div id="cajados" class="tab-content " >
	@if($idtab == 0)
		<div class="tab-pane container active"  id="panel4" aria-labelledby="pills-home-tab">
			@include('denuncia.forms.formDenuncia')
		</div>
		<div class="tab-pane "   id="panel11" aria-labelledby="pills-profile-tab">
			@include('denuncia.listas.registroVehiculos')
		</div>
		@else
		<div class="tab-pane container"  id="panel4" aria-labelledby="pills-home-tab">
			@include('denuncia.forms.formDenuncia')
		</div>
		<div class="tab-pane active"   id="panel11" aria-labelledby="pills-profile-tab">
			@include('denuncia.listas.registroVehiculos')
		</div>
		@endif
</div>
<!-- Fin pestañas -->

<!-- Pies -->
<!-- Modals -->
@include('denuncia.modals.vehiculoDetalle')


@endsection
