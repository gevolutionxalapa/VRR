<link rel="stylesheet" type="text/css" href="{{ asset('css/PreVehiculo/theme.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/PreVehiculo/fileinput.css') }}">

<div id="modalDetalleVehiculo" class="modal modal-responsive-sm" tabindex="-1" role="dialog" data-backdrop="static" style="display:none">
	<div class="modal-dialog modal-lg" role="document">

		<!-- Part A -->
		<div class="modal-content">

			<div class="modal-header">
        <div class="row">
  				<div class="col-md-12">
  					<ul class="nav nav-pills">
  						<li class="nav-item">
  							<a class="nav-link btn-outline-dark active" id="sectAdetailsbtn" >Detalle</a>
  						</li>
  						<li class="nav-item">
  							<a class="nav-link" id="sectBdetailsbtn">Imagenes</a>
  						</li>
  					</ul>
  				</div>
  			</div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
			</div>
			<div class="modal-body">

        <div class="partDetails" id="sectAdetails">
          <div class="row">
            <div class="col-md-12 text-center">
              <h5 class="modal-title">Detalle del vehículo robado</h5>
            </div>
          </div>
          <hr>
          <form id="form_registro" >
            {!!Form::text('id',null,['id' => 'id', 'hidden'])!!}
            <div class="row">
              <div class="col-lg col-md">
                <div class="form">
                  {!!Form::label('permiso', 'Documento de circulación')!!}
                </div>
                <div class="input-group">
                  <select class="templatingSelect10 form-control form-control-sm"  id="idDocCirculacion" style="width:100%" name="idDocCirculacion" >
                    <option selected="selected" value=""> </option>
                    @foreach($documentosCirculacion as $documento)
                    <option value="{{$documento->id}}" >{{$documento->nombre}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-lg col-md">
                <div id="divPlaca">
                  <div class="form">
                    {!!Form::label('placa', 'Placa')!!}
                  </div>
                  <div class="input-group" style="width:100%">
                    {!!Form::text('placa',null,['class'=>'form-control form-control-sm', 'null' ,'id' => 'idplaca2', 'placeholder' => 'HMZ2398 - Ejemplo' , 'maxlength'=>'7',  'style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'alphanumeric'])!!}
                  </div>
                </div>
                <div id="divFolio" style = 'display: none'>
                  <div class="form">
                    {!!Form::label('folio', 'Folio')!!}
                  </div>
                  <div class="input-group" style="width:100%">
                    {!!Form::text('folio',null,['class'=>'form-control form-control-sm', 'id' => 'folio', 'style'=>'text-transform: uppercase;', 'maxlength'=>'10'])!!}
                  </div>
                </div>
              </div>
              <div class="col-lg col-md">
                <div class="form-group">
                  {!!Form::label('idEstado', 'Entidad federativa')!!}
                  <select  class="templatingSelect2 form-control form-control-sm" style="width:100%" id="idEstados" name="idEstados" required>
                    <option value="" selected="selected" > </option>
                    @foreach($estados as $estado)
                    <option  value="{{$estado->id}}" >{{$estado->nombre}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg col-md">
                <div class="form-group">
                  {!!Form::label('idMarca', 'Marca')!!} <br>
                  <select  class="templatingSelect3 form-control form-control-sm" style="width:100%" id="idMarcas" name="idMarcas" required>
                    <option value="" selected="selected" > </option>
                    @foreach($marcas as $marca)
                    <option  value="{{$marca->id}}">{{$marca->nombre}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-lg col-md">
                {!!Form::label('idSubmarca', 'Submarca')!!}<br>
                <select  class="idSubmarcas form-control form-control-sm" style="width:100%"  id="idSubmarcas" name="idSubmarcas" required>
                  <option value="" required selected="selected"></option>
                </select>
              </div>
              <div class="col-lg col-md">
                {!!Form::label('modelo', 'Modelo')!!}
                <input class="form-control form-control-sm" style="width:100%" value="{{date('Y')}}" min="1885" max="{{date('Y')+1}}" name="modelos" type="number" required id="modelos">
              </div>
            </div>
            <div class="row">
              <div class="col-lg col-md">
                {!!Form::label('idColor','Color')!!} <br>
                <select  class="templatingSelect5 form-control form-control-sm" style="width:100%" id="idColors" name="idColors" required>
                  <option selected="selected" value=""> </option>
                  @foreach($colores as $color)
                  <option value="{{$color->id}}">{{$color->nombre}}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-lg col-md">
                {!!Form::label('numSeries','Número de serie')!!}
                {!!Form::text('numSeries',null, ['class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el número de serie','maxlength'=>'17','style'=>'text-transform: uppercase;',  'data-validation'=>'custom', 'data-validation'=>'alphanumeric'] )!!}
              </div>
              <div class="col-lg col-md">
                {!!Form::label('motors','Número de motor')!!}
                {!!Form::text('motors',null, ['class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el número de motor', 'maxlength'=>'10', 'style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'alphanumeric' ] )!!}
              </div>
            </div>
            <div class="row">
              <div class="col-lg col-md">
                {!!Form::label('nrpv2','NRPV')!!}
                {!!Form::text('nrpv2',null, ['class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el NRPV', 'style'=>'text-transform: uppercase;','required', 'maxlength'=>'20'] )!!}
              </div>
              <div class="col-lg col-md">
                {!!Form::label('idClaseVehiculo','Tipo de vehículo')!!}
                <select  class="templatingSelect7 form-control form-control-sm" style="width:100%"  id="idClaseVehiculo2" name="idClaseVehiculo2" required>
                  @foreach($clasevehiculo as $clasevehiculo)
                  <option value="{{$clasevehiculo->id}}">{{$clasevehiculo->nombre}}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-lg col-md">
                {!!Form::label('idSubclase','Subtipo de vehículo')!!}
                <select  class="templatingSelect1 form-control form-control-sm" style="width:100%" id="idSubclase2" name="idSubclase2" required>
                  <option selected="selected" value=""> </option>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="col-lg col-md">
                {!!Form::label('idUso','Tipo de uso')!!}
                <select  class="templatingSelect8 form-control form-control-sm" style="width:100%" id="idUso2" name="idUso2" required>
                  <option selected="selected" value=""> </option>
                  @foreach($tipouso as $tipouso)
                  <option value="{{$tipouso->id}}">{{$tipouso->nombre}}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-lg col-md">
                {!!Form::label('idProcedencia','Procedencia')!!} <br>
                <select style="width:100%" class="templatingSelect9 form-control form-control-sm"  id="idProcedencia2" name="idProcedencia2" required>
                  <option value="" selected>Seleccione</option>
                  <option value="2" >EXTRANJERO</option>
                  <option value="1">NACIONAL</option>
                </select>
              </div>
              <div class="col-lg col-md">
                {!!Form::label('idAseguradora','Aseguradora')!!}
                <select class="templatingSelect6 form-control form-control-sm" style="width:100%" required id="idAseguradora2" name="idAseguradora2" >
                  <option selected="selected" value=""> </option>
                  @foreach($aseguradoras as $aseguradora)
                  <option value="{{$aseguradora->id}}">{{$aseguradora->nombre}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-8 col-md-8">
                <div class="form-group">
                  {!!Form::label('numeroeconomico2','Número económico')!!}
                  {!!Form::text('numeroeconomico2',null, ['class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el número económico', 'style'=>'text-transform: uppercase;', 'data-validation'=>'number', 'maxlength'=>'20'] )!!}
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg col-md">
                {!!Form::label('senasPartic','Señas particulares')!!}
                <textarea class="form-control form-control-sm" placeholder="Ingrese las señas particulares" rows="3" name="senasPartic2" cols="50" style="text-transform: uppercase;" required id="senasPartic2"></textarea>
              </div>
            </div>
          </form>
          <hr>
          <div class="row">
            <div class="col-md-12 text-right">
              <label>Editar</label>
              <label class="switch">
                <input type="checkbox" id="btn_editar">
                <span class="slider round"></span>
              </label>
              <button type="button" class="btn btn-secondary" id="guardar_cambios" >Guardar</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
          </div>

        </div>
        <!-- Part Edit images -->
        <div class="partImages"  id="sectBdetails"style="display:none">
          <div class="row">
            <div class="col-md-12 text-center">
              <h5 class="modal-title">Imagenes del vehiculo</h5>
            </div>
          </div>
          <hr>
          <div id="doctosModal" class="row">
          </div>
          <div align="center">
            <div class="btn-group btn-group-toggle" data-toggle="buttons" id="selector3">
              <label class="btn btn-secondary form-check-label" onclick="sectionShow3()">
                <input type="radio" name="img" id="option1" autocomplete="off" value="archivo3">
                <i class="fa fa-folder-open"></i>
                Archivos
              </label>
              <label id="divBtnCamara" class="btn btn-secondary form-check-label " onclick="sectionShow3()">
                <input type="radio" name="img" id="option2" autocomplete="off" value="camara3" >
                <i class="fa fa-camera-retro"></i>
                C&aacute;mara
              </label>
            </div>
            <br>
            <div id="complemento_asc" style="display:none;">
                 <div class="row">
                <div class="col-lg-6 col-md-6">
                  <div >
                    <div id="iden" >
                      <div>
                        <label>Identificacion oficial</label>
                      </div>
                      <select class="listaiden1 form-control form-control-sm listaiden" id="listaiden_op3" name="listaiden1" responsive required>
                        <option selected value="" >--Seleccione una opción--</option>
                        @foreach($identificacionoficial as $identificacionoficial)
                        <option  value="{{$identificacionoficial->id}}" >{{$identificacionoficial->nombre}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
                    <div class="col-lg-6 col-md-6">
                      <div id="iden" >
                      <div>
                        <label>Factura del vehículo</label>
                      </div>
                      <select class="listaiden3 form-control form-control-sm listaiden" id="listaiden_op4" name="listaiden3" responsive required>
                        <option selected value="" >--Seleccione una opción--</option>
                        @foreach($factura as $factura)
                        <option  value="{{$factura->id}}" >{{$factura->nombre}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  </div>

            <div id="div5" style="display:none;" >
                  <br>
                  <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="file-loading" >
                      <input id="DOC2" class="btn_ide" name="ID_OFICIAL" type="file" accept="image/*" style="display: none;" required>
                    </div>
                 </div>
              <div class="col-lg-6 col-md-6">
                  <div class="file-loading">
                    <input id="DOC6" align="center" class="btn-fac" name="ID_FACTURA" type="file" accept="image/*" style="display: none;">
                  </div>
                </div>
              </div>
            </div>
          </div>

         <div id="div6" style="display:none;" >
            <br>
            <div class="row">
              <div class="col-lg-6 col-md-6">
                  <div class="" id="ife_mini1"></div>
                    <input type="hidden" id="mini_ife1" name="cam_ident" value="">
                    <button id="cam_ide1" type="button" class="btn btn-dark" data-toggle="modal" data-target="#profile-photo-camera-modal" onclick="setup('IFE')" disabled="disabled" data-toggle="popover" data-trigger="focus hover" title="ATENCIÓN:Capture una imagen" >
                      <i class="fa fa-camera"></i> Capturar imagen
                    </button>
                    <button type="button" id="borrar2"  class="btn btn-secondary" :disabled="form.busy" ata-target="#profile-photo-camera-modal"  disabled="disabled">
                      <i class="fa fa-trash"></i>
                    </button>
              </div>
              <div class="col-lg-6 col-md-6">
                <div class="" id="fact_mini1"></div>
                  <input type="hidden" id="mini_fac1" name="cam_fact" value="">
                  <button id="cam_fac1" type="button" class="btn btn-dark" data-toggle="modal" data-target="#profile-photo-camera-modal" onclick="setup('FACT')" disabled="disabled"  data-toggle="popover" data-trigger="focus hover" title="ATENCIÓN:Capture una imagen" required >
                    <i class="fa fa-camera"></i> Capturar imagen
                  </button>
                  <button type="button" id="borrar"  class="btn btn-secondary" :disabled="form.busy" ata-target="#profile-photo-camera-modal"  disabled="disabled">
                    <i class="fa fa-trash"></i>
                  </button>
              </div>
            </div>
          </div>
          </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <p>Secccion de carga de imagenes</p>
              <p>Todas las funciones aqui</p>
              <p>Todas las funciones aqui</p>
              <p>Todas las funciones aqui</p>
              <p>Todas las funciones aqui</p>
            </div>
          </div>
        </div>


			</div>
			<!-- <div class="modal-footer">

			</div> -->
		</div>
		<!-- partA -->



	</div>
</div>


<script src="{{ asset('js/prevehiculo/fileinput.min.js') }}"></script>
<script>
    var datosVeh;
    function verDetalle(){
      console.log('ver detalle nootificacion');
      $('#modalDetalleVehiculo').modal('show');
      $('#id').val(datosVeh.id);
      $('#idplaca').val(datosVeh.placas);
      $('#idEstados').val(datosVeh.idEstado).trigger('change');
      $('#idMarcas').val(datosVeh.idMarca).trigger('change');
      $('#idSubmarcas').val(datosVeh.idSubmarca).trigger('change');
      $('#modelos').val(datosVeh.modelo);
      $('#idColors').val(datosVeh.idColor).trigger('change');
      $('#numSeries').val(datosVeh.numSerie);
      $('#motors').val(datosVeh.numMotor);
      $('#nrpv2').val(datosVeh.nrpv);
      $('#idClaseVehiculo2').val(datosVeh.idClaseVehiculo).trigger('change');
      $('#idSubclase2').val(datosVeh.idTipoVehiculo).trigger('change');
      $('#idUso2').val(datosVeh.idTipoUso).trigger('change');
      $('#idProcedencia2').val(2).trigger('change');
      $('#idAseguradora2').val(datosVeh.idAseguradora).trigger('change');
      $('#senasPartic2').val(datosVeh.senasPartic);
      $("#idDocCirculacion").val(datosVeh.idDocumentoCirculacion).trigger('change');
      $('#folio').val(datosVeh.folioDoctoCirc);
      $('#numeroeconomico2').val(datosVeh.numeroeconomico);

    }
    /*
    function notificacion(){
    //console.log('notificacion');
    var ruta = "{{route('notificacion_panel')}}";
    datosVeh = peticionInfomacion('get',ruta,null);
    if(datosVeh){
    var ruta = "{{route('notificacion_uppanel',':id')}}";
    ruta = ruta.replace(':id', datosVeh.id);
    respuesta = peticionInfomacion('get',ruta,null);
    toastr.info('Se ha actualizado información de un vehiculo en el Panel de verificación <br> <button type="button" class="btn btn-toastr" onclick="verDetalle()">Ver detalles</button>', "Atención",{"preventDuplicates": true});
    }
    }
    setInterval('notificacion()',5000);
    */
    $('#form_registro').find('input, textarea, button, select').attr('disabled','disabled');
    $('#guardar_cambios').attr('disabled','disabled');;
    $('#btn_editar').bind('click', function () {
      if ($('#btn_editar').is(':checked')) {
        $('#form_registro').find('input, textarea, button, select').removeAttr('disabled','disabled');
        $('#guardar_cambios').removeAttr('disabled','disabled');
      } else {
        $('#form_registro').find('input, textarea, button, select').attr('disabled','disabled');
        $('#guardar_cambios').attr('disabled','disabled');
      }
    });
    $('#guardar_cambios').bind('click', function () {
      $.ajax({
        url : "{{route('updateRegistro')}}",
        data : $('#form_registro').serialize(),
        type : 'POST',
        success : function(json) {

          swal({
            title: "Datos actualizados correctamente",
            icon: "success",

          });
          $('#table').bootstrapTable('refresh');
          $('#modalDetalleVehiculo').modal('toggle');
          $('#form_registro').find('input, textarea, button, select').attr('disabled','disabled');
          $('#guardar_cambios').attr('disabled','disabled');
          $("input[type='checkbox']").prop("checked", false).attr("checked", false).removeAttr("checked");
          $('#guardar_cambios').attr('disabled','disabled');
        },
        error : function(xhr, status) {
          swal({
            title: "Error al guardar cambios",
            icon: "error",
          });
        },
        complete : function(xhr, status) {
        }
      });
    });
    //fuctions when you select a specifict option
    $('#sectAdetailsbtn').click(function(){
      $(this).addClass('active btn-outline-dark');
      $('#sectBdetailsbtn').removeClass('active');
      $("#sectAdetails").show('slow');
      $('#sectBdetails').hide('slow');
    });
    $('#sectBdetailsbtn').click(function(){
      $(this).addClass('active btn-outline-dark');
      $('#sectAdetailsbtn').removeClass('active btn-outline-dark');
      $('#sectBdetails').show('slow');
      $("#sectAdetails").hide('slow');
    });


     function sectionShow3(){
    $('.sec_upload').toggle('slow');
  }
//CARGA DE ARCHIVOS SELECCIONAR TIPO DE ARCHIVO A CARGAR
$("#selector3 :input").change(function(event) {
    //console.log(this); // points to the clicked input button
    var valor = $(event.target).val();
    if (valor == "archivo3") {
        $('#cam_ide').attr('disabled', true);
        $('#cam_fac').attr('disabled', true);
        $("#div5,#complemento_asc").show();
        $("#div6").hide();
        $('#DOC1').prop('required', true);
        $('#DOC5').prop('required', true);
        $('#cam_ide').removeAttr('required');
        //$("#listaiden_op1,#listaiden_op2").prop('required',true);
        $('#tipe_load').val(1);
        $('#mini_ife, #mini_fac').val("");
        $('#ife_mini, #fact_mini').html("");
        $("#listaiden_op1").val('').trigger('change');
        $("#listaiden_op2").val('').trigger('change');
    } else if (valor == "camara3") {
        $('#DOC1').css('display', 'none');
        $('#DOC5').css('display', 'none');
        $("#div5").hide();
        $("#div6,#complemento_asc").show();
        $('#cam_ide').prop('required', true);
        //$("#listaiden_op1,#listaiden_op2").prop('required',true);
        $('#tipe_load').val(2);
        $("#listaiden_op1").val('').trigger('change');
        $("#listaiden_op2").val('').trigger('change');
        $('#DOC1').removeAttr('required');
        $('#DOC5').removeAttr('required');

    }
});


  $('.btn_ide1').on('change', function(){
    $('#identidicacionCont').hide();
  });


  $('#listaiden_op4').change(function(){
    var valor = $('#listaiden_op4').val();
    if(valor!=''){
      var tipoC = $('#tipe_load').val();
      if (tipoC==1){
        $('#DOC6').css('display','');
        $('#cam_ide').attr('disabled',true);
        $('#borrar2').attr('disabled',true);
      }else{
        $('#cam_ide').removeAttr('disabled');
        $('#DOC6').css('display','none');
        $('#borrar2').removeAttr('disabled');
      }
    }
  });
  $('#listaiden_op3').change(function(){
    var valor = $('#listaiden_op3').val();
    if(valor!=''){
      var tipoC = $('#tipe_load').val();
      if (tipoC==1){
        $('#DOC2').css('display','');
        $('#cam_ide').attr('disabled',true);
        $('#borrar2').attr('disabled',true);
      }else{
        $('#cam_ide').removeAttr('disabled');
        $('#DOC2').css('display','none');
        $('#borrar2').removeAttr('disabled');
      }
    }
  });
</script>
