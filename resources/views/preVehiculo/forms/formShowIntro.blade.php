@extends('template.main')

@section('title')
Intrucciones
@endsection


@section('content')
<div class="container">
	<h1  class="text-center">¿Qué debo hacer si roban mi vehículo?</h1>
	<div class="row">
		<div class="col-sm-12">
			<div class="alert " role="alert" >
				<p class="text-justify">
					Si has sido víctima de robo de vehículo es necesario realizar una denuncia "penal" ante la Fiscalía General del Estado de Veracruz (FGE) esta institución,
					pone a su disposición el siguiente portal de asesoría para interponer la correspondiente denuncia. Para llevar a cabo este proceso el ciudadano debe
					seguir los siguientes pasos.
				</p>
				<p class="text-justify">
					<strong>1. Guía Rápida</strong>. Guía al ciudadano para el proceso de realizar el preregistro por robo de vehículo, dando a conocer los documentos
					necesarios que se deben presentar ante la Fiscalía General del Estado para hacer válida la denuncia. Es importante señalar que el proceso se agiliza
					 cuando se presentan todos los documentos señalados en la guía.
				</p>
				<p class="text-justify">
					<strong>2. Preregistro</strong>. Una vez que se cumpla con toda la documentación se procede a realizar el preregistro  llenando el formulario de acuerdo
					a los datos requeridos en el mismo. Al finalizar el proceso de registro se emite un <strong>FOLIO</strong> el cual es único y con el que se dará
					 seguimiento al proceso oficial de la denuncia.
				</p>
				<p class="text-justify">
					 El registro formal se realiza después del preregistro, acudiendo a la Unidad Integral de Procuración de Justicia mas cercana a la localización de los hechos.
				</p>
			</div>
		</div>
		<div class="col-md-12 text-center">
			<a href="{{route('showGuia')}}"  role="button" class="btn btn-primary" >Contestar la guía</a>
			<p></p>
		<div ></div>

		</div>
<p></p>
	</div>
	<p style="padding-bottom: 65px;"></p>

</div>

<script type="text/javascript">
	$( window ).ready(function() {
		$('#step_m1').attr("class",'nav-link active');
		// $('#step_m',).attr("class",'');
		// $('#step_m',).attr("class",'active');
		$('#step_m2 ').click(function(){
			window.location.replace("{{route('showGuia')}}");
		});

	});
</script>

@endsection
