@extends('template.main')

@section('title')
Guía
@endsection

@section('content')

<script src="{{ asset('js/prevehiculo/metodos-guia.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('css/archivo.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/slider/css/bookblock.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/slider/css/default.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/slider/css/demo3.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/PreVehiculo/theme.min.css') }}">
<link rel="stylesheet" href="{{asset('plugins/select2/select2.min.css')}}">
<script src="{{ asset('js/prevehiculo/peticiones.js') }}"></script>
<script src="{{ asset('plugins/select2/select2.min.js') }}"></script>


<div class="container" id="principal">

  <form  method="post" action="{{route('showMap')}}" name="frmGuia" id="frmGuia">

    <div class="container">
      <h2 id="tit" class="text-center"> Guía de requisitos para denuncia de robo de vehículos</h2>
    </div>
    <p></p>
    <div class="row">
      <div class="col-lg-12">
        <div class="alert" role="alert" align="justify">
          <b>NOTA: </b> Es recomendable que cuente con la mayoría de los requisitos para poder comprobar que usted es
          propietario del vehículo y dar seguimiento en caso de que sea recuperado.
        </div>
      </div>
    </div>

    <!-- Top Navigation -->
    <div class="row">
      <div class="col-lg-12">

        <div id="bb-bookblock" class="bb-bookblock bb-vertical">

          <!--Pregunta tipo de vehiculo -->
          <div class="bb-item" >
            <div class="card">
              <div class="card-header" style="background:white">
                <h5>1.-Seleccione el tipo de vehículo a reportar:</h5>
              </div>
              <div class="card-body">
                <div class="form-check form-check-inline">
                  <input type="radio" class="tipoVeh form-check-input" name="tipoVeh" id="tipoVeh" value="1"   required/>
                  <label class="form-check-label" for="tipoVeh">Nacional  </label>
                </div>
                <div class=" form-check form-check-inline">
                  <input type="radio" class="tipoVeh form-check-input" name="tipoVeh" id="tipoVeh" value="2"  required/>
                  <label class="form-check-label" for="tipoVeh">Extranjero   </label>
                </div>
              </div>
            </div>
            <div align="right">
              <p></p>
              <button type="button" id="bb-nav-next" class="btn btn-primary" disabled="true" data-toggle="tooltip"
              data-placement="right" title="Siguiente pregunta"><i class="fa fa-arrow-right"></i></button>
            </div>
          </div>

          <!--pregunta 1-->
          <div class="bb-item" style="display: none;">
            <div class="card">
              <div class="card-header" id="pregUnoN" style="display: none; background:white">
                <h5>2.-¿Usted es propietario del vehículo?</h5>
              </div>
              <div  class="card-header" id="pregUnoE" style="display: none; background:white">
                <h5 id="t1">2.-¿Tiene título de propiedad?<i id="t2" class="fa fa-info-circle ayuda"  data-toggle="modal" data-target="#modaltitulopropiedad"  style="font-size: 18px; float:right; color: #12188e;"></i> </h5>
              </div>
              <div class="card-body">
                <div class="form-check form-check-inline">
                  <input class="form-check-input bt_check preg1" type="radio" name="preg1" id="preg1.1" value="1" required>
                  <label class="form-check-label" for="preg1.1">SI</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input bt_check preg1" type="radio" name="preg1" id="preg1.2" value="0" required>
                  <label class="form-check-label" for="preg1.2">NO</label>
                </div>
                <div id="fac" style="display: none;">
                  <label>¿Usted cuenta con una factura? </label>
                  <div class="form-check form-check-inline">
                    <input class="form-check-input bt_check " type="radio" name="fac" id="cfac" value="1" >
                    <label class="form-check-label" for="cfac">SI</label>
                  </div>
                  <div class="form-check form-check-inline">
                    <input class="form-check-input bt_check " type="radio" name="fac" id="cfacN" value="0" >
                    <label class="form-check-label" for="cfacN">NO </label>
                  </div>
                  <i class="fa fa-info-circle ayuda " data-toggle="modal" data-target="#modalfactura" style="color: #12188e; font-size: 18px; float:right"></i>
                 </div>
                </div>
              </div>
              <div align="right">
                <p></p>
                <button type="button" id="bb-nav-prev" class="btn btn-primary" data-toggle="tooltip" data-placement="right" title="Regresar a la pregunta anterior"><i class="fa fa-arrow-left"></i></button>
                <button type="button" id="bb-nav-next1" class="btn btn-primary btn_block" disabled="true" align="right" data-toggle="tooltip" data-placement="right" title="Siguiente pregunta"><i class="fa fa-arrow-right"></i></button>
              </div>
            </div>

            <!--Pregunta 2 -->
            <div class="bb-item" style="display: none;">
              <div class="card">
                <div class="card-header" style="background:white">
                  <h5 id="t3">3.-¿Tiene tarjeta de circulación? <i id="t4" class="fa fa-info-circle ayuda" data-toggle="modal" data-target="#modaltarjetacirculacion"  style=" color: #12188e;float:right; font-size: 18px;"></i></h5>
                </div>
                <div class="card-body">
                  <div class="form-check form-check-inline">
                    <input class="form-check-input bt_check preg2" type="radio" name="preg2" id="preg2.1" value="1" required>
                    <label class="form-check-label bt_check preg2" for="preg2.1">SI</label>
                  </div>
                  <div class="form-check form-check-inline">
                    <input class="form-check-input bt_check preg2" type="radio" name="preg2" id="preg2.2" value="0" required>
                    <label class="form-check-label bt_check preg2" for="preg2.2">NO</label>
                  </div>
                </div>
              </div>
              <div align="right">
                <p></p>
                <button type="button" id="bb-nav-prev1" class="btn btn-primary" data-toggle="tooltip" data-placement="right" title="Regresar a la pregunta anterior"><i class="fa fa-arrow-left "></i></button>
                <button type="button" id="bb-nav-next2" class="btn btn-primary btn_block" disabled="true" align="right" data-toggle="tooltip" data-placement="right" title="Siguiente pregunta"><i class="fa fa-arrow-right"></i></button>
              </div>
            </div>

            <!--Pregunta 3 -->
            <div class="bb-item" style="display: none;">
              <div class="card">
                <div class="card-header" style="background:white">
                  <div id="pregTresN" style="display: none;">
                    <h5 id="t5">  4.-¿Cuenta con alg&uacute;n pago de tenencia del vehículo? <i id="t6" class="fa fa-info-circle ayuda " data-toggle="modal" data-target="#modalpagotenencia"  style="color: #12188e; float:right; font-size: 18px;"></i></h5>
                  </div>
                  <div id="pregTresE" style="display: none;">
                    <h5 id="t7">  4.-¿Cuenta con alg&uacute;n pago de tenencia del vehículo? <i id="id8" class="fa fa-info-circle ayuda" data-toggle="modal" data-target="#modalpagotenencia"  style="color: #12188e;float:right; font-size: 18px;"></i></h5>
                  </div>
                </div>
                <div class="card-body">
                  <div class="form-check form-check-inline">
                    <input class="form-check-input bt_check preg3" type="radio" name="preg3" id="preg3.1" value="1" required>
                    <label class="form-check-label" for="preg3.1">SI</label>
                  </div>
                  <div class="form-check form-check-inline">
                    <input class="form-check-input bt_check preg3" type="radio" name="preg3" id="preg3.2" value="0" required>
                    <label class="form-check-label" for="preg3.2">NO</label>
                  </div>
                </div>
              </div>
              <div align="right">
                <p></p>
                <button type="button" id="bb-nav-prev2" class="btn btn-primary" ><i class="fa fa-arrow-left" data-toggle="tooltip" data-placement="right" title="Regresar a la pregunta anterior"></i></button>
                <button type="button" id="bb-nav-next3" class="btn btn-primary btn_block" disabled="true" align="right" data-toggle="tooltip" data-placement="right" title="Siguiente pregunta"><i class="fa fa-arrow-right"></i></button>
              </div>
            </div>

            <!--Pregunta 4 -->
            <div class="bb-item" style="display: none;">
              <div class="card">
                <div class="card-header" style="background:white">
                  <h5 id="t9">5.-¿Cuenta con una identificación oficial?  <i id="t10" class="fa fa-info-circle ayuda " data-toggle="modal" data-target="#modaline"  style="color: #12188e; float:right; font-size: 18px;"></i></h5>
                </div>
                <div class="card-body">
                  <div class="form-check form-check-inline">
                    <input class="form-check-input bt_check preg4" type="radio" name="preg4" id="preg4.1" value="1" required>
                    <label class="form-check-label" for="preg4.1">SI</label>
                  </div>
                  <div class="form-check form-check-inline">
                    <input class="form-check-input bt_check preg4" type="radio" name="preg4" id="preg4.2" value="0" required>
                    <label class="form-check-label" for="preg4.2">NO</label>
                  </div>
                </div>
              </div>
              <div align="right">
                <p></p>
                <button type="button" id="bb-nav-prev3" class="btn btn-primary"   data-toggle="tooltip" data-placement="right" title="Regresar a la pregunta anterior"><i class="fa fa-arrow-left"></i></button>
                <button type="button" id="bb-nav-next4" class="btn btn-primary btn_block" disabled="true" align="right" data-toggle="tooltip" data-placement="right" title="Siguiente pregunta"><i class="fa fa-arrow-right"></i></button>
              </div>
            </div>

            <!--Pregunta 5-->
            <div class="bb-item" style="display: none;">
              <div class="card">
                <div class="card-header" style="background:white">
                  <h5 id="t11"> 6.-¿Cuenta con alguna placa vehicular?  <i id="t12" class="fa fa-info-circle ayuda " data-toggle="modal" data-target="#modalplaca"  style="color: #12188e;float:right; font-size: 18px;"></i></h5>
                </div>
                <div class="card-body">
                  <div class="form-check form-check-inline">
                    <input class="form-check-input bt_check preg6 " type="radio" name="preg6" id="preg6.1" value="1" required>
                    <label class="form-check-label" for="preg6.1">SI</label>
                  </div>
                  <div class="form-check form-check-inline">
                    <input class="form-check-input bt_check preg6" type="radio" name="preg6" id="preg6.2" value="0" required>
                    <label class="form-check-label" for="preg6.2">NO</label>
                  </div>
                  <div id="idPlacas" style="display:none;">
                    <select class="templatingSelect10 form-control form-control-sm listaplaca"  id="listaplaca" name="listaplaca" >
                      <option selected="selected" value=""> </option>
                      @foreach($documentosCirculacion as $documento)
                      <!-- <option value="{{$documento->id}}" @if($documento->id == 2 ) selected @endif>{{$documento->nombre}}</option> -->
                      <option value="{{$documento->id}}" >{{$documento->nombre}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
              <br>
              <div align="right">
                <div id="pregDosE" style="display: none">
                  <button type="button" id="bb-nav-prev4" class="btn btn-primary" ><i class="fa fa-arrow-left" data-toggle="tooltip" data-placement="right" title="Regresar a la pregunta anterior"></i></button>
                  <button type="button" id="bb-nav-next5" class="btn btn-primary btn_block" disabled="true" align="right" ><i class="fa fa-arrow-right" data-toggle="tooltip" data-placement="right" title="Siguiente pregunta" ></i></button>
                </div>
                <div id="pregDosN" style="display: none">
                  <button type="button" id="bb-nav-prev5" class="btn btn-primary" ><i class="fa fa-arrow-left" data-toggle="tooltip" data-placement="right" title="Regresar a la pregunta anterior"></i></button>
                  <button  value="submit" id="btn-enviar" class="btn btn-primary btn_block" disabled="true" align="right" ><i class="fa fa-check" data-toggle="tooltip" data-placement="right" title="Ir a pre-registro"></i></button>
                </div>
              </div>
            </div>

            <!--pregunta 6-->
            <div class="bb-item" style="display: none;">
              <div class="card">
                <div class="card-header" style="background:white">
                  <h5 id="t13">7.-¿Cuenta con pedimento de importación? <i id="t14" class="fa fa-info-circle ayuda" data-toggle="modal" data-target="#modalpedimiento"  style="color: #12188e; float:right; font-size: 18px;"></i></h5>
                </div>
                <div class="card-body">
                  <div class="form-check form-check-inline">
                    <input class="form-check-input bt_check preg5" type="radio" name="preg5" id="preg5.1" value="1" required>
                    <label class="form-check-label" for="preg5.1">SI</label>
                  </div>
                  <div class="form-check form-check-inline">
                    <input class="form-check-input bt_check preg5" type="radio" name="preg5" id="preg5.2" value="0" required>
                    <label class="form-check-label" for="preg5.2">NO</label>
                  </div>
                </div>
              </div>
              <div align="right">
                <p></p>
                <button type="button" id="bb-nav-prev6" class="btn btn-primary" ><i class="fa fa-arrow-left" data-toggle="tooltip" data-placement="right" title="Regresar a la pregunta anterior"></i></button>
                <button  value="submit" id="btn-enviar1" class="btn btn-primary btn_block" disabled="true" align="right"><i class="fa fa-check" data-toggle="tooltip" data-placement="right" title="Ir a pre-registro"></i></button>
              </div>
            </div>
          </div>
          <p></p>

          <div class="bb-custom-wrapper">
            <nav>
            </nav>
          </div>

        </div>
      </div>
    </form>
  </div>

	<script src="{{ asset('plugins/slider/js/modernizr.custom.js')}}"></script>
	<script src="{{ asset('plugins/slider/js/jquery.min.js')}}"></script>
	<script src="{{ asset('plugins/slider/js/jquery.bookblock.js')}}"></script>

	<script>
		var altura = $(document).height();
		function cargar(){
      		$("html").animate({scrollTop: $(document).height() }, 1000);
      	}

			var Page = (function() {

				var config = {
						$bookBlock : $( '#bb-bookblock' ),
						$navNext : $( '#bb-nav-next' ),
						$navNext1 : $( '#bb-nav-next1' ),
						$navNext2 : $( '#bb-nav-next2' ),
						$navNext3 : $( '#bb-nav-next3' ),
						$navNext4 : $( '#bb-nav-next4' ),
						$navNext5 : $( '#bb-nav-next5' ),
						$navPrev : $( '#bb-nav-prev' ),
						$navPrev1 : $( '#bb-nav-prev1' ),
						$navPrev2 : $( '#bb-nav-prev2' ),
						$navPrev3 : $( '#bb-nav-prev3' ),
						$navPrev4 : $( '#bb-nav-prev4' ),
						$navPrev5 : $( '#bb-nav-prev5' ),
						$navPrev6 : $( '#bb-nav-prev6' ),
						$navFirst : $( '#bb-nav-first' ),
						$navLast : $( '#bb-nav-last' )
					},
					init = function() {
						config.$bookBlock.bookblock( {
							speed : 800,
							shadowSides : 0.8,
							shadowFlip : 0.7
						} );
						initEvents();
					},
					initEvents = function() {

						var $slides = config.$bookBlock.children();

						// add navigation events
						config.$navNext.on( 'click touchstart', function() {
							config.$bookBlock.bookblock( 'next' );
							return false;
						} );

						config.$navNext1.on( 'click touchstart', function() {
							config.$bookBlock.bookblock( 'next' );
							return false;
						} );

						config.$navNext2.on( 'click touchstart', function() {
							config.$bookBlock.bookblock( 'next' );
							return false;
						} );

						config.$navNext3.on( 'click touchstart', function() {
							config.$bookBlock.bookblock( 'next' );
							return false;
						} );

						config.$navNext4.on( 'click touchstart', function() {
							config.$bookBlock.bookblock( 'next' );
							return false;
						} );
						config.$navNext5.on( 'click touchstart', function() {
							config.$bookBlock.bookblock( 'next' );
							return false;
						} );

						config.$navPrev.on( 'click touchstart', function() {
							config.$bookBlock.bookblock( 'prev' );
							return false;
						} );

						config.$navPrev1.on( 'click touchstart', function() {
							config.$bookBlock.bookblock( 'prev' );
							return false;
						} );

						config.$navPrev2.on( 'click touchstart', function() {
							config.$bookBlock.bookblock( 'prev' );
							return false;
						} );

						config.$navPrev3.on( 'click touchstart', function() {
							config.$bookBlock.bookblock( 'prev' );
							return false;
						} );

						config.$navPrev4.on( 'click touchstart', function() {
							config.$bookBlock.bookblock( 'prev' );
							return false;
						} );
						config.$navPrev5.on( 'click touchstart', function() {
							config.$bookBlock.bookblock( 'prev' );
							return false;
						} );
						config.$navPrev6.on( 'click touchstart', function() {
							config.$bookBlock.bookblock( 'prev' );
							return false;
						} );

						config.$navFirst.on( 'click touchstart', function() {
							config.$bookBlock.bookblock( 'first' );
							return false;
						} );

						config.$navLast.on( 'click touchstart', function() {
							config.$bookBlock.bookblock( 'last' );
							return false;
						} );

						// add swipe events
						$slides.on( {
							'swipeleft' : function( event ) {
								config.$bookBlock.bookblock( 'next' );
								return false;
							},
							'swiperight' : function( event ) {
								config.$bookBlock.bookblock( 'prev' );
								return false;
							}
						} );

						// add keyboard events
						$( document ).keydown( function(e) {
							var keyCode = e.keyCode || e.which,
								arrow = {
									left : 37,
									up : 38,
									right : 39,
									down : 40
								};

							switch (keyCode) {
								case arrow.left:
									config.$bookBlock.bookblock( 'prev' );
									break;
								case arrow.right:
									config.$bookBlock.bookblock( 'next' );
									break;
							}
						} );
					};

					return { init : init };

			})();


		</script>

  	<script>
  		Page.init();
  	</script>

  	<!-- Modal titulo de propiedad-->
      <div class="modal hide fade" id="modaltitulopropiedad" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true"  class="modal" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Título de propiedad</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" onClick='cargar();'>
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form>
                <div class="md-form">
                  <img  src="{{url('img/certificado-de-propiedad.jpg')}}" alt="titulo de propiedad" class="img-ayuda">
                  <p class="datoslegibles"><strong>Es importante que los datos marcados sean legibles:</strong></p>
                  <hr id="linea">
                  <ol>
                    <li>Datos del vehiculo</li>
                    <li>Datos del importador</li>
                    <li>Datos del propietario</li>
                  </ol>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>



	    <!-- Modal ine -->
      <div class="modal fade" id="modaline" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" class="modal" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Identificación oficial INE/IFE</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" onClick='cargar();'>
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <img  src="{{url('img/ine.jpg')}}" alt="identificación oficial" class="img-ayuda">
              <p class="datoslegibles"><strong>Es importante que los datos marcados sean legibles:</strong></p>
              <hr id="linea">
              <ol>
                <li>Nombre y apellidos</li>
                <li>Fotografía</li>
              </ol>
            </div>
          </div>
        </div>
      </div>


	    <!-- Modal pedimiento -->
      <div class="modal fade" id="modalpedimiento" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" class="modal" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Pedimento de importacion</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" onClick='cargar();'>
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <img  src="{{url('img/pedimento.jpg')}}" alt="pedimento de importacion" class="img-ayuda">
              <p class="datoslegibles"><strong>Es importante que los datos marcados con rojo sean legibles:</strong></p>
              <hr id="linea">
              <ol>
                <li>Número de pedimento</li>
                <li>Datos del vehículo</li>
              </ol>
            </div>
          </div>
        </div>
      </div>

	    <!-- Modal factura -->
      <div class="modal fade" id="modalfactura" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" class="modal" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Factura</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" onClick='cargar();'>
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <img src="{{url('img/factura.png')}}"  alt="factura" class="img-ayuda">
              <p></p>
              <p class="datoslegibles"><strong>Es importante que los datos marcados  sean legibles:</strong></p>
              <hr id="linea">
              <ol>
                <li>Datos del cliente y del vehiculo</li>
                <li>Sello</li>
              </ol>
            </div>
          </div>
        </div>
      </div>


	    <!-- Modal ordenamiento-->
      <div class="modal fade" id="modalordenamiento" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" class="modal" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Constancia de reordenamiento</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" onClick='cargar();'>
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <img src="{{url('img/reordenamiento.jpg')}}" alt="ordenamiento" class="img-ayuda">
              <p class="datoslegibles"><strong>Es importante que los datos marcados con rojo sean legibles:</strong></p>
              <hr id="linea">
              <ol>
                <li>Nombre del dueño</li>
                <li>Sello</li>
              </ol>
            </div>
          </div>
        </div>
      </div>


	    <!-- Modal pago de tenencia-->
      <div class="modal fade" id="modalpagotenencia" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" class="modal" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Pago de tenencia</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" onClick='cargar();'>
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <img src="{{url('/img/pagotenencia.JPG')}}" alt="pago tenencia" class="img-ayuda">
              <p class="datoslegibles"><strong>Es importante que los datos marcados sean legibles:</strong></p>
              <hr id="linea">
              <ol>
                <li>Nombre del dueño</li>
                <li>Linea de captura de ovh</li>
              </ol>
            </div>
          </div>
        </div>
      </div>


	    <!-- Modal tarjeta de circulacion-->
      <div class="modal fade" id="modaltarjetacirculacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" class="modal" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Tarjeta de circulación</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" onClick='cargar();'>
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <img src="{{url('img/tarjetaCirculacion.jpg')}}" alt="tarjeta circulacion" class="img-ayuda">
              <p class="datoslegibles"><strong>Es importante que los datos marcados sean legibles:</strong></p>
              <hr id="linea">
              <ol>
                <li>Nombre del propietario</li>
                <li>Datos del vehiculo (Número de serie, número de motor y placa)</li>
              </ol>
            </div>
          </div>
        </div>
      </div>

      <!-- Modal tarjeta de circulacion-->
	    <div class="modal fade" id="modalplaca" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" class="modal" data-backdrop="static" data-keyboard="false">
	      <div class="modal-dialog modal-dialog-centered" role="document">
	        <div class="modal-content">
	          <div class="modal-header">
	            <h5 class="modal-title" id="exampleModalLongTitle">Placa vehicular</h5>
	            <button type="button" class="close" data-dismiss="modal" aria-label="Close" onClick='cargar();'>
	              <span aria-hidden="true">&times;</span>
	            </button>
	          </div>
	          <div class="modal-body">
	            <img  src="{{url('img/placa.jpg')}}"  alt="placas" class="img-ayuda">
							<p class="datoslegibles"><strong>Es importante que los datos marcados sean legibles:</strong></p>
	            <hr id="linea">
	            <ol>
	              <li>Número de placa</li>
	            </ol>
	          </div>
	        </div>
	      </div>
	    </div>


      <script type="text/javascript">
      $(document).ready(function(){
        /**/var altura = $(document).height();
        $("html").animate({ scrollTop:+altura+"px" });
      });
      $( window ).ready(function() {
        //alert(altura);
        console.log('tiene que funcional');
        $('#step_m2').attr("class",'nav-link active');
        $('#step_m1 ').click(function(){
          window.location.replace("{{route('showIntro')}}");
        });


      });

      var flag = true;
      $("#frmGuia").submit(function(e){
        if(flag){
          e.preventDefault();

          var msj = '\n';
          var span = document.createElement('span');
          span.innerHTML='<ul>'
          var span2 = document.createElement('span');
          span2.innerHTML='</ul>'

          //REQUISITOS NACIONAL
          if (($('#tipoVeh:checked').val()==1)){
            if($('input:radio[name=preg1]:checked').val() == 0 ){
              msj = msj +'-Factura del vehículo '+'\n';
            }
            if($('input:radio[name=preg2]:checked').val() == 0 ){
              msj =msj +'-Tarjeta de circulación \n';
            }
            if($('input:radio[name=preg3]:checked').val() == 0 ){
              msj = msj +'-Pago de tenencia \n';
            }
            if($('input:radio[name=preg4]:checked').val() == 0 ){
              msj = msj +'-Identificación oficial \n';
            }
            if($('input:radio[name=preg5]:checked').val() == 0 ){
              msj = msj +'-Tarjeta de circulación \n';
            }
            if(msj.length>=2){
              swal("Requisitos incompletos","Usted puede continuar con el proceso de preregistro pero es IMPORTANTE que asista con los  siguientes requisitos: "+'\n'+msj+
              '\n'+"Estos requisitos pueden ser solicitados posteriormente"+'\n', "warning").then((value) => {
                flag =  false;
                $("#frmGuia").submit();
              });
              $('#step_m3 ').click(function(){
                window.location.replace("{{route('showMap')}}");
              });
            }else{
              swal("Requisitos completos", "Presione el botón para continuar", "success").then((value) => {
                flag =  false;
                $("#frmGuia").submit();
              });
              $('#step_m3 ').click(function(){
                window.location.replace("{{route('showMap')}}");
              });
            }
          }

          // REQUISITOS EXTRANJERO
          else   if (($('#tipoVeh:checked').val()==2)){
            if($('input:radio[name=preg1]:checked').val() == 0 ){

              msj =msj +'-Título de propiedad \n';
            }

            if($('input:radio[name=preg2]:checked').val() == 0 ){
            msj = msj +'-Tarjeta de circulación \n';
            }
            if($('input:radio[name=preg3]:checked').val() == 0 ){
              msj = msj +'-Pago de tenencia \n';
            }
            if($('input:radio[name=preg4]:checked').val() == 0 ){
              msj = msj +'-Identificación oficial \n';
            }
            if($('input:radio[name=preg5]:checked').val() == 0 ){
              msj = msj +'-Pedimento de importación \n';
            }
            if(msj.length>=2){
              swal("Requisitos incompletos", "Usted puede continuar con el proceso de preregistro pero es IMPORTANTE que asista con los  siguientes requisitos: "+'\n'+msj+'\n'+
              "Estos requisitos pueden ser solicitados posteriormente"+'\n', "warning").then((value) => {
                flag =  false;
                $("#frmGuia").submit();
              });
              $('#step_m3 ').click(function(){
                window.location.replace("{{route('showMap')}}");
              });
            }else{
              swal("Requisitos completos", "Presione el botón para continuar", "success").then((value) => {
                flag =  false;
                $("#frmGuia").submit();
              });
              $('#step_m3 ').click(function(){
                window.location.replace("{{route('showMap')}}");
              });
            }
          }
        }

      });
      </script>

@endsection
