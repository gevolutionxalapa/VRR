@section('title')
Preregistro
@endsection

<!-- link css -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/archivo.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/PreVehiculo/fileinput.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/PreVehiculo/theme.css') }}">
<!-- <link rel="stylesheet" href="{{ asset('css/reset.css')}}"> -->
<link rel="stylesheet" href="{{asset('css/top.css')}}">
<link rel="stylesheet" href="{{asset('plugins/select2/select2.css')}}">
{{-- <script type="text/javascript">var centreGot = false;</script>{!!$map['js']!!} --}}
<script src="{{ asset('js/modernizr.js') }}"></script>


<form id="formPredenuncia" enctype="multipart/form-data" method="POST" action="" accept-charset="UTF-8">
  <input type="hidden" name="latitud" value="{{$datos['latitud']}}">
  <input type="hidden" name="longitud" value="{{$datos['longitud']}}">
  {{Form::hidden('lat',null,['id'=>'lat'])}}
  {{Form::hidden('long',null,['id'=>'long'])}}
  <div class="row">
    <div class="col-md-7">
      <div class="card">
        <div class="card-header">
          <h5 align="left">Datos generales de la unidad</h5>
        </div>
        <div class="card-body">
          <input name="_token" type="hidden" value="6DekDHoPsxDQuzPw7fhVESQK0cHOU0YSOTp4MbAi">

          <div class="row">
            <div class="col-md-6 col-lg-6">
              <div class="form-group">
                <label for="idDocumentoCirculacion" class="col-form-label-sm">Documento de circulación<strong style="color: red">*</strong></label>
                <select class="templatingSelect10 form-control form-control-sm"  id="idDocumentoCirculacion" name="idDocumentoCirculacion" >
                  @foreach($documentosCirculacion as $documento)

                  <option value="{{$documento->id}}" >{{$documento->nombre}}</option>

                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-md-6 col-lg-6">
              <div class="form-group">
                <div id="divPlaca" @if($datos['flag_placa'] != 1) style = 'display: none' @endif>
                  <div class="form">
                    {!!Form::label('placa', 'Placa')!!}
                    <strong style="color: red">*</strong>
                  </div>
                  <div class="input-group">
                    {!!Form::text('placa',null,['class'=>'form-control form-control-sm nec', 'null' ,'id' => 'placa', 'placeholder' => 'HMZ2398 - EJEMPLO' , 'maxlength'=>'7',  'style'=>'text-transform: uppercase', 'data-validation'=>'custom', 'data-validation'=>'alphanumeric'])!!}
                  </div>
                </div>
                <div id="divFolio" @if($datos['flag_placa'] == 1) style = 'display: none' @endif>
                  <div class="form">
                    {!!Form::label('folioDoctoCirc', 'Folio')!!}
                    <strong style="color: red">*</strong>
                  </div>
                  <div class="input-group">
                    {!!Form::text('folioDoctoCirc',null,['class'=>'form-control form-control-sm', 'id' => 'folioDoctoCirc', 'maxlength'=>'10', 'style'=>'text-transform: uppercase', 'data-validation'=>'custom', 'data-validation'=>'alphanumeric'])!!}
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6 col-lg-6">
              <div class="form-group">
                <label for="idEstado" class="col-form-label-sm">Entidad federativa  de la placa <strong style="color: red">*</strong></label>
                <select  class="templatingSelect2 form-control form-control-sm"  id="idEstado" name="idEstado" required @if($datos['flag_placa']==2)disabled @endif>
                  <option value="" selected="selected"></option>
                  @foreach($estados as $estado)
                  <option  value="{{$estado->id}}" >{{$estado->nombre}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-md-6 col-lg-6">
              <div class="form-group">
                <label for="idMunicipio" class="col-form-label-sm">Municipio del incidente (Veracruz)<strong style="color: red">*</strong></label>
                <input type="text" id="text_municipio" name="text_municipio" hidden>
                <select class="templatingSelect11 form-control form-control-sm" required id="idMunicipio" name="idMunicipio" >
                  <option value="" selected="selected"></option>
                </select>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6 col-lg-6">
              <div class="form-group">
                <label for="idMarca" class="col-form-label-sm">Marca<strong style="color: red">*</strong></label>
                <select class="templatingSelect3 form-control form-control-sm" required id="idMarca" name="idMarca"  >
                  <option value="" selected="selected" > </option>
                  @foreach($marcas as $marca)
                  <option  value="{{$marca->id}}">{{$marca->nombre}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-md-6 col-lg-6">
              <div class="form-group">
                <label for="idSubmarca" class="col-form-label-sm">Submarca<strong style="color: red">*</strong></label>
                <input type="text" id="text_submarca" name="text_submarca" hidden>
                <select class="idSubmarca form-control form-control-sm" required id="idSubmarca" name="idSubmarca" >
                  <option value="" selected="selected"></option>
                </select>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6 col-lg-6">
              <div class="form-group">
                <label for="modelo" class="col-form-label-sm">Modelo<strong style="color: red">*</strong></label>
                <input class="form-control form-control-sm" value="{{date('Y')}}" min="1885" max="{{date('Y')+1}}" name="modelo" maxlength="4" type="number" required id="modelo">
              </div>
            </div>
            <div class="col-md-6 col-lg-6">
              <div class="form-group">
                <label for="idColor" class="col-form-label-sm">Color<strong style="color: red">*</strong></label>
                <select class="templatingSelect5 form-control form-control-sm" required id="idColor" name="idColor" >
                  <option selected="selected" value=""> </option>
                  @foreach($colores as $color)
                  <option value="{{$color->id}}">{{$color->nombre}}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6 col-lg-6">
              <div class="form-group">
                <label for="numSerie" class="col-form-label-sm nec">N&uacute;mero de serie</label>
                <input class="form-control form-control-sm" placeholder="INGRESE EL NÚMERO DE SERIE" maxlength="17" name="numSerie" type="text"  id="numSerie"  onkeyup="mayus(this);" >
              </div>
            </div>
            <div class="col-md-6 col-lg-6">
              <div class="form-group">
                <label for="numMotor" class="col-form-label-sm">N&uacute;mero de motor</label>
                <input class="form-control form-control-sm nec" placeholder="INGRESE EL NÚMERO DE MOTOR" maxlength="10" name="numMotor" type="text"  id="numMotor" style="text-transform: uppercase" >
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6 col-lg-6">
              <div class="form-group">
                <label for="idProcedencia" class="col-form-label-sm">Procedencia</label>
                <select class="form-control form-control-sm" id="idProcedencia" name="idProcedencia">
                  <option value="1" @if ($datos['tipoVeh'] == 1) selected @endif>NACIONAL</option>
                  <option value="2" @if ($datos['tipoVeh'] == 2) selected @endif>EXTRANJERO</option>
                </select>
              </div>
            </div>
            <div class="col-md-6 col-lg-6">
              <div class="form-group">
                <label for="telefono " class="col-form-label-sm nec">Tel&eacute;fono de contacto</label>
                <input class="form-control form-control-sm validanumericos" placeholder="INGRESE EL TELÉFONO" maxlength="10" name="telefono" type="text"  id="telefono" data-validation='number'>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12 col-lg-12">
              <div class="form-group">
                <label for="idAseguradora" class="col-form-label-sm">Aseguradora</label>
                <select class="templatingSelect6 form-control form-control-sm"  id="idAseguradora" name="idAseguradora" >
                  <option selected="selected" value=""> </option>@foreach($aseguradoras as $aseguradora)
                  <option value="{{$aseguradora->id}}">{{$aseguradora->nombre}}</option>@endforeach
                </select>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-12 col-md-12">
              <div class="form-group">
                <label for="senasPartic" class="col-form-label-sm">Se&ntilde;as particulares</label>
                <textarea class="form-control form-control-sm"  style="text-transform: uppercase" placeholder="INGRESE LAS SEÑAS PARTICULARES" rows="3" name="senasPartic" cols="50" id="senasPartic"></textarea>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>

    <!-- SECCION  DE CARGA DE IMAGENES POR BUSCADOR Y CAMARA WEB -->
    <div class="col-md-5 col-lg-5">
      <div class="card">
        <div class="card-header d-flex justify-content-between align-items-center">
          <h5 align="left" id="tit">Carga de documentos  </h5> <h5 id="btng" align="right" ><button type="submit" class="btn btn-primary" id="btnEmpty">Guardar</button></h5>
        </div>
        <br>
        <div align="center" >
          <div class="container" >
            <div class="btn-group btn-group-toggle primary" data-toggle="buttons" id="selector">
              <label class="btn btn-secondary btn-rounded  form-check-label ">
                <input type="radio" name="img"  id="opcion1"  value="archivo" data-validation="required" required>
                <i class="fa fa-upload"></i>
                Archivos
              </label>
              <label id="divBtnCamara" class=" btn btn-secondary btn-rounded form-check-label">
                <input align="center"  name="img" type="radio" id="opcion2" value="camara" data-validation="required">
                <i class="fa fa-camera"></i>
                Camara
              </label>
            </div>
          </div>
          <hr>
          <div id="complemento_asc" style="display:none;">
            <div class="row">
              <div class="container">
                <div class="col-lg-8 col-md-8">
                  <p align="center">Identificación oficial <strong style="color: red">*</strong></p>
                  <div id="iden" >
                    <select class="listaiden1 form-control form-control-sm listaiden" id="listaiden_op1" name="listaiden1" responsive required>
                      <option selected value="" >--Seleccione una opción--</option>
                      @foreach($identificacionoficial as $identificacionoficial)
                      <option  value="{{$identificacionoficial->id}}" >{{$identificacionoficial->nombre}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="div1" style="display:none;" >
              <br>
              <div class="row" align="center">
                <div class="col-lg-3 col-md-3"></div>
                <!--busqueda de archivos identificacion oficial-->
                <div class="col-lg-7 col-md-7">
                  <div class="file-loading" >
                    <input id="DOC1" class="btn_ide" name="ID_OFICIAL" type="file" accept="image/*" style="display: none;" >
                  </div>
                </div>
              </div>
            </div>
            <div class="div2" style="display:none;">
              <div class="row">
                <div class="col-lg-12 col-md-12" align="center">
                  <div class="" id="ife_mini"></div>
                  <input type="hidden" id="mini_ife" name="cam_ident" value="" >
                  <button id="cam_ide" type="button" class="btn btn-dark" data-toggle="modal" data-target="#profile-photo-camera-modal" onclick="setup('IFE')" disabled="disabled" data-toggle="popover" data-trigger="focus hover" title="ATENCIÓN:Capture una imagen" >
                    <i class="fa fa-camera"></i> Capturar imagen
                  </button>
                  <button type="button" id="borrar2"  class="btn btn-secondary" :disabled="form.busy" ata-target="#profile-photo-camera-modal"  disabled="disabled">
                    <i class="fa fa-trash"></i>
                  </button>
                </div>
              </div>
            </div>
            <br>
            <div class="row">
              <div class="container">
                <div class="col-lg-8 col-md-8">
                  <p align="center">Factura del vehículo <strong style="color: red">*</strong></p>
                  <div id="iden" >
                    <select class="listaiden3 form-control form-control-sm listaiden" id="listaiden_op2" name="listaiden3" required>
                      <option selected value="" >--Seleccione una opción--</option>
                      @foreach($factura as $factura)
                      <option  value="{{$factura->id}}" >{{$factura->nombre}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <input type="hidden" id="tipe_load" name="optCaptura" value="loadFileType">
         </div>
       </div>
          <div class="div1" style="display:none;">
            <br>
            <div class="row" align="center">
              <div class="col-lg-3 col-md-3"></div>
              <div class="col-lg-7 col-md-7">
                <div class="file-loading">
                  <input id="DOC5" class="btn-fac" name="ID_FACTURA" type="file" accept="image/*" style="display: none;" >
                </div>
              </div>
            </div>
          </div>
          <div class="div2" style="display:none;">
            <div class="row">
              <div class="col-lg-12 col-md-12" id="ifecap" align="center">
                <div class="" id="fact_mini"></div>
                <input type="hidden" id="mini_fac" name="cam_fact" value="">
                <button id="cam_fac" type="button" class="btn btn-dark" data-toggle="modal" data-target="#profile-photo-camera-modal" onclick="setup('FACT')" disabled="disabled"  data-toggle="popover" data-trigger="focus hover" title="ATENCIÓN:Capture una imagen" required >
                  <i class="fa fa-camera"></i> Capturar imagen
                </button>
                <button type="button" id="borrar"  class="btn btn-secondary" :disabled="form.busy" ata-target="#profile-photo-camera-modal"  disabled="disabled">
                  <i class="fa fa-trash"></i>
                </button>
              </div>
            </div>
          </div>
          <br>
        </div>
      </div>
    </div>

</form>
<p></p>
<br>

<div class="row">
  <div class="col-lg-12 col-md-12" id="mapa" style="display: none; padding-bottom:20px">
    <div>
      <h5 align="center">UIPJ más cercanas</h5>
    </div>
    <br>
    <div>
    {!!$map['html']!!}
    </div>
  </div>
</div>

<div class="card-body">
  <div align="right">
   <a href="#0" class="cd-top">Top</a>
  </div>
</div>
<div class="">
  <div class="part2">
    <div id="profile-photo-camera-modal" class="modal fade" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Captura de documentos</h5>
            <button type="button" onClick="close_cam(true)" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" id="cuerpo">
            <div id="web_cam_section" class="alert alert-danger" v-if="form.errors.has('photo')">
              <div id="my_camera"></div>
            </div>
            <div id="profile-photo-camera-preview">
              <div id="results" > </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" id="take_p" class="btn btn-primary" :disabled="form.busy" onClick="take_snapshot()">Capturar</button>
            <!-- <button type="button" class="btn btn-primary" :disabled="form.busy" onClick="close_cam(true)"  data-dismiss="modal">Cancelar</button> -->
          </div>
        </div>
      </div>
    </div>
    <style>
    #profile-photo-camera-preview,
    #profile-photo-camera-preview video {
      width: 100% !important;
      height: auto !important;
      min-width: 100px;
      min-height: 100px;
    }
    </style>
  </div>
  <br>
  <script type="text/javascript" src="{{ asset('js/webcam.min.js') }}"></script>

  <script language="JavaScript">
    function mostrarOpcionSeleccionada(idEstado=null) {
        //var $option = $select.find('option:selected');

        var identificador = (idEstado!=null)?idEstado:$('#idEstado').val();
        var respuesta;
        $('#idMunicipio').html('');
        if(identificador!=''){
          var ruta = "{{route('municipio',':estado')}}";
          ruta = ruta.replace(':estado', identificador);
          datos = null;
          respuesta = peticionInfomacion('get',ruta,datos);
          if (respuesta != null) {
            for(i=0; i<respuesta.estado.length; i++){
              $("#idMunicipio").append("<option value='"+respuesta.estado[i].idMunicipio+"'> "+respuesta.estado[i].nombre+"</option>");
            }
            if(respuesta.estado.length > 0 ){
              localStorage.setItem("idMunicipio", respuesta.estado[0].id);
            }
          }else{
            alert('Error de servidor');
          }
        }else{
        }
      }
  //eliminar fotografia de camara
  $(document).ready(function() {
    


    

    $("#borrar").click(function(event) {
      //$("#ife_mini").remove();
      $('#fact_mini').html('');
    });
    $("#borrar2").click(function(event) {
      $('#ife_mini').html('');
    });
  });
  //fin foto grafia de camara
  var cap="";
  Webcam.set({
    width: 320,
    height: 240,
    image_format: 'jpeg',
    jpeg_quality: 90
  });
  function take_snapshot(tipo) {
    Webcam.snap( function(data_uri) {
      document.getElementById('results').innerHTML ='<img class="img-thumbnail" src="'+data_uri+'"/>';
      var img = $("#results").children("img").clone();
      var tipo = (cap == 'FACT')? '#fact_mini':'#ife_mini';
      console.log('Este es el tipo -> '+tipo+' esta es la imagen ->'+img);
      $(tipo).html(img);
      //-----2
      var tipo2 = (cap == 'FACT')? '#mini_fac':'#mini_ife';
      $(tipo2).val(data_uri);
      //------2
      setTimeout(function() {
        $('#profile-photo-camera-modal').modal('hide');
        $("#results").html('');
      }, 3000);

    });
    close_cam(false);
    $('#web_cam_section').hide('slow');
  }
  function setup(tipo) {
    this.cap = tipo;
    Webcam.reset();
    Webcam.attach( '#my_camera' );
    $('#web_cam_section').show('slow');
  }
  function close_cam(act){
    if(act){
      $("#results").html('');
    }
    console.log('Calmando el evento');
    Webcam.reset();
  }
  </script>

</div>
<style media="screen">
#my_camera{
  display: block;
  margin: auto;
}
#web_cam_section{
  background-color: #717171;
  border-color: #424242;
}
#cuerpo{
  display: block;
  margin: auto;
}
#ife_mini,#fact_mini{
  padding-bottom: 1em;
}
</style>

<!-- javascript  -->
<script src="{{ asset('js/prevehiculo/peticiones.js') }}"></script>
<script src="{{ asset('js/prevehiculo/select2.js') }}"></script>
<script src="{{ asset('js/prevehiculo/ordenar.js') }}"></script>


<script src="{{ asset('js/prevehiculo/sortable.min.js') }}"></script>
<script src="{{ asset('js/prevehiculo/fileinput.min.js') }}"></script>
<script src="{{ asset('js/prevehiculo/locales/es.js') }}"></script>

<script src="{{ asset('js/prevehiculo/explorer-fa/theme.min.js') }}"></script>
<script src="{{ asset('js/prevehiculo/fa/theme.min.js') }}"></script>
<script src="{{ asset('js/prevehiculo/carga.js') }}"></script>
<script src="{{ asset('plugins/select2/select2.min.js') }}"></script>
<script src="{{ asset('js/geolocalizacion/geolocalizacion.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/camara/camara_validacion.js') }}"></script>
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script src="{{ asset('js/main.js') }}"></script>

<script type="text/javascript">
  $(document).ready(function() {
    // var tienePlaca= {{$datos['isExtranjera']}};
    //   console.log("entra aqui");
    // console.log(tienePlaca);
    // alert(tienePlaca);
    if({{$datos['isExtranjera']}}){
      //alert("placa");
      $('#idDocumentoCirculacion').val({{ $datos['isExtranjera'] }}).trigger('change')
    }else{
      //alert("no placa");
      $('#idDocumentoCirculacion').val({{ $datos['listaplaca'] }}).trigger('change')
    }
  });
var idEstado = 0;
var idMunicipio = 0;
// PLUGIN CARGA DE IMAGENES CON VISTA PREVIA
$("#DOC1").fileinput({
  theme: 'fa',
  showUpload: false,
  showCaption: false,
  browseClass: "btn btn-secondary",
  allowedFileExtensions: ["jpg"],
  maxImageWidth: 1024,
  maxImageHeight: 768,
  overwriteInitial: false,
  initialPreviewAsData: true,
   maxFileSize: 2000,
});
$("#DOC5").fileinput({
  theme: 'fa',
  showUpload: false,
  showCaption: false,
  browseClass: "btn btn-secondary ",
  fileTypes: "any",
  allowedFileExtensions: ["jpg"],
  maxImageWidth: 1024,
  maxImageHeight: 768,
  overwriteInitial: false,
  initialPreviewAsData: true,
   maxFileSize: 2000,
});
//rutaForm = '{{route('savePredenuncia.vehiculo')}}';

//Pintar mapa de acuerdo al municipio
$('#idMunicipio').change(function(){
  //alert();
  var identificador = $(this).val();
  var respuesta;
  var locations = [];
  if(identificador!=''){
    var ruta = "{{route('get_position',':municipio')}}";
    ruta = ruta.replace(':municipio', identificador);
    datos = null;
    respuesta = peticionInfomacion('get',ruta,datos);
    if (respuesta != null) {
      $('#mapa').css('display','');
      console.log('Total de elementos ->'+respuesta.length);
      for(i=0; i< respuesta.length; i++){
        var puntoMap = [respuesta[i]['nombre'],respuesta[i]['latitud'],respuesta[i]['longitud']];
        locations.push(puntoMap);
      }
      console.log('Este es el arreglo');
      console.log(locations);
      var map = new google.maps.Map(document.getElementById('map_canvas'), {
        zoom: 8,
       // center: new google.maps.LatLng(locations[0][1], locations[0][2]),
        center: new google.maps.LatLng(19.976312,-96.7279684),

        mapTypeId: google.maps.MapTypeId.ROADMAP
      });
      var infowindow = new google.maps.InfoWindow();
      var marker, i;

      for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(locations[i][1], locations[i][2]),
          map: map
        });
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
            infowindow.setContent(locations[i][0]);
            infowindow.open(map, marker);
          }
        })(marker, i));
      }
    }
  }
});

$("#idMarca").change(function() {
  var identificador = $(this).val();
  var respuesta;
  $('#idSubmarca').html('');
  if(identificador!=''){
    var ruta = "{{route('submarca',':marca')}}";
    ruta = ruta.replace(':marca', identificador);
    datos = null;
    respuesta = peticionInfomacion('get',ruta,datos);
    if (respuesta != null) {
      for(i=0; i<respuesta.estado.length; i++){
        $("#idSubmarca").append("<option value='"+respuesta.estado[i].id+"'> "+respuesta.estado[i].nombre+"</option>");
      }
      if(respuesta.estado.length > 0 ){
        localStorage.setItem("idSubmarca", respuesta.estado[0].id);
      }
      //console.log('cargando datos');
    }
  }
});

$("#idSubmarca").change(function() {
  localStorage.setItem("idSubmarca", $('#idSubmarca').val());
});

// $("#idEstado").change(function() {
//   var identificador = $(this).val();
//   var respuesta;
//   $('#idMunicipio').html('');
//   if(identificador!=''){
//     var ruta = "{{route('municipio',':estado')}}";
//     ruta = ruta.replace(':estado', identificador);
//     datos = null;
//     respuesta = peticionInfomacion('get',ruta,datos);
//     if (respuesta != null) {
//       for(i=0; i<respuesta.estado.length; i++){
//         $("#idMunicipio").append("<option value='"+respuesta.estado[i].idMunicipio+"'> "+respuesta.estado[i].nombre+"</option>");
//       }
//       if(respuesta.estado.length > 0 ){
//         localStorage.setItem("idMunicipio", respuesta.estado[0].id);
//       }
//       //console.log('cargando datos');
//     }
//   }
// });


$('#formPredenuncia').submit(function (e) {
  e.preventDefault();
  //alert($("#opcion2").val());
  $('#idEstado').removeAttr('disabled');
  var valor = $("#opcion2:checked").val();
  if(valor =='camara'){
    //alert("aqui");
    if($('#mini_ife').val()=='' && (valor == "camara")){
      toastr.warning("Debe de capturar la identificación oficial y la factura con la camara",{"preventDuplicates":true
            });
      $('.popover-dismiss').popover({
        trigger: 'hover'
      })

      return false;
    }

    if($('#mini_fac').val()=='' && (valor == "camara")){
      toastr.warning("Debe de capturar la identificación oficial y la factura con la camara",{"preventDuplicates":true
            });

    $('.popover-dismiss').popover({
      trigger: 'hover'
    })


         return false;
     }
  }

  // if (($('#placa').val() != "")||($('#numMotor').val() != "")||($('#numSerie').val() != "")) {
    var formData = new FormData(this);
    idEstado = $('#idEstado').val();
    idMunicipio = $('#idMunicipio').val();
    //var marca = $('select[name="idMarca"] option:selected').text();
    //var submarca = $('select[name="idSubmarca"] option:selected').text();
    $.ajax({
      url : "{{route('savePredenuncia.vehiculo')}}",
      data : formData,
      type : 'POST',
      cache: false,
      contentType : false,
      processData : false,

      success : function(json) {
        //console.log('Se agrego un vehiculo');
        //console.log(json);
        var ruta = '{{ route('generar-pdf') }}/'+json.token+'/'+idEstado+'/'+idMunicipio;

        var token = "'"+json.token+"'";
        //console.log(marca);
        var span = document.createElement('span');
        span.innerHTML= '<img src="data:image/png;base64,'+json.codigoQR+'">'
        +'<br><a href="'+ruta+'" class="" target="_blank">Descargar PDF</a>'
        +'<br><input id="correo" type="email" placeholder="CORREO ELECTRÓNICO" class="form-control form-control-sm" required>'
        +'<br><input type="button" value="Enviar" class="btn btn-primary" onclick="enviar_mail('+token+')">'
        +'<br><br><span id="email_mjs"></span>'
        swal({
          title: "Su folio de Preregistro es: "+json.token,
          text: "Su preregistro se realizo correctamente",
          icon: "success",
          showConfirmButton:true,
          confirmButtonText:"Guardar",
          content: span,
          closeOnClickOutside: false,
                });


      },
      error : function(xhr, status) {
        console.log('Disculpe, existió un problema');

        var errores = Object.values(xhr.responseJSON.errors);
        var msj = '';

        for(var i = 0; i < errores.length; i++)
        {
          for(var j =0 ; j < errores[i].length; j++)
          {
            msj = msj+errores[i][j]+'\n';
          }
        }
        console.log(msj);
        swal({
          title: "Error al introducir los datos",
          text: msj,
          icon: "error",

        });
      },

      complete : function(xhr, status) {
        console.log('Petición realizada');
         //window.location.href = "http://localhost/VRR/public/";

        //setTimeout(function(){window.top.location='ver-intro'}, 10000)

        $(".swal-button-container>button").bind("click",function(){window.top.location='ver-intro';});
      }

    });

    //limpiar formulario
    //window.location.href = "http://localhost/VRR/public/";
    document.getElementById("formPredenuncia").reset();
    $("#idEstado").val('').trigger('change');
    $("#idMarca").val('').trigger('change');
    //$("#idSubmarca").val('').trigger('change');
    // $("#idMunicipio").val('').trigger('change');
    $("#idColor").val('').trigger('change');
    $("#idAseguradora").val('').trigger('change');
    $('#ife_mini, #fact_mini').html('');
    $('#mini_ife,#mini_fac').val('');
    $('#complemento_asc, #div1, #div2').hide();
    var validator = $( "#formPredenuncia" ).validate();
    validator.resetForm();

  // }else{
  //   swal({
  //     title: "Datos incompletos",
  //     text: "Debe ingresar al menos uno de los siguientes 3 campos: "+'\n'+"Placa, Número de serie/VIN o Número de motor  ",
  //     icon: "error",
  //     button: "OK",
  //   });
  // }

});

function enviar_mail (codigo){
  var codigo = codigo;
  var correo =  $('#correo').val();
  var info = {"correo":correo, "folio": codigo };
  var msj = "";

  $.ajax({
    url : "{{route('sendMail')}}/"+correo+"/"+codigo+"/"+idEstado+"/"+idMunicipio,
    data : info,
    type : 'get',
    cache: false,
    contentType : false,
    processData : false,

    success : function(json) {
      $('#email_mjs').html('<div class="alert alert-primary" role="alert"> El codigo a sido enviado a su correo </div>');
    },

    error : function(xhr, status) {
      $('#email_mjs').html('<div class="alert alert-danger" role="alert"> Disculpe, existió un problema </div>');
    },
  });
}

// $( document ).ready(function() {

$('input, select, textarea').keypress(function(e){
  tecla = (document.all) ? e.keyCode : e.which;
  return (tecla != 13);
  //console.log('hola adios');
});
get_position('latitud', 'longitud');
verificar_camara('divBtnCamara');
console.log( "ready!" );
var idmarca = $("#idMarca").val();
if (idmarca > 0) {
  var ruta = "{{route('submarca',':marca')}}";
  ruta = ruta.replace(':marca', idmarca);
  datos = null;
  respuesta = peticionInfomacion('get',ruta,datos);
  if (respuesta != null) {
    for(i=0; i<respuesta.estado.length; i++){
      $("#idSubmarca").append("<option value='"+respuesta.estado[i].id+"'> "+respuesta.estado[i].nombre+"</option>");
      //console.log(respuesta.estado[i].id);
    }
    $('#idSubmarca').val(localStorage.idSubmarca).trigger("change");
  }
}
$('#listaiden_op1').change(function(){
  var valor = $('#listaiden_op1').val();
  if(valor!=''){
    var tipoC = $('#tipe_load').val();
    if (tipoC==1){
      $('#DOC1').css('display','');
      $('#cam_ide').attr('disabled',true);
      $('#borrar2').attr('disabled',true);
    }else{
      $('#cam_ide').removeAttr('disabled');
      $('#DOC1').css('display','none');
      $('#borrar2').removeAttr('disabled');
    }
  }
});
$('#listaiden_op2').change(function(){
  var valor = $('#listaiden_op2').val();
  if(valor!=''){
    var tipoC = $('#tipe_load').val();
    if (tipoC==1){
      $('#DOC5').css('display','');
      $('#cam_fac').attr('disabled',true);
      $('#borrar').attr('disabled',true);
    }else{
      $('#cam_fac').removeAttr('disabled');
      $('#DOC5').css('display','none');
      $('#borrar').removeAttr('disabled');
    }
  }
});
$('#idDocumentoCirculacion').change(function(){
  var doc= $('#idDocumentoCirculacion').val();
  console.log('Este es el que quiero');
  console.log(doc);
  if(doc==1 || doc == 5){
    $('#placa').val('').attr("required", true);
    $('#divPlaca').css('display','');
    $('#divFolio').css('display','none');
    $('#folioDoctoCirc').val('').attr("required", false);
    $('#numSerie','#numMotor').attr("required", false);
  }else{
    $('#placa').val('').attr("required", false);
    $('#divPlaca').css('display','none');
    $('#divFolio').css('display','');
    $('#folioDoctoCirc').val('').attr("required", true);
  }
});

// $("#idDocumentoCirculacion").change(function(){
//   var selec = $("#idDocumentoCirculacion").val();
//   console.log(selec);
//   if(selec ==  1 || selec == 5) {
//     $('#folioDoctoCirc').val('');
//     $('#folioDoctoCirc').attr("required", false);
//     $('#divPlaca').css('display','');
//     $('#divFolio').css('display','none');
//   } else {
//     $('#placa').val('');
//     $('#divPlaca').css('display','none');
//     $('#divFolio').css('display','');
//   }
// });

$("#idDocumentoCirculacion").change(function(){
      $('#placas').val('');
      $('#folioDoctoCirc').val('');
      var selec = $("#idDocumentoCirculacion").val();
      if(selec ==  1 || selec ==5) {
        if(selec == 1) {
          $("#placa").attr('maxlength','7');
        } else {
          $("#placa").attr('maxlength','10');
        }
        $('#idEstado').removeAttr('disabled');
        $('#divPlaca').css('display','');
        $('#divFolio').css('display','none');
        $('#idEstado').val(30).trigger('change');
       mostrarOpcionSeleccionada();
      } else {
        $('#idEstado').attr('disabled',true);
        $('#divPlaca').css('display','none');
        $('#divFolio').css('display','');
        $('#idEstado').val(33).trigger('change');
        mostrarOpcionSeleccionada(30);

      }
    });

onload = function(){
  var ele = document.querySelectorAll('.validanumericos')[0];
  ele.onkeypress= function(e){
    if(isNaN(this.value+String.fromCharCode(e.charCode)))
      return false;
  }
  ele.onpaste= function(e){
    e.preventDeFault();
  }
}


</script>
<script type="text/javascript" src="{{ asset('js/top.js') }}"></script>
