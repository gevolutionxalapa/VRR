@extends('template.main')

@section('title')
Mapa
@endsection


@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/styleBarSearchMap.css') }}">
{{-- <script type="text/javascript">var centreGot = false;</script> --}}
{{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyADLKCVRyl7I3LK5INMWsrqWhQgTkdCdVY&libraries=places" async defer></script> --}}
<script src="{{ asset('js/geolocalizacion/geolocalizacion.js') }}"></script>
{{-- <script src="{{ asset('js/geolocalizacion/barSearchMap.js') }}"></script> --}}

<div class="container">

	<h2 id ="tit" class="text-center">Ubicación de los hechos</h2>
	<div class="row">
		<input id="pac-input" class="controls" type="text" placeholder="Buscar ubicación">
		<div class="col-md-12" id="mapa">
			{!!$map['html']!!}
		</div>
	</div>
	<div class="row">

		<div class="col-sm-12">
			<div class="alert">
				<form  method="post" action="{{route('cargar-guia')}}" name="frmMapa" id="frmMapa">
					<div align="right">
    					<button  value="submit" id="btn-enviar" class="btn btn-primary btn_block" align="right" ><i class="fa fa-check" data-toggle="tooltip" data-placement="right" title="Ir a pre-registro"></i></button>
			    	</div>
			    	{!!Form::hidden('latMap',null,['class'=>'form-control form-control-sm', 'id' => 'latMap', 'onkeyup'=>'mayus(this);'])!!}
					{!!Form::hidden('lonMap',null,['class'=>'form-control form-control-sm', 'id' => 'lonMap', 'onkeyup'=>'mayus(this);'])
					!!}
					{!!Form::hidden('tipoVeh',$datos['tipoVeh'],['class'=>'form-control form-control-sm', 'id' => 'tipoVeh'])!!}
					{!!Form::hidden('flag_placa',$datos['flag_placa'],['class'=>'form-control form-control-sm', 'id' => 'flag_placa'])
					!!}
					{!!Form::hidden('listaplaca',$datos['listaplaca'],['class'=>'form-control form-control-sm', 'id' => 'listaplaca'])
					!!}
					{!!Form::hidden('isExtranjera',$datos['isExtranjera'],['class'=>'form-control form-control-sm', 'id' => 'isExtranjera'])
					!!}
				</form>
			</div>
		</div>

	</div>

</div>
<script type="text/javascript">
	$( window ).ready(function() {
		$('#step_m3').attr("class",'nav-link active');

		$('#step_m2 ').click(function(){
			window.location.replace("{{route('showGuia')}}");
		});
		$('#step_m4 ').click(function(){
			window.location.replace("{{route('showPredenuncia.vehiculo')}}");
		});

		map = new google.maps.Map(document.getElementById('map_canvas'), {
			center: { lat: 19.976312, lng: -96.7279684 },
			zoom: 7,
			mapTypeId: 'roadmap'
		});
		initAutocomplete();


	});
</script>

@endsection
