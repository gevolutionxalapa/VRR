<link rel="stylesheet" href="{{asset('css/foundation-icons/foundation-icons.css')}}" />

<form class="form cf">
  <div class="wizard">
    <div class="wizard-inner">
      <div class="connecting-line"></div>
      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="nav-item">
          <a href="{{route('showIntro')}}" id="step_m1" data-toggle="tab" aria-controls="step1" role="tab" title="Paso 1" class="nav-link disabled">
            <span class="round-tab">
              <i class="fa fa-bookmark"></i>
            </span>
          </a>
        </li>

        <li role="presentation" class="nav-item">
          <a href="{{route('showGuia')}}" id="step_m2" data-toggle="tab" aria-controls="step2" role="tab" title="Paso 2" class="nav-link disabled">
            <span class="round-tab">

              <i class="fa fa-list-ol"></i>
            </span>
          </a>
        </li>
        <li role="presentation" class="nav-item">
          <a href="{{route('showMap')}}" id="step_m3" data-toggle="tab" aria-controls="step3" role="tab" title="Paso 3" class="nav-link disabled">
            <span class="round-tab">

              <i class="fi-compass"></i>
            </span>
          </a>
        </li>
        <li role="presentation" class="nav-item">
          <a href="{{route('showPredenuncia.vehiculo')}}" id="step_m4" data-toggle="tab" aria-controls="step4" role="tab" title="Paso 4" class="nav-link disabled">
            <span class="round-tab">
              <i class="fi-social-foursquare"></i>
            </span>
          </a>
        </li>
      </ul>
    </div>
  </div>
</form>

<style media="screen">
.wizard {
  margin: 20px auto;
  background: #e1e2e1;
}

.wizard .nav-tabs {
  position: relative;
  margin: 40px auto;
  margin-bottom: 0;
  border-bottom-color: #e0e0e0;
}

.wizard > div.wizard-inner {
  position: relative;
}

.connecting-line {
  height: 2px;
  background: gray;
  position: absolute;
  width: 80%;
  margin: 0 auto;
  left: 0;
  right: 0;
  top: 50%;
  z-index: 1;
}

.wizard .nav-tabs > li.active > a,
.wizard .nav-tabs > li.active > a:hover,
.wizard .nav-tabs > li.active > a:focus {
  color: #555555;
  cursor: default;
  border: 0;
  border-bottom-color: transparent;
}

span.round-tab {
  width: 70px;
  height: 70px;
  line-height: 70px;
  display: inline-block;
  border-radius: 100px;
  background: #eaeaea;
  border: 2px solid gray;
  z-index: 2;
  position: absolute;
  left: 0;
  text-align: center;
  font-size: 25px;
}

span.round-tab i {
  color: #555555;
}

.wizard li a.active span.round-tab {
  background: #fff;
  border: 2px solid #101010;

}

.wizard li a.active span.round-tab i {
  color: #101010;
}

span.round-tab:hover {
  color: #333;
  border: 2px solid #333;
}

.wizard .nav-tabs > li {
  width: 25%;
}

.wizard li a:after {
  content: " ";
  position: relative;
  left: 46%;
  top: -20px;
  opacity: 0;
  margin: 0 auto;
  bottom: 0px;
  border: 5px solid transparent;
  border-bottom-color: #5bc0de;
  transition: 0.1s ease-in-out;
}

.wizard li.active.nav-item:after {
  content: " ";
  position: relative;
  left: 46%;
  top: -20px;
  opacity: 1;
  margin: 0 auto;
  bottom: 0px;
  border: 10px solid transparent;
  border-bottom-color: #5bc0de;
}

.wizard .nav-tabs > li a {
  width: 70px;
  height: 70px;
  margin: 20px auto;
  border-radius: 100%;
  padding: 0;
  position: relative;
}

.wizard .nav-tabs > li a:hover {
  background: transparent;
}

.wizard .tab-pane {
  position: relative;
  padding-top: 50px;
}

.wizard h3 {
  margin-top: 0;
}

@media( max-width: 585px) {

  .wizard {
      width: 90%;
      height: auto !important;
  }

  span.round-tab {
      font-size: 16px;
      width: 50px;
      height: 50px;
      line-height: 50px;
  }

  .wizard .nav-tabs > li a {
      width: 50px;
      height: 50px;
      line-height: 50px;
  }
</style>
