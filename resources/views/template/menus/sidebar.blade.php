<aside class="main-sidebar elevation-4 barra-izquierda collapsado" style="overflow-x: hidden;" >
	<!-- Brand Logo -->
	<a href="{{ url('home') }}" class="brand-link">
		{{-- <img src="https://rawcdn.githack.com/Romaincks/assets/master/img/only-escudo-ver.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
		style="opacity: .8"> --}}
		<img src="{{ asset('img/only-escudo-ver.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
		style="opacity: .8">
		<span class="brand-text font-weight-light">FGE | Veracruz</span>
	</a>

	<!-- Sidebar -->
	<div  class="sidebar font-weight-light">
		<!-- Sidebar user panel (optional) -->
		@auth
		<div class="user-panel mt-3 pb-3 mb-3 d-flex">
			<div class="image">
				<a href="#fotoPerfil" id="botonModalFoto" data-toggle='modal' data-target='#fotoPerfil'> @if (Auth::user()->foto!=null || Auth::user()->foto!="")  <img id='perfilImg' src="{{ asset("storage/fotoFiscal/".Auth::user()->foto) }}" class="img-circle elevation-2" alt="User Image">
				@else
					<img id='perfilImg' src="{{ asset('admin/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
				@endif
				</a>
			</div>
			<div class="info">
				<a href="#" class="d-block" id="">{{ Auth::user()->nombres}}</a>
				<a href="#" class="d-block" id="">{{ Auth::user()->apellidos }}</a>
				<a href="#" class="d-block"> <small>Numero de fiscal </small><b> {{ Auth::user()->numFiscal}}</b></a>
			</div>
		</div>
		@endauth

		<!-- Sidebar Menu -->
		<nav class="mt-2">
			<ul class="nav nav-pills nav-sidebar flex-column"  data-widget="treeview" role="menu" data-accordion="false">
					<!-- Add icons to the links using the .nav-icon class
					 	with font-awesome or any other icon font library -->
						@can('home')<li class="nav-item" ><a data-toggle="tooltip" title="Inicio"  id="home" href="{!!env('SSO_IP_UIPJ').'inicio'!!}" class=" {{ Request::is('inicio') ? 'active' : '' }} nav-link" ><i class=" nav-icon fa fa-home"></i> <p> Inicio</p> <span></span></a></li>@endcan
						<li class="nav-item has-treeview" id="barra"><a href="#" class="nav-link" ><i class="nav-icon fa fa-folder" ></i><p>Carpeta<i class="right fa fa-angle-left"></i></p>
								</a>
								<ul class="nav nav-treeview">
									@if(isset($idCarpeta))
									<li class="nav-item" ><a href="{!!env('SSO_IP_UIPJ').'carpeta', $idCarpeta!!}" class="active nav-link"><i class="items fa fa-circle"></i><p class="textItems">Carpeta Abierta</p> <span></span></a></li>
									@endif
									<li class="nav-item"><a href="{!!env('SSO_IP_UIPJ').'carpetas'!!}" class="{{ Request::is('detalle-carpeta') ? 'active' : '' }} nav-link"><i class="items fa fa-info"></i><p class="textItems">Detalle de Carpeta</p> <span></span></a></li>
									<li class="nav-item"><a href="{!!env('SSO_IP_UIPJ').'iniciar-carpeta'!!}" class="{{ Request::is( 'iniciar-carpeta') ? 'active' : '' }} nav-link"><i class="items fa fa-plus"></i><p class="textItems">Nueva carpeta</p></a></li>
									@can('carpetas_uat')<li class="nav-item"><a href="{{ route('carpetas.uat') }}" class="{{ Request::is( 'carpetas-uat') ? 'active' : '' }} nav-link"><i class="items fa fa-exchange"></i><p class="textItems">Asignar</p> <span></span></a></li>@endcan
									@can('index_turnado')<li class="nav-item"><a href="{!!env('SSO_IP_UIPJ').'turnado-carpeta'!!}" class="{{ Request::is('turnado-carpeta') ? 'active' : '' }} nav-link"><i class="items fa fa-share-square-o"></i><p class="textItems">Turnar</p></a></li>@endcan
									
								</ul>
							</li>

							<li class="nav-item" ><a data-toggle="tooltip" title="ProcesoPenal"  id="home" href="http://scp.fiscaliaveracruz.gob.mx/public/login" class="nav-link" ><i class=" nav-icon fa fa-university"></i> <p> Proceso penal</p> <span></span></a></li>
						<li class="nav-item has-treeview" id="barra">
							<a href="#" class="nav-link">
								<i class="nav-icon fa fa-book" ></i>
								<p>Libros<i class="right fa fa-angle-left"></i></p>
							</a> 
							<ul class="nav nav-treeview" >
								@can('libro_gobierno')<li class="nav-item"><a href="{!!env('SSO_IP_UIPJ').'libro-gobierno'!!}" class=" {{ Request::is( 'libro-gobierno') ? 'active' : '' }} nav-link"><i class=" items fa fa-book"></i><p class="textItems">De gobierno</p></a></li>@endcan
								<li class="nav-item"><a href="{!!env('SSO_IP_UIPJ').'libro-oficios'!!}" class=" {{ Request::is( 'libro-oficios') ? 'active' : '' }} nav-link"><i class=" items fa fa-file-text"></i> <p class="textItems">De oficios</p></a></li>
							</ul>
						</li>

						<li class="nav-item"><a href="{!!env('SSO_IP_UIPJ').'bitacora'!!}" class="{{ Request::is( 'bitacora') ? 'active' : '' }} nav-link"><i class="nav-icon fa fa-pencil-square-o"></i><p>Bitácora</p> <span></span></a></li>
						<li class="nav-item"><a href="{!!env('SSO_IP_UIPJ').'agenda'!!}" class="{{ Request::is( 'agenda') ? 'active' : '' }} nav-link"><i class="nav-icon fa fa-calendar"></i><p>Agenda</p> <span></span></a></li>
						<li class="nav-item"><a href="{!!env('SSO_IP_UIPJ').'ver-oficios'!!}" class="{{ Request::is('ver-oficios') ? 'active' : '' }} nav-link"><i class="nav-icon fa fa-font"></i><p >Oficios</p><span></span></a></li>
						<li class="nav-item"><a href="{!!env('SSO_IP_UIPJ').'administrador-de-auxiliar'!!}" class="{{ Request::is( 'administrador-de-auxiliar') ? 'active' : '' }} nav-link"><i class="nav-icon fa fa-user-plus"></i><p>Auxiliares</p> <span></span></a></li>
						<li class="nav-item"><a href="{!!env('SSO_IP_UIPJ').'cadena-custodia'!!}" class="{{ Request::is( 'cadena-custodia') ? 'active' : '' }} nav-link"><i class="nav-icon fa fa-link" ></i><p>Cadena de custodia</p> <span></span></a></li>
						<li class="nav-item"><a href="{!!env('SSO_IP_UIPJ').'panel-de-administracion'!!}" class="{{ Request::is( 'panel-de-administracion') ? 'active' : '' }} nav-link"><i class="nav-icon fa fa-users" ></i><p>Panel de administración</p> <span></span></a></li>
						<li class="nav-item"><a href="{!!env('SSO_IP_UIPJ').'unidad-detalle'!!}" class="{{ Request::is( 'unidad') ? 'active' : '' }} nav-link"><i class="nav-icon fa fa-building" ></i><p>Detalle de unidad</p> <span></span></a></li>
						
						<li class="nav-item has-treeview" data-widget="tree"><a href="#" class="{{ Request::is( 'ver-registro','ver-vehrecuperados','ver-vehentregados','estadistica-maps') ? 'active' : '' }} nav-link"><i class="nav-icon fa fa-car"></i><p>Vehículos<i class="right fa fa-angle-left"></i></p></a>

							<ul class=" nav nav-treeview">
								<li class="nav-item">
									<a href="{{ url('/ver-registro') }}" class="{{ Request::is( 'ver-registro') ? 'active' : '' }} nav-link">
										<p >Robados</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="{{ url('/ver-vehrecuperados') }}" class="{{ Request::is( 'ver-vehrecuperados') ? 'active' : '' }} nav-link">
										<p>Recuperados</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="{{ url('/ver-vehentregados') }}" class="{{ Request::is( 'ver-vehentregados') ? 'active' : '' }} nav-link">
										<p>Entregados</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="{{ url('estadistica-maps') }}" class="{{ Request::is( 'estadistica-maps') ? 'active' : '' }} nav-link">
										<p>Reportes</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="{{ url('historial-vrr') }}" class="{{ Request::is( 'historial-vrr') ? 'active' : '' }} nav-link">
										<p>Historial</p>
									</a>
								</li>
							</ul>
						</li>
						

						<li class="nav-item bottom" ><a class="nav-link" href="" onclick="event.preventDefault();
							document.getElementById('logout-form').submit();"><i class=" nav-icon fa fa-power-off" ></i><p> Cerrar sesión</p></a>
							<form id="logout-form" action="" method="POST" style="display: none;">
								{{ csrf_field() }}
							</form>
						</li>
			</ul>

		</nav>
		<!-- /.sidebar-menu -->
		</div>
		<!-- /.sidebar -->
		</aside>
