<aside class="main-sidebar elevation-4 barra-izquierda collapsado" style="overflow-x: hidden;" >
	<!-- Brand Logo -->
	<a href="{{ url('/home') }}" class="brand-link">
		<img src="https://rawcdn.githack.com/Romaincks/assets/master/img/only-escudo-ver.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
		style="opacity: .8">
		<span class="brand-text font-weight-light">FGE | Veracruz</span>
	</a>

	<!-- Sidebar -->
	<div  class="sidebar font-weight-light">
		<!-- Sidebar user panel (optional) -->
		@auth
		<div class="user-panel mt-3 pb-3 mb-3 d-flex">
			<div class="image">
				<img src="{{ asset('admin/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
			</div>
			<div class="info">
				<a href="#" class="d-block">{{ Auth::user()->nombres." ". Auth::user()->apellidos }}</a>
				<a href="#" class="d-block"> <small>Numero de fiscal </small><b> {{ Auth::user()->numFiscal}}</b></a>
			</div>
		</div>
		@endauth

		<!-- Sidebar Menu -->
		<nav class="mt-2">
			<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false" >
					<!-- Add icons to the links using the .nav-icon class
						with font-awesome or any other icon font library-->
						<li class="nav-item"><a href="{{ url('/home') }}" class=" {{ Request::is( 'home') ? 'active' : '' }} nav-link"><i class=" nav-icon fa fa-home"></i> <p> Inicio</p> <span></span></a></li>
						<li class="nav-item"><a href="{{ url('/iniciar-carpeta') }}" class="nav-link "><i class=" nav-icon fa fa-book"></i> <p> Libro de gobierno</p><span></span></a></li>
						@if(isset($idCarpeta))
						<li class="nav-item" ><a href="{{ url('/iniciar-carpeta') }}" class="nav-link"><i class="nav-icon fa fa-folder-open"></i><p>Carpeta Abierta</p> <span></span></a></li>
						@endif
						<li class="nav-item"><a href="{{ url('/iniciar-carpeta') }}" class="nav-link"><i class="nav-icon fa fa-folder"></i><p>Nueva carpeta</p> <span></span></a></li>
						<li class="nav-item"><a href="" class="nav-link"><i class="nav-icon fa fa-pencil-square-o"></i><p>Bitácora</p> <span></span></a></li>
						<li class="nav-item"><a href="{{ url('/iniciar-agenda') }}" class="nav-link"><i class="nav-icon fa fa-calendar"></i><p>Agenda</p> <span></span></a></li>
						<li class="nav-item has-treeview" data-widget="tree"><a href="#" class="{{ Request::is( 'ver-registro','ver-vehrecuperados','ver-vehentregados','estadistica-maps') ? 'active' : '' }} nav-link"><i class="nav-icon fa fa-car"></i><p>Vehículos<i class="right fa fa-angle-left"></i></p></a>

							<ul class=" nav nav-treeview">
								<li class="nav-item">
									<a href="{{ url('/ver-registro') }}" class="{{ Request::is( 'ver-registro') ? 'active' : '' }} nav-link">
										<p >Robados</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="{{ url('/ver-vehrecuperados') }}" class="{{ Request::is( 'ver-vehrecuperados') ? 'active' : '' }} nav-link">
										<p>Recuperados</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="{{ url('/ver-vehentregados') }}" class="{{ Request::is( 'ver-vehentregados') ? 'active' : '' }} nav-link">
										<p>Entregados</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="{{ url('estadistica-maps') }}" class="{{ Request::is( 'estadistica-maps') ? 'active' : '' }} nav-link">
										<p>Reportes</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="{{ url('historial-vrr') }}" class="{{ Request::is( 'historial-vrr') ? 'active' : '' }} nav-link">
										<p>Historial</p>
									</a>
								</li>
							</ul>
						</li>



						<li class="nav-item bottom" ><a class="nav-link" href="" onclick="event.preventDefault();
							document.getElementById('logout-form').submit();"><i class=" nav-icon fa fa-power-off" ></i><p> Cerrar sesión</p></a>
							<form id="logout-form" action="" method="POST" style="display: none;">
								{{ csrf_field() }}
							</form>
						</li>
			</ul>

		</nav>
		<!-- /.sidebar-menu -->
		</div>
		<!-- /.sidebar -->
		</aside>
