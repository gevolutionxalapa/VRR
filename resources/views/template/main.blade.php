  <!doctype html>
  <html lang="en">

  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title','Default')| Vehículos</title>
    <link rel="icon" href="{{ asset('img/iconofge.jpg') }}">
  </head>

  <body>
    @include('template.menus.steps')

    <link rel="icon" href="{{ asset('img/iconofge.png') }}">
    {{--  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">  --}}
    <link href="{{ asset('css/fontGoogleApi.min.css') }}" rel="stylesheet">
    <link href="{{asset ('plugins/bootstrap/css/bootstrapFGE.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/theme-jquery-validation.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/toastr.min.css') }}">
    {{--  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css"/>  --}}
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome-4.7.0/css/font-awesome.min.css') }}" />
    <link rel="stylesheet" href="{{asset ('css/sweetalert.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/showGuia.css') }}">
    <link rel="stylesheet" href="{{asset('css/PreVehiculo/w3.css')}}">
    <script type="text/javascript">var centreGot = false;</script> 
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyADLKCVRyl7I3LK5INMWsrqWhQgTkdCdVY&libraries=places" async defer></script>
    <script src="{{ asset('js/geolocalizacion/barSearchMap.js') }}"></script>

    <script src="{{ asset('plugins/jquery/jquery-3.3.1.js')}}"></script>
    <script src="{{ asset('js/jquery.form-validator.min.js')}}" ></script>
    <script src="{{ asset('js/validations.js') }}"></script>
    <script src="{{asset('plugins/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/prevehiculo/sweetalertGuia.min.js')}}"></script>
    {{--  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>  --}}
    <script src="{{ asset('js/toastr.min.js')}}"></script>
    <script src="{{asset('js/popper.min.js')}}"></script>


    <script>
      function mayus(e) {
        e.value = e.value.toUpperCase();
    }
    toastr.options = {
          "closeButton": true,
          "debug": false,
          "newestOnTop": true,
          "progressBar": true,
          "positionClass": "toast-top-right",
          "preventDuplicates": false,
          "onclick": null,
          "showDuration": "300",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }
    </script>

    <section>
      @yield('content')
    </section>

    <script type="text/javascript">
       $.validate({
            lang : 'es'
        });
    </script>

    @yield('scripts')

  </body>

  </html>
