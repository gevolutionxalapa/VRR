        <!doctype html>
        <html lang="en">

        <head>
          <!-- Required meta tags -->
          <meta charset="utf-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=device-width, initial-scale=1">

          <title>@yield('title','Default')| Vehiculos</title>
          <link rel="icon" href="{{ asset('img/iconofge.jpg') }}">

          <link href="{{asset ('plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
          <link rel="stylesheet" href="{{ asset('admin/dist/css/adminlte.min.css') }}">
          <link rel="stylesheet" href="{{asset ('css/sweetalert.min.css')}}">
          <link rel="stylesheet" type="text/css" href="{{ asset('plugins/font-awesome-4.7.0/css/font-awesome.min.css') }}">
          <!-- Theme style -->
          <link rel="stylesheet" href="{{asset('plugins/select2/select2.min.css')}}">
          <link rel="stylesheet" href="{{asset ('css/font-neosans.css') }}">
          <link rel="stylesheet" href="{{ asset('css/toastr.min.css') }}">
          <link rel="stylesheet" href="{{ asset('css/cssfonts.css') }}">
          <link rel="stylesheet" href="{{ asset('css/pagination.css') }}">
          <link rel="stylesheet" href="{{ asset('css/estilos.css') }}">
          <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatable/bootstrap-table.css') }}">
          <link rel="stylesheet" type="text/css" href="{{ asset('css/styleBarSearchMap.css') }}">

          <link rel="stylesheet" href="{{ asset('css/theme-jquery-validation.min.css') }}">
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
          <link rel="stylesheet" href="{{ asset('css/viewer.min.css')}}">
          <link rel="stylesheet" href="{{ asset('plugins/fileinput/css/fileinput.min.css') }}">
          <script src="{{ asset('plugins/jquery/jquery-3.3.1.min.js')}}"></script>
          <script src="{{ asset('plugins/datatable/bootstrap-table.min.js')}}"></script>
           <script src="{{ asset('plugins/datatable/bootstrap-table-es-MX.js')}}"></script>
           {{-- <script src="{{ asset('js/<.min.js') }}"></script> --}}
           <script src="{{asset('js/viewer.min.js')}}"></script>
           <script type="text/javascript" src="{{ asset('js/webcam.min.js') }}"></script>
           <script type="text/javascript" src="{{ asset('js/camara/camara_validacion.js') }}"></script>
          <script type="text/javascript">var centreGot = false;</script>
          <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyADLKCVRyl7I3LK5INMWsrqWhQgTkdCdVY&libraries=places" async defer></script>
          <script src="{{ asset('js/geolocalizacion/geolocalizacion.js') }}"></script>
          <script src="{{ asset('js/geolocalizacion/barSearchMap.js') }}"></script>
          <script src="{{ asset('js/modernizr.js') }}"></script>


        @yield('css')
        </head>

        <body class="hold-transition sidebar-mini sidebar-collapse">
          <div class="wrapper" >
            <!-- Navbar -->
            @include('template.menus.navbar')
            <!-- /.navbar -->
            <!-- Main Sidebar Container -->
            @include('template.menus.sidebar')


            <div class="content-wrapper">
              <!-- Content Header (Page header) -->
              <div class="content-header">
                <div class="container-fluid">
                  <div class="row mb-2">
                    <div class="col-sm-6">
                      <h1 class="m-0 text-dark">@yield('title')</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                      @isset($idCarpeta)
                      <li class="breadcrumb-item"><a href="#">{{ $idCarpeta }}</a></li>
                      @else

                      <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                      @endif
                      <li class="breadcrumb-item active">@yield('title')</li>
                    </ol>
            </div><!-- /.col -->
                  </div><!-- /.row -->
                </div><!-- /.container-fluid -->
              </div>
              <!-- Main content -->
              <section class="content">
                <div class="container-fluid">          
            @if  (isset (Auth::user()->numFiscal))
            @yield('content')
            @else
            <div class="card">
            @yield('contenido')
              </div>
            @endif          
          </div>
              </section>

            </div>

            <!-- Control Sidebar (a secondary optional sidebar) -->
            @include('admin.modals.foto-perfil')
            <!-- /.control-sidebar -->

            <!-- Main Footer -->

          </div>

          <script src="{{ asset('js/jquery-ui.min.js')}}" ></script>
          <script src="{{ asset('js/popper.min.js')}}"></script>
          <script src="{{asset('plugins/bootstrap/js/bootstrap.min.js')}}"></script>
          <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
          <script src="{{ asset('js/denuncia/select2.js') }}"></script>
          <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
          <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
          <script src="{{ asset('js/jquery.form-validator.min.js')}}" ></script>
          <script src="{{ asset('js/validations.js') }}"></script>

          <!-- Bootstrap -->
          <script src="{{ asset('admin/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
          <!-- AdminLTE App -->
          <script src="{{ asset('admin/dist/js/adminlte.min.js') }}"></script>
          <!-- OPTIONAL SCRIPTS -->
          <script src="{{ asset('admin/dist/js/demo.min.js') }}"></script>
          <!-- jVectorMap -->
          <script src="{{ asset('admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
          <script src="{{ asset('admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
          <script src="{{ asset('js/denuncia/ordenar.js') }}"></script>
          <script src="{{ asset('js/toastr.min.js')}}"></script>
          <script src="{{ asset('js/prevehiculo/sortable.min.js') }}"></script>
          <script src="{{ asset('js/prevehiculo/fileinput.min.js') }}"></script>
          <script src="{{ asset('js/prevehiculo/es.js') }}"></script>
          <script src="{{ asset('js/prevehiculo/carga.js') }}"></script>
          <script src="{{ asset('js/prevehiculo/explorer-fa/theme.min.js') }}"></script>
          <script src="{{ asset('js/prevehiculo/fa/theme.min.js') }}"></script>
          <script src="{{ asset('js/denuncia/fileinputdoc.js') }}"></script>
          {{-- <script type="text/javascript" src="{{ asset('js/top.js') }}"></script>--}}
          <script src="{{ asset('js/main.js') }}"></script>

          
          <script>
          function mayus(e) {
            e.value = e.value.toUpperCase();
          }

          </script>

          <script type="text/javascript">

          $.validate({
            lang:'es'
          });

          toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
          }

          $('#numMotor').blur(function(){
            toastr.warning("Ha modificado el número de motor", "Atención",{
              "preventDuplicates": true
            });
          });


          $('#motor').blur(function(){
            toastr.warning("Ha modificado el número de motor", "Atención",{
              "preventDuplicates": true
            });
          });

          $('#placa').blur(function(){
            toastr.warning("Ha modificado el número de placa", "Atención",{
              "preventDuplicates": true
            });
          });

          </script>
          @yield('scripts')
          @include('template.menus.footer')
        </body>

        </html>
