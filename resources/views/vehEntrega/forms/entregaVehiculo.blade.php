@section('title')
Vehículos Entregados
@endsection

<link rel="stylesheet" type="text/css" href="{{ asset('css/PreVehiculo/theme.css') }}">
<link  rel="stylesheet" type="text/css" href="{{ asset('css/PreVehiculo/fileinput.css') }}">

<div class="container-fluid">
  <br>
  <div class="card">
    {!! Form::open(['method' => 'POST','files' => true, 'id' => 'formEntregados'])  !!}
    <div class="card-header">
      <div class="row">
        <div class="col-lg-6 col-md-6" >
      <h5 align="justify">Búsqueda de vehículo</h5>
    </div>
      <div class="col-lg-6 col-md-6" align="right">
        <button type="button" class="btn btn-secondary" name="buscar" id="buscarVeh">Buscar</button>
        <button type="submit" class="btn btn-primary" id="btnEmpty">Guardar</button>
      </div>
    </div>
    </div>
    <div class="card-body">
      <div class="row">
        <ul class="nav nav-pills mb-3 " id="pills-tab" role="tablist">
          <li class="nav-item2">
            <a class="nav-link active btn-outline-dark " id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true"> Registro de vehículo entregado </a>
          </li>
          <li class="nav-item2">
            <a class="nav-link btn-outline-dark" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Vehículos entregados</a>
          </li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-4 col-lg-4 ">
          <div class="form">
            {!!Form::label('placa', 'Placa')!!}
          </div>
          <div class="input-group">
            {!!Form::text('placaB',null,['class'=>'form-control form-control-sm', 'null' ,'id' => 'placaB', 'placeholder' => 'HMZ2398 - Ejemplo' , 'maxlength'=>'7',  'style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'alphanumeric'])!!}
            {{-- 'data-validation-regexp'=>'^((([A-ZÑ]|[\d])+)){5,7}$' --}}
          </div>
        </div>
        <div class="col-md-4 col-lg-4">
          <div class="form-group">
            {!!Form::label('serieB','Número de serie')!!}
            {!!Form::text('serieB',null, ['id'=>'serieB' ,'class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el número de serie', 'required','maxlength'=>'17','data-validation'=>'custom','style'=>'text-transform: uppercase;',  'data-validation'=>'custom', 'data-validation'=>'alphanumeric'] )!!}
          </div>
        </div>
        <div class="col-md-4 col-lg-4">
          <div class="form-group">
            {!!Form::label('motorB','Número de motor')!!}

            {!!Form::text('motorB',null, ['id'=>'motorB','class' => 'form-control form-control-sm','maxlength'=>'10',  'placeholder' => 'Ingrese el número de motor','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'alphanumeric'] )!!}
          </div>
        </div>
      </div>
    </div>

    <div class="card-header">

      <h5 align="justify">Datos generales del vehículo entregado</h5>
    </div>
    <br>
    <div class="card-body">
      <div class="row">
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('serieAlterada','Número de serie alterada')!!}
            <select class=" form-control form-control-sm" data-live-search="true" id="idSerieAlterada" name="idSerieAlterada" >
              <option selected="selected" value=""> </option>
              <option  value="0">No </option>
              <option  value="1">SI </option>
            </select>
          </div>
        </div>

        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('serie','Número de serie')!!}

            {!!Form::text('numSerie',null, ['id'=>'numSerie','class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el número de serie', 'required','minlength'=>'10','maxlength'=>'17','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'alphanumeric'] )!!}

            {{-- {!!Form::text('numSerie',null, ['class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el número de serie', 'required','minlegth'=>'10','maxlength'=>'17','onkeyup'=>'mayus(this);', 'data-validation'=>'custom', 'data-validation-regexp'=>'^([A-Z\d]{3}([A-Z\d]{5})([A-Z\d]{3})([A-Z\d]{6}))$'] )!!} --}}
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('motorAlterado','Número de motor alterado')!!}
            <select class=" form-control form-control-sm" data-live-search="true" id="idMotorAlterado" name="idMotorAlterado" required >
              <option selected="selected" value=""> </option>
              <option  value="0">NO </option>
              <option  value="1">SI </option>
            </select>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('motor','Número de motor')!!}
            {!!Form::text('motor',null, ['id'=>'motor','class' => 'form-control form-control-sm','maxlength'=>'10',  'placeholder' => 'Ingrese el número de motor','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'alphanumeric','disabled'=>'disabled' ] )!!}
            {{-- 'data-validation-regexp'=>'^((([A-Z]|[\d])+)+){10}$' --}}
          </div>
        </div>

        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idEstado', 'Entidad federativa')!!}
            <select  class="templatingSelect2 form-control form-control-sm"  id="idEstado" name="idEstado" style="font-size:14px" required onchange="if(this.value!=0) {document.getElementById('idMunicipio').disabled = false}">
              <option value="" selected="selected" > </option>
              @foreach($estados as $estado)
              <option  value="{{$estado->id}}" >{{$estado->nombre}}</option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idMunicipio', 'Municipio')!!}
            <input type="text" id="text_municipio" name="text_municipio" hidden>
            <select class="templatingSelect11 form-control form-control-sm" required id="idMunicipio" name="idMunicipio" disabled>
              <option value="" selected="selected"></option>
            </select>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idCP','Código Postal')!!}
            {!!Form::text('cp',null, ['id'=>'cp','class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el código postal', 'required','minlength'=>'1','minlength'=>'5','maxlength'=>'5','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'number'] )!!}
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idCalle','Calle')!!}
            {!!Form::text('calle',null, ['id'=>'calle','class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese la calle', 'required','style'=>'text-transform: uppercase;'] )!!}
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idColonia','Colonia')!!}
            {!!Form::text('colonia',null, ['id'=>'colonia','class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese la colonia', 'required','style'=>'text-transform: uppercase;'] )!!}
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-2 col-md-2">
          <div class="form-group">
            {!!Form::label('numExt','Número Externo')!!}
            {!!Form::text('numExt',null, ['id'=>'numExt','class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el número externo', 'required','minlength'=>'1','maxlength'=>'10','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'alphanumeric'] )!!}
          </div>
        </div>
        <div class="col-lg-2 col-md-2">
          <div class="form-group">
            {!!Form::label('numInt','Número Interno')!!}
             {!!Form::text('numInt',null, ['id'=>'numInt' ,'class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el número interno', 'minlength'=>'1','maxlength'=>'10','data-validation'=>'custom','style'=>'text-transform: uppercase;'] )!!}
          </div>
        </div>
        <div class="col-lg-8 col-md-8">
          <div class="form-group">
            {!!Form::label('refeEntrega','Referencia de la entrega')!!}
            <textarea class="form-control form-control-sm" placeholder="Referencia del lugar de la recuperación" rows="2" name="refeEntrega" cols="90" style="text-transform: uppercase;" id="senasPartic" required=""></textarea>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idInspeccion','Inspección realizada')!!}
            <select class="templatingSelect18 form-control form-control-sm" id="idInspeccion" name="idInspeccion" required>
              <option value="" selected>Seleccione</option>
              <option value="1" >NO</option>
              <option value="0">SI</option>
            </select>
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idAutoridad','Autoridad que entrega')!!}
            <select  class="templatingSelect20 form-control form-control-sm"  id="idAutoridad" name="idAutoridad" required>
              <option selected="selected" value=""> </option>
              <option value="1">MP </option>
              <option value="2">Fiscalía </option>
            </select>
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idFechaEntrega','Fecha de entrega')!!}
            <input class="form-control form-control-sm" type="date" value="hoy" min="<?php $hoy=date("Y-m-d"); echo $hoy;?>" id="idFechaEntrega" name="idFechaEntrega" required>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idHoraEntrega','Hora de entrega')!!}
            <input class="form-control form-control-sm" type="time" value="13:45:00" id="idHoraEntrega" name="idHoraEntrega" required>
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idEvaluacion','Monto del vehículo')!!}
            {!!Form::text('idEvaluacion',null, ['class' => 'form-control form-control-sm', 'placeholder' => 'Evaluación del vehículo antes su entrega','minlength'=>'1','maxlength'=>'10','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'number'] )!!}
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idfacturaV','Factura del vehículo')!!}
            {!!Form::text('idfacturaV',null, ['class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese número de la factura del vehículo a entregar','minlength'=>'1','maxlength'=>'10','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'number','required'] )!!}
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idFechaFactura','Fecha de la factura')!!}
            <input class="form-control form-control-sm" type="date" value="hoy" id="idFechaFactura" name="idFechaFactura">
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          {!!Form::label('idPersonaEntrega','Persona a quien se entrega')!!}
          <div class="form-inline">
            <div class="form-check">
              <input class="form-check-input btn_persona" name="checkPersona" type="radio" id="checkPersona" value="1" disabled="disabled">
              <label class="form-check-label" for="checkPersona">Propietario</label>
            </div>

            <div class="form-check">
              <input class="form-check-input btn_persona" name="checkPersona" type="radio" id="checkPersona" value="2" disabled="disabled">
              <label class="form-check-label" for="checkPersona">Representante</label>
            </div>
          </div>
          {{-- <div class="form-group">
            {!!Form::label('idPersonaEntrega','Persona a quién se entrega')!!}
            <select class="templatingSelect9 form-control form-control-sm" id="idPersonaEntrega" name="idPersonaEntrega" required>
              <option selected value="">Seleccione</option>
              <option value="1" >PROPIETARIO</option>
              <option value="2">REPRESENTANTE LEGAL</option>
            </select>
          </div> --}}
        </div>
        {{-- <div class="row">
          <div class="col-lg-4 col-md-4">
            <div class="form-group">
              {!!Form::label('idNombre','Nombre')!!}
              {!!Form::text('idNombre',null, ['class' => 'form-control form-control-sm', 'placeholder' => 'Escribe el nombre','minlegth'=>'1','maxlength'=>'10','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'number','required'] )!!}
            </div>
          </div>
          <div class="col-lg-4 col-md-4">
            <div class="form-group">
              {!!Form::label('idApellidoP','Apellido Paterno')!!}
              {!!Form::text('idApellidoP',null, ['class' => 'form-control form-control-sm', 'placeholder' => 'Escribe el apellido paterno','minlegth'=>'1','maxlength'=>'10','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'number','required'] )!!}
            </div>
          </div>
          <div class="col-lg-4 col-md-4">
            <div class="form-group">
              {!!Form::label('idApellidoM','Apellido Materno')!!}
              {!!Form::text('idApellidoM',null, ['class' => 'form-control form-control-sm', 'placeholder' => 'Escribe el apellido materno','minlegth'=>'1','maxlength'=>'10','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'number'] )!!}
            </div>
          </div>

        </div> --}}
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idPersonaEntrega1','Propietario/Representante',['id'=>'labelPersona'])!!}
            <select class="templatingSelect21 form-control form-control-sm" id="idPersonaEntrega" name="idPersonaEntrega" required disabled="disabled">
              <option selected value="">Seleccione</option>
            </select>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 col-md-4">
          <div id="iden" >
            <label>Identificación oficial</label>
            <select class="templatingSelect17 form-control form-control-sm" id="listaiden_op1" name="listaiden1" class="listaiden">
              <option selected value="" >--Seleccione identificación--</option>
              @foreach($identificacionoficial as $identificacionoficial)
              <option  value="{{$identificacionoficial->id}}" >{{$identificacionoficial->nombre}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idCompDomicilio','Comprobante de domicilio')!!}
            <select class="templatingSelect19 form-control form-control-sm" id="idCompDomicilio"  name="idCompDomicilio" required>
              <option  value="">Seleccione</option>
              <option  value="2">Agua</option>
              <option  value="3">Internet</option>
              <option value="1">Luz</option>
            </select>
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idOtroDocumento','Otro documento')!!}
            {!!Form::text('idOtroDocumento',null, ['class' => 'form-control form-control-sm', 'placeholder' => 'Especifica otro comprobante de domicilio','minlength'=>'1','maxlength'=>'10','style'=>'text-transform: uppercase;'] )!!}
          </div>
        </div>
      </div>
      <div class="card-footer" align="right">
      <a href="#0" class="cd-top">Top</a>
      </div>
    </div>

  </div>
</div>





<div class="modal fade bd-example-modal-lg" id="vehRecuperados" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header" align="center">
        <h5 class="modal-title" id="exampleModalLabel" align="text-center">Coincidencias</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="row">
        <div class="modal-body" style="width: 60%;">
          <div class="card-header">
            Vehículos
          </div>
          <div class="card-body">
            <table id="tablevehiculos" style="text-transform: uppercase;" class="table table-bordered table-hover table-responsive-sm" ></table>
          </div>
        </div>
        <div class="modal-body" style="width: 40%;">
          <div class="card-header">
            Datos complementarios
          </div>
          <div class="card-body" id='datosComplementarios'>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal" >Cerrar</button>
      </div>
    </div>
  </div>
</div>

{!!Form::text('idVeh',null, ['hidden','id'=>'idVeh'] )!!}
{!!Form::text('idCarpeta',null, ['hidden','id'=>'idCarpeta'] )!!}

<!--</form>-->
{!! Form::close()!!}

<script src="{{ asset('js/denuncia/peticiones.js') }}"></script>

<script type="text/javascript">


function mostrarOpcionSeleccionada() {
        //var $option = $select.find('option:selected');

        var identificador = $('#idEstado').val();
        var respuesta;
        $('#idMunicipio').html('');
        if(identificador!=''){
          var ruta = "{{route('municipio',':estado')}}";
          ruta = ruta.replace(':estado', identificador);
          datos = null;
          respuesta = peticionInfomacion('get',ruta,datos);
          if (respuesta != null) {
            for(i=0; i<respuesta.estado.length; i++){
              $("#idMunicipio").append("<option value='"+respuesta.estado[i].idMunicipio+"'> "+respuesta.estado[i].nombre+"</option>");
            }
            if(respuesta.estado.length > 0 ){
              localStorage.setItem("idMunicipio", respuesta.estado[0].id);
            }
          }else{
            alert('Error de servidor');
          }
        }else{
        }
      }
$(document).ready(function(){

  window.onload = function(){
  	  fecha = new Date();
  	  texto = document.getElementById("hoy");
  	  texto.innerHTML = idFechaEntrega;
    }
    window.onload = function(){
    	  fecha = new Date();
    	  texto = document.getElementById("hoy");
    	  texto.innerHTML = idFechaFactura;
      }
  $('#idEstado').val(30).trigger('change');
       mostrarOpcionSeleccionada();
  $("#idEstado").change(function() {
    var identificador = $(this).val();
    var respuesta;
    $('#idMunicipio').html('');
    if(identificador!=''){
      var ruta = "{{route('municipio',':estado')}}";
      ruta = ruta.replace(':estado', identificador);
      datos = null;
      respuesta = peticionInfomacion('get',ruta,datos);
      if (respuesta != null) {
        for(i=0; i<respuesta.estado.length; i++){
          $("#idMunicipio").append("<option value='"+respuesta.estado[i].idMunicipio+"'> "+respuesta.estado[i].nombre+"</option>");
        }
        if(respuesta.estado.length > 0 ){
          localStorage.setItem("idMunicipio", respuesta.estado[0].id);
        }
        console.log('cargando datos');
      }else{
        alert('Error de servidor');
      }
    }else{
      //alert('no aplica');
    }
  });

  $("#idMunicipio").change(function() {
    localStorage.setItem("idMunicipio", $('#idMunicipio').val());
  });

  $("#idSubmarca").change(function() {
    localStorage.setItem("idSubmarca", $('#idSubmarca').val());
  });


  $("#serieB" ).blur(function() {
    console.log('este es el valor'+idx);
    var prohibido =['SE IGNORA', 'NULL','null','se ignora' ];
    var cadenaUser = $("#serieB").val();
    var idx = prohibido.indexOf(cadenaUser);
    console.log('esta es mi cadena '+cadenaUser);
    console.log('este es el valor'+idx);
    if(idx > -1){
      $("#serieB").val("");
    }
  });

  $('#motorB').blur(function(){
    toastr.warning("Ha modificado el número de motor", "Atención",{
      "preventDuplicates": true
    });
  });

  $('#motor').blur(function(){
    toastr.warning("Ha modificado el número de motor", "Atención",{
      "preventDuplicates": true
    });
  });

  $('#numSerie').blur(function(){
    toastr.warning("Ha modificado el número de serie/VIN", "Atención",{
      "preventDuplicates": true
    });
  });

  $('#serieB').blur(function(){
    toastr.warning("Ha modificado el número de serie/VIN", "Atención",{
      "preventDuplicates": true
    });
  });

  $('#buscarVeh').bind('click', function () {
    $('#datosComplementarios').empty();
    $('#vehRecuperados').modal('show');
    var placa = $('#placaB').val();
    placa = (placa== '') ? 0 : placa;
    var serie = $('#serieB').val();
    serie = (serie== '') ? 0 : serie;
    var motor = $('#motorB').val();
    motor = (motor== '') ? 0 : motor;
    var tipoCons = 2;
    //alert(placa+','+serie+','+motor);

    //registros vehiculos involucrados

    var ruta3 = "{{route('get_recuperados',['placa'=>':placa','serie'=>':serie','motor'=>':motor'])}}";
    ruta3 = ruta3.replace(':placa', placa);
    ruta3 = ruta3.replace(':serie', serie);
    ruta3 = ruta3.replace(':motor', motor);
    datos = null;
    //console.log(ruta)
    recuperados = peticionInfomacion('get',ruta3,datos);
    console.log('recuperados');
    console.log(recuperados);
    console.log('-------------');
    var datosRecuperados = [];
          for(i = 0; i < recuperados.registros.length; i++){
              var reg = {
                id: recuperados.registros[i].id,
                numCarpeta: recuperados.registros[i].numCarpeta,
                placas: recuperados.registros[i].placas,
                nrpv: recuperados.registros[i].nrpv,
                numSerie: recuperados.registros[i].numSerie,
                numMotor: recuperados.registros[i].numMotor,
                nomFiscal: recuperados.nomFiscal[i].nomFiscal,
                idCarpeta: recuperados.registros[i].idCarpeta,
                modelo: recuperados.registros[i].modelo,
                permiso: recuperados.registros[i].permiso,
                senasPartic: recuperados.registros[i].senasPartic

              }
              datosRecuperados.push(reg);
          }

    $('#tablevehiculos').bootstrapTable('load',datosRecuperados);

  });


  $('#idSerieAlterada').change(function(){
    //alert($('#serieB').val());
    if($(this).val()==0){
      //alert("entra aqui");
      $('#numSerie').val($('#serieB').val());
      $('#numSerie').attr('disabled','disabled');
    }else{
      $('#numSerie').val('');
      $('#numSerie').removeAttr('disabled');
    }
  });

  $('#idMotorAlterado').change(function(){
    //alert($('#serieB').val());
    if($(this).val()==0){
      //alert("entra aqui");
      $('#motor').val($('#motorB').val());
      $('#motor').attr('disabled','disabled');
    }else{
      $('#motor').val('');
      $('#motor').removeAttr('disabled');
    }
  });

  $('.btn_persona').change(function(){
    var name="";
    $('#idPersonaEntrega').html('');
    var persona = $(this).val();

    ($(this).val()==1)?$('#labelPersona').text("Propietario"):$('#labelPersona').text("Representante");;
    var idCarpeta = $('#idCarpeta').val();
    //alert(idCarpeta);
    var ruta = "{{route('persona_entrega',['persona'=>':persona','idCarpeta'=>':idCarpeta'])}}";
    ruta = ruta.replace(':persona', persona);
    ruta = ruta.replace(':idCarpeta', idCarpeta);
    datos = null;
    //console.log(ruta)
    personas = peticionInfomacion('get',ruta,datos);
    console.log(personas);
    if (personas != null) {

        for(i=0; i<personas.length; i++){

          name = ($(this).val()==1)?personas[i].persona:personas[i].representanteLegal;

          $("#idPersonaEntrega").append("<option value='"+personas[i].idPersona+"'> "+name+"</option>");
        }
        // if(personas.estado.length > 0 ){
        //   localStorage.setItem("idMunicipio", personas.estado[0].id);
        // }
        // console.log('cargando datos');
      }else{
        alert('Error de servidor');
      }
  });

  $('#formEntregados').submit(function (e) {
    $('#numSerie').removeAttr('disabled');
    $('#motor').removeAttr('disabled');
    e.preventDefault();

    var formData = new FormData(this);

    var numSerie = $('#serieB').val();
    if(numSerie!=""){
      //console.log('Este es el numero de serie ->'+numSerie);
      var ruta ="{{route('niv',['num_serie'])}}";
      ruta = ruta.replace("num_serie",numSerie);
      //console.log(ruta);
      var respuestaNiv = peticionInfomacion('get',ruta);
      console.log(respuestaNiv);
      statusMsj = respuestaNiv.request.status;
      msj = respuestaNiv.request.mensaje;
      console.log(msj);
    }
    if(statusMsj=='correcto'){clase='alert alert-success';}else{clase='alert alert-danger';}

    swal({
      title: "¿Está seguro que desea continuar?",
      text: "",
      icon: "warning",
      buttons: true,
      dangerMode: true,
      //content: span,
    })
    .then((willDelete) => {
      if (willDelete) {
        swal("Su registro se ha registrado satisfactoriamente", {
          icon: "success",
        });
        $.ajax({
          url : "{{route('saveVehEntregado')}}",
          data : formData,
          type : 'POST',
          cache: false,
          contentType : false,
          processData : false,
          success : function(json) {
            console.log('Se agrego un vehiculo');
            console.log(json);
						$('#table').bootstrapTable('refresh');

            swal({
              title: "Registro creado exitosamente",
              text: "Su registro se realizo correctamente",
              icon: "success",
            });

            // $('#table').bootstrapTable('refresh');
             $("#placaB").val('');
             $("#serieB").val('');
             $("#motorB").val('');
             $("#idSerieAlterada").val('').trigger('change');
             $("#numSerie").val('');
             $("#idMotorAlterado").val('').trigger('change');
             $("#idEstado").val('').trigger('change');
             $("#idMunicipio").val('').trigger('change');
             $("#cp").val('');
             $("#calle").val('');
             $("#colonia").val('');
             $("#numExt").val('');
             $("#numInt").val('');
             $("#refeEntrega").val('');
             $("#idInspeccion").val('').trigger('change');
             $("#idAutoridad").val('').trigger('change');
             $("#idFechaEntrega").val('').trigger('change');
             $("#idHoraEntrega").val('').trigger('change');
             $("#idEvaluacion").val('');
             $("#idfacturaV").val('');
             $("#idFechaFactura").val('');
             $("#checkPersona").removeAttr("checked");
             $("#idPersonaEntrega").val('').trigger('change');
             $('#labelPersona').text("Propietario/Representante");
             $("listaiden_op1").val('').trigger('change');
             $("#idCompDomicilio").val('').trigger('change');
             $("#formEntregados")[0].reset();


          },

          error : function(xhr, status) {
            console.log('Disculpe, existió un problema');
            console.log(xhr);
            swal({
              title: "Error al introducir los datos",
              text: 'msj',
              icon: "error",
            });
          },
          complete : function(xhr, status) {
            console.log('Petición realizada');
          }
        });
      } else {
        swal("Su registro ha sido cancelado");
      }
    });
    console.log("{{route('saveVehEntregado')}}");
  });

});

$('#tablevehiculos').bootstrapTable({
  search:  true,
  showColumns: true,
  showRefresh: true,
    //url: ruta,
    buttonsAlign :'left',
    searchAlign :'left',
    idField: 'id',
    iconsPrefix: 'fa',
    pagination : true,
    pageSize: 5,
    columns: [{
      field: 'id',
      title: 'ID',
      sortable: true,
      visible: false
    }, {
      field: 'numCarpeta',
      title: 'No. Carpeta',
      sortable: true
    },{
      field: 'placas',
      title: 'Placas',
      sortable: true
    }, {
      field: 'nrpv',
      title: 'NRPV',
      sortable: true,
      visible: false
    }, {
      field: 'numSerie',
      title: 'No. serie',
      sortable: true
    }, {
      field: 'numMotor',
      title: 'No. Motor',
      sortable: true
    },{
        field: 'nomFiscal',
        title: 'Nombre Fiscal',
        sortable: true
      }],
    //width: '50%',
  });

  $('#tablevehiculos').on('click-row.bs.table', function(tr, row){
    console.log(row);
      //deshabilitar();
    $('#idVeh').val(row.id);
    $('#idCarpeta').val(row.idCarpeta);
    $('#placaB').val(row.placas);

    $('#serieB').val(row.numSerie);
    $('#motorB').val(row.numMotor);


    $('#datosComplementarios').empty();
    $('#datosComplementarios').append('<p><b>Carpeta:</b> '+row.numCarpeta+'</p>');
    $('#datosComplementarios').append('<p><b>Nombre Fiscal:</b> '+row.nomFiscal+'</p>');
    $('#datosComplementarios').append('<p><b>Placa:</b> '+row.placas+'</p>');
    $('#datosComplementarios').append('<p><b>No. Serie:</b> '+row.numSerie+'</p>');
    $('#datosComplementarios').append('<p><b>No. Motor:</b> '+row.numMotor+'</p>');
    $('#datosComplementarios').append('<p><b>NRPV:</b> '+row.nrpv+'</p>');
    //if(row.tipo == 2 || row.tipo == 3){
      $('#datosComplementarios').append('<p><b>Modelo:</b> '+row.modelo+'</p>');
      $('#datosComplementarios').append('<p><b>Permiso:</b> '+row.permiso+'</p>');
      $('#datosComplementarios').append('<p><b>Señas particulares:</b> '+row.senasPartic+'</p>');
    $('.btn_persona').removeAttr('disabled');
    $('#idPersonaEntrega').removeAttr('disabled');
    //}
  });
  document.getElementById('#idFechaEntrega').value = new Date().toDateInputValue();

</script>
