@section('title')
Vehículos Entregados
@endsection

<link rel="stylesheet" type="text/css" href="{{ asset('css/PreVehiculo/theme.css') }}">
<link  rel="stylesheet" type="text/css" href="{{ asset('css/PreVehiculo/fileinput.css') }}">

<div class="container-fluid">
  <br>
  <div class="card">
    {!! Form::open(['method' => 'POST','files' => true, 'id' => 'formValEntregados'])  !!}
    <br>
    <div class="card-body">
      <div class="row">
        <div class="col-lg-12 col-md-12">
            <p>FAVOR DE VERIFICAR LOS DATOS DE ACUEDO A LAS SIGUIENTES OBSERVACIONES</p><hr>
              <div id="error" class="error" style="color:red;border-color: red;border-radius: 3px;">
                
              </div>
         </div>
      </div>
      <div class="row">
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {{Form::hidden('id1',null,['id'=>'id1'])}}
            {!!Form::label('serieAlterada1','Número de serie alterada')!!}
            <select class=" form-control form-control-sm" data-live-search="true" id="idSerieAlterada1" name="idSerieAlterada1" >
              <option selected="selected" value=""> </option>
              <option  value="0">No </option>
              <option  value="1">SI </option>
            </select>
          </div>
        </div>

        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('serie','Número de serie')!!}

            {!!Form::text('numSerie1',null, ['id'=>'numSerie1','class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el número de serie', 'required','minlength'=>'10','maxlength'=>'17','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'alphanumeric'] )!!}

            {{-- {!!Form::text('numSerie',null, ['class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el número de serie', 'required','minlegth'=>'10','maxlength'=>'17','onkeyup'=>'mayus(this);', 'data-validation'=>'custom', 'data-validation-regexp'=>'^([A-Z\d]{3}([A-Z\d]{5})([A-Z\d]{3})([A-Z\d]{6}))$'] )!!} --}}
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('motorAlterado','Número de motor alterado')!!}
            <select class=" form-control form-control-sm" data-live-search="true" id="idMotorAlterado1" name="idMotorAlterado1" required >
              <option selected="selected" value=""> </option>
              <option  value="0">NO </option>
              <option  value="1">SI </option>
            </select>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('motor','Número de motor')!!}
            {!!Form::text('motor1',null, ['id'=>'motor1','class' => 'form-control form-control-sm','maxlength'=>'10',  'placeholder' => 'Ingrese el número de motor','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'alphanumeric','disabled'=>'disabled' ] )!!}
            {{-- 'data-validation-regexp'=>'^((([A-Z]|[\d])+)+){10}$' --}}
          </div>
        </div>

        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idEstado1', 'Entidad federativa')!!}
            <select  class="templatingSelect2 form-control form-control-sm"  id="idEstado1" name="idEstado1" style="font-size:14px" required onchange="if(this.value!=0) {document.getElementById('idMunicipio1').disabled = false}">
              <option value="" selected="selected" > </option>
              @foreach($estados as $estado)
              <option  value="{{$estado->id}}" >{{$estado->nombre}}</option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idMunicipio1', 'Municipio')!!}
            <input type="text" id="text_municipio" name="text_municipio" hidden>
            <select class="templatingSelect11 form-control form-control-sm" required id="idMunicipio1" name="idMunicipio1" disabled>
              <option value="" selected="selected"></option>
            </select>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idCP','Codigo Postal')!!}
            {!!Form::text('cp1',null, ['id'=>'cp1','class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el código postal', 'required','minlength'=>'1','minlength'=>'5','maxlength'=>'5','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'number'] )!!}
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idCalle','Calle')!!}
            {!!Form::text('calle1',null, ['id'=>'calle1','class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese la calle', 'required','style'=>'text-transform: uppercase;'] )!!}
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idColonia','Colonia')!!}
            {!!Form::text('colonia1',null, ['id'=>'colonia1','class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese la colonia', 'required','style'=>'text-transform: uppercase;'] )!!}
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-2 col-md-2">
          <div class="form-group">
            {!!Form::label('numExt','Número Externo')!!}
            {!!Form::text('numExt1',null, ['id'=>'numExt1','class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el número externo', 'required','minlength'=>'1','maxlength'=>'10','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'alphanumeric'] )!!}
          </div>
        </div>
        <div class="col-lg-2 col-md-2">
          <div class="form-group">
            {!!Form::label('numInt','Número Interno')!!}
             {!!Form::text('numInt1',null, ['id'=>'numInt1' ,'class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el número interno', 'minlength'=>'1','maxlength'=>'10','data-validation'=>'custom','style'=>'text-transform: uppercase;'] )!!}
          </div>
        </div>
        <div class="col-lg-8 col-md-8">
          <div class="form-group">
            {!!Form::label('refeEntrega','Referencia de la entrega')!!}
            <textarea class="form-control form-control-sm" placeholder="Referencia del lugar de la recuperación" rows="2" name="refeEntrega1" cols="90" style="text-transform: uppercase;" id="refeEntrega1"></textarea>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idInspeccion','Inspección realizada')!!}
            <select class="templatingSelect9 form-control form-control-sm" id="idInspeccion11" name="idInspeccion11" required>
              <option value="" selected>Seleccione</option>
              <option value="1" >NO</option>
              <option value="0">SI</option>
            </select>
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idAutoridad','Autoridad que entrega')!!}
            <select  class="templatingSelect8 form-control form-control-sm"  id="idAutoridad1" name="idAutoridad1" required>
              <option selected="selected" value=""> </option>
              <option value="1">MP </option>
              <option value="2">Fiscalía </option>

            </select>
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idFechaEntrega','Fecha de entrega')!!}
            <input class="form-control form-control-sm" type="date" value="2011-08-19" id="idFechaEntrega1" name="idFechaEntrega1" required>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idHoraEntrega','Hora de entrega')!!}
            <input class="form-control form-control-sm" type="time" value="13:45:00" id="idHoraEntrega1" name="idHoraEntrega1" required>
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idEvaluacion','Monto del vehiculo')!!}
            {!!Form::text('idEvaluacion1',null, ['id'=>'idEvaluacion1','class' => 'form-control form-control-sm', 'placeholder' => 'Evaluación del vehículo antes su entrega','minlength'=>'1','maxlength'=>'10','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'number'] )!!}
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idfacturaV','Factura del vehiculo')!!}
            {!!Form::text('idfacturaV1',null, ['id'=>'idfacturaV1','class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese número de la factura del vehículo a entregar','minlength'=>'1','maxlength'=>'10','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'number','required'] )!!}
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idFechaFactura','Fecha de la factura')!!}
            <input class="form-control form-control-sm" type="date" value="2011-08-19" id="idFechaFactura1" name="idFechaFactura1">
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          {!!Form::label('idPersonaEntrega','Persona a quién se entrega')!!}
       {{--    <div class="form-inline">
            <div class="form-check">
              <input class="form-check-input btn_persona" name="checkPersona1" type="radio" id="checkPersona1" value="1" disabled="disabled">
              <label class="form-check-label" for="checkPersona">Propietario</label>
            </div>

            <div class="form-check">
              <input class="form-check-input btn_persona" name="checkPersona1" type="radio" id="checkPersona1" value="2" disabled="disabled">
              <label class="form-check-label" for="checkPersona">Representante</label>
            </div>
          </div> --}}
          {{-- <div class="form-group">
            {!!Form::label('idPersonaEntrega','Persona a quién se entrega')!!}
            <select class="templatingSelect9 form-control form-control-sm" id="idPersonaEntrega" name="idPersonaEntrega" required>
              <option selected value="">Seleccione</option>
              <option value="1" >PROPIETARIO</option>
              <option value="2">REPRESENTANTE LEGAL</option>
            </select>
          </div> --}}
        </div>
        {{-- <div class="row">
          <div class="col-lg-4 col-md-4">
            <div class="form-group">
              {!!Form::label('idNombre','Nombre')!!}
              {!!Form::text('idNombre',null, ['class' => 'form-control form-control-sm', 'placeholder' => 'Escribe el nombre','minlegth'=>'1','maxlength'=>'10','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'number','required'] )!!}
            </div>
          </div>
          <div class="col-lg-4 col-md-4">
            <div class="form-group">
              {!!Form::label('idApellidoP','Apellido Paterno')!!}
              {!!Form::text('idApellidoP',null, ['class' => 'form-control form-control-sm', 'placeholder' => 'Escribe el apellido paterno','minlegth'=>'1','maxlength'=>'10','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'number','required'] )!!}
            </div>
          </div>
          <div class="col-lg-4 col-md-4">
            <div class="form-group">
              {!!Form::label('idApellidoM','Apellido Materno')!!}
              {!!Form::text('idApellidoM',null, ['class' => 'form-control form-control-sm', 'placeholder' => 'Escribe el apellido materno','minlegth'=>'1','maxlength'=>'10','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'number'] )!!}
            </div>
          </div>

        </div> --}}
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idPersonaEntrega1','Propietario/Representante',['id'=>'labelPersona'])!!}
            <select class="templatingSelect9 form-control form-control-sm" id="idPersonaEntrega1" name="idPersonaEntrega1" required disabled="disabled">
              <option selected value="">Seleccione</option>
            </select>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 col-md-4">
          <div id="iden" >
            <label>Identificacion oficial</label>
            <select class="templatingSelect9 form-control form-control-sm" id="listaiden_op11" name="listaiden11" class="listaiden">
              <option selected value="" >--Seleccione identificación--</option>
              @foreach($identificacionoficial as $identificacionoficial)
              <option  value="{{$identificacionoficial->id}}" >{{$identificacionoficial->nombre}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idCompDomicilio','Comprobante de domicilio')!!}
            <select class="templatingSelect9 form-control form-control-sm" id="idCompDomicilio1" name="idCompDomicilio1" required>
              <option  value="">Seleccione</option>
              <option  value="2">Agua</option>
              <option  value="3">Internet</option>
              <option value="1">Luz</option>
            </select>
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idOtroDocumento','Otro documento')!!}
            {!!Form::text('idOtroDocumento1',null, ['id'=>'idOtroDocumento1','class' => 'form-control form-control-sm', 'placeholder' => 'Especifica comprobante de domicilio','minlength'=>'1','maxlength'=>'10','style'=>'text-transform: uppercase;'] )!!}
          </div>
        </div>
      </div>
      <div class="card-footer" align="right">
      <a href="#0" class="cd-top">Top</a>
      </div>
    </div>

  </div>
</div>

{!!Form::text('idVeh',null, ['hidden','id'=>'idVeh'] )!!}
{!!Form::text('idCarpeta',null, ['hidden','id'=>'idCarpeta'] )!!}

<!--</form>-->
{!! Form::close()!!}

<script src="{{ asset('js/denuncia/peticiones.js') }}"></script>

<script type="text/javascript">

$(document).ready(function(){

  $("#idEstado1").change(function() {
    var identificador = $(this).val();
    var respuesta;
    $('#idMunicipio1').html('');
    if(identificador!=''){
      var ruta = "{{route('municipio',':estado')}}";
      ruta = ruta.replace(':estado', identificador);
      datos = null;
      respuesta = peticionInfomacion('get',ruta,datos);
      if (respuesta != null) {
        for(i=0; i<respuesta.estado.length; i++){
          $("#idMunicipio1").append("<option value='"+respuesta.estado[i].id+"'> "+respuesta.estado[i].nombre+"</option>");
        }
        if(respuesta.estado.length > 0 ){
          localStorage.setItem("idMunicipio", respuesta.estado[0].id);
        }
        console.log('cargando datos');
      }else{
        alert('Error de servidor');
      }
    }else{
      //alert('no aplica');
    }
  });

  $("#idMunicipio").change(function() {
    localStorage.setItem("idMunicipio", $('#idMunicipio').val());
  });

  $("#idSubmarca").change(function() {
    localStorage.setItem("idSubmarca", $('#idSubmarca').val());
  });


  $("#serieB" ).blur(function() {
    console.log('este es el valor'+idx);
    var prohibido =['SE IGNORA', 'NULL','null','se ignora' ];
    var cadenaUser = $("#serieB").val();
    var idx = prohibido.indexOf(cadenaUser);
    console.log('esta es mi cadena '+cadenaUser);
    console.log('este es el valor'+idx);
    if(idx > -1){
      $("#serieB").val("");
    }
  });

  $('#motorB').blur(function(){
    toastr.warning("Ha modificado el número de motor", "Atención",{
      "preventDuplicates": true
    });
  });

  $('#motor1').blur(function(){
    toastr.warning("Ha modificado el número de motor", "Atención",{
      "preventDuplicates": true
    });
  });

  $('#numSerie1').blur(function(){
    toastr.warning("Ha modificado el número de serie/VIN", "Atención",{
      "preventDuplicates": true
    });
  });

  $('#serieB').blur(function(){
    toastr.warning("Ha modificado el número de serie/VIN", "Atención",{
      "preventDuplicates": true
    });
  });

  $('#buscarVeh').bind('click', function () {
    $('#datosComplementarios').empty();
    $('#vehRecuperados').modal('show');
    var placa = $('#placaB').val();
    placa = (placa== '') ? 0 : placa;
    var serie = $('#serieB').val();
    serie = (serie== '') ? 0 : serie;
    var motor = $('#motorB').val();
    motor = (motor== '') ? 0 : motor;
    var tipoCons = 2;
    //alert(placa+','+serie+','+motor);

    //registros vehiculos involucrados

    var ruta3 = "{{route('get_recuperados',['placa'=>':placa','serie'=>':serie','motor'=>':motor'])}}";
    ruta3 = ruta3.replace(':placa', placa);
    ruta3 = ruta3.replace(':serie', serie);
    ruta3 = ruta3.replace(':motor', motor);
    datos = null;
    //console.log(ruta)
    recuperados = peticionInfomacion('get',ruta3,datos);
    console.log('recuperados');
    console.log(recuperados);
    console.log('-------------');

    $('#tablevehiculos').bootstrapTable('load',recuperados.registros);

  });


  $('#idSerieAlterada1').change(function(){
    //alert($('#serieB').val());
    if($(this).val()==0){
      //alert("entra aqui");
      $('#numSerie1').val($('#serieB').val());
      $('#numSerie1').attr('disabled','disabled');
    }else{
      $('#numSerie1').val('');
      $('#numSerie1').removeAttr('disabled');
    }
  });

  $('#idMotorAlterado1').change(function(){
    //alert($('#serieB').val());
    if($(this).val()==0){
      //alert("entra aqui");
      $('#motor1').val($('#motorB').val());
      $('#motor1').attr('disabled','disabled');
    }else{
      $('#motor1').val('');
      $('#motor1').removeAttr('disabled');
    }
  });

  $('.btn_persona').change(function(){
    var name="";
    $('#idPersonaEntrega1').html('');
    var persona = $(this).val();
    ($(this).val()==1)?$('#labelPersona').text("Propietario"):$('#labelPersona').text("Representante");;
    var idCarpeta = $('#idCarpeta').val();
    var ruta = "{{route('persona_entrega',['persona'=>':persona','idCarpeta'=>':idCarpeta'])}}";
    ruta = ruta.replace(':persona', persona);
    ruta = ruta.replace(':idCarpeta', idCarpeta);
    datos = null;
    //console.log(ruta)
    personas = peticionInfomacion('get',ruta,datos);
    console.log(personas);
    if (personas != null) {

        for(i=0; i<personas.length; i++){

          name = ($(this).val()==1)?personas[i].persona:personas[i].representanteLegal;

          $("#idPersonaEntrega1").append("<option value='"+personas[i].idPersona+"'> "+name+"</option>");
        }
        // if(personas.estado.length > 0 ){
        //   localStorage.setItem("idMunicipio", personas.estado[0].id);
        // }
        // console.log('cargando datos');
      }else{
        alert('Error de servidor');
      }
  });

});

$('#tablevehiculos').bootstrapTable({
  search:  true,
  showColumns: true,
  showRefresh: true,
    //url: ruta,
    buttonsAlign :'left',
    searchAlign :'left',
    idField: 'id',
    iconsPrefix: 'fa',
    pagination : true,
    pageSize: 5,
    columns: [{
      field: 'id',
      title: 'ID',
      sortable: true,
      visible: false
    }, {
      field: 'numCarpeta',
      title: 'No. Carpeta',
      sortable: true
    },{
      field: 'placas',
      title: 'Placas',
      sortable: true
    }, {
      field: 'nrpv',
      title: 'NRPV',
      sortable: true,
      visible: false
    }, {
      field: 'numSerie',
      title: 'No. serie',
      sortable: true
    }, {
      field: 'numMotor',
      title: 'No. Motor',
      sortable: true
    }],
    //width: '50%',
  });

  $('#tablevehiculos').on('click-row.bs.table', function(tr, row){
    console.log(row);
      //deshabilitar();
    $('#idVeh').val(row.id);
    $('#idCarpeta').val(row.idCarpeta);
    $('#placaB').val(row.placas);

    $('#serieB').val(row.numSerie);
    $('#motorB').val(row.numMotor);


    $('#datosComplementarios').empty();
    $('#datosComplementarios').append('<p><b>Carpeta:</b> '+row.numCarpeta+'</p>');
    $('#datosComplementarios').append('<p><b>Placa:</b> '+row.placas+'</p>');
    $('#datosComplementarios').append('<p><b>No. Serie:</b> '+row.numSerie+'</p>');
    $('#datosComplementarios').append('<p><b>No. Motor:</b> '+row.numMotor+'</p>');
    $('#datosComplementarios').append('<p><b>NRPV:</b> '+row.nrpv+'</p>');
    //if(row.tipo == 2 || row.tipo == 3){
      $('#datosComplementarios').append('<p><b>Modelo:</b> '+row.modelo+'</p>');
      $('#datosComplementarios').append('<p><b>Permiso:</b> '+row.permiso+'</p>');
      $('#datosComplementarios').append('<p><b>Señas particulares:</b> '+row.senasPartic+'</p>');
    $('.btn_persona').removeAttr('disabled');
    $('#idPersonaEntrega').removeAttr('disabled');
    //}
  })

</script>
