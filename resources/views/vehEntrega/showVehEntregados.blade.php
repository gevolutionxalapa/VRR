<!--template-->
@extends('template.main2')
<!--menu-->

<!-- Contenido -->
@section('content')


			<!-- Contenido -->
			<div class="tab-content" id="pills-tabContent">
				<div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
					@include('vehEntrega.forms.entregaVehiculo')
				</div>
				<div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
					@include('vehEntrega.listas.entregadosVehiculos')
				</div>
			</div>



@endsection
