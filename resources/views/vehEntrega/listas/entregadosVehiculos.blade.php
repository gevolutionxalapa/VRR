@section('title')
Entregados
@endsection
<!--menu-->


<div class="row">
	<div class="col-lg-12 col-md-12">
		<br>
		<div class="card">
			<div class="card-header">
				<h5>Registros de vehiculos entregados</h5>
			</div>
			<div class="card-body">
				<ul class="nav nav-pills">
	       <li style="padding-right: 10px"><a href="{{route('showVehEntregados')}}" class="nav-link  btn-outline-dark " ><span class="glyphicon glyphicon-home"></span> Registro de vehiculo entregado </a></li>
	       <li class="active"><a href="#" class="nav-link active btn-outline-dark " ><span class="glyphicon glyphicon-user"></span>Vehiculos entregados</a></li>
	      </ul>
			<table id="table" class="table table-bordered table-hover table-responsive-sm"></table>
		</div>
		</div>
	</div>
</div>
{{-- MODAL --}}

<div id="modalValidacion" class="modal" tabindex="-1" role="dialog" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Detalle del vehículo recuperado</h5>
        <button type="button" class="close btn_cerrar" data-dismiss="modal" id="cerrar" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form_validacion">
          {!!Form::text('id1',null,['id' => 'id1'])!!}
          {!!Form::text('llaveExterna',null,['id' => 'llaveExterna', 'hidden'])!!}
          <div class="row">
            <div class="col-lg col-md">
              <p>FAVOR DE VERIFICAR LOS DATOS DE ACUERDO A LAS SIGUIENTES OBSERVACIONES</p><hr>
              <div id="error" style="color:red;border-color: red;border-radius: 3px;">

              </div>
              {{-- {!!Form::label('Comentario Entidad','Comentario relacionado a los datos erróneos')!!}
              <textarea class="form-control form-control-sm" placeholder="Comentario" rows="3" name="comentario" cols="50" style="text-transform: uppercase;" required id="comentario" readonly="readonly"></textarea> --}}
            </div>
          </div>
          <div class="row">
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('serieAlterada1','Número de serie alterada')!!}
            <select class=" form-control form-control-sm" data-live-search="true" id="idSerieAlterada1" name="idSerieAlterada1" >
              <option selected="selected" value=""> </option>
              <option  value="0">No </option>
              <option  value="1">SI </option>
            </select>
          </div>
        </div>

        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('serie','Número de serie')!!}

            {!!Form::text('numSerie1',null, ['id'=>'numSerie1','class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el número de serie', 'required','minlength'=>'10','maxlength'=>'17','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'alphanumeric'] )!!}

            {{-- {!!Form::text('numSerie',null, ['class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el número de serie', 'required','minlegth'=>'10','maxlength'=>'17','onkeyup'=>'mayus(this);', 'data-validation'=>'custom', 'data-validation-regexp'=>'^([A-Z\d]{3}([A-Z\d]{5})([A-Z\d]{3})([A-Z\d]{6}))$'] )!!} --}}
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('motorAlterado','Número de motor alterado')!!}
            <select class=" form-control form-control-sm" data-live-search="true" id="idMotorAlterado1" name="idMotorAlterado1" required >
              <option selected="selected" value=""> </option>
              <option  value="0">NO </option>
              <option  value="1">SI </option>
            </select>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('motor','Número de motor')!!}
            {!!Form::text('motor1',null, ['id'=>'motor1','class' => 'form-control form-control-sm','maxlength'=>'10',  'placeholder' => 'Ingrese el número de motor','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'alphanumeric'] )!!}
            {{-- 'data-validation-regexp'=>'^((([A-Z]|[\d])+)+){10}$' --}}
          </div>
        </div>

        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idEstado1', 'Entidad federativa')!!}
            <select  class="templatingSelect2 form-control form-control-sm"  id="idEstado1" name="idEstado1" style="font-size:14px" required onchange="if(this.value!=0) {document.getElementById('idMunicipio1').disabled = false}">
              <option value="" selected="selected" > </option>
              @foreach($estados as $estado)
              <option  value="{{$estado->id}}" >{{$estado->nombre}}</option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idMunicipio1', 'Municipio')!!}
            <input type="text" id="text_municipio" name="text_municipio" hidden>
            <select class="templatingSelect11 form-control form-control-sm" required id="idMunicipio1" name="idMunicipio1" disabled>
              <option value="" selected="selected"></option>
            </select>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idCP','Codigo Postal')!!}
            {!!Form::text('cp1',null, ['id'=>'cp1','class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el código postal', 'required','minlength'=>'1','minlength'=>'5','maxlength'=>'5','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'number'] )!!}
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idCalle','Calle')!!}
            {!!Form::text('calle1',null, ['id'=>'calle1','class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese la calle', 'required','style'=>'text-transform: uppercase;'] )!!}
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idColonia','Colonia')!!}
            {!!Form::text('colonia1',null, ['id'=>'colonia1','class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese la colonia', 'required','style'=>'text-transform: uppercase;'] )!!}
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-2 col-md-2">
          <div class="form-group">
            {!!Form::label('numExt','Número Externo')!!}
            {!!Form::text('numExt1',null, ['id'=>'numExt1','class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el número externo', 'required','minlength'=>'1','maxlength'=>'10','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'alphanumeric'] )!!}
          </div>
        </div>
        <div class="col-lg-2 col-md-2">
          <div class="form-group">
            {!!Form::label('numInt','Número Interno')!!}
             {!!Form::text('numInt1',null, ['id'=>'numInt1' ,'class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese el número interno', 'minlength'=>'1','maxlength'=>'10','data-validation'=>'custom','style'=>'text-transform: uppercase;'] )!!}
          </div>
        </div>
        <div class="col-lg-8 col-md-8">
          <div class="form-group">
            {!!Form::label('refeEntrega','Referencia de la entrega')!!}
            <textarea class="form-control form-control-sm" placeholder="Referencia del lugar de la recuperación" rows="2" name="refeEntrega1" cols="90" style="text-transform: uppercase;" id="refeEntrega1"></textarea>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idInspeccion','Inspección realizada')!!}
            <select class="templatingSelect9 form-control form-control-sm" id="idInspeccion11" name="idInspeccion11" required>
              <option value="" selected>Seleccione</option>
              <option value="1" >NO</option>
              <option value="0">SI</option>
            </select>
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idAutoridad','Autoridad que entrega')!!}
            <select  class="templatingSelect8 form-control form-control-sm"  id="idAutoridad1" name="idAutoridad1" required>
              <option selected="selected" value=""> </option>
              <option value="1">MP </option>
              <option value="2">Fiscalía </option>

            </select>
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idFechaEntrega','Fecha de entrega')!!}
            <input class="form-control form-control-sm" type="date" value="2011-08-19" id="idFechaEntrega1" name="idFechaEntrega1" required>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idHoraEntrega','Hora de entrega')!!}
            <input class="form-control form-control-sm" type="time" value="13:45:00" id="idHoraEntrega1" name="idHoraEntrega1" required>
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idEvaluacion','Monto del vehiculo')!!}
            {!!Form::text('idEvaluacion1',null, ['id'=>'idEvaluacion1','class' => 'form-control form-control-sm', 'placeholder' => 'Evaluación del vehículo antes su entrega','minlength'=>'1','maxlength'=>'10','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'number'] )!!}
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idfacturaV','Factura del vehiculo')!!}
            {!!Form::text('idfacturaV1',null, ['id'=>'idfacturaV1','class' => 'form-control form-control-sm', 'placeholder' => 'Ingrese número de la factura del vehículo a entregar','minlength'=>'1','maxlength'=>'10','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'number','required'] )!!}
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idFechaFactura','Fecha de la factura')!!}
            <input class="form-control form-control-sm" type="date" value="2011-08-19" id="idFechaFactura1" name="idFechaFactura1">
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          {!!Form::label('idPersonaEntrega','Persona a quién se entrega')!!}
       {{--    <div class="form-inline">
            <div class="form-check">
              <input class="form-check-input btn_persona" name="checkPersona1" type="radio" id="checkPersona1" value="1" disabled="disabled">
              <label class="form-check-label" for="checkPersona">Propietario</label>
            </div>

            <div class="form-check">
              <input class="form-check-input btn_persona" name="checkPersona1" type="radio" id="checkPersona1" value="2" disabled="disabled">
              <label class="form-check-label" for="checkPersona">Representante</label>
            </div>
          </div> --}}
          {{-- <div class="form-group">
            {!!Form::label('idPersonaEntrega','Persona a quién se entrega')!!}
            <select class="templatingSelect9 form-control form-control-sm" id="idPersonaEntrega" name="idPersonaEntrega" required>
              <option selected value="">Seleccione</option>
              <option value="1" >PROPIETARIO</option>
              <option value="2">REPRESENTANTE LEGAL</option>
            </select>
          </div> --}}
        </div>
        {{-- <div class="row">
          <div class="col-lg-4 col-md-4">
            <div class="form-group">
              {!!Form::label('idNombre','Nombre')!!}
              {!!Form::text('idNombre',null, ['class' => 'form-control form-control-sm', 'placeholder' => 'Escribe el nombre','minlegth'=>'1','maxlength'=>'10','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'number','required'] )!!}
            </div>
          </div>
          <div class="col-lg-4 col-md-4">
            <div class="form-group">
              {!!Form::label('idApellidoP','Apellido Paterno')!!}
              {!!Form::text('idApellidoP',null, ['class' => 'form-control form-control-sm', 'placeholder' => 'Escribe el apellido paterno','minlegth'=>'1','maxlength'=>'10','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'number','required'] )!!}
            </div>
          </div>
          <div class="col-lg-4 col-md-4">
            <div class="form-group">
              {!!Form::label('idApellidoM','Apellido Materno')!!}
              {!!Form::text('idApellidoM',null, ['class' => 'form-control form-control-sm', 'placeholder' => 'Escribe el apellido materno','minlegth'=>'1','maxlength'=>'10','style'=>'text-transform: uppercase;', 'data-validation'=>'custom', 'data-validation'=>'number'] )!!}
            </div>
          </div>

        </div> --}}
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idPersonaEntrega1','Propietario/Representante',['id'=>'labelPersona'])!!}
            <select class="templatingSelect9 form-control form-control-sm" id="idPersonaEntrega1" name="idPersonaEntrega1" required disabled="disabled">
              <option selected value="">Seleccione</option>
            </select>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 col-md-4">
          <div id="iden" >
            <label>Identificacion oficial</label>
            <select class="templatingSelect9 form-control form-control-sm" id="listaiden_op11" name="listaiden11" class="listaiden">
              <option selected value="" >--Seleccione identificación--</option>
              @foreach($identificacionoficial as $identificacionoficial)
              <option  value="{{$identificacionoficial->id}}" >{{$identificacionoficial->nombre}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idCompDomicilio','Comprobante de domicilio')!!}
            <select class="templatingSelect9 form-control form-control-sm" id="idCompDomicilio1" name="idCompDomicilio1" required>
              <option  value="">Seleccione</option>
              <option  value="2">Agua</option>
              <option  value="3">Internet</option>
              <option value="1">Luz</option>
            </select>
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="form-group">
            {!!Form::label('idOtroDocumento','Otro documento')!!}
            {!!Form::text('idOtroDocumento1',null, ['id'=>'idOtroDocumento1','class' => 'form-control form-control-sm', 'placeholder' => 'Especifica comprobante de domicilio','minlength'=>'1','maxlength'=>'10','style'=>'text-transform: uppercase;'] )!!}
          </div>
        </div>
      </div>


        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" id="guardar_validacion" >Guardar</button>
        <button type="button" class="btn btn-secondary btn_cerrar" id="cerrar" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
	$("#idEstado1").change(function() {
    var identificador = $(this).val();
    var respuesta;
    $('#idMunicipio1').html('');
    if(identificador!=''){
      var ruta = "{{route('municipio',':estado')}}";
      ruta = ruta.replace(':estado', identificador);
      datos = null;
      respuesta = peticionInfomacion('get',ruta,datos);
      if (respuesta != null) {
        for(i=0; i<respuesta.estado.length; i++){
          $("#idMunicipio1").append("<option value='"+respuesta.estado[i].id+"'> "+respuesta.estado[i].nombre+"</option>");
        }
        if(respuesta.estado.length > 0 ){
          localStorage.setItem("idMunicipio", respuesta.estado[0].id);
        }
        console.log('cargando datos');
      }else{
        alert('Error de servidor');
      }
    }else{
      //alert('no aplica');
    }
  });

  $("#idMunicipio").change(function() {
    localStorage.setItem("idMunicipio", $('#idMunicipio').val());
  });

  $('#motor1').blur(function(){
    toastr.warning("Ha modificado el número de motor", "Atención",{
      "preventDuplicates": true
    });
  });

  $('#numSerie1').blur(function(){
    toastr.warning("Ha modificado el número de serie/VIN", "Atención",{
      "preventDuplicates": true
    });
  });
	/* Creacion de los elementos extras a mostrar*/
	function operateFormatter(value, row, index) {
		return [
		'<a class="detalleVehiculo btn btn-link" href="javascript:void(0)" title="Detalle del vehiculo">',
		'<i class="fa fa-info" aria-hidden="true"></i>',
		'</a>  ',
		].join('');
	}
	function operateFormatter3(value, row, index){
		var color = "";
		var icono = "";
		var ruta = "{{route('validacionEntregados',':idVeh')}}";
		ruta = ruta.replace(':idVeh', row.idVeh);
		// console.log('Esta es la ruta -> '+ruta);
		//ruta = ruta.replace(':tipoVal', 1);
		// console.log(ruta);
		statusBus = peticionInfomacion('get',ruta,null);
		// console.log('Datos BUS');
		// console.log(statusBus[0].busCNS);
		if(statusBus[0].busCNS==2){
			 icono = "fa fa-upload";
			 color = "#138C13";
		}
		if(statusBus[0].busCNS==3){
				//alert('entra aqui');
			 icono = "fa fa-download";
			 color = "#8C1333";
		}
		if(statusBus[0].busFGE==0 && statusBus[0].busCNS ==0){
			color = "#BDBDBD";
			icono = "fa fa-circle-o";
		}
		if(statusBus[0].busFGE==1 && statusBus[0].busCNS ==0){
			color = "#DDA61D";
			icono = "fa fa-exclamation-circle";
		}
		if(statusBus[0].busFGE==1 && statusBus[0].busCNS==1){
			 color = "#DDA61D";
			 icono = "fa fa-upload";
		}

		if(statusBus[0].busCNS==0 && statusBus[0].busCNSKey==3){
			 icono = "fa fa-download";
			 color = "#8C1333";
		}

		return [
			'<a class="statusRegistro"  href="javascript:void(0)" title="Validación de Registro" align="center" >',
			'<i class="'+icono+'" aria-hidden="true" style="color:'+color+';"> </i>',
			'</a>  ',
		].join('');
	}

	/* Asignacion de funciones a los elemento agreados anteriormente en operateFormatter*/
	window.operateEvents = {
		'click .detalleVehiculo': function (e, value, row, index) {
			console.log(row);
		},
		'click .statusRegistro': function (e, value, row, index) {

			var ruta = "{{route('validacionEntregados',':idVeh')}}";
			ruta = ruta.replace(':idVeh', row.idVeh);
			//ruta = ruta.replace(':tipoVal', 1);

			//console.log(row);
			statusBus = peticionInfomacion('get',ruta,null);
			console.log('Datos BUS');
			console.log(statusBus[0].busFGE);

				if(statusBus[0].busCNS==2){
				 $(this).popover({title: "Validación", content: "El registro se encuentra en PLATAFORMA NACIONAL", placement: "right"});
					$(this).popover('show');
				}
				if((statusBus[0].busCNS==3) || (statusBus[0].busCNS==0 && statusBus[0].busCNSKey==3)){
					//$('#error').html('');
					var error = "";
					var ruta1 = "{{route('validacionEntregadosData',':idVeh')}}";
					ruta1 = ruta1.replace(':idVeh',row.idVeh);
					//ruta = ruta.replace(':tipoVal', 1);
					dataError = peticionInfomacion('get',ruta1,null);
					console.log(ruta1);
					console.log(row);
					if (dataError.length > 0) {
						for(i=0; i<dataError.length; i++){
							//alert(i);
							error = error + '<p>'+dataError[i].DESC_ERROR+'</p>';

						}
						console.log(error);
						$('#error').append(error);
					}



					//Creal modal con errores
					$(this).popover('destroy');
					$('#modalValidacion').modal('show');
					$('#id1').val(row.idVeh);
					$('#llaveExterna').val(dataError[0].LLAVE);


					$('#idSerieAlterada1').val(row.SERIE_ALTERADA).trigger('change');
					$('#numSerie1').val(row.SERIE);
					$('#idMotorAlterado1').val(row.MOTOR_ALTERADO).trigger('change');
					$('#motor1').val(row.MOTOR);
					$('#idEstado1').val(row.ID_ENTIDAD_ENTREGA).trigger('change');
					$('#idMunicipio1').val(row.ID_MUNICIPIO_ENTREGA).trigger('change');
					$('#cp1').val(row.CP_ENTREGA);
					$('#calle1').val(row.CALLE_ENTREGA);
					$('#colonia1').val(row.COLONIA_ENTREGA);
					$('#numExt1').val(row.NUMEXT_ENTREGA);
					$('#numInt1').val(row.NUMINT_ENTREGA);
					$('#refeEntrega1').val(row.REFERENCIA_ENTREGA);

					$('#idInspeccion11').val(row.INSPECCION).trigger('change');
					$('#idAutoridad1').val(row.ID_FUENTE_ENTREGA).trigger('change');
					$('#idFechaEntrega1').val(row.FECHA_ENTREGA);
					$('#idHoraEntrega1').val(row.HORA_ENTREGA);
					$('#idEvaluacion1').val(row.MONTO_VEHICULO);
					$('#idfacturaV1').val(row.FACTURA_VEHICULO);
					$('#idFechaFactura1').val(row.FECHA_FACTURA);
					//$('#listaiden_op11').val(); true
					//$('#idCompDomicilio1').val(); true
					//$('#idOtroDocumento1').val(); true


				}
				if(statusBus[0].busFGE==1 && statusBus[0].busCNS ==0 && statusBus[0].busCNSKey ==0){
					$(this).popover({title: "Validación", content: "El registro se ha enviado al BUS", placement: "right"});
					$(this).popover('show');
				}
				if(statusBus[0].busFGE==1 && statusBus[0].busCNS==1){
					$(this).popover({title: "Validación", content: "El registro está en validación en la CNS", placement: "right"});
					$(this).popover('show');
				}
				// if(statusBus[0].busCNS==0 && statusBus[0].busCNSKey==3){
				//  icono = "fa fa-download";
				//  color = "#8C1333";
				// }

			setTimeout(function () {
				$('.statusRegistro').popover('hide');
			}, 7000);
		},




	};


	/* Configuracion de la tabla de registros de vehiculos */
	var ruta = "{{route('getVehEntregados')}}";
	$('#table').bootstrapTable({
		search:  true,
		showColumns: true,
		showRefresh: true,
		url: ruta,
		buttonsAlign :'right',
		searchAlign :'right',
		idField: 'id',
		iconsPrefix: 'fa',
		pagination : true,
		pageSize: 5,
		columns: [{
			field: 'id',
			title: 'ID',
			sortable: true,
			visible: false
		},{
			field: 'BUS',
			title: 'P.NACIONAL',
			events: operateEvents,
			formatter: operateFormatter3
			// sortable: true
		},{
			field: 'SERIE_ALTERADA',
			title: 'SERIE ALTERADA',
			sortable: true
		},{
			field: 'MOTOR_ALTERADO',
			title: 'MOTOR ALTERADO',
			sortable: true
		}, {
			field: 'SERIE',
			title: 'SERIE',
			sortable: true
		}, {
			field: 'MOTOR',
			title: 'MOTOR',
			sortable: true
		}, {
			field: 'MONTO_VEHICULO',
			title: 'MONTO VEHICULO',
			sortable: true
		}, {
			field: 'FACTURA_VEHICULO',
			title: 'FACTURA VEHICULO',
			sortable: true
		}, {
			field: 'FECHA_FACTURA',
			title: 'FECHA FACTURA',
			sortable: true
		}, {
			field: 'CALLE_ENTREGA',
			title: 'CALLE ENTREGA',
			sortable: true
		}, {
			field: 'NUMEXT_ENTREGA',
			title: 'NUMEXT ENTREGA',
			sortable: true
		}, {
			field: 'NUMINT_ENTREGA',
			title: 'NUMINT ENTREGA',
			sortable: true
		}, {
			field: 'COLONIA_ENTREGA',
			title: 'COLONIA ENTREGA',
			sortable: true
		}, {
			field: 'estado',
			title: 'ENTIDAD ENTREGA',
			sortable: true
		}, {
			field: 'CP_ENTREGA',
			title: 'CP ENTREGA',
			sortable: true
		}, {
			field: 'REFERENCIA_ENTREGA',
			title: 'REFERENCIA ENTREGA',
			sortable: true,
			visible: false
		}, {
			field: 'FECHA_ENTREGA',
			title: 'FECHA ENTREGA',
			sortable: true,
			visible: false
		}, {
			field: 'HORA_ENTREGA',
			title: 'HORA ENTREGA',
			sortable: true,
			visible: false
		}, {
			field: 'CRED_ELECT_PROP',
			title: 'CREDEDENCIA ELECTORAL',
			sortable: true,
			visible: false
		}, {
			field: 'PASAPORTE_PROP',
			title: 'PASAPORTE',
			sortable: true,
			visible: false
		}, {
			field: 'CEDULA_PROF_PROP',
			title: 'CEDULA_PROFESIONAL',
			sortable: true,
			visible: false
		}, {
			field: 'COMPROB_DOMIC_PROP',
			title: 'COMPROBANTE DOMICILIO',
			sortable: true,
			visible: false
		}, // {
		// 	field: 'operate',
		// 	title: 'Acciones',
		// 	align: 'center',
		// 	events: operateEvents,
		// 	formatter: operateFormatter
		// }
    ],
	});


	 $('#guardar_validacion').bind('click', function () {
      $('#form_validacion').find('input, textarea, button, select').removeAttr('disabled');
      $.ajax({
        url : "{{route('actualizarValidacionEntregados')}}",
        data : $('#form_validacion').serialize(),
        type : 'POST',
        success : function(json) {
          swal({
            title: "Datos actualizados correctamente",
            icon: "success",

          });
          $('#table').bootstrapTable('refresh');
          $('#modalValidacion').modal('toggle');
          $('#guardar_validacion').attr('disabled','disabled');
        },


        error : function(xhr, status) {
          swal({
            title: "Error al guardar cambios",
            icon: "error",
          });
        },
        complete : function(xhr, status) {
        }
      });
    });
</script>
</div>
