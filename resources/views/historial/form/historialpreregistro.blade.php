@section('title')
Historial Preregistros
@endsection
<!--menu-->


<div class="row">
	<div class="col-lg-12 col-md-12">
		<br>
		<div class="card">
			<div class="card-header">
				<h5>Historial de Preregistros</h5>
			</div>
			<div class="card-body">
				<ul class="nav nav-pills">
     <li style="padding-right: 10px"><a href="{{route('index-historial')}}" class="nav-link  btn-outline-dark " ><span class="glyphicon glyphicon-home"></span> Historial Registro</a></li>
     <li class="active"><a href="#" class="nav-link active btn-outline-dark " ><span class="glyphicon glyphicon-user"></span> Historial Preregistro</a></li>
   </ul>
				<table id="table2" class="table table-bordered table-hover table-responsive-sm"></table>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	/* Creacion de los elementos extras a mostrar*/
	function operateFormatter(value, row, index) {
		return [
		'<a class="historialpre" href="javascript:void(0)" title="Detalle del vehiculo">',
		'<i class="fa fa-info" aria-hidden="true"></i>',
		'</a>  ',
		].join('');
	}
	function operateFormatterFolio(value, row, index) {
		var valor = '';
		if(row.status_token == 1){
			valor = 'Pendiente';
		}
		if(row.status_token == 3){
			valor = 'Procesado';
		}

		return [valor].join('');
	}

	/* Asignacion de funciones a los elemento agreados anteriormente en operateFormatter*/
	window.operateEvents = {
		'click .historialpre': function (e, value, row, index) {
			console.log(row);
		},
	};


	/* Configuracion de la tabla de registros de vehiculos */
	var ruta = "{{route('getpreregistros')}}";
	$('#table2').bootstrapTable({
		search:  true,
		showColumns: true,
		showRefresh: true,
		url: ruta,
		buttonsAlign :'right',
		searchAlign :'right',
		idField: 'id',
		iconsPrefix: 'fa',
		pagination : true,
		pageSize: 5,
		columns: [{
			field: 'id',
			title: 'ID',
			sortable: true,
			visible: false
		},{
			field: 'status_token',
			title: 'Folio',
			sortable: true,
			formatter: operateFormatterFolio
		},{
			field: 'token',
			title: 'Folio',
			sortable: true
		},{
			field: 'placas',
			title: 'Placas',
			sortable: true
		}, {
			field: 'modelo',
			title: 'Modelo',
			sortable: true
		}, {
			field: 'nrpv',
			title: 'NRPV',
			sortable: true
		}, {
			field: 'folioDoctoCirc',
			title: 'Permiso',
			sortable: true
		}, {
			field: 'numSerie',
			title: 'No. serie',
			sortable: true
		}, {
			field: 'numMotor',
			title: 'No. Motor',
			sortable: true
		}, {
			field: 'procedencia',
			title: 'Procedencia',
			sortable: true
		}, {
			field: 'marca',
			title: 'Marca',
			sortable: true
		}, {
			field: 'submarca',
			title: 'Submarca',
			sortable: true,
			visible: false
		}, {
			field: 'color',
			title: 'Color',
			sortable: true,
			visible: false
		}, {
			field: 'aseguradora',
			title: 'Aseguradora',
			sortable: true,
			visible: false
		}, {
			field: 'senasPartic',
			title: 'Señas Particulares',
			sortable: true,
			visible: false
		}, {
			visible: false,
			field: 'operate',
			title: 'Acciones',
			align: 'center',
			events: operateEvents,
			formatter: operateFormatter
		}],
	});
</script>
