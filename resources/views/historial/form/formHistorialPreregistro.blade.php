@section('title')
Historial Registros
@endsection
<!--menu-->

<div class="row">
	<div class="col-lg-12 col-md-12">
		<br>
		<div class="card">
			<div class="card-header">
				<h5>Historial de Registros</h5>
			</div>
			<div class="card-body">
				<table id="table" class="table table-bordered table-hover table-responsive-sm"></table>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	/* Creacion de los elementos extras a mostrar*/
	function operateFormatter(value, row, index) {
		return [
		'<a class="historial" href="javascript:void(0)" title="Detalle del vehiculo">',
		'<i class="fa fa-info" aria-hidden="true"></i>',
		'</a>  ',
		].join('');
	}

	/* Asignacion de funciones a los elemento agreados anteriormente en operateFormatter*/
	window.operateEvents = {
		'click .historial': function (e, value, row, index) {
			console.log(row);
		},
	};


	/* Configuracion de la tabla de registros de vehiculos */
	var ruta = "{{route('getVehEntregados')}}";
	$('#table').bootstrapTable({
		search:  true,
		showColumns: true,
		showRefresh: true,
		url: ruta,
		buttonsAlign :'right',
		searchAlign :'right',
		idField: 'id',
		iconsPrefix: 'fa',
		pagination : true,
		pageSize: 5,
		columns: [{
			field: 'id',
			title: 'ID',
			sortable: true,
			visible: false
		},{
			field: 'placas',
			title: 'Placas',
			sortable: true
		}, {
			field: 'modelo',
			title: 'Modelo',
			sortable: true
		}, {
			field: 'nrpv',
			title: 'NRPV',
			sortable: true
		}, {
			field: 'permiso',
			title: 'Permiso',
			sortable: true
		}, {
			field: 'numSerie',
			title: 'No. serie',
			sortable: true
		}, {
			field: 'numMotor',
			title: 'No. Motor',
			sortable: true
		}, {
			field: 'procedencia',
			title: 'Procedencia',
			sortable: true
		}, {
			field: 'marca',
			title: 'Marca',
			sortable: true
		}, {
			field: 'submarca',
			title: 'Submarca',
			sortable: true,
			visible: false
		}, {
			field: 'color',
			title: 'Color',
			sortable: true,
			visible: false
		}, {
			field: 'tipoVehiculo',
			title: 'Tipo',
			sortable: true,
			visible: false
		}, {
			field: 'claseVehiculo',
			title: 'Clase',
			sortable: true,
			visible: false
		}, {
			field: 'tipoUSo',
			title: 'Tipo de uso',
			sortable: true,
			visible: false
		}, {
			field: 'aseguradora',
			title: 'Aseguradora',
			sortable: true,
			visible: false
		}, {
			field: 'senasPartic',
			title: 'Señas Particulares',
			sortable: true,
			visible: false
		}, {
			field: 'operate',
			title: 'Acciones',
			align: 'center',
			events: operateEvents,
			formatter: operateFormatter
		}],
	});
</script>
</div>
