<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
	<title>Test</title>
</head>
<body>
	<h3>Test</h3>
	{!! Form::open([ 'route' => 'savePredenuncia.vehiculo', 'method' => 'POST']) !!}
		
		{!! Form::label('nombre', 'Nombre', ['for' => 'nombre'] ) !!}
		{!! Form::text('nombre', null , ['class' => 'form-control', 'id' => 'nombre', 'placeholder' => 'vehiculo' ]  ) !!}


		{!! Form::label('color', 'Color', ['for' => 'color'] ) !!}
		{!! Form::text('color', null , ['class' => 'form-control', 'id' => 'color', 'placeholder' => 'color' ]  ) !!}
		


		<button type="submit" class="btn btn-success btn-block">Guardar</button>
	{!! Form::close() !!}

	<button id="btnAjax">Enviar por ajax</button>


	<label for="latitude">Latitude: </label> <div id="latitude"></div><br />
        <label for="longitude">Longitude: </label> <div id="longitude"> </div><br />
        <div id="status"> </div><br />
        <input type="button" onclick="requestPosition()" value="Get Latitude and Longitude"  /> 
	<script type="text/javascript">

		$(document).ready(function(){
			


			console.log('Jquery cargado');
			$('#btnAjax').bind('click', function (argument) {
				console.log('Enviando peticion');

				$.ajax({
					url : 'http://localhost/VRR/public/guardar-predenuncia',
					data : { 
						placas: $('#placa').val(),
						idEstado: 1,
						idSubmarca: 1,
						modelo: 1,
						nrpv: "A",
						idColor: 1,
						permiso: "P",
						numSerie: "ABC123",
						numMotor: "ABC123",
						idTipoVehiculo: 1,
						idTipoUso: 1,
						senasPartic: "ABC",
						idProcedencia: 1,
						idAseguradora: 1
					},
					type : 'POST',
					dataType : 'json',
				 
					success : function(json) {
						console.log('Se agrego un vehiculo');
					},

					error : function(xhr, status) {
						console.log('Disculpe, existió un problema');
						console.log(xhr);
					},

					complete : function(xhr, status) {
						console.log('Petición realizada');
					}
				});
			});




			var nav = null; 

		    function requestPosition() {
		        if (nav == null) {
		            nav = window.navigator;
		        }

		        var geoloc = nav.geolocation;
		        if (geoloc != null) {
		            geoloc.getCurrentPosition(successCallback, errorCallback);
		        }

		    }

		    function successCallback(position) {
		        document.getElementById("latitude").innerHTML = 
		            position.coords.latitude;
		        document.getElementById("longitude").innerHTML = 
		            position.coords.longitude;
		      
		    }

		    function errorCallback(error) {
		        var strMessage = "";

		        // Check for known errors
		        switch (error.code) {
		            case error.PERMISSION_DENIED:
		                strMessage = "Access to your location is turned off. "  +
		                    "Change your settings to turn it back on.";
		                break;
		            case error.POSITION_UNAVAILABLE:
		                strMessage = "Data from location services is " + 
		                    "currently unavailable.";
		                break;
		            case error.TIMEOUT:
		                strMessage = "Location could not be determined " +
		                    "within a specified timeout period.";
		                break;
		            default:
		                break;
		        }

		        document.getElementById("status").innerHTML = strMessage;
		    }



		});
		
	</script>

</body>
</html>