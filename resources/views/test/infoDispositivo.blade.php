<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
	<title>Test</title>
</head>
<body>
	<h3>Test</h3>
    
    <input type="button" id='getPosition' value="Obtener datos del dispositivo"  />
    <br>
	<div id="status"> </div>
	<script src="{{ asset('js/prevehiculo/peticiones.js') }}"></script>
	<script type="text/javascript">

		$(document).ready(function(){

			function get_position () {

				var nav = null;
				var position = null; 

				function requestPosition() {
					if (nav == null) {
						nav = window.navigator;
					}

					var geoloc = nav.geolocation;
					if (geoloc != null) {
						position = geoloc.getCurrentPosition(successCallback, errorCallback);
					}
				}

				function successCallback(pos) {
					position  = { latitud: pos.coords.latitude, longitud: pos.coords.longitude };
					console.log(position);
				}

				function errorCallback(error) {
					// Check for known errors
					switch (error.code) {
						case error.PERMISSION_DENIED:
							strMessage = "Access to your location is turned off. "  +
								"Change your settings to turn it back on.";
							break;
						case error.POSITION_UNAVAILABLE:
							strMessage = "Data from location services is " + 
								"currently unavailable.";
							break;
						case error.TIMEOUT:
							strMessage = "Location could not be determined " +
								"within a specified timeout period.";
							break;
						default:
							break;
					}
				}

				requestPosition();

			}

			

		    requestPosition();

		    $('#getPosition').bind('click', function () {
		    	console.log('click');
		    	datos = position;
		    	$('#status').empty();
		    	var ruta = "{{ route('info-dispositivo') }}";
				rsp = peticionInfomacion('post',ruta,datos);
				console.log(rsp);
				//var texto = JSON.parse(respuesta);

				$('#status').text('Navegador: '+rsp.navegador);
				$('#status').append('<br><br>Direccion ip: '+rsp.ip);
				$('#status').append('<br><br>Nombre del equipo: '+rsp.equipo);
				$('#status').append('<br><br>Sistema operativo: '+rsp.so);
				$('#status').append('<br><br>Ubicacion latitud: '+rsp.latitud);
				$('#status').append('<br><br>Ubicacion longitud: '+rsp.longitud);
		    });




		});
		
	</script>
</body>
</html>