<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	{!! Form::open(['route' => 'savePersona', 'method' => 'POST'])  !!}
		{!!Form::label('nombre','Nombre: ')!!}
		<br><br>
		{!!Form::text('nombre')!!}
		<br><br>
		{!!Form::label('rfc','RFC: ')!!}
		<br><br>
		{!!Form::text('rfc')!!}
		<br><br>
		{!!Form::label('telefono','Teléfono: ')!!}
		<br><br>
		{!!Form::text('telefono')!!}
		<br><br>
		{!!Form::label('tipoPersona','Tipo de Persona: ')!!}
		<br><br>
		{!!Form::select('tipoPersona', ['1' => 'Persona Fisica', '2' => 'Persona Moral'], '1')!!}
		<br><br>
		{!! Form::submit('Guardar') !!}
	{!! Form::close() !!}
</body>
</html>