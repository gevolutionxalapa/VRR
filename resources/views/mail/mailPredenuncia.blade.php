<link rel="stylesheet" type="text/css" href="{{ asset('plugins/boostrap/css/bootstrap.min.css') }}">


<div class="container">
  <div class="row">
    <div class="col-md-12 text-center">
      <img  id="logo" src="https://rawcdn.githack.com/Romaincks/assets/master/img/logo-fge-svg.svg" style="    -webkit-filter: brightness(0.5)contrast(3.4)hue-rotate(217deg)saturate(9.9);filter: brightness(0.5)contrast(3.4)hue-rotate(217deg)saturate(9.9);width: 50%; display: table; margin: 0 auto;">
    </div>
    <div class="" style="padding-top: 20px; background-color: gray; height: 5px;">

    </div>
    <div class="col-md-4" >
      <table class="table">
        <tbody>
          <tr>
            <th scope="row">Marca</th>
            <td>Alfa Romeo</td>
          </tr>
          <tr>
            <th scope="row">Modelo</th>
            <td>Ejemplo</td>
          </tr>
          <tr>
            <th scope="row">Fecha</th>
            <td>28/03/2018</td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="col-md-4 text-center">
      <h7>C&oacute;digo de preregistro</h7>
      <h6>{{ $token }}</h6>
    </div>
    <div class="col-md-4">

      <img src="data:image/png;base64,  {{ $codigoQR }}">

    </div>
  </div>
</div>
