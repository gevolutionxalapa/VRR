<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>PDF</title>
  <link rel="icon" href="{{ asset('img/iconofge.png') }}">
  <link rel="stylesheet" href={{ asset('fonts/neosans/css/font-neosans.css')}}>
  <style>
  /* font css */
  html,body,p,div{height: 100%; font-size: 14px;   margin: 0;}
  body, p{
    font-family: 'neosanspro-regular';
    font-weight: normal;
    font-style: normal;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
  h1, h2, h3, h4, h5, h6 div {
    font-family: 'neosanspro-bold';
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
  h1, .h1 {
    font-size: 2.5rem;
  }
  h2, .h2 {
    font-size: 2rem;
  }
  h3, .h3 {
    font-size: 1.75rem;
  }
  h4, .h4 {
    font-size: 1.5rem;
  }
  h5, .h5 {
    font-size: 1.25rem;
  }
  h6, .h6 {
    font-size: 1rem;
  }
  /* section */
  section{
    outline: 1px;
    border-top: 1px solid #000;
    border-left: 1px solid #000;
    border-bottom: 1px solid #000;
    border-right: 1px solid #000;
  }
  /* table css */
  *, ::after, ::before {
    box-sizing: border-box;
  }
  table {
    border-collapse: collapse;
  }
  table {
    display: table;
    border-collapse: separate;
    border-spacing: 0px;
    border-color: grey;
  }
  .table {
    width: 100%;
    max-width: 100%;
    margin-bottom: 1rem;
    background-color: transparent;
  }
  thead {
    display: table-header-group;
    vertical-align: middle;
    border-color: inherit;
  }
  tr {
    display: table-row;
    vertical-align: inherit;
    border-color: inherit;
  }
  .table thead th {
    vertical-align: middle;
    border-bottom: 0px;
  }
  .table-sm td, .table-sm th {
    padding: .5rem;
  }
  .thfeg-report, .thfeg-report>td, .thfeg-report>th {
    background-color: #424242;
    color: white;
  }
  .ths{
    font-weight:bold;
    background-color: #bdbdbd;
  }
  .thm{
    font-family: 'neosanspro-italic';
  }      
  .thm td {
    border-top: 2px solid #bdbdbd;
  }
  .tbt{
    border-top: 2px solid #bdbdbd;
  }
  .text-center{
    text-align: center;
  }
  .pull-lftmes {
    padding-left: 30px;
    float: left !important;
  }
  .pull-rghtmes {
    padding-right: 30px;
    float: right !important;
  }
  .text-sm-left {
    text-align: left!important;
  }
  .text-sm-center {
    text-align: center!important;
  }
  .text-muted {
    color: #8d8d8d !important;
  }
</style>
</head>


<body>

  <section>
    <table class="table table-sm table-striped " style="width: 100%">
      <thead class="thfeg-report">
        <tr>
          <th scope="col" style="background-color: #424242; color: white;">
            <img src={{ asset('img/fge-logo-blanco-png.png')}} alt="Fiscalía General" style="height:150px">
          </th>
          <th colspan="5" class="text-sm-left" style="background-color: #424242; color: white;">
            <p class="h4" style="font-size: 22px;">Preregistro </p>
            <br>
            <p>El presente documento contiene un folio que deberá presentar ante el fiscal correspondiente para dar seguimiento a su denuncia.</p>
          </th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="col" class="text-sm-left ths">Descripción</th>
          <th scope="col" class="text-sm-left ths">Folio de preregistro</th>
          <th scope="col" colspan="4" class="text-sm-left ths">C&oacute;digo QR</th>
        </tr>
        <tr>
          <td><strong>Marca:</strong>{{$vehiculo[0]->marca}}<br><strong>Modelo:</strong>{{$vehiculo[0]->submarca}}<br><strong>Fecha:</strong> 04 de junio de 2018</td>
          <td> {{$Folio}}</td>
          <td><img style="width:150px;" src="data:image/png;base64,{{$QRCode}}"></td>
        </tr>
        <tr  class="thm">
          <td colspan="6" align="center" ><br><p style="font-size: 18px;">¿A dónde acudir?</p><br><br></td>
        </tr>
        <tr class="text-muted">
        
            @foreach ($unidades as $unidad)
            <td style="font-size: 14px; color:gray; padding-bottom: 5px">{{ $unidad->nombre }}, dirección : {{$unidad->calle}}, col {{$unidad->colonia}}, num {{$unidad->numero}}, cp {{$unidad->CodPos}}</td>
            @endforeach
      </tr>
      <tr class="thm">
        <td  colspan="6" style="padding-top: 5px"><small class="text-muted">Aquí colocar leyenda en caso de requerirlo: El presente documento no tiene valor fiscal o leyenda de donde localizar el aviso de privacidad.</small></td>
      </tr>
    </tbody>
  </table>
</section>

</body>

</html>
{{-- <style>
    html, body {
      height: 100%;
      margin: 0;
    }
    body, p{
      font-family: 'neosanspro-regular';
      font-weight: normal;
      font-style: normal;
    -webkit-font-smoothing: antialiased;
      -moz-osx-font-smoothing: grayscale;
    }
    h1, h2, h3, h4, h5, div {
    font-family: 'neosanspro-regular';
  -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}
h6 {
  font-family: 'neosanspro-bold';
  -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}


  .line_div{
    float: left;
    /* width: 150px; */
    height: 75px;
    margin: 5px;
    /* width: 19em; */
    /* border: 3px solid #73AD21; */
  }

</style>
 --}}