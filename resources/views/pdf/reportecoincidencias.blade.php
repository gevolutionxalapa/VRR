<!DOCTYPE html>
<html>
<head>

    <meta charset="UTF-8" format="A4">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PDF</title>
    <link rel="icon" href="{{ asset('img/iconofge.png') }}">
    <link rel="stylesheet" href={{ asset('fonts/neosans/css/font-neosans.css')}}>


    <style>
      /* font css */
      html,body,p,div{font-size: 14px;}
      body, p{
        font-family: 'neosanspro-regular';
        font-weight: normal;
        font-style: normal;
      -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        }
      h1, h2, h3, h4, h5, h6 div {
          font-family: 'neosanspro-bold';
          -webkit-font-smoothing: antialiased;
          -moz-osx-font-smoothing: grayscale;
          }
        h1, .h1 {
          font-size: 2.5rem;
          }
        h2, .h2 {
          font-size: 2rem;
          }
        h3, .h3 {
          font-size: 1.75rem;
          }
        h4, .h4 {
          font-size: 1.5rem;
          }
        h5, .h5 {
          font-size: 1.25rem;
          }
        h6, .h6 {
          font-size: 1rem;
          }
     /* section */
         section{
            outline: 1px;
            border-top: 1px solid #000;
            border-left: 1px solid #000;
            border-bottom: 1px solid #000;
            border-right: 1px solid #000;
        }
     /* table css */
        *, ::after, ::before {
        box-sizing: border-box;
        }
        .table {
            width: 100%;
            max-width: 100%;
            margin-bottom: 1rem;
            background-color: transparent;
        }
        table {
            border-collapse: collapse;
        }
        table {
            display: table;
            border-collapse: separate;
            border-spacing: 1px;
            border-color: grey;
        }
        thead {
            display: table-header-group;
            vertical-align: middle;
            border-color: inherit;
        }
        tr {
        display: table-row;
        vertical-align: inherit;
        border-color: inherit;
        }
        .table thead th {
        /* vertical-align: middle; */
        border-bottom: 0px;
        }
        .table-sm td, .table-sm th {
        padding: .3rem;
        }
        .thfeg-report, .thfeg-report>td, .thfeg-report>th {
        background-color: #424242;
        color: white;
        }
        .table td, .table th {
        vertical-align: top;
        border-bottom: 1px solid #bdbdbd;
        }
        .ths{
          font-family: 'neosanspro-bold';
          background-color: #bdbdbd;
        }
        .thm{
          font-family: 'neosanspro-italic';
        }
          .thm td {
            border-top: 4px solid #bdbdbd;
            }
        .tbt{
          border-top: 2px solid #bdbdbd;
        }
        .text-center{
          text-align: center;
        }
      .pull-lftmes {
        padding-left: 30px;
        float: left !important;
      }
      .pull-rghtmes {
        padding-right: 30px;
        float: right !important;
      }
      strong{
        font-family: 'neosanspro-bold';
      }
        </style>

</head>

<body>


   <table width=100%>
     <thead>
       <tr>
         <th scope="col" style="background-color: #424242; color: white;">
           <img  id="logo" src={{ asset('img/fge-logo-blanco-png.png')}} style="float: left; margin: 0px 15px 15px 0px; width: 12%;">
         </th>
         <th colspan="4" class="text-sm-left" style="background-color: #424242; color: white;">
           <br>
            <p>FISCALÍA GENERAL DEL ESTADO DE VERACRUZ DE IGNACIO DE LA LLAVE.<br>
           DIRECCIÓN DEL CENTRO DE INFORMACIÓN E INFRAESTRUCTURA TECNOLÓGICA</p><br>
           <p >(COINCIDENCIAS CON VEHÍCULOS INVOLUCRADOS EN ALGUNA CARPETA DE INVESTIGACIÓN Y/O CON REPORTE DE ROBO)</p></div>
         </th>
       </tr>
     </thead>
   </table>
    <table align="center" width=100%>
     <thead>
      <tr>
      <th scope="col" style="background-color: #bdbdbd;"  colspan="6"><font color="black">COINCIDENCIAS DE VEHÍCULOS ROBADOS</font></th>
      </tr>
    </thead>
    <tbody align="center">
    <tr align="center">
      <th>No. DE CARPETA</th>
      <th>PLACA</th>
      <th>NÚMERO DE MOTOR</th>
      <th>NÚMERO DE SERIE</th>
      <th>NRPV</th>
      <th>FISCAL</th>
    </tr>
@for ($i = 0; $i < count($robados['registros']); $i++)
    <tr >
      <th><p>{{$robados['registros'][$i]->numCarpeta}}</p></th>
      <th><p>{{$robados['registros'][$i]->placas}}</p></th>
      <th><p>{{$robados['registros'][$i]->numMotor}}</p></th>
      <th><p>{{$robados['registros'][$i]->numSerie}}</p></th>
      <th><p>{{$robados['registros'][$i]->nrpv }}</p></th>
      <th><p>{{$robados['nomFiscal'][$i]->nomFiscal }}</p></th>
    </tr>
  @endfor
  </tbody>
</table >

   <table align="center" width=100%>
     <thead>
      <tr>
        <th scope="col" style="background-color: #bdbdbd;"  colspan="6"><font color="black">COINCIDENCIAS DE VEHÍCULOS INVOLUCRADOS</font></th>
      </tr>
    </thead>
    <tbody>
    <tr>
      <th >No. DE CARPETA</th>
      <th >PLACA</th>
      <th >NÚMERO DE MOTOR</th>
      <th >NÚMERO DE SERIE</th>
      <th >NRPV</th>
      <th >FISCAL</th>
    </tr>
    @foreach ($involucrados as $involucrados)
    <tr>
      <th><p> {{$involucrados->numCarpeta}} </p></th>
      <th><p>{{$involucrados->placas}}</p></th>
      <th><p>{{$involucrados->numMotor}}</p></th>
      <th><p>{{$involucrados->numSerie}}</p></th>
      <th><p>{{$involucrados->nrpv}}</p></th>
      <th><p>{{$involucrados->nomFiscal}}</p></th>
    </tr>
    @endforeach
  </tbody>
</table>

<table  align="center" width=100%>
     <thead>
      <tr>
        <th scope="col" style="background-color: #bdbdbd;"  colspan="6"><font color="black">COINCIDENCIAS DE VEHÍCULOS RECUPERADOS</font></th>
      </tr>
    </thead>
    <tbody>
    <tr>
      <th>No. DE CARPETA</th>
      <th>PLACA</th>
      <th>NÚMERO DE MOTOR</th>
      <th >NÚMERO DE SERIE</th>
      <th >NRPV</th>
      <th >FISCAL</th>
    </tr>
@for ($i = 0; $i < count($recuperados['registros']); $i++)
    <tr>
      <th >
        <p>{{$recuperados['registros'][$i]->numCarpeta}}</p>
      </th>
      <th><p>{{$recuperados['registros'][$i]->placas}}</p></th>
      <th><p>{{$recuperados['registros'][$i]->numMotor}}</p></th>
      <th><p>{{$recuperados['registros'][$i]->numSerie}}</p></th>
      <th><p>{{$recuperados['registros'][$i]->nrpv }}</p></th>
      <th><p>{{$recuperados['nomFiscal'][$i]->nomFiscal }}</p></th>
    </tr>
  @endfor

   {{--  @foreach ($recuperados['registros'] as $recuperados1)
    <tr>
      <th><p> {{$recuperados1->numCarpeta}} </p></th>
      <th><p>{{$recuperados1->placas}}</p></th>
      <th><p>{{$recuperados1->numMotor}}</p></th>
      <th><p>{{$recuperados1->numSerie}}</p></th>
      <th><p>{{$recuperados1->nrpv}}</p></th>
      @foreach ($recuperados['nomFiscal'] as $recuperados2)@endforeach
      <th><p>{{$recuperados1['nomFiscal']->nomFiscal}}</p></th>
    </tr>
    @endforeach --}}

  </tbody>
</table>







</body>
</html>
