<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8" format="A4">
    <title>PDF</title>
</head>


<body>

<div class="sectio">
    <p align="center"><strong>FISCALÍA GENERAL DEL ESTADO</strong><br>
    <strong>Dirección del Centro de Información</strong><br>
    <strong>Reporte de vehículos</strong><br>
    (Registro Nacional de Vehículos Robados y Recuperados)</p>
    <table align="center">
      <tbody>
        <tr>
          <td style="background: #efefef;">LUGAR Y FECHA</td>
          <td> <p>{{ $datosUIPJ->fecha_averigua }}</p> </td>
          <td style="background: #efefef;">NO.FOLIO</td>
          <td>  </td>
        </tr>
      </tbody>
    </table>
    <br>
    <table  align="center">
     <thead>
      <tr>
      <th scope="col" style=""  colspan="4"><font color="#FFFFFF">DENUNCIA DE ROBO DE VEHÍCULO </th>
      </tr>
    </thead>
    </tbody>
    <tr>
      <th scope="col" style=""  colspan="4" >DATOS DE LA INVESTIGACIÓN MINISTERIAL</th>
    </tr>
    <tr>
      <td text-align="center" colspan="2" style="background: #efefef;">NÚMERO INVESTIGACIÓN MINISTERIAL</td>
      <td text-align="center" style="background: #efefef;">FECHA DE INICIO</td>
      <td text-align="center" style="background: #efefef;">HORA DE INICIO</td>
    </tr>
    <tr>
      <td colspan="2"><p>{{ $datosUIPJ->averiguacion}}</p></td>
      <td><p>{{ $datosUIPJ->fecha_averigua}}</p></td>
      <td><p>{{ $datosUIPJ->hora_averigua}}</p></td>
    </tr>
    <tr>
      <td style="background: #efefef;">AGENCIA DEL MINISTERIO PÚBLICO</td>
      <td colspan="3"><p>{{ $datosUIPJ->agencia_mp}}</p></td>
    </tr>
    <tr>
      <td style="background: #efefef;"></td>
      <td colspan="3" style="padding:.6em 0em"></td>
    </tr>

    <tr>
      <td style="background: #efefef;">NOMBRE DEL FISCAL</td>
      <td colspan="3"><p>{{ $datosUIPJ->agente_mp}}</p></td>
    </tr>

  </tbody>
</table>

    <table align="center">
      <tbody>
        <tr>
        <th text-align="center">DATOS DEL ROBO </th>
      </tr>
    </tbody>
  </table>
  <table>
    <tbody>
      <tr>
        <td text-align="center" colspan="2" style="background: #efefef;">FECHA DE ROBO: </td>
        <td text-align="center" colspan="2" style="background: #efefef;">HORA DEL ROBO:</td>
      </tr>
      <tr>
        <td colspan="2"><p>{{ $datosUIPJ->fecha_robo}}</p></td>
        <td colspan="2"><p>{{ $datosUIPJ->hora_robo}}</p></td>
      </tr>
      <tr>
        <td text-align="center" colspan="4" style="background: #efefef;">CALLE Y NÚMERO </td>
      </tr>
      <tr>
        <td text-align="center" colspan="4"><p>{{ $datosUIPJ->calle_robo}}  {{ $datosUIPJ->num_ext_robo}}  {{ $datosUIPJ->num_int_robo}}</p> </td>
      </tr>
      <tr>
        <td text-align="center" colspan="2" style="background: #efefef;">COLONIA </td>
        <td text-align="center" colspan="2" style="background: #efefef;">MUNICIPIO Y/O DELEGACIÓN</td>
      </tr>
      <tr>
        <td text-align="center" colspan="2">{{ $datosUIPJ->colonia_robo}}</td>
        <td text-align="center" colspan="2"> {{ $datosUIPJ->id_municipio_robo}}</td>
      </tr>
      <tr>
        <td colspan="2" style="background: #efefef;">ENTIDAD FEDERATIVA</td>
        <td colspan="2" style="background: #efefef;">CÓDIGO POSTAL</td>
      </tr>
      <tr>
        <td text-align="center" colspan="2">{{ $datosUIPJ->id_entidad_robo}} </td>
        <td text-align="center" colspan="2">{{ $datosUIPJ->cp_robo}}</td>
      </tr>
    </tbody>
    </table>

    <table align="center">
      <tbody>
        <tr>
          <th text-align="center">DATOS GENERALES DEL DENUNCIANTE </th>
      </tr>
    </tbody>
    </table>

      <table>
        <tbody>
          <tr>
            <td  colspan="2" style="background: #efefef;">TIPO DE PERSONA </td>
            <td colspan="2">{{ $datosUIPJ->tipo}}</td>

            {{--  <td align="center"> FÍSICA | <strong>{{$datosUIPJ->tipo==} {{( $datosUIPJ->denunciante[0]->esEmpresa == 0)? 'X': '' }} </strong> </td>  --}}
            {{--  <td align="center"> MORAL | <strong> {{($datosUIPJ->tipo == 0)? '': 'X' }}</strong> </td>  --}}
          </tr>
          <tr>
            <td colspan="2" style="background: #efefef;">NOMBRE(S)</td>
            <td style="background: #efefef;">APELLIDO PATERNO</td>
            <td style="background: #efefef;">APELLIDO MATERNO</td>
          </tr>
          <tr>
            <td colspan="2">
              {{ $datosUIPJ->nombre_den}}
            </td>
            <td>{{ $datosUIPJ->paterno_den}}
            </td>
            <td>{{ $datosUIPJ->materno_den}}
            </td>
          </tr>
          <tr>
            <td style="background: #efefef;">RFC</td>
            <td style="background: #efefef;">CURP</td>
            <td colspan="2" style="background: #efefef;" text-align="center">SEXO</td>
          </tr>
          <tr>
            <td>
                  <p>{{ $datosUIPJ->rfc_den}}</p>
            </td>
            <td>
                  <p>{{ $datosUIPJ->curp_den}}</p>
            </td>
            <td>FEMENINO |<strong> {{( $datosUIPJ->sexo_den == 2)? 'X': '' }}</strong></td>
            <td>MASCULINO |<strong> {{( $datosUIPJ->sexo_den == 1)? 'X': '' }}</strong></td>
          </tr>
          <tr>
            <td text-align="center" colspan="4" style="background: #efefef;">CALLE Y NÚMERO </td>
          </tr>
          <tr>
            <td text-align="center" colspan="4">
                  <p>{{ $datosUIPJ->calle_den.' No. '.$datosUIPJ->numext_dom_den.' Int. '.$datosUIPJ->numint_dom_den}}</p>
            </td>
          </tr>
          <tr>
            <td text-align="center" colspan="2" style="background: #efefef;">COLONIA </td>
            <td text-align="center" colspan="2" style="background: #efefef;">MUNICIPIO Y/O DELEGACIÓN</td>
          </tr>
          <tr>
            <td text-align="center" colspan="2">
              <p>{{ $datosUIPJ->colonia_den}}</p>
            </td>
            <td text-align="center" colspan="2">
              <p>{{ $datosUIPJ->id_municipio_den}}</p>
            </td>
          </tr>
          <tr>
            <td style="background: #efefef;">ENTIDAD FEDERATIVA</td>
            <td style="background: #efefef;">CÓDIGO POSTAL</td>
            <td style="background: #efefef;">TELÉFONO</td>
            <td style="background: #efefef;">CORREO ELECTRÓNICO</td>
          </tr>
          <tr>
            <td text-align="center">
                  <p>{{ $datosUIPJ->id_entidad_den}}</p>
            </td>
            <td text-align="center">
                  <p>{{ $datosUIPJ->cp_den}}</p>
            </td>
            <td text-align="center">
                  <p>{{ $datosUIPJ->telefono_den}}</p>
            </td>
            <td text-align="center">
                  <p></p>
            </td>
          </tr>
        </tbody>
        </table>



      <table align="center">
        <tbody>
          <tr>
            <th text-align="center">MODUS OPERANDI (SOLO EN CASO DE VIOLENCIA) </th>
        </tr>
      </tbody>
      </table>
      <table>
        <tbody>
          <tr>
            <td colspan="2" style="background: #efefef;">MODALIDAD DE ROBO </td>
            <td style="background: #efefef;">NO.VÍCTIMAS DURANTE EL ROBO</td>
            <td style="background: #efefef;">NO.DE PRESUNTOS RESPONSABLES</td>
          </tr>
          <tr>
            <td >SIN VIOLENCIA |</td>
            <td >CON VIOLENCIA |-</td>
            <td> <p>{{$datosUIPJ->victimas}}</p> </td>
            <td><p>{{$datosUIPJ->responsables}}</p></td>
          </tr>
          <tr>
            <td colspan="2" style="background: #efefef;">TIPO DE LUGAR DONDE SE EFECTUÓ EL ROBO</td>
            <td colspan="2" style="background: #efefef;">EL DELINCUENTE SE HIZO PASAR POR</td>
          </tr>
          <tr>
            <td colspan="2"><p>{{$datosUIPJ->referencia_robo}}</p></td>
            <td colspan="2"><p>-</p></td>
          </tr>
          <tr>
            <td colspan="2" style="background: #efefef;">PECULIARIDADES</td>
            <td colspan="2" style="background: #efefef;">SEÑAS PARTICULARES</td>
          </tr>
          <tr>
            <td colspan="2">{{$datosUIPJ->peculiaridades}}</td>
            <td colspan="2"><p>{{$datosUIPJ->vestimenta}}</p></td>
          </tr>
          <tr>
            <td colspan="2" style="background: #efefef;">VESTIMENTA</td>
            <td colspan="2" style="background: #efefef;">COMPORTAMIENTO</td>
          </tr>
          <tr>
            <td colspan="2"><p>{{$datosUIPJ->vestimenta}}</p></td>
            <td colspan="2">{{$datosUIPJ->comportamiento}}</td>
          </tr>
        </tbody>
      </table>
      <table>
        <tbody>
          <tr>
            <td text-align="center" rowspan="2" style="background: #efefef;">ARMAS UTILIZADAS </td>
            <td style="padding: ">{{$datosUIPJ->id_arma_asoc}}</td>
            <td ></td>
            <td ></td>
            <td ></td>
          </tr>
          <tr>
            <td>-</td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td text-align="center" rowspan="2" style="background: #efefef;">DELITOS ASOCIADOS</td>
            <td>-</td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>-</td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td text-align="center" colspan="5" style="background: #efefef;">VEHÍCULOS ASOCIADOS AL ROBO </td>
          </tr>
          <tr>
            <td text-align="center" style="background: #efefef;">MARCA</td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td style="background: #efefef;">SUBMARCA</td>

            <td ></td>
            <td ></td>
          </tr>
          <tr>
            <td text-align="center" style="background: #efefef;">RCA</td>
            <td>-</td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td text-align="center" style="background: #efefef;">SUBMARCA</td>
            <td>-</td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
        </tbody>
        </table>
        <table align="center">
          <tbody>
            <tr>
              <th text-align="center">DATOS DEL VEHÍCULO </th>
          </tr>
        </tbody>
        </table>
        <table>
          <tbody>

            <tr>
              <td text-align="center" style="background: #efefef;">PLACAS </td>
              <td text-align="center" style="background: #efefef;">ENTIDAD FEDERATIVA</td>
              <td style="background: #efefef;">MARCA</td>
              <td text-align="center" style="background: #efefef;">SUBMARCA</td>
            </tr>
            <tr>
              <td>{{ $datosVehiculo->placas }}</td>
              <td>{{ $datosVehiculo->estado }}</td>
              <td>{{ $datosVehiculo->marca }}</td>
              <td>{{ $datosVehiculo->submarca }}</td>
            </tr>
            <tr>
              <td style="background: #efefef;">MODELO</td>
              <td style="background: #efefef;">COLOR</td>
              <td style="background: #efefef;">PERMISO</td>
              <td style="background: #efefef;">SERIE/VIN</TD>
            </tr>
            <tr>
              <td>{{ $datosVehiculo->modelo }}</td>
              <td>{{ $datosVehiculo->color }}</td>
              <td>{{ $datosVehiculo->permiso }}</td>
              <td>{{ $datosVehiculo->numSerie }}</td>
            </tr>
            <tr>
              <td style="background: #efefef;">MOTOR</td>
              <td colspan="2" style="background: #efefef;">TIPO DE VEHICULO</td>
              <td style="background: #efefef;">TIPO DE USO</td>
            </tr>
            <tr>
              <td>{{ $datosVehiculo->numMotor }}</td>
              <td colspan="2">{{ $datosVehiculo->tipoVehiculo }}</td>
              <td>{{ $datosVehiculo->tipoUSo }}</td>
            </tr>
            <tr>
              <td colspan="4" style="background: #efefef;">SEÑAS PARTICULARES </td>
            </tr>
            <tr>
              <td colspan="4" >{{ $datosVehiculo->senasPartic }}</td>
            </tr>

          </tbody>
        </table>
        <br>
        <table >
          <tbody>
            <tr>
              <td  style="border: hidden" align="center" >
                <p>ELABORÓ</p>
                <br><br><br><br>
                <p>_____________________________________</p>
              </td>
              <!-- <td style="border: hidden"></td> -->
              <td style="border: hidden" align="center" >
                <p>CONFORMIDAD</p>
                <br><br><br><br>
                <p>_____________________________________</p>
                <br>
                <p style="font-size:10px">FISCAL VIGÉSIMO QUINTO DE LA UNIDAD INTEGRAL DE PROCURACIÓN DE JUSTICIA</p>
              </td>
              <td  style="border: hidden" align="center">
                <p stye>SELLO DE RECIBIDO (RESERVADO ENLACE)</p>
                <table>
                  <tbody>
                    <tr>
                      <td style="padding:3em 0em"></td>
                    </tr>
                  </tbody>
                </table>
                <p style="font-size:12px">Fecha:_________</p>
                <p style="font-size:12px">Hora:_________</p>
                <p style="font-size:12px">Firma:_________</p>

              </td>
            </tr>

          </tbody>
        </table>
        <br>
          <table>
            <tbody>
              <tr>
                <th >FECHA CAPTURA</th>
                <td ></td>
                <th >HORA DE CAPTURA</th>
                <td ></td>
                <th >NOMBRE DE QUIEN CAPTURA</th>
                <td ></td>
              </tr>
            </tbody>
          </table>

  </div>


  <style>
  table{
    width: 100%;
    border-collapse: collapse;
  }
  td {
    width: 25%;
    text-align: left;
    vertical-align: top;
    border: 0.5px solid #000;
    border-collapse: collapse;

  }
  thead th{
    width: 25%;
    text-align: center;
    vertical-align: top;
    border: 0.5px solid #000;
    border-collapse: collapse;
    background: #1d1d1d;
    font-color:#FFFFFF;
  }
  body th{
    width: 25%;
    text-align: center;
    vertical-align: top;
    border: 0.5px solid #000;
    border-collapse: collapse;
    background: #8d8d8d;
  }

  .linea{
    border-bottom: 1px dotted #000;
  }
  html, body {
    height: 100%;
    margin: 0;
  }
  body, p{
    font-family: 'neosanspro-regular';
    font-weight: normal;
    font-style: normal;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
  h1, h2, h3, h4, h5, div {
    font-family: 'neosanspro-regular';
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
  h6 {
    font-family: 'neosanspro-bold';
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }


  .line_div{
    float: left;
    /* width: 150px; */
    height: 75px;
    margin: 5px;
    /* width: 19em; */
    /* border: 3px solid #73AD21; */
  }

</style>



</body>
</html>
