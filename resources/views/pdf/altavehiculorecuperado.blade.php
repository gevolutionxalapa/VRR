<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" format="A4">
	<title>PDF</title>
</head>
<body>
<div class="sectio">
    <p align="center"><strong>PROCURADURÍA GENERAL DEL ESTADO</strong><br>
    <strong>SUBPROCURADURÍA REGIONAL DE JUSTICIA SEDE:___________________</strong><br>
    <strong>Reporte de vehículos</strong><br>
    (Registro Nacional de Vehículos Robados y Recuperados)</p>
    <table align="center">
			<tbody>
				<tr>
					<td style="background: #efefef;">LUGAR Y FECHA</td>
					<td> <p>{{ $datosUIPJ->fecha_averigua }}</p> </td>
					<td style="background: #efefef;">NO.FOLIO</td>
					<td>  </td>
				</tr>
			</tbody>
  	</table>
	<br>
	<table  align="center">
		<thead>
		 <tr>
		 <th scope="col" style=""  colspan="4"><font color="#FFFFFF">RECUPERACIÓN DE VEHÍCULOS ROBADOS </th>
		 </tr>
	 </thead>
			<tbody>
			<tr>
				<td text-align="center" style="background: #efefef;">VEHÍCULO</td>
				<td text-align="right">RECUPERADO | X</strong></td>
  			<td text-align="right">ASEGURADO |  </strong></td>
				<td text-align="right">(TACHAR EL QUE CORRESPONDA)</td>
			</tr>
  		</tbody>
	</table>

    <table  align="center">
    		<tr>
    			<th scope="col"  colspan="4">DATOS REFERENCIALES DE LA DENUNCIA DE ROBO (SI APLICA)</th>
    		</tr>
    	<tbody>
			<tr>
				<td text-align="center" style="background: #efefef;">NÚMERO INVESTIGACIÓN MINISTERIAL</td>
				<td text-align="center" style="background: #efefef;">NO. DE FOLIO DE DENUNCIA</td>
				<td text-align="center" style="background: #efefef;">PLACA</td>
				<td text-align="center" style="background: #efefef;">SERIE</td>
			</tr>
			<tr>
				<td><p>{{ $datosUIPJ->averiguacion}}</p></td>
				<td><p> </p></td>
				<td><p>{{ $datosVehiculo->placas}}</p></td>
				<td><p>{{ $datosVehiculo->numSerie}}</p></td>
			</tr>
  		</tbody>
	</table>
	<table  align="center">
    		<tr>
    			<th scope="col" colspan="3">INFORMACIÓN DE LA RECUPERACIÓN</th>
    		</tr>
    	<tbody>
			<tr>
				<td text-align="center" style="background: #efefef;">NÚMERO INVESTIGACIÓN MINISTERIAL</td>
				<td text-align="center" style="background: #efefef;">FECHA DE INICIO</td>
				<td text-align="center" style="background: #efefef;">HORA DE INICIO</td>
			</tr>
			<tr>
				<td><p>{{ $datosUIPJ->averiguacion}}</p></td>
				<td><p>{{ $datosUIPJ->fecha_averigua}}</p></td>
				<td><p>{{ $datosUIPJ->hora_averigua}}</p></td>
			</tr>
			<tr>
				<td text-align="center" style="background: #efefef;">AGENCIA DEL MINISTERIO PÚBLICO</td>
				<td colspan="2" text-align="center">{{ $datosUIPJ->agencia_mp}}</td>
			</tr>
			<tr>
				<td text-align="center" style="background: #efefef;">NOMBRE DEL FISCAL ESPECIALIZADO</td>
				<td colspan="2" text-align="center"> {{ $datosUIPJ->agente_mp}}</td>
			</tr>
			<tr>
				<td text-align="center" style="background: #efefef;">FECHA</td>
				<td text-align="center" style="background: #efefef;">HORA</td>
				<td text-align="center" style="background: #efefef;">CALLE Y NUMERO</td>
			</tr>
			<tr>
				<td><p>2018-05-01</p></td>
				<td><p>12:57:00</p></td>
				<td><p>2 DE ABRIL No. 23 Int. S/N</p></td>
			</tr>
			<tr>
				<td text-align="center" style="background: #efefef;">ENTIDAD FEDERATIVA</td>
				<td colspan="2" text-align="center" style="background: #efefef;">MUNICIPIO / DELEGACION</td>
			</tr>
			<tr>
				<td><p>VERACRUZ DE IGNACIO DE LA LLAVE</p></td>
				<td colspan="2"><p>VERACRUZ</p></td>
			</tr>
			<tr>
				<td colspan="2" text-align="center" style="background: #efefef;">COLONIA</td>
				<td text-align="center" style="background: #efefef;">CODIGO POSTAL</td>
			</tr>
			<tr>
				<td colspan="2" ><p>2 CAMINOS</p></td>
				<td><p>91726</p></td>
			</tr>

  		</tbody>
	</table>
	<table  align="center">
    		<tr>
    			<th  colspan="3">DEPÓSITO DE RESGUARDO</th>
    		</tr>
    	<tbody>
			<tr>
				<td text-align="center" style="background: #efefef;">ENTIDAD</td>
				<td text-align="center" style="background: #efefef;">NOMBRE DEL DEPÓSITO</td>
				<td text-align="center" style="background: #efefef;">NUMERO DE CONTROL DEPÓSITO</td>
			</tr>
			<tr>
				<td><p>Veracruz</p></td>
				<td><p>{{ $datosVehiculo->deposito}}</p></td>
				<td><p>22</p></td>
			</tr>
  		</tbody>
	</table>
    <table align="center">
        	<tr>
            	<th colspan="4" text-align="center">DATOS DEL VEHÍCULO </th>
        	</tr>

        <tbody>
            <tr>
            	<td text-align="center" style="background: #efefef;">PLACAS </td>
            	<td text-align="center" style="background: #efefef;">ENTIDAD FEDERATIVA</td>
            	<td style="background: #efefef;">MARCA</td>
            	<td text-align="center" style="background: #efefef;">SUBMARCA</td>
            </tr>
            <tr>
            	<td>{{ $datosVehiculo->placas}}</td>
            	<td>{{ $datosVehiculo->estado}}</td>
            	<td>{{ $datosVehiculo->marca}}</td>
				<td>{{ $datosVehiculo->submarca}}</td>
            </tr>
            <tr>
            	<td style="background: #efefef;">MODELO</td>
            	<td style="background: #efefef;">COLOR</td>
            	<td style="background: #efefef;">PERMISO</td>
            	<td style="background: #efefef;">SERIE/VIN</TD>
            </tr>
            <tr>
            	<td>{{ $datosVehiculo->modelo}}</td>
            	<td>{{ $datosVehiculo->color}}</td>
            	<td>{{ $datosVehiculo->permiso}}</td>
            	<td>{{ $datosVehiculo->numSerie}}</td>
            </tr>
            <tr>
            	<td style="background: #efefef;">MOTOR</td>
            	<td colspan="2" style="background: #efefef;">TIPO DE VEHICULO</td>
            	<td style="background: #efefef;">TIPO DE USO</td>
            </tr>
            <tr>
            	<td>{{ $datosVehiculo->numMotor}}</td>
            	<td colspan="2">{{ $datosVehiculo->tipoVehiculo}}</td>
            	<td>{{ $datosVehiculo->tipoUSo}}</td>
            </tr>
            <tr>
            	<td colspan="4" style="background: #efefef;">SEÑAS PARTICULARES </td>
            </tr>
            <tr>
            	<td colspan="4" >{{ $datosVehiculo->senasPartic}}</td>
            </tr>
		</tbody>
	</table>
	<table  align="center">
    		<tr>
    			<th colspan="3">FICHA CRIMINÓGENA (SI HUBO DETENIDOS)</th>
    		</tr>
    	<tbody>
			<tr>
				<td text-align="center" style="background: #efefef;">NOMBRE DE LA BANDA</td>
				<td text-align="center" style="background: #efefef;">MODALIDAD DELICTIVA</td>
				<td text-align="center" style="background: #efefef;">MEDIO DE COMISIÓN</td>
			</tr>
			<tr>
				<td><p>--</p></td>
				<td><p></p></td>
				<td><p></p></td>
			</tr>
  		</tbody>
	</table>
	<table  align="center">
			<tr>
    			<th colspan="4" text-align="center" >DESGLOSE DE LOS INTEGRANTES</th>
    		</tr>
				<tbody>

			<tr>
				<td text-align="center" style="background: #efefef;">ALIAS</td>
				<td text-align="center" style="background: #efefef;">NOMBRES(S)</td>
				<td text-align="center" style="background: #efefef;">APELLIDO PATERNO</td>
				<td text-align="center" style="background: #efefef;">APELLIDO MATERNO</td>
			</tr>
			<tr>
				<td><p>--</p></td>
				<td><p></p></td>
				<td><p></p></td>
				<td><p></p></td>
			</tr>

  		</tbody>
	</table>
	<br>
	<table >
		<tbody>
			<tr>
				<td  style="border: hidden" align="center" >
					<p>ELABORÓ</p>
					<br><br><br><br>
					<p>_____________________________________</p>
				</td>
				<td style="border: hidden" align="center" >
					<p>CONFORMIDAD</p>
					<br><br><br><br>
					<p>_____________________________________</p>
					<br>
					<p style="font-size:10px">FISCAL VIGÉSIMO QUINTO DE LA UNIDAD INTEGRAL DE PROCURACIÓN DE JUSTICIA</p>
				</td>
				<td  style="border: hidden" align="center">
					<p stye>SELLO DE RECIBIDO (RESERVADO ENLACE)</p>
					<table>
						<tbody>
							<tr>
								<td style="padding:3em 0em"></td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
    <br>
	<table>
		<tbody>
			<tr>
				<th >FECHA CAPTURA</th>
				<td ></td>
				<th >HORA DE CAPTURA</th>
				<td ></td>
				<th >NOMBRE DE QUIEN CAPTURA</th>
				<td ></td>
			</tr>
		</tbody>
	</table>
  </div>


  <style>
  table{
    width: 100%;
    border-collapse: collapse;
  }
  td {
    width: 25%;
    text-align: left;
    vertical-align: top;
    border: 0.5px solid #000;
    border-collapse: collapse;

  }
  thead th{
    width: 25%;
    text-align: center;
    vertical-align: top;
    border: 0.5px solid #000;
    border-collapse: collapse;
    background: #1d1d1d;
    font-color:#FFFFFF;
  }
  body th{
    width: 25%;
    text-align: center;
    vertical-align: top;
    border: 0.5px solid #000;
    border-collapse: collapse;
    background: #8d8d8d;
  }

  .linea{
    border-bottom: 1px dotted #000;
  }
  html, body {
    height: 100%;
    margin: 0;
  }
  body, p{
    font-family: 'neosanspro-regular';
    font-weight: normal;
    font-style: normal;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
  h1, h2, h3, h4, h5, div {
    font-family: 'neosanspro-regular';
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
  h6 {
    font-family: 'neosanspro-bold';
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }


  .line_div{
    float: left;
    /* width: 150px; */
    height: 75px;
    margin: 5px;
    /* width: 19em; */
    /* border: 3px solid #73AD21; */
  }

</style>



</body>
</html>
