<?php

namespace App\Traits;

use DB;
use App\Models\VehRobadoBus;
use App\Models\VehRecuperadosBus;
use App\Models\VehEntregadosBus;

trait BusTrait
{

    public static function conversion_bus($datosRobo, $vvr, $llavero =1,$insert=1){
      // dd($datosRobo);

      $llaveExterna = ($insert==2)?$llavero: '30'.'-'.$datosRobo->averiguacion.'-'.$vvr['idVeh'];
      $oper = ($insert==2)?'CAMBIO':'ALTA';

      DB::table('vehiculo')->where('id',$vvr['idVeh'])->update(['llave_externa' => $llaveExterna]);

      // switch ($denunc->sexo){
      //   case 'HOMBRE': $sexo_den = 1; break;
      //   case 'MUJER': $sexo_den = 2; break;
      //   default : $sexo_den = 0; break;
      // }
      // dd($datosRobo->averiguacion);
      $datosBus = array(
        //'id_alterna'=>0,
        //'id_fuente'=>0,
        'nrpv'=>$vvr['nrpv'],
        'nrpv' =>($vvr['nrpv'] == 'S/N' || $vvr['nrpv'] == null )? ' ' : $vvr['nrpv'],
        'averiguacion'=>$datosRobo->averiguacion,
        'fecha_averigua'=>$datosRobo->fecha_averigua,
        'hora_averigua'=>substr($datosRobo->hora_averigua,0,5),
        'agencia_mp'=>(strlen($datosRobo->agencia_mp) > 40)?substr($datosRobo->agencia_mp, 0, 40):$datosRobo->agencia_mp,
        'agente_mp'=>$datosRobo->agente_mp,
        'id_modalidad'=>$datosRobo->id_modalidad,
        'responsables'=>$datosRobo->responsables,
        'victimas'=>$datosRobo->victimas,
        'peculiaridades'=>($datosRobo->peculiaridades=='')?'SIN DATO':$datosRobo->peculiaridades,
        'senas_delincuente'=>$datosRobo->senas_delincuente,
        'vestimenta'=>$datosRobo->vestimenta,
        'comportamiento'=>$datosRobo->comportamiento,
        'id_puesto'=>($datosRobo->id_puesto=='')?0:$datosRobo->id_puesto,
        'fecha_robo'=>$datosRobo->fecha_robo,
        'hora_robo'=>substr($datosRobo->hora_robo,0,5),
        'calle_robo'=>$datosRobo->calle_robo,
        'num_ext_robo'=>$datosRobo->num_ext_robo,
        'num_int_robo'=>$datosRobo->num_int_robo,
        'colonia_robo'=>$datosRobo->colonia_robo,
        'id_municipio_robo'=>($datosRobo->id_municipio_robo == 2497)? 0:$datosRobo->id_municipio_robo,
        'id_entidad_robo'=>($datosRobo->id_entidad_robo == 33)?0:$datosRobo->id_entidad_robo,
        'cp_robo'=>$datosRobo->cp_robo,
        'referencia_robo'=>($datosRobo->referencia_robo == null)?'':$datosRobo->referencia_robo,
        'id_tipo_lugar'=>($datosRobo->id_tipo_lugar == 24)?0:$datosRobo->id_tipo_lugar,
        'id_delito'=>$datosRobo->id_delito,
        'id_arma_asoc'=>$datosRobo->id_arma_asoc,
        'id_marca_asoc'=>($datosRobo->id_marca_asoc=="")?0:$datosRobo->id_marca_asoc,
        'id_submarca_asoc'=>($datosRobo->id_submarca_asoc=='')?0:$datosRobo->id_submarca_asoc,
        'nombre_den'=>$datosRobo->nombre_den,
        'paterno_den'=>($datosRobo->paterno_den == null)?'':$datosRobo->paterno_den,
        'materno_den'=>($datosRobo->materno_den == null)?'':$datosRobo->materno_den,
        'rfc_den'=>($datosRobo->rfc_den == null)?'':$datosRobo->rfc_den,
        'curp_den'=>($datosRobo->curp_den == null)?'':$datosRobo->curp_den,
        'licencia_den'=>($datosRobo->licencia_den==null)?'':$datosRobo->licencia_den,
        'pasaporte_den'=>($datosRobo->pasaporte==null)?'':$datosRobo->pasaporte,
        'sexo_den'=>($datosRobo->sexo_den)?$datosRobo->sexo_den:0,
        'calle_den'=>($datosRobo->calle_den == 'SIN INFORMACION')?'':$datosRobo->calle_den,
        'numext_dom_den'=>($datosRobo->numext_dom_den == 'S/N')? 0 : $datosRobo->numext_dom_den,
        'numint_dom_den'=>($datosRobo->numint_dom_den == 'S/N')? 0 :$datosRobo->numint_dom_den,
        'colonia_den'=>$datosRobo->colonia_den,
        'id_municipio_den'=>($datosRobo->id_municipio_den == 2497)? 0:$datosRobo->id_municipio_den,
        'id_entidad_den'=>($datosRobo->id_entidad_den == 33) ? 0: $datosRobo->id_entidad_den,
        'cp_den'=>$datosRobo->cp_den,
        'telefono_den'=>($datosRobo->telefono_den == null)? '' :$datosRobo->telefono_den,
        'correo_den'=>$datosRobo->correo_den,
        'placa'=>($vvr['placas'])?$vvr['placas']:'0000000',
        'placa_extranjera'=>$vvr['placa_extranjera'],
        'permiso'=>($vvr['folioDoctoCirc'])?$vvr['folioDoctoCirc']:' ',
        'id_entidad_placa'=>0,
        'id_marca'=>($vvr['idMarca'] == 999)? 0 :$vvr['idMarca'],
        'id_submarca'=>$vvr['idSubmarca'],
        'modelo'=>$vvr['modelo'],
        'id_color'=>($vvr['idColor'] == 99)? 0 : $vvr['idColor'],
        'serie'=>$vvr['numSerie'],
        'motor'=>($vvr['numMotor'] == null)? '' : $vvr['numMotor'],
        'id_clase_vehiculo'=>($vvr['idClaseVehiculo'] == 99)? 0 : $vvr['idClaseVehiculo'],
        'id_tipo_vehiculo'=>($vvr['idTipoVehiculo'] == 999)? 0 : $vvr['idTipoVehiculo'],
        'id_tipo_uso'=>($vvr['idTipoUso'] == 99)? 0 : $vvr['idTipoUso'],
        'senas' =>($vvr['senasPartic'] == 'S/N' || $vvr['senasPartic'] == null )? ' ' : $vvr['senasPartic'],
        'id_procedencia'=>($vvr['idProcedencia'] == 4)? 0 : $vvr['idProcedencia'],
        'id_aseguradora'=>($vvr['idAseguradora'] == 99)? 0 : $vvr['idAseguradora'],
        'MOVIMIENTO'=>$oper,
        'fiscalia'=>5,
        //'id'=>$vvr[''],
        //'robo'=>$vvr[''],
        //'fechaact'=>$vvr[''],
        'idVehiculo'=>$vvr['idVeh'],
        'busFGE'=>0,
        'busCNS'=>0,
        'busCNSKey'=>0,
        //'fechaenv'=>$vvr[''],
        //'horaenv'=>$vvr[''],
        //'fecharesp'=>$vvr[''],
        //'horaresp'=>$vvr[''],
        'llave_externa'=>$llaveExterna,
        'placa_extranjera'=>0
      );

      //dd($datosBus);
      $vehiculoBus = VehRobadoBus::create($datosBus);
	}

	public static function conversion_bus_recuperados($datosRobo, $vvr,$insert=1){

		$oper = ($insert==2)?'CAMBIO':'ALTA';

		//dd($datosRobo);
		$datosBus = array(
			//'id_alterna'=>0,
			//'id_fuente'=>0,
			'documento'=>0,
			'fecha_docto'=>date("1900-01-01"),
			'hora_docto'=>'00:00',
			'agencia_docto'=>(strlen($datosRobo->agencia_mp) > 40)? substr($datosRobo->agencia_mp, 0, 40) : $datosRobo->agencia_mp,
			'agente_docto'=>$datosRobo->agente_mp,
			'placa'=>($vvr->placa)?$vvr->placa:'0000000',
			'permiso'=>($vvr->permiso)?$vvr->permiso:' ',
			'motor'=>($vvr->motor == null)? '' : $vvr->motor,
			'serie'=>$vvr->motor,
      		'senas' =>($vvr->senas == null )? ' ' : $vvr->senas,
			'id_entidad_recupera'=>($vvr->id_entidad_recupera == 33)? 0: $vvr->id_entidad_recupera,
			'id_deposito' =>$vvr->id_deposito,
			'calle_rec' =>$vvr->calle_rec,
			'numext_rec' =>$vvr->numext_rec,
			'numint_rec' =>($vvr->numint_rec == 'S/N' || $vvr->numint_rec == null )? ' ' : $vvr->numint_rec,
			'colonia_rec' =>$vvr->colonia_rec,
			'id_municipio_rec' =>($vvr->id_municipio_rec == 2497)? 0: $vvr->id_municipio_rec,
			'cp_rec' =>$vvr->cp_rec,
			'referencia_rec' =>$vvr->referencia_rec,
			'id_color' =>$vvr->id_color,
			'fecha_rec' =>($vvr->fecha_rec == '0000-00-00')? date("1900-01-01") : $vvr->fecha_rec,
			'hora_rec' => substr($vvr->hora_rec,0,5),
			'llave_externa'=>$vvr->llave_externa,
			'idVehiculo'=>$vvr->idVeh,
			'busFGE'=>0,
			'busCNS'=>0,
			'busCNSKey'=>0,
			//'fechaenv'=>$vvr[''],
			//'horaenv'=>$vvr[''],
			//'fecharesp'=>$vvr[''],
			//'horaresp'=>$vvr[''],
			'idbus' => 0,
			'fiscalia' => 1
		);

		//dd($datosBus);
		$vehiculoBus = VehRecuperadosBus::create($datosBus);
	}

	public static function conversion_bus_entregados($datosRobo =null, $vvr,$insert=1){

		$oper = ($insert==2)?'CAMBIO':'ALTA';

		$vehiculo_bus = array (
			'idVeh'=> $vvr['idVeh'],
			'SERIE_ALTERADA'=> $vvr['SERIE_ALTERADA'],
			'MOTOR_ALTERADO'=> $vvr['MOTOR_ALTERADO'],
			'CALLE_ENTREGA'=> $vvr['CALLE_ENTREGA'],
			'NUMEXT_ENTREGA'=> $vvr['NUMEXT_ENTREGA'],
			'NUMINT_ENTREGA'=> $vvr['NUMINT_ENTREGA'],
			'COLONIA_ENTREGA'=> $vvr['COLONIA_ENTREGA'],
			'ID_MUNICIPIO_ENTREGA'=> ($vvr['ID_MUNICIPIO_ENTREGA']  == 2497) ? 0 : $vvr['ID_MUNICIPIO_ENTREGA'],
			'ID_ENTIDAD_ENTREGA'=> ($vvr['ID_ENTIDAD_ENTREGA'] == 33) ? 0 : $vvr['ID_ENTIDAD_ENTREGA'],
			'CP_ENTREGA'=> $vvr['CP_ENTREGA'],
			'REFERENCIA_ENTREGA'=> $vvr['REFERENCIA_ENTREGA'],
			'INSPECCION'=> $vvr['INSPECCION'],
			'FECHA_ENTREGA'=> $vvr['FECHA_ENTREGA'],
			'HORA_ENTREGA'=> substr($vvr['HORA_ENTREGA'],0,5),
			'SERIE'=> $vvr['SERIE'],
			'MOTOR'=> $vvr['MOTOR'],
			'MONTO_VEHICULO'=> $vvr['MONTO_VEHICULO'],
			'FACTURA_VEHICULO'=> '',
			'FECHA_FACTURA'=> date("1900-01-01"),
			'CRED_ELECT_PROP'=> '',
			'PASAPORTE_PROP'=> '',
			'CEDULA_PROF_PROP'=> '',
			'COMPROB_DOMIC_PROP'=> '',
			'PERSONA_ENTREGA'=> 1,
			'busFGE'=>0,
			'busCNS'=>0,
			'busCNSKey'=>0,
			'llave_externa'=>$vvr['LLAVE_EXTERNA'],
		);

		$vehiculoBus = VehEntregadosBus::create($vehiculo_bus);
	}


}
