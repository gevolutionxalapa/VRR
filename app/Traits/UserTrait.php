<?php

namespace App\Traits;

trait UserTrait
{
    public static function get_vehiculo_uipj()
    {
        $vehiculo = array
        (
            array(
                'id' => 1,
                'numCarpeta' => 'UIPJ/DXI/1/1/2018',
                'idTipifDelito' => 2,
                'status' => 'INVOLUCRADO',
                'placas' => '1111111',
                'idEstado' => 10,
                'idSubmarca' => 14332,
                'modelo' => 2018,
                'nrpv' => 454787845478,
                'idColor' => 11,
                'permiso' => 'Sin informacion',
                'numSerie' => '11111111111111111',
                'numMotor' => '1111111111',
                'idTipoVehiculo' => 10,
                'idTipoUso' => 10,
                'senasPartic' => 'No tiene',
                'idProcedencia' => 2,
                'idAseguradora' => 25
            ),
            array(
                'id' => 2,
                'numCarpeta' => 'UIPJ/DV/1/1/2018',
                'idTipifDelito' => 3,
                'status' => 'INVOLUCRADO',
                'placas' => 'QWE123',
                'idEstado' => 10,
                'idSubmarca' => 14000,
                'modelo' => 2016,
                'nrpv' => 123456789012,
                'idColor' => 11,
                'permiso' => 'Sin informacion',
                'numSerie' => '11111111111111111',
                'numMotor' => '1111111111',
                'idTipoVehiculo' => 2,
                'idTipoUso' => 5,
                'senasPartic' => 'Faros delanteros rotos',
                'idProcedencia' => 3,
                'idAseguradora' => 15
            ),
            array(
                'id' => 3,
                'numCarpeta' => 'UIPJ/DXV/1/1/2018',
                'idTipifDelito' => 1,
                'status' => 'INVOLUCRADO',
                'placas' => 'MRF123',
                'idEstado' => 10,
                'idSubmarca' => 10000,
                'modelo' => 1998,
                'nrpv' => 121212121212,
                'idColor' => 11,
                'permiso' => 'Licencia',
                'numSerie' => '89089089089089089',
                'numMotor' => '1231231231',
                'idTipoVehiculo' => 2,
                'idTipoUso' => 5,
                'senasPartic' => 'Abolladura en la defensa',
                'idProcedencia' => 6,
                'idAseguradora' => 12
            ),
            array(
                'id' => 4,
                'numCarpeta' => 'UIPJ/DXII/1/1/2018',
                'idTipifDelito' => 2,
                'status' => 'INVOLUCRADO',
                'placas' => 'FG56876',
                'idEstado' => 7,
                'idSubmarca' => 14300,
                'modelo' => 2018,
                'nrpv' => 2006,
                'idColor' => 11,
                'permiso' => 'Sin informacion',
                'numSerie' => '56784534124321456',
                'numMotor' => '8976452316',
                'idTipoVehiculo' => 4,
                'idTipoUso' => 9,
                'senasPartic' => 'Sin placas',
                'idProcedencia' => 7,
                'idAseguradora' => 4
            ),
            array(
                'id' => 5,
                'numCarpeta' => 'UIPJ/DIII/1/1/2018',
                'idTipifDelito' => 2,
                'status' => 'INVOLUCRADO',
                'placas' => 'MRF432',
                'idEstado' => 25,
                'idSubmarca' => 10332,
                'modelo' => 2016,
                'nrpv' => 454787845478,
                'idColor' => 11,
                'permiso' => 'Sin informacion',
                'numSerie' => '78945678945678965',
                'numMotor' => '5555555555',
                'idTipoVehiculo' => 4,
                'idTipoUso' => 5,
                'senasPartic' => 'Le falta una llanta',
                'idProcedencia' => 7,
                'idAseguradora' => 12
            )

        );
        return response()->json($vehiculo, 200);
    }
}