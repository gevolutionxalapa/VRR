<?php

namespace App\Traits;
use DB;
trait UipjTrait
{
    //Funcion para obtener los datos que necesita el bus de robados
    public static function get_datos_uipj($idCarpeta)
    {
        // dd($idCarpeta);
       $carpeta=DB::connection('uipj')->table('carpeta')
            ->join('control_carpeta','control_carpeta.idCarpeta','carpeta.id')
            ->join('unidad','unidad.id','control_carpeta.idUnidad')
            ->join('users','users.id','control_carpeta.idFiscal')
            ->leftjoin('tipif_delito','tipif_delito.idCarpeta','control_carpeta.idCarpeta')
            ->leftjoin('variables_persona','variables_persona.idCarpeta','carpeta.id')
            ->leftjoin('variables_persona as variables_persona_denun','variables_persona_denun.idCarpeta','carpeta.id')
            ->leftjoin('extra_denunciado','extra_denunciado.idVariablesPersona','variables_persona.id')
            ->leftjoin('extra_denunciante','extra_denunciante.idVariablesPersona','variables_persona_denun.id')
            ->join('domicilio','domicilio.id','tipif_delito.idDomicilio')
            ->join('cat_colonia','cat_colonia.id','domicilio.idColonia')
            ->join('cat_municipio','cat_municipio.id','domicilio.idMunicipio')
            ->join('cat_estado','cat_estado.id','cat_municipio.idEstado')
            ->join('persona','persona.id','variables_persona_denun.idPersona')
            ->join('domicilio as domicilio_den','domicilio_den.id','variables_persona_denun.idDomicilio')
            ->join('cat_colonia as cat_colonia_den','cat_colonia_den.id','domicilio_den.idColonia')
            ->join('cat_municipio as cat_municipio_den','cat_municipio_den.id','domicilio_den.idMunicipio')
            ->join('notificacion','notificacion.id','extra_denunciante.idNotificacion')
            ->where('control_carpeta.idCarpeta',$idCarpeta)
            ->where('variables_persona.idCarpeta',$idCarpeta)
            ->where('variables_persona_denun.idCarpeta',$idCarpeta)
            ->whereIn('tipif_delito.idAgrupacion1',array('60','63','65','66'))
            ->select('carpeta.numCarpeta as averiguacion',
                'carpeta.created_at as fecha_averigua',
                DB::raw('DATE_FORMAT(carpeta.created_at,"%H:%i") as hora_averigua'),
                'unidad.nombre as agencia_mp',
                DB::raw('CONCAT(users.nombres," ",users.apellidos) as agente_mp'),
                DB::raw('case when tipif_delito.conViolencia =0 THEN 2 ELSE 1 END  as id_modalidad'),
                DB::raw('ifnull(sum(case when extra_denunciado.id !="" THEN 1 ELSE 0 END),0)  as responsables'),
                DB::raw('sum(case when extra_denunciante.id !="" THEN 1 ELSE 0 END)  as victimas'),
                DB::raw('CONCAT("") as peculiaridades'),
                DB::raw('ifnull(extra_denunciado.vestimenta,"") as vestimenta'),
                DB::raw('ifnull(extra_denunciado.senasPartic,"") as senas_delincuente'),
                DB::raw('CONCAT("") as comportamiento'),
                DB::raw('ifnull(extra_denunciado.idPuesto,"") as id_puesto'),
                DB::raw('CONCAT(tipif_delito.fecha," ",tipif_delito.hora) as fecha_robo'),
                DB::raw('case when persona.esEmpresa=1 THEN "MORAL" else "FISICA" end as tipo'),
                'tipif_delito.hora as hora_robo',
                'unidad.idRegion as idRegion',
                'domicilio.calle as calle_robo',
                'domicilio.numExterno as num_ext_robo',
                'domicilio.numinterno as num_int_robo',
                'cat_colonia.nombre as colonia_robo',
                'domicilio.idMunicipio as id_municipio_robo',
                'cat_estado.id as id_entidad_robo',
                'cat_colonia.codigoPostal as cp_robo',
                'tipif_delito.puntoReferencia as referencia_robo',
                'tipif_delito.idLugar as id_tipo_lugar',
                'tipif_delito.idDelito as id_delito',
                'tipif_delito.idArma as id_arma_asoc',
                DB::raw('CONCAT("") as id_marca_asoc'),
                DB::raw('CONCAT("") as id_submarca_asoc'),
                DB::raw('(ifnull(persona.nombres," ")) as nombre_den'),
                DB::raw('(ifnull(persona.primerAp," ")) as paterno_den'),
                DB::raw('(ifnull(persona.segundoAp," ")) as materno_den'),
                DB::raw('ifnull(persona.rfc," ") as rfc_den'),
                DB::raw('ifnull(persona.curp," ") as curp_den'),
                DB::raw('ifnull((case when variables_persona_denun.docIdentificacion ="LICENCIA PARA CONDUCIR" THEN variables_persona_denun.numDocIdentificacion ELSE " " END)," ")  as licencia_den'),
                DB::raw('ifnull((case when variables_persona_denun.docIdentificacion ="PASAPORTE" THEN variables_persona_denun.numDocIdentificacion ELSE " " END)," ")  as pasaporte'),
                DB::raw('case when persona.sexo ="MUJER" THEN 2
                                when persona.sexo ="HOMBRE" THEN 1 ELSE null
                                END as sexo_den'),
                'domicilio_den.calle as calle_den',
                'domicilio_den.numExterno as numext_dom_den',
                'domicilio_den.numInterno as numint_dom_den',
                'cat_colonia_den.nombre as colonia_den',
                'domicilio_den.idMunicipio as id_municipio_den',
                'cat_municipio_den.idEstado as id_entidad_den',
                'cat_colonia_den.codigoPostal as cp_den',
                'variables_persona_denun.telefono as telefono_den',
                'notificacion.correo as correo_den',
                'unidad.idRegion as idRegion')
            ->groupBy('control_carpeta.id')
            ->first();
        return $carpeta;
    }

    public static function relVehCarpeta($idCarpt){
        // $carpeta = DB::connection('uipj')->table('carpeta')
        //     ->where('numCarpeta','=',$numCarpeta)
        //     ->first();
        $carpeta = DB::connection('uipj')->table('carpeta')
            ->join('control_carpeta','control_carpeta.idCarpeta','carpeta.id')
            ->join('unidad','unidad.id','control_carpeta.idUnidad')
            ->join('users','users.id','control_carpeta.idFiscal')
            ->where('carpeta.id','=',$idCarpt)
            ->first();
            //dd($carpeta);
        return $carpeta;
    }

    public static function getPersona($persona,$idCarpeta){
        $tipoPersona = ($persona==1)?0:1;
        $persona = DB::connection('uipj')->table('extra_denunciante')
        ->join('variables_persona','variables_persona.id','=','extra_denunciante.idVariablesPersona')
        ->join('persona','persona.id','=','variables_persona.idPersona')
        ->where('esEmpresa',$tipoPersona)
        ->where('idCarpeta',$idCarpeta)
        ->select('persona.id as idPersona',DB::raw('CONCAT(persona.nombres," ",persona.primerAp," ",persona.segundoAp) AS persona'),'variables_persona.representanteLegal')
        ->get();

        return $persona;
    }

    //Funcion para obtener los datos que necesita el bus de recuperados
    public static function get_datos_uipj_recuperado($idCarpeta){
        $data = DB::connection('uipj')->table('carpeta')
            ->join('unidad', 'unidad.id', '=', 'carpeta.idUnidad')
            ->join('users', 'users.id', '=', 'carpeta.idFiscal')
            ->where('carpeta.id','=', $idCarpeta)
            //Falta incorporar el folio, fecha y hora del documento de recuperacion
            ->select('unidad.nombre as agencia_docto', 'unidad.idRegion',DB::raw('CONCAT(users.nombres," ", apellidos) As agente_docto'))
            ->first();
        return $data;
    }
}
