<?php

namespace App\Traits;
use DB;
trait UatTrait
{
    //Funcion para obtener los datos que necesita el bus de robados
    public static function get_datos_uat($idCarpeta)
    {
        /* Datos de la carpeta y del Robo */
        $uat = DB::connection('uat')->table('carpeta')
            ->join('unidad', 'unidad.id', '=', 'carpeta.idUnidad')
            ->join('users', 'users.id', '=', 'carpeta.idFiscal')
            ->join('tipif_delito', 'tipif_delito.id', '=', 'carpeta.id')
            ->join('domicilio', 'domicilio.id', '=', 'tipif_delito.idDomicilio')
            ->join('cat_municipio', 'cat_municipio.id','=', 'domicilio.idMunicipio')
            ->join('cat_localidad', 'cat_localidad.id','=', 'domicilio.idLocalidad')
            ->join('cat_colonia', 'cat_colonia.id','=', 'domicilio.idColonia')
            ->join('cat_estado', 'cat_estado.id','=', 'cat_municipio.idEstado')        
            ->join('variables_persona', 'variables_persona.id','=', 'carpeta.id')     
            ->where('carpeta.id','=', $idCarpeta)
            //Datos de la carpeta
            ->select('numCarpeta', 'fechaInicio', 'horaIntervencion','idFiscal', 'carpeta.idUnidad', 'unidad.idZona as idRegion', 'unidad.descripcion as nombreFiscalia', 'users.nombres', 'apellidos',DB::raw('CONCAT(users.nombres," ", apellidos) As nombreFiscal'), 
            //Datos del robo
            'fecha', 'hora', 'calle', 'numExterno', 'numInterno', 'codigoPostal', 'cat_colonia.nombre as colonia', 'cat_localidad.nombre as localidad', 'cat_municipio.nombre as municipio', 'cat_municipio.id as id_municipio', 'cat_estado.nombre as estado', 'cat_estado.id as id_estado',
            'conViolencia as id_modalidad', 'idLugar', DB::raw('CONCAT("Entre calle ", entreCalle," y calle ", yCalle) As referenciaRobo'))
            ->first();

        /* Datos del denunciante */
        $denunciante = DB::connection('uat')->table('extra_denunciante')
            ->join('variables_persona', 'variables_persona.id', '=', 'extra_denunciante.idVariablesPersona')
            ->join('persona', 'persona.id', '=', 'variables_persona.idPersona')
            ->join('domicilio', 'domicilio.id', '=', 'variables_persona.idDomicilio')
            ->join('cat_municipio', 'cat_municipio.id','=', 'domicilio.idMunicipio')
            ->join('cat_localidad', 'cat_localidad.id','=', 'domicilio.idLocalidad')
            ->join('cat_colonia', 'cat_colonia.id','=', 'domicilio.idColonia')
            ->join('cat_estado', 'cat_estado.id','=', 'cat_municipio.idEstado')
            ->select('extra_denunciante.id', 'persona.nombres', 'persona.primerAp', 'persona.segundoAp', 'persona.rfc', 'persona.curp', 'persona.esEmpresa', 'variables_persona.edad', 'persona.sexo', 'variables_persona.telefono',
            'calle', 'numExterno', 'numInterno', 'codigoPostal', 'cat_colonia.nombre as colonia', 'cat_localidad.nombre as localidad', 'cat_municipio.nombre as municipio', 'cat_municipio.id as id_municipio', 'cat_estado.nombre as estado', 'cat_estado.id as id_estado')
            ->where('variables_persona.idCarpeta', '=', $idCarpeta)
            ->first();
        
        $data = array(
            'datosCarpetaRobo' => $uat,
            'denunciante' => $denunciante
        );
        return $data;
    }

    //Funcion para obtener los datos que necesita el bus de recuperados
    public static function get_datos_uat_recuperado($idCarpeta){
        $uat = DB::connection('uat')->table('carpeta')
            ->join('unidad', 'unidad.id', '=', 'carpeta.idUnidad')
            ->join('users', 'users.id', '=', 'carpeta.idFiscal')  
            ->where('carpeta.id','=', $idCarpeta)
            //Falta incorporar el folio, fecha y hora del documento de recuperacion
            ->select('unidad.descripcion as agencia_docto', 'unidad.idZona as idRegion',DB::raw('CONCAT(users.nombres," ", apellidos) As agente_docto'))
            ->first();

        return $uat;
    }
}