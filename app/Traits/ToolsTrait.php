<?php

namespace App\Traits;

use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
//Modelos
use App\Models\RelDocsVehCat;
use App\Models\CatSubCategoriaDocumento as SubCateDoctos;
use App\Models\Vehiculo;
use App\Models\RelVehiculoCarpeta;
use App\Models\TemporalVehiculo;
use App\Models\VehRobadoBus;
use DB;

trait ToolsTrait
{
    public static function save_image($archivo, $idsubcategoria, $idVehiculo, $optCaptura = 1, $idImagen=0)
    {
        //dd($archivo);
        if($idsubcategoria!=14){
            ToolsTrait::delete_image($idsubcategoria, $idVehiculo);
        }
        $categoria = SubCateDoctos::where('id','=',$idsubcategoria)->first();
        $ruta =  $categoria->almacenamiento;
        $destino = "";
        $date=Carbon::now('America/Monterrey');
                                                // int(2012)
        // var_dump($date->month);                                        // int(9)
        // var_dump($date->day);                                          // int(5)
        // var_dump($date->hour);                                         // int(23)
        // var_dump($date->minute);                                       // int(26)
        // var_dump($date->second);                                       // int(11)
        // var_dump($date->micro);


        // $rename = str_replace('-','',str_replace(':','',str_replace(' ','',str_replace('.', '', Carbon::now('America/Monterrey')))));
        $rename=$date->year.$date->month.$date->day.$date->hour.$date->minute.$date->second.$date->micro;

        if($optCaptura == 1){
            $fileName = $rename.'.'.$archivo->getClientOriginalExtension();
            $archivo->storeAs($ruta, $fileName);
            $destino = $ruta.$fileName;
        } else {
            $imgIdent1 = str_replace('data:image/jpeg;base64,', '',$archivo);
            $imgIdent1 =base64_decode($imgIdent1);
            $destino = $ruta.$rename.'.jpg';
            Storage::disk('local')->put($destino, $imgIdent1);
        }

        $registro1 = new RelDocsVehCat();
        $registro1->destino = $destino;
        $registro1->idVehiculo = $idVehiculo;
        //$registro1->idImagen =0;
        if($idsubcategoria==14 and $optCaptura==1){
            $registro1->idImagen = $idImagen;
        }
        $registro1->idSubCategoriaDocumento = $idsubcategoria;
        $registro1->save();
        return true;
    }

    public static function delete_image($idsubcategoria, $idVehiculo,$idImagen=null)
    {
      $exist_documento = RelDocsVehCat::where(array('idVehiculo'=> $idVehiculo, 'idSubCategoriaDocumento' =>$idsubcategoria))
      ->when($idImagen!=null, function ($query) use ($idImagen) {
        return $query->where('idImagen','=', $idImagen);
        })
      ->first();
      if($exist_documento != null) {
        $rutaCompleta = $exist_documento->destino;
        Storage::disk('local')->delete($rutaCompleta);
        DB::table('rel_docs_veh_cat')->where(array('idVehiculo'=>$idVehiculo, 'idSubCategoriaDocumento' => $idsubcategoria))
        ->when($idImagen!=null, function ($query) use ($idImagen) {
          return $query->where('idImagen','=', $idImagen);
            })
        ->delete();
        }
        return true;
    }

    public static function getRobados($placa, $serie, $motor,$token=null){

                //dd($token);
        $registros = DB::table('vehiculo')
        ->join('cat_submarca','cat_submarca.id','=','vehiculo.idSubmarca')
        ->join('cat_tipo_vehiculo','cat_tipo_vehiculo.id','=','vehiculo.idTipoVehiculo')
        ->join('rel_vehiculo_carpeta','rel_vehiculo_carpeta.idVehiculo','=','vehiculo.id')
        ->where(function ($query) use ($placa, $serie, $motor) {
            $query->where('placas','=', $placa)
            ->orWhere('numSerie','=', $serie)
            ->orWhere('numMotor','=', $motor);
        })
        ->where('vehiculo.status','=',1)
        ->when($token!=null, function ($query) use ($token) {
            return $query->where('token','!=', $token);
        })
        ->select('vehiculo.*','rel_vehiculo_carpeta.numCarpeta','rel_vehiculo_carpeta.numFiscal','rel_vehiculo_carpeta.idCarpeta',
            'cat_submarca.idMarca', 'cat_tipo_vehiculo.idClaseVehiculo',
            DB::raw('(CASE
                    WHEN vehiculo.nrpv = " " THEN "SIN INFORMACION"
                    WHEN vehiculo.nrpv != " " THEN vehiculo.nrpv
                    END) AS nrpv'),
            DB::raw('(CASE
                    WHEN vehiculo.senasPartic = " " THEN "SIN INFORMACION"
                    WHEN vehiculo.senasPartic != " " THEN vehiculo.senasPartic
                    END) AS senasPartic')
            )

        ->get();
        //dd(count($registros));
        $datoFiscal = array();
           for($i = 0; $i < count($registros); $i++){
            $idFiscal=($registros[$i]->numFiscal!=0)?$registros[$i]->numFiscal:0;
            $nomFiscal = ToolsTrait::getFiscal($idFiscal);
            array_push($datoFiscal,$nomFiscal);
            }

        $info = array(
            'registros' => $registros,
            'nomFiscal' => $datoFiscal,
        );
        //dd($info);
        return response()->json($info);
    }

    public static function getRecuperados($placa, $serie, $motor,$token=null,$nrpv=null){
        $idFiscal = "";
        $registros = DB::table('vehiculo')
        ->join('cat_submarca','cat_submarca.id','=','vehiculo.idSubmarca')
        ->join('cat_tipo_vehiculo','cat_tipo_vehiculo.id','=','vehiculo.idTipoVehiculo')
        ->join('rel_vehiculo_carpeta','rel_vehiculo_carpeta.idVehiculo','=','vehiculo.id')
        ->where('vehiculo.status','=',2)
        ->where(function ($query) use ($placa, $serie, $motor) {
            $query->where('placas','=', $placa)
            ->orWhere('numSerie','=', $serie)
            ->orWhere('numMotor','=', $motor);
        })
        ->select('vehiculo.*','rel_vehiculo_carpeta.numCarpeta','rel_vehiculo_carpeta.numFiscal','rel_vehiculo_carpeta.idCarpeta',
            'cat_submarca.idMarca', 'cat_tipo_vehiculo.idClaseVehiculo',
            DB::raw('(CASE
                    WHEN vehiculo.nrpv = " " THEN "SIN INFORMACION"
                    WHEN vehiculo.nrpv != " " THEN vehiculo.nrpv
                    END) AS nrpv'),
            DB::raw('(CASE
                    WHEN vehiculo.senasPartic = " " THEN "SIN INFORMACION"
                    WHEN vehiculo.senasPartic != " " THEN vehiculo.senasPartic
                    END) AS senasPartic')
        )
        ->get();

         $datoFiscal = array();
           for($i = 0; $i < count($registros); $i++){
            $idFiscal=($registros[$i]->numFiscal!=0)?$registros[$i]->numFiscal:0;
            $nomFiscal = ToolsTrait::getFiscal($idFiscal);
            array_push($datoFiscal,$nomFiscal);
            }

        $info = array(
            'registros' => $registros,
            'nomFiscal' => $datoFiscal,
        );


        return response()->json($info);
    }

    public static function getRegistrosUipj($placa, $serie, $motor){

        //return 0;

        $registrosCarpeta = DB::connection('uipj')->table('vehiculo')
        ->join('tipif_delito','tipif_delito.id','=','vehiculo.idTipifDelito')
        ->join('carpeta','carpeta.id','=','tipif_delito.idCarpeta')
        ->join('control_carpeta','control_carpeta.idCarpeta','=','carpeta.id')
        ->join('users','users.id','=','control_carpeta.idFiscal')
        ->where(function ($query) use ($placa, $serie, $motor) {
            $query->where('placas','=', $placa)
            ->orWhere('numSerie','=', $serie)
            ->orWhere('numMotor','=', $motor);
        })
        ->select('vehiculo.*','carpeta.numCarpeta',DB::raw('CONCAT(users.nombres, " ", users.apellidos) AS nomFiscal'))
        ->get();



        return response()->json($registrosCarpeta);
    }

    public static function getCountRobados($placa, $serie, $motor,$token=null){
            //dd($token);
        //$nrpvB = ($nrpv=='SIN INFORMACION')?'':$nrpv;
        $placaCoin = DB::table('vehiculo')
        ->join('rel_vehiculo_carpeta','rel_vehiculo_carpeta.idVehiculo','=','vehiculo.id')
        ->where('placas','=',$placa)
        ->when($token!=null, function ($query) use ($token) {
            return $query->where('token','!=', $token);
        })
        ->where('vehiculo.status','=',1)
        ->count();
        $serieCoin =  DB::table('vehiculo')
        ->join('rel_vehiculo_carpeta','rel_vehiculo_carpeta.idVehiculo','=','vehiculo.id')
        ->where('numSerie','=',$serie)
        ->when($token!=null, function ($query) use ($token) {
            return $query->where('token','!=', $token);
        })
        ->where('vehiculo.status','=',1)
        ->count();
        $motorCoin = DB::table('vehiculo')
        ->join('rel_vehiculo_carpeta','rel_vehiculo_carpeta.idVehiculo','=','vehiculo.id')
        ->where('numMotor','=',$motor)
        ->when($token!=null, function ($query) use ($token) {
            return $query->where('token','!=' ,$token);
        })
        ->where('vehiculo.status','=',1)
        ->count();
        $nrpvCoin = DB::table('vehiculo')
        ->join('rel_vehiculo_carpeta','rel_vehiculo_carpeta.idVehiculo','=','vehiculo.id')
        //->where('nrpv','=',$nrpvB)
        ->when($token!=null, function ($query) use ($token) {
            return $query->where('token', '!=', $token);
        })
        ->where('vehiculo.status','=',1)
        ->count();

        $sumaCoin = $placaCoin + $serieCoin + $motorCoin + $nrpvCoin;

        if($sumaCoin>0){
            $result=array(
                'placas'=>$placaCoin,
                'numSerie'=>$serieCoin,
                'numMotor'=>$motorCoin,
                //'nrpv'=>$nrpvCoin,
            );
            $status=201;
        }else{
            $result=array(
                'placas'=>0,
                'numSerie'=>0,
                'numMotor'=>0,
                'nrpv'=>0,
            );
            $status=421;
        }

        $info=array(
            'status'=>"correcto",
            'estado'=>$result,
            'status'=>$status,
        );
        return response()->json($info);
    }

    public static function getCountInvolucrados($placa, $serie, $motor){
            //dd($token);
        //$nrpvB = ($nrpv=='SIN INFORMACION')?'':$nrpv;
            //dd($nrpvB);
        $placaCoin = DB::connection('uipj')->table('vehiculo')
        ->join('tipif_delito','tipif_delito.id','=','vehiculo.idTipifDelito')
        ->join('carpeta','carpeta.id','=','tipif_delito.idCarpeta')
        ->where('placas','=', $placa)
        ->select('vehiculo.*','carpeta.numCarpeta')
        ->count();
        $serieCoin = DB::connection('uipj')->table('vehiculo')
        ->join('tipif_delito','tipif_delito.id','=','vehiculo.idTipifDelito')
        ->join('carpeta','carpeta.id','=','tipif_delito.idCarpeta')
        ->where('numSerie','=', $serie)
        ->select('vehiculo.*','carpeta.numCarpeta')
        ->count();
        $motorCoin = DB::connection('uipj')->table('vehiculo')
        ->join('tipif_delito','tipif_delito.id','=','vehiculo.idTipifDelito')
        ->join('carpeta','carpeta.id','=','tipif_delito.idCarpeta')
        ->where('numMotor','=', $motor)
        ->select('vehiculo.*','carpeta.numCarpeta')
        ->count();
        // $nrpvCoin = DB::connection('uipj')->table('vehiculo')
        // ->join('tipif_delito','tipif_delito.id','=','vehiculo.idTipifDelito')
        // ->join('carpeta','carpeta.id','=','tipif_delito.idCarpeta')
        // ->where('nrpv','=', $nrpvB)
        // ->select('vehiculo.*','carpeta.numCarpeta')
        // ->count();
            //dd($nrpvCoin);
        $sumaCoin = $placaCoin + $serieCoin + $motorCoin ;

        if($sumaCoin>0){
            $result=array(
                'placas'=>$placaCoin,
                'numSerie'=>$serieCoin,
                'numMotor'=>$motorCoin,
                //'nrpv'=>$nrpvCoin,
            );
            $status=201;
        }else{
            $result=array(
                'placas'=>0,
                'numSerie'=>0,
                'numMotor'=>0,
                //'nrpv'=>0,
            );
            $status=421;
        }

        $info=array(
            'status'=>"correcto",
            'estado'=>$result,
            'status'=>$status,
        );
            //dd($info);
        return response()->json($info);
    }

    public static function get_vehiculo_uipj(){
        $vehiculo = array
        (
            array(
                'id' => 1,
                'numCarpeta' => 'UIPJ/DXI/1/1/2018',
                'idTipifDelito' => 2,
                'status' => 'INVOLUCRADO',
                'placas' => 'QWE123',
                'idEstado' => 10,
                'idSubmarca' => 14332,
                'modelo' => 2018,
                'nrpv' => 454787845478,
                'idColor' => 11,
                'permiso' => 'Sin informacion',
                'numSerie' => '12345678901234567',
                'numMotor' => '1231231231',
                'idTipoVehiculo' => 10,
                'idTipoUso' => 10,
                'senasPartic' => 'No tiene',
                'idProcedencia' => 2,
                'idAseguradora' => 25
            ),
            array(
                'id' => 2,
                'numCarpeta' => 'UIPJ/DV/1/1/2018',
                'idTipifDelito' => 3,
                'status' => 'INVOLUCRADO',
                'placas' => 'QWE123',
                'idEstado' => 10,
                'idSubmarca' => 14000,
                'modelo' => 2016,
                'nrpv' => 123456789012,
                'idColor' => 11,
                'permiso' => 'Sin informacion',
                'numSerie' => '11111111111111111',
                'numMotor' => '1111111111',
                'idTipoVehiculo' => 2,
                'idTipoUso' => 5,
                'senasPartic' => 'Faros delanteros rotos',
                'idProcedencia' => 3,
                'idAseguradora' => 15
            ),
            array(
                'id' => 3,
                'numCarpeta' => 'UIPJ/DXV/1/1/2018',
                'idTipifDelito' => 1,
                'status' => 'INVOLUCRADO',
                'placas' => 'MRF123',
                'idEstado' => 10,
                'idSubmarca' => 10000,
                'modelo' => 1998,
                'nrpv' => 121212121212,
                'idColor' => 11,
                'permiso' => 'Licencia',
                'numSerie' => '89089089089089089',
                'numMotor' => '1231231231',
                'idTipoVehiculo' => 2,
                'idTipoUso' => 5,
                'senasPartic' => 'Abolladura en la defensa',
                'idProcedencia' => 6,
                'idAseguradora' => 12
            ),
            array(
                'id' => 4,
                'numCarpeta' => 'UIPJ/DXII/1/1/2018',
                'idTipifDelito' => 2,
                'status' => 'INVOLUCRADO',
                'placas' => 'FG56876',
                'idEstado' => 7,
                'idSubmarca' => 14300,
                'modelo' => 2018,
                'nrpv' => 2006,
                'idColor' => 11,
                'permiso' => 'Sin informacion',
                'numSerie' => '56784534124321456',
                'numMotor' => '8976452316',
                'idTipoVehiculo' => 4,
                'idTipoUso' => 9,
                'senasPartic' => 'Sin placas',
                'idProcedencia' => 7,
                'idAseguradora' => 4
            ),
            array(
                'id' => 5,
                'numCarpeta' => 'UIPJ/DIII/1/1/2018',
                'idTipifDelito' => 2,
                'status' => 'INVOLUCRADO',
                'placas' => 'MRF432',
                'idEstado' => 25,
                'idSubmarca' => 10332,
                'modelo' => 2016,
                'nrpv' => 454787845478,
                'idColor' => 11,
                'permiso' => 'Sin informacion',
                'numSerie' => '78945678945678965',
                'numMotor' => '5555555555',
                'idTipoVehiculo' => 4,
                'idTipoUso' => 5,
                'senasPartic' => 'Le falta una llanta',
                'idProcedencia' => 7,
                'idAseguradora' => 12
            )

        );
        return response()->json($vehiculo, 200);
    }

    public static function getCountRecuperados($placa, $serie, $motor, $nrpv,$token=null){
            //dd($token);
        $nrpvB = ($nrpv=='SIN INFORMACION')?'':$nrpv;
        $placaCoin = DB::table('vehiculo')
        ->join('rel_vehiculo_carpeta','rel_vehiculo_carpeta.idVehiculo','=','vehiculo.id')
        ->where('placas','=',$placa)
        ->when($token!=null, function ($query) use ($token) {
            return $query->where('token','!=', $token);
        })
        ->where('vehiculo.status','=',2)
        ->count();
        $serieCoin =  DB::table('vehiculo')
        ->join('rel_vehiculo_carpeta','rel_vehiculo_carpeta.idVehiculo','=','vehiculo.id')
        ->where('numSerie','=',$serie)
        ->when($token!=null, function ($query) use ($token) {
            return $query->where('token','!=', $token);
        })
        ->where('vehiculo.status','=',2)
        ->count();
        $motorCoin = DB::table('vehiculo')
        ->join('rel_vehiculo_carpeta','rel_vehiculo_carpeta.idVehiculo','=','vehiculo.id')
        ->where('numMotor','=',$motor)
        ->when($token!=null, function ($query) use ($token) {
            return $query->where('token','!=' ,$token);
        })
        ->where('vehiculo.status','=',2)
        ->count();
        $nrpvCoin = DB::table('vehiculo')
        ->join('rel_vehiculo_carpeta','rel_vehiculo_carpeta.idVehiculo','=','vehiculo.id')
        ->where('nrpv','=',$nrpvB)
        ->when($token!=null, function ($query) use ($token) {
            return $query->where('token', '!=', $token);
        })
        ->where('vehiculo.status','=',2)
        ->count();

        $sumaCoin = $placaCoin + $serieCoin + $motorCoin + $nrpvCoin;

        if($sumaCoin>0){
            $result=array(
                'placas'=>$placaCoin,
                'numSerie'=>$serieCoin,
                'numMotor'=>$motorCoin,
                'nrpv'=>$nrpvCoin,
            );
            $status=201;
        }else{
            $result=array(
                'placas'=>0,
                'numSerie'=>0,
                'numMotor'=>0,
                'nrpv'=>0,
            );
            $status=421;
        }

        $info=array(
            'status'=>"correcto",
            'estado'=>$result,
            'status'=>$status,
        );
        return response()->json($info);
    }

    public static function validacionRobados($idVeh){
        $estatusBus = VehRobadoBus::where('idVehiculo','=',$idVeh)
        ->where('status_muestra','=',0)
        ->select('busFGE','busCNS','busCNSKey')
        ->get();


            // $tipoVal = ($tipoVal==1)?'status_policia':'status_enlace';
            // //dd($tipoVal);
            //  $status = Vehiculo::where('id','=',$idVeh)
            //          ->select($tipoVal)
            //          ->first();
        return $estatusBus;
    }

    public static function validacionRobadosData($idVeh){

        $data = DB::table('resultado_operacion_error')
        ->join('vehiculo_robado_bus','vehiculo_robado_bus.llave_externa','=','resultado_operacion_error.LLAVE' )
        ->join('vehiculo','vehiculo.id','=','vehiculo_robado_bus.idVehiculo')
        ->where('vehiculo.id','=',$idVeh)
        ->where('vehiculo_robado_bus.status_muestra','=',0)
        ->select('resultado_operacion_error.DESC_ERROR','resultado_operacion_error.LLAVE')
        ->get();

        return $data;
    }

    public static function getFiscal($idFiscal){
            //dd($idFiscal);
       $fiscal = DB::connection('uipj')->table('users')
       ->where('id','=', $idFiscal)
       ->select(DB::raw('CONCAT(users.nombres, " ", users.apellidos) AS nomFiscal'))
       ->first();

       return $fiscal;
    }

    public static function comprobacionRepuve($placa=0,$serie=0){
        $array = array ();
        if($placa != 0 || $serie != 0) {

            $busqueda;
            if($placa != '0'){
                $busqueda = '||'.$placa.'|||||';
            }
            else if($serie != '0'){
                $busqueda = $serie.'|||||||';
            }
            else{
                $busqueda = $serie.'||'.$placa.'|||||';
            }

            // $data = [
            //  'Cuenta' => 'FiscaliaCI',
            //  'Password'   => 'z8H9E)86',
            //  'Cadena'     => $busqueda
            // ];

            try {
                //$client = new SoapClient("http://10.27.60.69/Ws2.asmx?WSDL");
                //$response = $client->__soapCall("DoConsRepRoboCamp", array($data));
                //$response = $client->DoConsRepRoboCamp($data);


                $ch = curl_init('http://192.108.22.227/script_repuve/repuve.php?q='.$busqueda);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
                curl_setopt($ch, CURLOPT_HTTPHEADER, ['Accept: application/json']);

                $response = curl_exec($ch);
                $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                $data = json_decode($response, true);
                //dd($ch, $response, $httpCode, $data);
                //dd($data['DoConsRepRoboCampResult']);


                if($data['DoConsRepRoboCampResult']!='ERR:402'){
                    $arregloInfo = explode('|',$data['DoConsRepRoboCampResult']);
                    $elementos = ['IdEstatus','Fecha Robo','Placa','VIN','Fuente de Robo','Número de Averiguación',
                        'Agente de Ministerio Público','Fecha de Averiguación','Marca','SubMarca','Modelo','Color'];

                    for($i=0; $i<count($elementos); $i++){
                        $array[$elementos[$i]]=$arregloInfo[$i];
                    }
                }else{
                    $array['IdEstatus']=$data['DoConsRepRoboCampResult'];
                }


            } catch (\Exception $e) {
                //return $e->getMessage();
                $array['IdEstatus'] = 'ERR:402';
                $array['msj'] = "ERROR :Por el momento, el servicio del Registro Público Vehícular (REPUVE) no esta disponible";
            }

        } else {
            $array['IdEstatus'] = 'ERR:402';
            $array['msj'] ="Se debe de introducir un número de PLACA o de VIN válidos";
        }

        return response()->json($array);
    }

    public static function sessionSistema($urlCodificada){
      $decoficado = base64_decode($urlCodificada);
      $sistema = substr($decoficado,0,strpos($decoficado, '-'));
      $idenCarpt = substr($decoficado,strpos($decoficado, '-')+1,strlen($decoficado));
      switch ($sistema) {
        case 'uipj':
          $registro = DB::connection('uipj')->table('carpeta')->where('id','=',$idenCarpt)->select('numCarpeta')->first();
          $infCarpt = array('access'=>'true','sistema'=>$sistema,'idenCarpeta'=>$registro->numCarpeta, 'id'=>$idenCarpt) ;
          break;
        case 'otros':
          $registro = DB::connection('uipj')->table('carpeta')->where('id','=',$idenCarpt)->select('numCarpeta')->first();
          $infCarpt = array('access'=>'true','sistema'=>$sistema,'idenCarpeta'=>$registro->numCarpeta, 'id'=>$idenCarpt);
          break;
        default:
          //en caso de no venir con la informacion por url
          $infCarpt = array('access'=>'true','sistema'=>'uipj','idenCarpeta'=>'UIPJ/D11/XAL1/1/2/2018', 'id'=>'2');
          break;
      }
      return $infCarpt;
    }





}
