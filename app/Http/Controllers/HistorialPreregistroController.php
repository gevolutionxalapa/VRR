<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class HistorialPreregistroController extends Controller
{
    public function index()
    {
      return view('historial.form.historialpreregistro');
    }

    /* Funcion qu utiliza el datable para cargar datos */
    public function getPreregistros()
    {
      $registros = DB::table('vehiculo')
            ->join('cat_submarca','cat_submarca.id','=','vehiculo.idSubmarca')
            ->join('cat_marca', 'cat_marca.id', '=', 'cat_submarca.idMarca')
            ->join('cat_color','cat_color.id', '=','vehiculo.idColor')
            ->join('cat_tipo_vehiculo','cat_tipo_vehiculo.id','=','vehiculo.idTipoVehiculo')
            ->join('cat_clase_vehiculo','cat_clase_vehiculo.id','=','cat_tipo_vehiculo.idClaseVehiculo')
            ->join('cat_procedencia','cat_procedencia.id','=','vehiculo.idProcedencia')
            ->join('cat_aseguradora','cat_aseguradora.id','=','vehiculo.idAseguradora')
            ->select('vehiculo.*','cat_marca.nombre as marca','cat_submarca.nombre as submarca', 'cat_submarca.idMarca','cat_color.nombre as color','cat_tipo_vehiculo.nombre as tipoVehiculo','cat_clase_vehiculo.nombre as claseVehiculo','cat_clase_vehiculo.id as idClaseVehiculo','cat_procedencia.nombre as procedencia','cat_aseguradora.nombre as aseguradora')
            //->where(array('tipoDenuncia'=>1))
            //->where(array('status'=>1))
            ->where('token','!=', '')
            ->get();

        return response()->json($registros, 200);
    }
}
