<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
//Validador datos vehiculos
use App\Http\Requests\StoreVehiculoRequest;
// Envio de correo
use Illuminate\Support\Facades\Mail;
use App\Mail\MailPredenuncia as sendMail;
use Illuminate\Support\Facades\Storage;

//Call Models
use DB;
use App\Models\Vehiculo;
use App\Models\CatEstado;
use App\Models\CatMunicipio;
use App\Models\CatMarca;
use App\Models\CatSubmarca;
use App\Models\CatAseguradora;
use App\Models\CatProcedencia;
use App\Models\CatColor;
use App\Models\Persona;
use App\Models\DispositivoInfo;
use App\Models\CatDocumentoCirculacion;
use App\Models\RelDocsVehCat;
use App\Models\UbicacionVehiculo;
use App\Models\CatSubCategoriaDocumento as SubCateDoctos;
use SimpleSoftwareIO\QrCode\BaconQrCodeGenerator;
use Carbon\Carbon;
// Trait para cargar de imagenes
use App\Traits\ToolsTrait;

class PreVehiculoController extends Controller
{
	use ToolsTrait;
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */

	/**
	 * Muestra el formulario de la la predenuncia del vehiculo
	 */
	public function showPredenuncia()
	{
		$datosguia = array(
			'tipoVeh'=> 2, //1 nacional 2 extranjero
			'flag_placa' => 1,
			'listaplaca' => 1,
			'latitud' => 0,
			'longitud' => 0,
			'isExtranjera' => 2,
		);

		$estados = CatEstado::all();
		$marcas = CatMarca::all();
		$aseguradoras = CatAseguradora::all();
		$procedencias = CatProcedencia::all();
		$colores = CatColor::all();
		$identificacionoficial = SubCateDoctos::where('idCategoriaDocumento','=','1')->get();
		$factura = SubCateDoctos::where('idCategoriaDocumento','=','2')->get();
		//Pintar mapa
		$config = array();
	    $config['center'] = 'auto';
	    $config['onboundschanged'] = 'if (!centreGot) {
	            var mapCentre = map.getCenter();
	            marker_0.setOptions({
	                position: new google.maps.LatLng(mapCentre.lat(), mapCentre.lng())
	            });
	        }
	        centreGot = true;';

	    app('map')->initialize($config);

	    // set up the marker ready for positioning
	    // once we know the users location
	    $marker = array();
	    app('map')->add_marker($marker);

	    $map = app('map')->create_map();


		$documentosCirculacion = CatDocumentoCirculacion::all()->sortBy('nombre');

			return view('preVehiculo.showPredenuncia', array(
				'estados'=>$estados,
				'marcas'=>$marcas,
				'aseguradoras' => $aseguradoras,
				'colores' => $colores,
				'documentosCirculacion' => $documentosCirculacion,
				'datos'=>$datosguia,
				'identificacionoficial'=>$identificacionoficial,
				'identificacionoficial1'=>$identificacionoficial,
				'factura'=>$factura,
				'factura1'=>$factura,
				'map'=>$map,
			));

	}

	/**
	 * Muestra la guia para poder levantar la predenuncia del vehiculo
	 * @return [view] [guia]
	 */
	 public function showGuia()
	 {
		 // $documentosCirculacion = CatDocumentoCirculacion::all()->sortBy('nombre');

		 $documentosCirculacion = CatDocumentoCirculacion::whereBetween('id',[2,4])->get();
		 return view('preVehiculo.showGuia',array('documentosCirculacion' => $documentosCirculacion,
	 ));
 }

	public function showMap(Request $request)
	{
		//dd($request);
		//$flag_placa = ($request->input('preg6') !=null) ? 1: 2;
		$flag_placa = ($request->input('preg6') ==1) ? 1: 2;
		$tipoVeh = $request->input('tipoVeh') ;
		$listaplaca = ($request->input('listaplaca') != null) ? $request->input('listaplaca'): 1;
		if($listaplaca==1 && $flag_placa==1){
			$isExtranjera = ($flag_placa==1 && $tipoVeh==2)?5:1;
		}else{
			$isExtranjera = '';
		}


		$datosguia = array(
			'tipoVeh'=> $tipoVeh, //1 nacional 2 extranjero
			'flag_placa' => $flag_placa,
			'listaplaca' => $listaplaca,
			'isExtranjera' => ($isExtranjera ==  null ) ? 0 : $isExtranjera
		);

		$config = array();
		$config['center'] = 'Veracruz';
		$config['zoom'] = 7;
		//$config['places'] = TRUE;
		//$config['placesLocation'] = '19.976312,-96.7279684';
		$config['placesRadius'] = 400;

		$config['onclick'] = 'alert(\'You just clicked at: \' + event.latLng.lat() + \', \' + event.latLng.lng());';
		$config['zoomControl'] = false;
		$config['scaleControl'] = true;

	    app('map')->initialize($config);

	    // set up the marker ready for positioning
	    // once we know the users location
	    $marker = array();
		$marker['position'] = '19.976312,-96.7279684';
		$marker['draggable'] = true;
		$marker['ondragend'] = '$("#latMap").val(event.latLng.lat()); $("#lonMap").val(event.latLng.lng());';
		// $marker['ondragend'] = 'alert(\'You just dropped me at: \' + event.latLng.lat() + \', \' + event.latLng.lng());';

	    app('map')->add_marker($marker);

	    $map = app('map')->create_map();
		return view('preVehiculo.showMap', array(
			'map' => $map,
			'datos' => $datosguia,
		));
	}
	/*
	*
	*/
	 public function showIntro()
	{

		return view('preVehiculo.showIntro');
	}


	/**
	 * Recibe los datos de la guia para enviarlos a los
	 * @param  Request $request [campos de la ]
	 * @return [view]           [formulario de la predenuncia del vehiculo]
	 */
	public function recibirDatos(Request $request)
	{
		//dd($request->all());
	  //$tipoVeh = ($request->input('tipoVeh') != null) ? 1 : 2;
		// $flag_placa = ($request->input('preg6') != null) ? 1: 2;
		//$flag_placa = ($request->input('preg6') !=null) ? 1: 2;

		$listaplaca = ($request->input('listaplaca') != null) ? $request->input('listaplaca'): 1;
		$latMap = ($request->input('latMap') != null) ? $request->input('latMap'): 0;
		$lonMap = ($request->input('lonMap') != null) ? $request->input('lonMap'): 0;

		$datosguia = array(
			'tipoVeh'=> $request->input('tipoVeh'), //1 nacional 0 extranjero
			'flag_placa' => $request->input('flag_placa'),
			'listaplaca' => $listaplaca,
			'latitud' => $latMap,
			'longitud' => $lonMap,
			'isExtranjera' => $request->input('isExtranjera')
		);



		//dd($datosguia);

    //	dd($datosguia);
		$estados = CatEstado::all();
		$marcas = CatMarca::all();
		$aseguradoras = CatAseguradora::all();
		$procedencias = CatProcedencia::all();
		$colores = CatColor::all();
		$identificacionoficial = SubCateDoctos::where('idCategoriaDocumento','=','1')->get();
		$factura = SubCateDoctos::where('idCategoriaDocumento','=','2')->get();
		//dd($estados);

		$config = array();
	    $config['center'] = 'auto';
	    $config['onboundschanged'] = 'if (!centreGot) {
	            var mapCentre = map.getCenter();
	            marker_0.setOptions({
	                position: new google.maps.LatLng(mapCentre.lat(), mapCentre.lng())
	            });
	        }
	        centreGot = true;';

	    app('map')->initialize($config);

	    // set up the marker ready for positioning
	    // once we know the users location
	    $marker = array();
	    app('map')->add_marker($marker);

	    $map = app('map')->create_map();

		//dd($identificacionoficia);
		$documentosCirculacion = CatDocumentoCirculacion::all();
			return view('preVehiculo.showPredenuncia', array(
				'estados'=>$estados,
				'marcas'=>$marcas,
				'aseguradoras' => $aseguradoras,
				'procedencias'=>$procedencias,
				'colores' => $colores,
				'documentosCirculacion' => $documentosCirculacion,
				'datos'=>$datosguia,
				'identificacionoficial'=>$identificacionoficial,
				'identificacionoficial1'=>$identificacionoficial,
				'factura'=>$factura,
				'factura1'=>$factura,
				'map'=>$map,
			));


	}




	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function savePredenuncia(StoreVehiculoRequest $request){
		//dd($request->all());
		$placa = ($request->input('placa') == null) ? '0000000': $request->input('placa');
		$idDocumentoCirculacion = ($request->input('idDocumentoCirculacion') == null) ? 1: $request->input('idDocumentoCirculacion');
		$placa_extranjera = ($idDocumentoCirculacion == 5) ? 1: 0;
		$numSerie = ($request->input('numSerie') == null) ? '': $request->input('numSerie');
		$numMotor = ($request->input('numMotor') == null) ? '': $request->input('numMotor');
		$senasPartic = ($request->input('senasPartic') == null) ? '': $request->input('senasPartic');
		$idAseguradora = ($request->input('idAseguradora') == null) ? 1: $request->input('idAseguradora');
		$idProcedencia = ($request->input('idProcedencia') == null) ? 1: $request->input('idProcedencia');
		$folio = ($request->input('folioDoctoCirc') == null) ? '': $request->input('folioDoctoCirc');
		$telefono = ($request->input('telefono') == null) ? '': $request->input('telefono');
    $placa=strtoupper($placa); 

		$token = $this->generateToken();
		$idE = $request->input('idEstado');
		$idM = $request->input('idMunicipio');
		$codigo_qr = $this->qr($token, 200, false, $idE, $idM);
		//save information of Vehicle
		$vehiculo = Vehiculo::create([
			'placa_extranjera' => $placa_extranjera,
			'placas' => $placa,
			'idEstado' => $request->input('idEstado'),
			// 'idEstadoPlaca' => $request->input('idEstado'),
			'idSubmarca' => $request->input('idSubmarca'),
			'idMunicipio' => $request->input('idMunicipio'),
		   // 'idMunicipio' =>33,
			'modelo' => $request->input('modelo'),
			'idColor' => $request->input('idColor'),
			'folioDoctoCirc' => $folio,
			'numSerie' => $numSerie,
			'numMotor' => $numMotor,
			'senasPartic' => $senasPartic,
			'idProcedencia' => $idProcedencia,
			'idAseguradora' => $idAseguradora,
			'idTipoUso' => 99,
			'idTipoVehiculo'=>999,
			'token' => $token,
			'telefono'=>$telefono,
			'idDocumentoCirculacion' => $idDocumentoCirculacion
		]);
		//get information of device
		$vehiculo_id = $vehiculo->id;
		$this->get_info_dispositivo($vehiculo->id, $request->input('lat'), $request->input('long'));
		$ubicacion = UbicacionVehiculo::create([
			'latitud' => $request->input('latitud'),
			'longitud' => $request->input('longitud'),
			'status' => 1,
			'idDeposito' => 5 ,
			'idVehiculo' => $vehiculo_id
		]);

		//here us will defined the folder and sub-folder to save picture
		if($request->input('optCaptura')=='1'){
			if($request->file('ID_OFICIAL')!=null){
				$archivo = $request->file('ID_OFICIAL');
				$test = ToolsTrait::save_image($archivo, $request->input('listaiden1'), $vehiculo_id, 1);

			}
			if($request->file('ID_FACTURA')!=null){
				$archivo = $request->file('ID_FACTURA');
				$test = ToolsTrait::save_image($archivo, $request->input('listaiden3'), $vehiculo_id, 1);

			}
		}
		if($request->input('optCaptura')=='2'){
			//case when are of webcam
			if($request->input('cam_ident')){
				$archivo = $request->input('cam_ident');
				$test = ToolsTrait::save_image($archivo, $request->input('listaiden1'), $vehiculo_id, 2);

			}
			if($request->input('cam_fact')){
				$archivo = $request->input('cam_fact');
				$test = ToolsTrait::save_image($archivo, $request->input('listaiden3'), $vehiculo_id, 2);

			}
		}
		return response()->json(array('token'=> $vehiculo->token,'codigoQR' => $codigo_qr), 200);
	}

	public function mailPredenuncia($correo,$token='Saludos', $idEst=0, $idMun=0){

		$codigoQR = $this->qr($token, 200, false,$idEst, $idMun);
		Mail::to($correo)->send(new sendMail($token, $codigoQR));
		return response()->json('Correo Enviado', 200);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}


	/**
	 * Genera token alfanumerico de 6 digitos para la predenuncia
	 */

	public function generateToken()
	{
		$flag = true;
		$token = '';
		$longitud = 6;
		//Configuracion del token
		$pattern = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$max = strlen($pattern)-1;

		while ($flag) {
			//Crecion del token
			for($i=0;$i < $longitud;$i++) {
				$token .= $pattern{mt_rand(0,$max)};
			}

			//Confirmar que el token no exista
			$dato = Vehiculo::where('token', $token)->value('token');
			if($dato) {
				$token = '';
			} else {
				$flag = false;
			}
		}

		return $token;


	}


	public function get_submarca($marca){
		$submarcas = CatSubmarca::where('idMarca',$marca)->orderBy('nombre','ASC')->get();
		$info=array(
			'status'=>"correcto",
			'estado'=>$submarcas,
		);
		return response()->json($info);

	}

	public function get_municipio($estado){
		$municipios = CatMunicipio::where(array('idEstado'=>$estado))->orderBy('nombre','ASC')->get();

		$info=array(
			'status'=>"correcto",
			'estado'=>$municipios,
		);
		return response()->json($info);
	}

	public function get_subcatdocumento($catdocto){
		$subcatdoctos = SubCateDoctos::where(array('idCategoriaDocumento'=>$catdocto))->orderBy('nombre','ASC')->get();

		$info=array(
			'status'=>"correcto",
			'estado'=>$subcatdoctos,
		);
		return response()->json($info);
	}

	/**
	 * Funcion utilizada para obtener informacion del cliente (usuario)
	 * Se obtiene el navegador, ip, nombre del equipo y sistema operativo
	 * adicionalmente con una peticion ajax se puede obtener la ubicacion
	 * siempre y cuando el usuario lo permita
	 * en caso de que no lo permita, la ubicacion por defecto es 0
	 * @param  Request $request [coordenadas de la ubicacion]
	 */
	public function get_info_dispositivo($id, $latitud=0, $longitud=0)
	{
		$latitud = ($latitud != null) ? $latitud : 0;
		$longitud = ($longitud != null) ? $longitud : 0;

		$info = array(
			'navegador' => $_SERVER['HTTP_USER_AGENT'],
			'ip' => $_SERVER["REMOTE_ADDR"],
			'equipo' => gethostname(),
			'so' => php_uname(),
			'latitud' => $latitud,
			'longitud' => $longitud
		);


		$dispositivo = new DispositivoInfo;
		$dispositivo->navegador = $info['navegador'];
		$dispositivo->ip = $info['ip'];
		$dispositivo->equipo = $info['equipo'];
		$dispositivo->so = $info['so'];
		$dispositivo->latitud = $info['latitud'];
		$dispositivo->longitud = $info['longitud'];
		$dispositivo->idVehiculo = $id;
		$dispositivo->save();


		return response()->json($info, 200);

	}

	/**
	 * funcion que genera un codigo QR
	 * @param  string  $msj  [texto a convertir en msj]
	 * @param  integer $size [tamaño de la imagen QR en pixeles]
	 * @param  integer $tipo [si regresa con la etiqueta img o solo la codificacion en base64]
	 * @return [type]        [imagen del codigo QR]
	 */
	public function qr($msj = '', $size = 300, $tipo = true, $idEst=0, $idMun=0)
	{
		if($idEst == 30){
			$unidad = DB::table('cat_unidad')
				->join('rel_distrito_unidad', 'rel_distrito_unidad.idUnidad', '=', 'cat_unidad.id')
				->join('rel_distrito_municipio','rel_distrito_municipio.idDistrito','=','rel_distrito_unidad.idDistrito')
				->select('cat_unidad.latitud', 'cat_unidad.longitud','cat_unidad.nombre')
				->where(array('rel_distrito_municipio.idMunicipio'=>$idMun))->first();
			$texto = "FOLIO: ".$msj."\nMapa: https://maps.google.com/?ll=".$unidad->latitud.",".$unidad->longitud."&z=18&t=m";
		} else {
			$texto = "FOLIO: ".$msj;
		}

		$size = (($size > 1000) || $size < 100) ? 300 : $size;
		$qrcode = new BaconQrCodeGenerator;
		$img_b64 = base64_encode($qrcode->errorCorrection('H')->format('png')->size($size)->merge('/public/img/iconofge.jpg')->generate($texto));
		//return 'data:image/png;base64,'.$img_b64;
		if($tipo) {
			return '<img src="data:image/png;base64,'.$img_b64.'">';
		} else {
			return $img_b64;
		}
	}
}
