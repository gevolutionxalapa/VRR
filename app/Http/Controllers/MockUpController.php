<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\UipjTrait;
class MockUpController extends Controller
{
   public function Mockup($idCarpeta=null){
   		$carpeta = array(
   			'investigacion' => array (
   				'id' => 1,
   				'numCarpeta' => 'UIPJ/DXI/1/1/2018',
   				'fecha' => '2018-01-16',
   				'hora' => '13:00:02',
   				'nomUnidad' => 'UIPJ Xalapa',
   				'nomCompleto' => 'Unidad Integral de Procuración de Justicia del Distrito Judicial XI de Xalapa',
   				'numFiscal' => 1,
   				'nomFiscal' => 'Juan Pérez Vargas',
   			),
   			'robo' => array(
   				'fecha' => '2018-01-16',
   				'hora' => '02:00:00',
   				'calle' => 'av. arco sur',
   				'numExterno' => 24,
   				'numInterno' => '2B',
   				'colonia' => 'Carolino Anaya',
   				'localidad' => 'Xalapa-Enriquez',
   				'municipio' => 'Veracruz',
   				'cp' => '91159',
   				'descripcionHechos' => 'Estaba comiendo un pan cuando de repente se llevaron mi carro, dos tipos que venían en moto.',
   			),
   			'acusacion' => array(
   				'denunciante' => array(
   					0 => array(
              'nombre' => 'Enrique',
              'primerAp'=> 'González',
              'segundoAp' => 'Arenas',
	   					'calle' => 'Indeco Animas',
	   					'numExterno' => 24,
	   					'numInterno' => '2B',
	   					'colonia' => 'Animas',
		   				'localidad' => 'Xalapa-Enriquez',
		   				'municipio' => 'Veracruz',
		   				'cp' => '91146',
		   				'telefono' => '8104523',
		   				'corre' => 'sin dato',
		   				'sexo' => 'M',
		   				'esEmpresa' => 0,
		   				'rfc' => 'VRR123456',
		   				'curp' => 'VRR12456VZCZD02',
		   			),
		   			// 1 => array(
            //   'nombre' => 'Muaricio',
            //   'primerAp'=> 'Escalante',
            //   'segundoAp' => 'Penoso',
	   				// 	'calle' => 'Indeco Animas',
	   				// 	'numExterno' => 24,
	   				// 	'numInterno' => '2B',
	   				// 	'colonia' => 'Animas',
		   			// 	'localidad' => 'Xalapa-Enriquez',
		   			// 	'municipio' => 'Veracruz',
		   			// 	'cp' => '91146',
		   			// 	'telefono' => '8104523',
		   			// 	'corre' => 'sin dato',
		   			// 	'sexo' => 'M',
		   			// 	'esEmpresa' => 0,
		   			// 	'rfc' => 'VRR123456',
		   			// 	'curp' => 'VRR12456VZCZD02',
		   			// ),
   				),
   				'denunciado' => array(
   					0 => array(
	   					'nombre' => 'Luis Jorge Pablo',
	   					'calle' => 'Indeco Animas',
	   					'numExterno' => 24,
	   					'numInterno' => '2B',
	   					'colonia' => 'Animas',
		   				'localidad' => 'Xalapa-Enriquez',
		   				'municipio' => 'Veracruz',
		   				'cp' => '91146',
		   				'telefono' => '8104523',
		   				'corre' => 'sin dato',
		   				'sexo' => 'M',
		   				'esEmpresa' => 0,
		   				'rfc' => 'VRR123456',
		   				'curp' => 'VRR12456VZCZD02',
		   			),
		   			1 => array(
	   					'nombre' => 'Luis Jorge Pablo',
	   					'calle' => 'Indeco Animas',
	   					'numExterno' => 24,
	   					'numInterno' => '2B',
	   					'colonia' => 'Animas',
		   				'localidad' => 'Xalapa-Enriquez',
		   				'municipio' => 'Veracruz',
		   				'cp' => '91146',
		   				'telefono' => '8104523',
		   				'corre' => 'sin dato',
		   				'sexo' => 'M',
		   				'esEmpresa' => 0,
		   				'rfc' => 'VRR123456',
		   				'curp' => 'VRR12456VZCZD02',
		   			),
   				),
   			),
   			'modus' => array(
   				'conViolencia' => 1,
   				'numVictimas' => 2,
   				'lugarHechos' => 'Calle',
   				'puesto' => 'vendedor',
   				'senasPartic' => 'Tatuaje en el hombro derecho',
   				'vestimenta' => 'Usaba tenis color blanco',
   				'arma' => 'hacha de leñador',
   				'vehiculos' => array(
   					0 => array(
   						'marca' => 'CHEVROLET',
   						'submarca' => 'MATIZE',
   						'placas' => 'ED1E3T',
   						'numSerie' => '11111111111111111',
   						'numMotor' => '1111111111',
   					),
   					1 => array(
   						'marca' => 'NISSAN',
   						'submarca' => 'CENTRA',
   						'placas' => 'POG123',
   						'numSerie' => '3VWFB81H5SM003982',
   						'numMotor' => '845521542',
   					),
   				),
   			),


   		);

   		$carpeta['modus']['numResponsables'] = sizeof($carpeta['acusacion']['denunciado']);

   		if($idCarpeta==null){
   			return response()->json('Estas chavo para ser chavo',419);

   		}else{
   			return response()->json($carpeta,200);
   		}




   }

   public function getDB($idCarpeta){
	   $datos = UipjTrait::get_vehiculo_uipj($idCarpeta);
	   dd($datos);
   }
}
