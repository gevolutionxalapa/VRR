<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function updateFoto(Request $request){
        // dump($request->file);
        $user=User::find(Auth::user()->id);
        if($user->foto!=null || $user->foto!=""){
            unlink(public_path().(DIRECTORY_SEPARATOR.'storage'.DIRECTORY_SEPARATOR.'fotoFiscal'.DIRECTORY_SEPARATOR.$user->foto));
        }
        $file = $request->file;
        $name = 'foto'.$user->nombres.'_'.time().'.'.$file->getClientOriginalExtension();
        $path = public_path().DIRECTORY_SEPARATOR.'storage'.DIRECTORY_SEPARATOR.'fotoFiscal'.DIRECTORY_SEPARATOR;
        $file->move($path, $name);
        $user->foto=$name;
        $user->save();
        return ['success'=>true,'ruta'=>$user->foto];
    }
}
