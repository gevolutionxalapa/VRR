<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Persona;

class PersonaController extends Controller
{
    public function showPersona(){

		//return view('preVehiculo.persona');
		return view('test.persona');
	}


   public function savePersona(Request $request){


   		$persona = new Persona;

   		$persona->nombre = $request->input('nombre');
   		$persona->rfc = $request->input('rfc');
   		$persona->telefono = $request->input('telefono');
   		$persona->tipoPersona = $request->input('tipoPersona');

		$persona->save();

		return response()->json(array('sucess' => true));


   }
}
