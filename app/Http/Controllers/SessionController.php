<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
class SessionController extends Controller
{
    public function sessionUAT(){
    	$uat = array(
    		'id' => 2,
    		'numCarpeta' => 'UAT/2018',
    		'idFiscal' => 2,
    		'session_id' => 'qjRrVAYPFUexCLPJEZUmzLZGDS99lpStGZj5sJZA', 
    		'grupo' => 'facilitador'
    	);
    	
    	$fiscalin =  DB::connection('uat')->table('users')->where('id','=',$uat['idFiscal'])
    	->where('session_id','=',$uat['session_id'])
    	->get();

    	
    	$exist_grupo = ($fiscalin[0]->grupo=='facilitador')?1:0;
		
		if($exist_grupo==0){
			return redirect()->back()->with('error', 'No se pudo establecer sesión.');
		}else{
			$data = Array (
			 	'session_id' => $uat['session_id'],
			 	'email' => $fiscalin[0]->email , 
			 	'password' => $fiscalin[0]->password,
			 	'name' => $fiscalin[0]->username
			 ); 
			//dd($data);
			Session::put('uat',$data);
		}
		$items = Session::get('uat');
		//dd($items);
		//dd($exist_grupo);
    	//return view('sesion');
    	return redirect()->route('showDenuncia')->with('exito', 'Tienes session');
    }
}
