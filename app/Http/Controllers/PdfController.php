<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Vehiculo;
use App\Models\CatMarca;
use App\Models\CatSubMarca;
use App\Models\RelDistritoMunicipio;
use DB;
use App\Traits\UipjTrait;
use App\Traits\ToolsTrait;
//PDF's library for create document relative
use Mpdf\Mpdf;
use SimpleSoftwareIO\QrCode\BaconQrCodeGenerator;

class PdfController extends Controller
{
    //
  use ToolsTrait;
    public function getIndex($folio='Un saludo', $idEst=0, $idMun=0){

      $vehiculo = DB::table('vehiculo')
                ->join('cat_submarca','cat_submarca.id','=','vehiculo.idSubmarca')
                ->join('cat_marca','cat_marca.id','=','cat_submarca.idMarca')
                ->select('cat_marca.nombre as marca','cat_submarca.nombre as submarca','vehiculo.idMunicipio')
                ->where(array('token'=>$folio))
                ->get();


      $unidades = DB::table('cat_unidad')
        ->join('rel_distrito_unidad', 'rel_distrito_unidad.idUnidad', '=', 'cat_unidad.id')
        ->join('rel_distrito_municipio','rel_distrito_municipio.idDistrito','=','rel_distrito_unidad.idDistrito')
        ->select('cat_unidad.nombre','cat_unidad.calle','cat_unidad.colonia','cat_unidad.numero','cat_unidad.idCp as CodPos')
        ->where(array('rel_distrito_municipio.idMunicipio'=>$vehiculo[0]->idMunicipio))->get();

      $msj = $folio;
      $size = 300;
      $size = (($size > 1000) || $size < 100) ? 300 : $size;
      $qrcode = new BaconQrCodeGenerator;
      if($idEst == 30){
			$unidad = DB::table('cat_unidad')
				->join('rel_distrito_unidad', 'rel_distrito_unidad.idUnidad', '=', 'cat_unidad.id')
				->join('rel_distrito_municipio','rel_distrito_municipio.idDistrito','=','rel_distrito_unidad.idDistrito')
				->select('cat_unidad.latitud', 'cat_unidad.longitud','cat_unidad.nombre')
				->where(array('rel_distrito_municipio.idMunicipio'=>$idMun))->first();
			$msj = "FOLIO: ".$folio."\nMapa: https://maps.google.com/?ll=".$unidad->latitud.",".$unidad->longitud."&z=18&t=m";
		} else {
			$msj = "FOLIO: ".$folio;
		}
      $img_b64 = base64_encode(
                $qrcode
                ->errorCorrection('H')
                ->format('png')
                ->size($size)
                ->merge('/public/img/iconofge.jpg')
                ->generate($msj));
      // return view('pdf.index',array('QRCode'=>$img_b64));
      $html = view('pdf.index',array('QRCode'=>$img_b64,'Folio'=>$folio,'vehiculo'=>$vehiculo, 'unidades'=>$unidades))->render();
      //$stylesheet = file_get_contents(_BASE_PATH.'bootstrap.css');
      $namefile = 'formato_'.time().'.pdf';
      $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
      $fontDirs = $defaultConfig['fontDir'];
      $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
      $mpdf = new Mpdf([
           'fontDir' => array_merge($fontDirs, [
               public_path() . '/fonts',
           ]),
           'default_font' => 'arial',
            //"format" => [264.8,188.9],
           'format'=>'a4-L',

       ]);
       $mpdf->SetDisplayMode('fullpage');
       //$mpdf->WriteHTML($stylesheet, 1);
       $mpdf->WriteHTML($html);
       $mpdf->Output($namefile,"I");
    }



    public function reporte($idVeh){
        $vehiculo = DB::table('vehiculo')
            ->join('cat_estado', 'cat_estado.id', '=', 'vehiculo.idEstado')
            ->join('cat_submarca','cat_submarca.id','=','vehiculo.idSubmarca')
            ->join('cat_marca', 'cat_marca.id', '=', 'cat_submarca.idMarca')
            ->join('cat_color','cat_color.id', '=','vehiculo.idColor')
            ->join('cat_tipo_vehiculo','cat_tipo_vehiculo.id','=','vehiculo.idTipoVehiculo')
            ->join('cat_clase_vehiculo','cat_clase_vehiculo.id','=','cat_tipo_vehiculo.idClaseVehiculo')
            ->join('cat_tipo_uso','cat_tipo_uso.id','=','vehiculo.idTipoUso')
            ->join('cat_procedencia','cat_procedencia.id','=','vehiculo.idProcedencia')
            ->join('cat_aseguradora','cat_aseguradora.id','=','vehiculo.idAseguradora')
            ->join('rel_vehiculo_carpeta','rel_vehiculo_carpeta.idVehiculo','=','vehiculo.id')
            ->select('vehiculo.*','cat_marca.nombre as marca','cat_submarca.nombre as submarca', 'cat_submarca.idMarca','cat_color.nombre as color','cat_tipo_vehiculo.nombre as tipoVehiculo','cat_clase_vehiculo.nombre as claseVehiculo','cat_clase_vehiculo.id as idClaseVehiculo','cat_tipo_uso.nombre as tipoUSo','cat_procedencia.nombre as procedencia','cat_aseguradora.nombre as aseguradora',
            'numCarpeta', 'idCarpeta', 'cat_estado.nombre as estado')
            ->where(array('vehiculo.id'=>$idVeh))->first();
        // dd($vehiculo->idCarpeta);
        $datos = UipjTrait::get_datos_uipj($vehiculo->idCarpeta);
        // dd($datos);


        $size = 300;
        $size = (($size > 1000) || $size < 100) ? 300 : $size;
        //$html = view('pdf.reportevehiculo',array('investigacion'=>$data->investigacion,'robo'=>$data->robo,'acusacion'=>$data->acusacion,'modus'=>$data->modus))->render();
        $html = view('pdf.reportevehiculo',array('datosUIPJ'=>$datos, 'datosVehiculo'=>$vehiculo))->render();
        //$stylesheet = file_get_contents(_BASE_PATH.'bootstrap.css');
        $namefile = 'formato_'.time().'.pdf';
        $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];
        $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
        $mpdf = new Mpdf([
            'fontDir' => array_merge($fontDirs, [
                public_path() . '/fonts',
            ]),
            'default_font' => 'arial',
                //"format" => [264.8,188.9],
            'format'=>'legal',

        ]);
        $mpdf->SetDisplayMode('fullpage');
        //$mpdf->WriteHTML($stylesheet, 1);
        $mpdf->WriteHTML($html);
        $mpdf->Output($namefile,"I");

    }

    public function reporteAltaVehiculo ($idVeh){
        $vehiculo = DB::table('vehiculo')
            ->join('cat_estado', 'cat_estado.id', '=', 'vehiculo.idEstado')
            ->join('cat_submarca','cat_submarca.id','=','vehiculo.idSubmarca')
            ->join('cat_marca', 'cat_marca.id', '=', 'cat_submarca.idMarca')
            ->join('cat_color','cat_color.id', '=','vehiculo.idColor')
            ->join('cat_tipo_vehiculo','cat_tipo_vehiculo.id','=','vehiculo.idTipoVehiculo')
            ->join('cat_clase_vehiculo','cat_clase_vehiculo.id','=','cat_tipo_vehiculo.idClaseVehiculo')
            ->join('cat_tipo_uso','cat_tipo_uso.id','=','vehiculo.idTipoUso')
            ->join('cat_procedencia','cat_procedencia.id','=','vehiculo.idProcedencia')
            ->join('cat_aseguradora','cat_aseguradora.id','=','vehiculo.idAseguradora')
            ->join('rel_vehiculo_carpeta','rel_vehiculo_carpeta.idVehiculo','=','vehiculo.id')
            ->join('ubicacion_vehiculo','ubicacion_vehiculo.idVehiculo','=','vehiculo.id')
            ->join('cat_deposito','cat_deposito.id','=','ubicacion_vehiculo.idDeposito')
            ->select('vehiculo.*','cat_marca.nombre as marca','cat_submarca.nombre as submarca', 'cat_submarca.idMarca','cat_color.nombre as color','cat_tipo_vehiculo.nombre as tipoVehiculo','cat_clase_vehiculo.nombre as claseVehiculo','cat_clase_vehiculo.id as idClaseVehiculo','cat_tipo_uso.nombre as tipoUSo','cat_procedencia.nombre as procedencia','cat_aseguradora.nombre as aseguradora',
            'numCarpeta', 'idCarpeta', 'cat_estado.nombre as estado', 'cat_deposito.nombre as deposito')
            ->where(array('vehiculo.id'=>$idVeh))->first();

            //dd($vehiculo);

        $datos = UipjTrait::get_datos_uipj($vehiculo->idCarpeta);

        // dd($datos);

        $size = 300;
        $size = (($size > 1000) || $size < 100) ? 300 : $size;
        $html = view('pdf.altavehiculorecuperado',array('datosUIPJ'=>$datos, 'datosVehiculo'=>$vehiculo))->render();
        $namefile = 'formato_'.time().'.pdf';
        $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];
        $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
        $mpdf = new Mpdf([
             'fontDir' => array_merge($fontDirs, [
                 public_path() . '/fonts',
             ]),
             'default_font' => 'arial',
             'format'=>'legal',
         ]);
         $mpdf->SetDisplayMode('fullpage');
         $mpdf->WriteHTML($html);
         $mpdf->Output($namefile,"I");
    }

    public function reporteCoincidencias (Request $request){

        // dd($request->all());
        // $data = $request->input('dataVehiculo');
        // $data = json_decode($data);
        // dd($data);
      $robados = ToolsTrait::getRobados($request->input('sendPlaca'),$request->input('sendSerie'),$request->input('sendMotor'),$request->input('sendToken'));
      $involucrados = ToolsTrait::getRegistrosUipj($request->input('sendPlaca'),$request->input('sendSerie'),$request->input('sendMotor'),2);
      $recuperados = ToolsTrait::getRecuperados($request->input('sendPlaca'),$request->input('sendSerie'),$request->input('sendMotor'),$request->input('sendToken'));
      //dd($recuperados);

      //$robados = json_encode($robados);
      $size = 300;
      $size = (($size > 1000) || $size < 100) ? 300 : $size;
      $html = view('pdf.reportecoincidencias',array('robados'=>$robados->original,'involucrados'=>$involucrados->original,'recuperados'=>$recuperados->original))->render();
      //$stylesheet = file_get_contents(_BASE_PATH.'bootstrap.css');
      $namefile = 'formato_'.time().'.pdf';
      $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
      $fontDirs = $defaultConfig['fontDir'];
      $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
      $mpdf = new Mpdf([
           'fontDir' => array_merge($fontDirs, [
               public_path() . '/fonts',
           ]),
           'default_font' => 'arial',
            //"format" => [264.8,188.9],
           'format'=>'a4-L',

       ]);
       $mpdf->SetDisplayMode('fullpage');
       //$mpdf->WriteHTML($stylesheet, 1);
       $mpdf->WriteHTML($html);
       $mpdf->Output($namefile,"I");

    }
}
