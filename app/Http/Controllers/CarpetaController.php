<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Vehiculo;
use App\Models\RelVehiculoCarpeta;
use Alert;
use Illuminate\Support\Facades\Auth;
use App\Models\Bitacora;

//use Storage;

class CarpetaController extends Controller
{
    public function showCarpeta(){
    	//return view('/carpeta.carpeta');
    	//return 'Hola';
    	$file=array(
    	  	'idCarpeta' => '2',
    	  	'placas' =>'XZV900',
    	  	'idEstado' => '30',
    	  	'idSubmarca' => '11192',
    	  	'modelo' => '1056',
    	  	'idColor' => '5',
    	  	'numSerie' => '52651',
    	  	'numMotor' => '4455',
    	  	'permiso' => '784KL',
    	  	'idTipoVehiculo' => '7',
    	  	'idTipoUso' => '8',
    	  	'idAseguradora' => '17',
    	  	'status' => '1',
    	  	'senasPartic' => 'GOLPE EN LA DEFENSA LADO DERECHO',
    	  	'numFiscal' => '2',
    	  	'idUnidad' => '11',
    	  	'nivel' => '2',
    	  	'tokenSesion' => 'wewdnqw263431bgg31h'



    	  
    	);
		$resultCount = DB::table('vehiculo')
			->where('numSerie', '=',$file['numSerie'])
			->orWhere('numMotor','=',$file['numMotor'])
			->count();
		//dd($resultCount);
		if($resultCount == 0){
			
			//Vehiculos
			$vehiculo = new Vehiculo();
	        $vehiculo->placas = $file['placas'];
	        $vehiculo->idEstado = $file['idEstado'];
	        $vehiculo->idSubmarca = $file['idSubmarca'];
	        $vehiculo->modelo = $file['modelo'];
	        $vehiculo->idColor = $file['idColor'];
	        $vehiculo->numSerie = $file['numSerie'];
	        $vehiculo->numMotor = $file['numMotor'];
	        $vehiculo->permiso = $file['permiso'];
	        $vehiculo->idTipoVehiculo = $file['idTipoVehiculo'];
	        $vehiculo->idTipoUso = $file['idTipoUso'];
	        $vehiculo->idAseguradora = $file['idAseguradora'];
	        $vehiculo->status = $file['status'];
	        $vehiculo->senasPartic = $file['senasPartic'];
	        $vehiculo->save();
	        
	        //Carpeta
	        $idVeh = $vehiculo->id; /*DB::table('vehiculo')
	        	->where('numSerie','=',$file['numSerie'])
	        	->value('id')*/;
	        //dd("Datos guardados");
	        $carpeta = new RelVehiculoCarpeta();
	        $carpeta->numCarpeta = $file['idCarpeta'];
	        $carpeta->numFiscal = $file['numFiscal'];
	        $carpeta->idUnidad = $file['idUnidad'];
	        $carpeta->nivel = $file['nivel'];
	        $carpeta->tokenSesion = $file['tokenSesion'];
	        $carpeta->idVehiculo = $idVeh;
	        $carpeta->save();
	        Bitacora::create(['idUsuario' => Auth::user()->id, 'tabla' => 'vehiculo', 'accion' => 'insert', 'descripcion' => 'Se ha registrado un vehiculo proveniente de carpetas.', 'idFilaAccion' => $vehiculo->id]);
	        
	        return response()->json(array('success' => 'Datos guardados'));
	        //Alert::success('Datos guardados', 'Hecho')->persistent("Aceptar");
		}/**/else{

			$resultVeh = DB::table('vehiculo')
	        ->where('numSerie','=',$file['numSerie'])
	        ->value('id');
	        //dd($resultVeh);
	        $carpeta = new RelVehiculoCarpeta();
	        $carpeta->numCarpeta = $file['idCarpeta'];
	        $carpeta->idVehiculo = $resultVeh;
	        $carpeta->numFiscal = $file['numFiscal'];
	        $carpeta->idUnidad = $file['idUnidad'];
	        $carpeta->nivel = $file['nivel'];
	        $carpeta->tokenSesion = $file['tokenSesion'];
	        $carpeta->status = 2;
	        $carpeta->save();
	        Bitacora::create(['idUsuario' => Auth::user()->id, 'tabla' => 'carpeta', 'accion' => 'update', 'descripcion' => 'Se ha realizado vinculacion de vehiculo con carpeta.', 'idFilaAccion' => $carpeta->id]);
	        //dd($file);
	        $vehiculo = CarpetaController::getVehiculo($file,$resultVeh);
	        //return view('carpeta')->with('archivo', $vehiculo);
	        if($vehiculo==1){
	        	$msj = "Los datos son similares";
	        }else{
	        	$msj = "Existen datos diferentes";
	        }
	        return response()->json(array('success' => $msj));
		}
	
		//dd($numSerie);
    }

    public static function getVehiculo($contents,$resultVeh){
    	$idVeh = $resultVeh;
    	//echdd($contents);
		/*$vehiculo = DB::table('vehiculo')
			->join('cat_estado','cat_estado.id', '=', 'vehiculo.idEstado')
			->join('cat_submarca','cat_submarca.id','=','vehiculo.idSubmarca')
			->join('cat_color','cat_color.id','=','vehiculo.idColor')
			->join('cat_tipo_vehiculo','cat_tipo_vehiculo.id','=','vehiculo.idTipoVehiculo')
			->join('cat_tipo_uso','cat_tipo_uso.id','=','')*/
		$comparacion = DB::table('vehiculo')
			->where('id', '=', $idVeh)
			->where('placas', '=', $contents['placas'])
			->where('idEstado', '=', $contents['idEstado'])
			->where('idSubmarca', '=', $contents['idSubmarca'])
			->where('modelo', '=', $contents['modelo'])
			->where('idColor', '=', $contents['idColor'])
			->where('numSerie', '=', $contents['numSerie'])
			->where('numMotor', '=', $contents['numMotor'])
			->where('idTipoVehiculo', '=', $contents['idTipoVehiculo'])
			->where('idAseguradora', '=', $contents['idAseguradora'])
			->count();
	return  $comparacion;
	}

	public function turnadoFiscal($numFiscal,$numFiscalNew){
		$turnado = RelVehiculoCarpeta::where('numFiscal',$numFiscal)
				->update(['numFiscal' => $numFiscalNew]);

		
		if($turnado){

			$msj = "Turnado exitoso: ".$numFiscalNew;
			Bitacora::create(['idUsuario' => Auth::user()->id, 'tabla' => 'carpeta', 'accion' => 'update', 'descripcion' => 'Se ha registrado turnado de fiscal.', 'idFilaAccion' => $numFiscalNew]);
		}else{
			$msj = "Turnado no exitoso";
		}
		return response()->json(array('success' => $msj));
	}
}
