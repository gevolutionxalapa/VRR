<?php

namespace App\Http\Controllers;
use App\Models\VehiculosEntregados;
use Illuminate\Http\Request;
use DB;
use App\Models\CatEstado;
use App\Models\VehEntregadosBus;
use App\Models\CatSubCategoriaDocumento as SubCateDoctos;
use App\Traits\UipjTrait;
use App\Traits\UatTrait;
use App\Traits\BusTrait;
use App\Models\Bitacora;
use Illuminate\Support\Facades\Auth;



class VehEntregadoController extends Controller
{
  use UipjTrait;

  public function index(){
    return view('vehEntrega.index');
  }

  public function showVehEntregados () {
      $estados = CatEstado::all();
       $identificacionoficial = SubCateDoctos::where('idCategoriaDocumento','=','1')->get()->sortBy('nombre');
    return view('vehEntrega.showVehEntregados', array(
        'estados'=>$estados,
        'identificacionoficial'=>$identificacionoficial,
            'identificacionoficial1'=>$identificacionoficial,
    ));
	}

  public function saveVehEntregado(Request $request){

  	//dd($request->input('idAutoridad'));
    $numInt = ($request->input('numInt')==null)?'':$request->input('numInt');
    $status=DB::table('vehiculo')
    ->where('id','=',$request->input('idVeh'))
    ->update(['status'=>3]);

    $vehiculo = DB::table('vehiculo')
      ->where('id','=', $request->input('idVeh'))
      ->select('llave_externa')
      ->first();
 
    $datos_vehiculo = array (
      'idVeh'=> $request->input('idVeh'),
			'SERIE_ALTERADA' => $request->input('idSerieAlterada'),
			'MOTOR_ALTERADO' => $request->input('idMotorAlterado'),
			'CALLE_ENTREGA' => $request->input('calle'),
			'NUMEXT_ENTREGA' => $request->input('numExt'),
			'NUMINT_ENTREGA' => $numInt,
			'COLONIA_ENTREGA' => $request->input('colonia'),
			'ID_MUNICIPIO_ENTREGA' => $request->input('idMunicipio'),
			'ID_ENTIDAD_ENTREGA' => $request->input('idEstado'),
			'CP_ENTREGA' => $request->input('cp'),
      'ID_FUENTE_ENTREGA' => $request->input('idAutoridad'),
			'REFERENCIA_ENTREGA' => $request->input('refeEntrega'),
			'INSPECCION' => $request->input('idInspeccion'),
			'FECHA_ENTREGA' => $request->input('idFechaEntrega'),
			'HORA_ENTREGA' =>$request->input('idHoraEntrega'),
			'SERIE' => $request->input('numSerie'),
			'MOTOR' => $request->input('motor'),
			'MONTO_VEHICULO' => $request->input('idEvaluacion'),
			'FACTURA_VEHICULO' => $request->input('idfacturaV'),
			'FECHA_FACTURA' => $request->input('idFechaFactura'),
      'PERSONA_ENTREGA' => $request->input('idPersonaEntrega'),
      'LLAVE_EXTERNA' => $vehiculo->llave_externa
    );

		VehiculosEntregados::create($datos_vehiculo);
		BusTrait::conversion_bus_entregados(null, $datos_vehiculo);

    Bitacora::create(['idUsuario' => Auth::user()->id, 'tabla' => 'vehiculo_entregado', 'accion' => 'insert', 'descripcion' => 'Se ha realizado registro de un vehiculo entregado.', 'idFilaAccion' =>$request->input('idVeh')]);

  	return response()->json('Registro creado exitosamente', 200);
  }

  public function getVehEntregados () {
    $vehiculo = DB::table('vehiculos_entregados')
      ->join('vehiculo', 'vehiculo.id', '=', 'vehiculos_entregados.idVeh')
      ->join('cat_estado', 'cat_estado.id', '=', 'vehiculos_entregados.ID_ENTIDAD_ENTREGA')
      ->join('cat_municipio', 'cat_municipio.id', '=', 'vehiculos_entregados.ID_MUNICIPIO_ENTREGA')
      ->select('vehiculos_entregados.*', 'cat_estado.nombre as estado', 'cat_municipio.nombre as municipio')
      ->get();
    return response()->json($vehiculo);
  }

  public function getPersonas($persona,$idCarpeta){
      $personaEntrega = UipjTrait::getPersona($persona,$idCarpeta);
      //dd($personaEntrega);
      return $personaEntrega;
  }

  public static function validacionEntregados($idVeh){
    $estatusBus = VehEntregadosBus::where('idVeh','=',$idVeh)
      ->where('status_muestra','=',0)
      ->select('busFGE','busCNS','busCNSKey')
      ->get();
    
    return $estatusBus;
  }

  public static function validacionEntregadosData($idVeh){

    $data = DB::table('resultado_operacion_error')
    ->join('vehiculo_entregado_bus','vehiculo_entregado_bus.llave_externa','=','resultado_operacion_error.LLAVE' )
    ->join('vehiculo','vehiculo.id','=','vehiculo_entregado_bus.idVeh')
    ->where('vehiculo.id','=',$idVeh)
    ->where('vehiculo_entregado_bus.status_muestra','=',0)
    ->where('resultado_operacion_error.TIPO_INFORMACION','=','ENTREGADO')
    ->select('resultado_operacion_error.DESC_ERROR','resultado_operacion_error.LLAVE')
    ->get();
    
    return $data;
  }

  public function actualizarValidacionEntregados(Request $request){
    //dd($request->all());

    //Se marca el status_muestra como procesado 
    VehEntregadosBus::where('idVeh','=',$request->input('id1'))
      ->update(['status_muestra'=>1]);

    //Se obtiene la llave para ser insertada nuevamente en el bus
    // $vehiculo = DB::table('vehiculo')
    //   ->where('id','=', $request->input('id1'))
    //   ->select('llave_externa')
    //   ->first();
      //dd($vehiculo);
    //Arreglo que contiene los datos a actualizar
    $datos_vehiculo = array (
			'idVeh' => $request->input('id1'),
			'SERIE_ALTERADA' => $request->input('idSerieAlterada1'),
			'MOTOR_ALTERADO' => $request->input('idMotorAlterado1'),
			'CALLE_ENTREGA' => $request->input('calle1'),
			'NUMEXT_ENTREGA' => $request->input('numExt1'),
			'NUMINT_ENTREGA' => $request->input('numInt1'),
			'COLONIA_ENTREGA' => $request->input('colonia1'),
			'ID_MUNICIPIO_ENTREGA' => $request->input('idMunicipio1'),
			'ID_ENTIDAD_ENTREGA' => $request->input('idEstado1'),
			'CP_ENTREGA' => $request->input('cp1'),
			'REFERENCIA_ENTREGA' => $request->input('refeEntrega1'),
			'INSPECCION' => $request->input('idInspeccion11'),
			'FECHA_ENTREGA' => $request->input('idFechaEntrega1'),
			'HORA_ENTREGA' =>$request->input('idHoraEntrega1'),
			'SERIE' => $request->input('numSerie1'),
			'MOTOR' => $request->input('motor1'),
			'MONTO_VEHICULO' => $request->input('idEvaluacion1'),
			'FACTURA_VEHICULO' => $request->input('idfacturaV1'),
			'FECHA_FACTURA' => $request->input('idFechaFactura1'),
      'PERSONA_ENTREGA' =>2 //$request->input('idPersonaEntrega1')
      //'LLAVE_EXTERNA' => $request->input('llaveExterna')
    );

    //Se actualizan los datos del vehiculo
    DB::table('vehiculos_entregados')
      ->where('idVeh','=',$request->input('id1'))
      ->update($datos_vehiculo);
    $datos_vehiculo['LLAVE_EXTERNA']=$request->input('llaveExterna');
    //Se agrega el registro al bus para volver a ser procesado 
    BusTrait::conversion_bus_entregados(null, $datos_vehiculo,2);

    Bitacora::create(['idUsuario' => Auth::user()->id, 'tabla' => 'vehiculo_entregado', 'accion' => 'update', 'descripcion' => 'Se ha realizado una modificaacion en el registro de un vehiculo entregado.', 'idFilaAccion' =>$request->input('id1')]);

    return response()->json('El registro se actualiz&oacute; correctamente', 200);
  }

  
}
