<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Artisaninweb\SoapWrapper\SoapWrapper;
use SoapClient;
use App\Traits\ToolsTrait;

class SoapController extends Controller
{
	use ToolsTrait;
	/**
	 * @var SoapWrapper
	*/
	protected $soapWrapper;

	/**
	 * SoapController constructor.
	*
	* @param SoapWrapper $soapWrapper
	*/
	//public function __construct(SoapWrapper $soapWrapper)
	public function __construct()
	{
		// $this->soapWrapper = $soapWrapper;
	}

	public function consultaRepuve($placa=0,$serie=0)
	{

		$comprobacion = ToolsTrait::comprobacionRepuve($placa, $serie);
    	return $comprobacion;
		// $array = array ();
		// if($placa != 0 || $serie != 0) {

		// 	$busqueda;
		// 	if($placa != '0'){
		// 		$busqueda = '||'.$placa.'|||||';
		// 	}
		// 	else if($serie != '0'){
		// 		$busqueda = $serie.'|||||||';
		// 	}
		// 	else{
		// 		$busqueda = $serie.'||'.$placa.'|||||';
		// 	}

		// 	// $data = [
		// 	// 	'Cuenta' => 'FiscaliaCI',
		// 	// 	'Password'   => 'z8H9E)86',
		// 	// 	'Cadena'     => $busqueda
		// 	// ];

		// 	try {
		// 		//$client = new SoapClient("http://10.27.60.69/Ws2.asmx?WSDL");
		// 		//$response = $client->__soapCall("DoConsRepRoboCamp", array($data));
		// 		//$response = $client->DoConsRepRoboCamp($data);
				

		// 		$ch = curl_init('http://192.108.22.227/script_repuve/repuve.php?q='.$busqueda);
  //       		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  //   			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
  //   			curl_setopt($ch, CURLOPT_HTTPHEADER, ['Accept: application/json']);

		// 		$response = curl_exec($ch);
		// 		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		// 		$data = json_decode($response, true);
		// 		//dd($ch, $response, $httpCode, $data);
		// 		//dd($data['DoConsRepRoboCampResult']);


		// 		if($data['DoConsRepRoboCampResult']!='ERR:402'){
		// 			$arregloInfo = explode('|',$data['DoConsRepRoboCampResult']);
		// 			$elementos = ['IdEstatus','Fecha Robo','Placa','VIN','Fuente de Robo','Número de Averiguación',
		// 				'Agente de Ministerio Público','Fecha de Averiguación','Marca','SubMarca','Modelo','Color'];

		// 			for($i=0; $i<count($elementos); $i++){
		// 				$array[$elementos[$i]]=$arregloInfo[$i];
		// 			}
		// 		}else{
		// 			$array['IdEstatus']=$data['DoConsRepRoboCampResult'];
		// 		}


		// 	} catch (\Exception $e) {
		// 		//return $e->getMessage();
		// 		$array['IdEstatus'] = 'ERR:402';
		// 		$array['msj'] = "ERROR :Por el momento, el servicio del Registro Público Vehícular (REPUVE) no esta disponible";
		// 	}

		// } else {
		// 	$array['IdEstatus'] = 'ERR:402';
		// 	$array['msj'] ="Se debe de introducir un número de PLACA o de VIN válidos";
		// }

		// return response()->json($array);
	}
	
	public function consultaSAT($rfc_emisor, $rfc_receptor, $importe, $uuid)
	{
		try {
    		$soapclient = new Soapclient("https://consultaqr.facturaelectronica.sat.gob.mx/consultacfdiservice.svc?wsdl"); 

			// $rfc_emisor = "HERY970831EU9"; 
			// $rfc_receptor = "FGE1501303L2"; 
			// $importe = "662.00"; 
			// $uuid = "F733864F-2519-41D7-9244-73202F379D77";

			$factura = "?re=$rfc_emisor&rr=$rfc_receptor&tt=$importe&id=$uuid";
			$prm = array('expresionImpresa'=>$factura); 
			$buscar=$soapclient->Consulta($prm); 

			$result = array (
				'codigo' => $buscar->ConsultaResult->CodigoEstatus,
				'estado' => $buscar->ConsultaResult->Estado
			);

			return response()->json($result, 200);

		} catch (\Exception $e) {
			//return $e->getMessage();
			return response()->json('ERROR :Por el momento, el servicio del SAT no esta disponible', 421);
		}
	}

}
