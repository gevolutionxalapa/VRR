<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\CatUnidad;
use App\Models\RelDistritoMunicipio;
use App\Models\RelDistritoUnidad;

class ExamenController extends Controller
{
    //
    public function niv($num_serie){
      $serie = str_split(strtoupper($num_serie));
      $val_serie = array();
      $val_abc = true;
      $tabla = array(
        'A'=>1,'B'=>2,'C'=>3,'D'=>4,'E'=>5,'J'=>1,'K'=>2,'L'=>3,'M'=>4,'N'=>5,
        'S'=>2,'T'=>3,'U'=>4,'V'=>5,'W'=>6,'X'=>7,'Y'=>8,'Z'=>9,'F'=>6,'G'=>7,
        'H'=>8,'P'=>7,'R'=>9,
      );
      $letras_na = array('I','O');
      $num_posicion = array(8,7,6,5,4,3,2,10,0,9,8,7,6,5,4,3,2);
      $mjs = '';
      $status = 201;
      $mjsSta = "";
      if(count($serie) == 17){
        foreach ($serie as $element) {
          array_push($val_serie,(isset($tabla[$element]))?(string)$tabla[$element]:(string)$element);
        }
        foreach ($letras_na as $letra) {
          if(in_array($letra,$val_serie))
            $val_abc = false;
        }
        if($val_abc){
          $suma = 0;
          for ($i=0; $i < count($num_posicion) ; $i++) {
            $suma+=$num_posicion[$i]*$val_serie[$i];
          }
          $residuo = $suma%11;
          $mjs = ($residuo == $serie[8] || $residuo == 10)?'La serie del vehiculo es valida':'La serie del vehiculo es invalida';
          $status = ($residuo == $serie[8] || $residuo == 10)?200:201;
          $mjsSta = ($residuo == $serie[8] || $residuo == 10)?'correcto':'error';
        }else{
          $mjs = 'Existe una letra incorrecta en la serie';
          $mjsSta = "error";
          $status = 201;
        }
      }else {
        $mjs = 'El numero de serie no tienen una longitud correcta';
        $mjsSta = "error";
        $status = 201;
      }
      $reques = array('mensaje'=>$mjs,'status'=>$mjsSta);
      return response()->json(array('request'=>$reques), $status);
    }

    public function get_position($idMunicipio){
      //echo $idMunicipio;
       $unidades = DB::table('cat_unidad')
        ->join('rel_distrito_unidad', 'rel_distrito_unidad.idUnidad', '=', 'cat_unidad.id')
        ->join('rel_distrito_municipio','rel_distrito_municipio.idDistrito','=','rel_distrito_unidad.idDistrito')
        ->select('cat_unidad.latitud', 'cat_unidad.longitud','cat_unidad.nombre')
        ->where(array('rel_distrito_municipio.idMunicipio'=>$idMunicipio))->get();

        // $info=array(
        //             'status'=>"correcto",
        //             'codigo'=>201,
        //             'estado'=>$unidades,
        // );


      return response()->json($unidades, 201);
       // return response()->json($info);
    }
}
