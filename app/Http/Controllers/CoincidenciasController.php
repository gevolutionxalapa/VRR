<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Vehiculo;
use App\Models\RelVehiculoCarpeta;
use DB;
use App\Models\Bitacora;
use App\Traits\ToolsTrait;
//use App\Traits\BusTrait;

class CoincidenciasController extends Controller
{
    use ToolsTrait;

    public function getVehRobados($placa, $serie, $motor,$token=null){
    	//dd($placa);
        $robados = ToolsTrait::getRobados($placa, $serie, $motor,$token);
    	return $robados;
    }

    public function getCountVehRobados($placa, $serie, $motor,$token=null){
    	$countRobados = ToolsTrait::getCountRobados($placa, $serie, $motor,$token);

    	return $countRobados;
    }

    public function getVehRecuperados($placa, $serie, $motor,$token=null){
    	//dd($placa);
    	$recuperados = ToolsTrait::getRecuperados($placa, $serie, $motor,$token);

    	return $recuperados;
    }

    public function getVehInvolucrados($placa, $serie, $motor){
    	//dd($placa);
    	$involucrados = ToolsTrait::getRegistrosUipj($placa, $serie, $motor);

    	return $involucrados;
    }

    public function getCountVehInvolucrados($placa, $serie, $motor){
    	$countInvolucrados = ToolsTrait::getCountInvolucrados($placa, $serie, $motor);

    	return $countInvolucrados;
    }

    public function getCountVehRecuperados($placa, $serie, $motor,$nrpv ,$token=null){
        $countRecuperados = ToolsTrait::getCountRecuperados($placa, $serie, $motor,$nrpv);

        return $countRecuperados;
    }

}
