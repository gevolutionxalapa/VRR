<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Vehiculo;
use App\Models\CatEstado;
use App\Models\CatMarca;
use App\Models\CatSubMarca;
use App\Models\CatAseguradora;
use App\Models\CatProcedencia;
use App\Models\CatColor;
use App\Models\Persona;
use App\Models\DispositivoInfo;
use App\Models\CatTipoVehiculo;
use App\Models\CatTipoUso;
use App\Models\CatClaseVehiculo;
use App\Models\RelDocsVehCat;
use App\Models\RelVehiculoCarpeta;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Bitacora;
use App\Models\UbicacionVehiculo;
use App\Models\CatSubCategoriaDocumento as SubCateDoctos;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Models\CatDocumentoCirculacion;
use App\Models\VehiculosRecuperados;
use App\Http\Controllers\RegVehiculoController;
use App\Traits\ToolsTrait;
use App\Traits\UipjTrait;
use App\Traits\UatTrait;
use App\Traits\BusTrait;
use App\Models\CatDeposito;
use App\Models\VehRecuperadosBus;

class VehRecuperadosController extends Controller
{

    use ToolsTrait;
    use UipjTrait;

     public function registroVehiculosRecuperados()
     {
         $estados = CatEstado::all();
         $marcas = CatMarca::all();
         $aseguradoras = CatAseguradora::all();
         $procedencias = CatProcedencia::all();
         $colores = CatColor::all();
         $identificacionoficial = SubCateDoctos::where('idCategoriaDocumento','=','1')->get();
         $factura = SubCateDoctos::where('idCategoriaDocumento','=','2')->get();
         $tipouso = CatTipoUso::all();
         $clasevehiculo = CatClaseVehiculo::all();
         $documentosCirculacion = CatDocumentoCirculacion::all();
         return view('denuncia.listas.registroVehiculosRecuperados', array(
             'estados'=>$estados,
             'marcas'=>$marcas,
             'aseguradoras' => $aseguradoras,
             'procedencias'=>$procedencias,
             'colores' => $colores,
             'tipousos' => $tipouso,
             'clasevehiculos' => $clasevehiculo,
             'identificacionoficial'=>$identificacionoficial,
                 'identificacionoficial1'=>$identificacionoficial,
                 'factura'=>$factura,
                 'factura1'=>$factura,
                 'documentosCirculacion' => $documentosCirculacion,
         ));
    }

    public function saveImage(Request $request){
        //dd($request->all());
        $save = ToolsTrait::save_image($request->cargarImagen[0],14,$request->idVeh,1,$request->file_id);
        if($save){
            return response()->json('Exito', 200);
        }
    }

    public function deleteImage($idVeh,$imagen){
        //dd($imagen,$idVeh);
        $delete = ToolsTrait::delete_image(14,$idVeh,$imagen);
        if($delete){
             return response()->json('Exito', 200);
        }
    }

    public function saveRecuperados(Request $request){
        //dd($request->all());
        //$getCarpeta = UipjTrait::relVehCarpeta();

        $identyCarpt = $request->input('idSecurity');
        $codificacion = base64_encode($identyCarpt);
        $informe = ToolsTrait::sessionSistema($codificacion);



        if($informe['access']=="true") {
            $datosCarpeta = UipjTrait::get_datos_uipj($informe['id']);
            // dd($datosCarpeta);
            $folio = ($request->input('folioDoctoCirc') == null) ? 'NA': $request->input('folioDoctoCirc');
            $observacion = ($request->input('observacion') == null) ? 'NA': $request->input('observacion');
            $placa = ($request->input('placa') == null) ? '0000000': $request->input('placa');
            $motor = ($request->input('motor') == null) ? 'NA': $request->input('motor');
            $serie = ($request->input('numSerie') == null) ? 'NA': $request->input('numSerie');
            $placa_extranjera = ($request->input('idDocumentoCirculacion') == 5) ? 1: 0;



            $exist_vehiculo = DB::table('vehiculo')->where(array('placas'=>$placa, 'numMotor' => $motor, 'numSerie' => $serie))->first();
            if($exist_vehiculo){
                $vehiculo_id = $exist_vehiculo->id;
                $vehiculo = DB::table('vehiculo')
                    ->where('id',$vehiculo_id)
                    ->update([
                    'observacion' => $request->input('observacion'),
                    'status' => 2
                    ]);

                Bitacora::create(['idUsuario' => Auth::user()->id, 'tabla' => 'vehiculo', 'accion' => 'update', 'descripcion' => 'Se ha realizado actualizacion de registro de un vehiculo recuperado.', 'idFilaAccion' => $vehiculo_id]);
            }else{
                $vehiculo = Vehiculo::create([
                    'placa_extranjera' => $placa_extranjera,
                    'placas' => $placa,
                    'idEstado' => $request->input('idEstado'),
                    'idSubmarca' => $request->input('idSubmarca'),
                    'modelo' => $request->input('modelo'),
                    'idColor' => $request->input('idColor'),
                    'numSerie' => $request->input('numSerie'),
                    'numMotor' => $request->input('motor'),
                    'senasPartic' => $request->input('senasPartic'),
                    'nrpv' => $request->input('nrpv'),
                    'folioDoctoCirc' => $folio,
                    'idProcedencia' => $request->input('idProcedencia'),
                    'idAseguradora' => $request->input('idAseguradora'),
                    'idTipoVehiculo' => $request->input('idSubclase'),
                    'idTipoUso' => $request->input('idUso'),
                    'idDocumentoCirculacion' => $request->input('idDocumentoCirculacion'),
                    'observacion' => $observacion,
                    'status' => 2,
                    'tipoDenuncia' => 2,
                ]);

                $vehiculo_id = $vehiculo->id;
                // $carpeta = RelVehiculoCarpeta::create([
                //     'numCarpeta' => $getCarpeta->numCarpeta,
                //     'idCarpeta' => $getCarpeta->id,
                //     'idVehiculo' => $vehiculo_id,
                //     'numFiscal' => $getCarpeta->idFiscal,
                // ]);
                $carpeta = RelVehiculoCarpeta::create([
                    'numCarpeta' => $datosCarpeta['averiguacion'],
                    'idCarpeta' => $informe['id'],
                    'idVehiculo' => $vehiculo_id,
                    //'numFiscal' => $getCarpeta->idFiscal,
                ]);
                Bitacora::create(['idUsuario' => Auth::user()->id, 'tabla' => 'vehiculo', 'accion' => 'insert', 'descripcion' => 'Se ha realizado registro de un vehiculo recuperado.', 'idFilaAccion' => $vehiculo_id]);
            }
            //Asiganacion de valores por defecto de la ubicacion
            $latitud = ($request->input('latMap') ==  null )? 0 : $request->input('latMap');
            $longitud = ($request->input('lonMap') == null )? 0 : $request->input('lonMap');
            $ubicacion = UbicacionVehiculo::create([
                'latitud' => $latitud,
                'longitud' => $longitud,
                'status' => 2,
                'idDeposito' => $request->input('idResguardo'),
                'idVehiculo' => $vehiculo_id
            ]);

            //Guardado de informacion extra del vehiculo
            $vehiculos_recuperados = VehiculosRecuperados::create([
                'idVeh' => $vehiculo_id,
                'id_entidad_rec' => $request->input('idEstado2'),
                'id_municipio_rec' => $request->input('idMunicipio2'),
                'cp_rec' => $request->input('cp'),
                'calle_rec' => $request->input('calle'),
                'colonia_rec' => $request->input('colonia'),
                'numext_rec' => $request->input('numExt'),
                'numint_rec' => $request->input('numInt'),
                'fecha_rec' => $request->input('idFechaEntrega'),
                'hora_rec' => $request->input('idHoraEntrega'),
            ]);

            $vehiculo = DB::table('vehiculo')
                ->join('vehiculos_recuperados', 'vehiculos_recuperados.idVeh', '=', 'vehiculo.id')
                ->join('ubicacion_vehiculo', 'ubicacion_vehiculo.idVehiculo', '=', 'vehiculo.id')
                ->select('vehiculo.id as idVeh', 'placas as placa', 'folioDoctoCirc as permiso', 'numSerie as serie', 'numMotor as motor', 'senasPartic as senas', 'idDeposito as id_deposito',
                    'id_entidad_rec as id_entidad_recupera', 'calle_rec', 'numext_rec', 'numint_rec', 'colonia_rec', 'id_municipio_rec', 'cp_rec', 'observacion as referencia_rec', 'idColor as id_color', 'fecha_rec', 'hora_rec',
                    'llave_externa')
                ->where(array('vehiculo.id'=>$vehiculo_id))->first();

            //$datosCarpeta = UatTrait::get_datos_uat_recuperado(3);

            BusTrait::conversion_bus_recuperados($datosCarpeta, $vehiculo,1);

            return response()->json('Denuncia creada exitosamente', 200);
        }
    }

    public function test_carpeta($idCarpeta){

        dd(base64_encode('uipj-1'));
        $denunciantes = DB::connection('uipj')->table('extra_denunciante')
           ->join('variables_persona', 'variables_persona.id', '=', 'extra_denunciante.idVariablesPersona')
           ->join('persona', 'persona.id', '=', 'variables_persona.idPersona')
           ->select('variables_persona.id',DB::raw('CONCAT(persona.nombres," ",ifnull(persona.primerAp," ")," ",ifnull(persona.segundoAp," ")) as nombre'))
           ->where(array('variables_persona.idCarpeta'=>$idCarpeta,'extra_denunciante.esDesaparecido'=>0))
           ->pluck('id','nombre');

        dd($denunciantes);

        $registros = DB::table('vehiculo')
        ->join('cat_estado', 'cat_estado.id', '=', 'vehiculo.idEstado')
        ->join('cat_submarca','cat_submarca.id','=','vehiculo.idSubmarca')
        ->join('cat_marca', 'cat_marca.id', '=', 'cat_submarca.idMarca')
        ->join('cat_color','cat_color.id', '=','vehiculo.idColor')
        ->join('cat_tipo_vehiculo','cat_tipo_vehiculo.id','=','vehiculo.idTipoVehiculo')
        ->join('cat_clase_vehiculo','cat_clase_vehiculo.id','=','cat_tipo_vehiculo.idClaseVehiculo')
        ->join('cat_tipo_uso','cat_tipo_uso.id','=','vehiculo.idTipoUso')
        ->join('cat_procedencia','cat_procedencia.id','=','vehiculo.idProcedencia')
        ->join('cat_aseguradora','cat_aseguradora.id','=','vehiculo.idAseguradora')
        ->join('rel_vehiculo_carpeta', 'rel_vehiculo_carpeta.idVehiculo','=','vehiculo.id')
        ->join('vehiculo_robado_bus','vehiculo_robado_bus.idVehiculo','=','vehiculo.id')
        ->select('placas', 'vehiculo.modelo', 'vehiculo.nrpv', 'vehiculo.numSerie', 'vehiculo.numMotor',
            'senasPartic', 'numeroeconomico','porcentaje',
            'vehiculo_robado_bus.busFGE','vehiculo_robado_bus.busCNS','vehiculo_robado_bus.busCNSKey',
            'cat_marca.nombre as marca','cat_submarca.nombre as submarca',
            'cat_color.nombre as color','cat_tipo_vehiculo.nombre as tipoVehiculo',
            'cat_clase_vehiculo.nombre as claseVehiculo', 'cat_tipo_uso.nombre as tipoUSo',
            'cat_procedencia.nombre as procedencia','cat_aseguradora.nombre as aseguradora',
            'cat_estado.nombre as entidadPlaca',
            DB::raw('(CASE
                WHEN vehiculo.placa_extranjera = 1 THEN CONCAT(placas,"-","Extranjera")
                WHEN vehiculo.placa_extranjera = 0 THEN CONCAT(placas,"-","Nacional")
                END) AS placas'
            ),
            DB::raw('(CASE
                WHEN busFGE = 1 AND busCNS = 0 AND busCNSKey = 0 THEN "El registro se ha enviado al BUS"
                WHEN busFGE = 1 AND busCNS = 1 THEN "El registro está en validación en la CNS"
                WHEN busCNS = 2 THEN "El registro se encuentra en PLATAFORMA NACIONAL"
                WHEN busCNS = 3 THEN "Existe error en los datos"
                WHEN busCNS = 0 THEN "Aun no ha sido procesado"
                END) AS status_bus'
            ),
            DB::raw('(CASE
                WHEN status_repuve = 0 THEN "Sin Verificar"
                WHEN status_repuve = 1 THEN "Existe con reporte de ROBO"
                WHEN status_repuve = 2 THEN "Existe sin reporte de ROBO"
                WHEN status_repuve = 3 THEN "No existe sin reporte de ROBO"
                WHEN status_repuve = 4 THEN "No existe con reporte de ROBO"
                END) AS status_repuve'
            )
        )
        ->where(array('tipoDenuncia'=>2, 'vehiculo.status'=>1, 'idCarpeta'=> $id))
        ->get();
        dd($registros);

        $datosCarpeta = UipjTrait::get_datos_uipj($id);
        dd($datosCarpeta);
    }
    public function validacionRecuperadosBus(Request $request){
       //dd($request->all());
       $idVehiculo = $request->input('id1');

        $recuperado = DB::table('vehiculos_recuperados')
              ->where(array('idVeh'=>$idVehiculo))
              ->update([
                'calle_rec' => $request->input('calle1'),
                'numext_rec' => $request->input('numExt1'),
                'numint_rec' => $request->input('numInt1'),
                'numext_rec' => $request->input('numExt1'),
                'colonia_rec' => $request->input('colonia1'),
                'id_municipio_rec' => $request->input('idMunicipio1'),
                'id_entidad_rec' => $request->input('idEstado1'),
                'cp_rec' => $request->input('cp1'),
                'fecha_rec' => $request->input('idFechaEntrega1'),
                'hora_rec' => $request->input('idHoraEntrega1')
              ]);
        $deposito = DB::table('ubicacion_vehiculo')
              ->where(array('idVehiculo'=>$idVehiculo))
              ->update([
                'idDeposito' => $request->input('idResguardo1')
              ]);
        $observacion = DB::table('vehiculo')
              ->where(array('id'=>$idVehiculo))
              ->update([
                'observacion' => $request->input('observacion1')
              ]);
         VehRecuperadosBus::where('IDVEHICULO','=',$idVehiculo)
            ->update(['status_muestra'=>1]);

        $this->save_recuperado_bus($idVehiculo);

        return response(
        )->json('Actualizacion creada exitosamente', 200);

    }

    public function existenImagenes ($idVehiculo) {
      $imagen= RelDocsVehCat::where('idVehiculo','=',$idVehiculo)
                      ->where(array('idSubCategoriaDocumento' => 14))
                      ->get();
                    //dd($docDiligencia);
      if(count($imagen) > 0){
        return 1;
      } else {
        return 0;
      }
    }

    public function showVehRecuperados($identyCarpt =  null)
    {

        $codificacion = base64_encode($identyCarpt);
        // dd($codificacion);
        $informe = ToolsTrait::sessionSistema($codificacion);
        // dd($informe);

        if($informe['access']=="true"){
            $numCarpetaSistema=$informe['idenCarpeta'];
            $estados = CatEstado::all();
            $marcas = CatMarca::all();
            $aseguradoras = CatAseguradora::all();
            $procedencias = CatProcedencia::all();
            $colores = CatColor::all();
            $identificacionoficial = SubCateDoctos::where('idCategoriaDocumento','=','1')->get();
            $factura = SubCateDoctos::where('idCategoriaDocumento','=','2')->get();
            $tipouso = CatTipoUso::all();
            $clasevehiculo = CatClaseVehiculo::all();
            $documentosCirculacion = CatDocumentoCirculacion::all();
            $depositos = CatDeposito::all()->sortBy('nombre');
            $config = array();
            $config['center'] = '19.976312,-96.7279684';
            $config['zoom'] = 7;
            $config['places'] = TRUE;
            $config['placesLocation'] = '19.976312,-96.7279684';
            $config['placesRadius'] = 400;
            app('map')->initialize($config);
            // set up the marker ready for positioning
            // once we know the users location
            $marker = array();
            $marker['position'] = '19.976312,-96.7279684';
            $marker['draggable'] = true;
            $marker['ondragend'] = '$("#latMap").val(event.latLng.lat()); $("#lonMap").val(event.latLng.lng());';
            // $marker['ondragend'] = 'alert(\'You just dropped me at: \' + event.latLng.lat() + \', \' + event.latLng.lng());';
            app('map')->add_marker($marker);
            $map = app('map')->create_map();

            return view('denuncia.showVehRecuperados', array(
                'estados'=>$estados,
                'marcas'=>$marcas,
                'aseguradoras' => $aseguradoras,
                'depositos' => $depositos,
                'procedencias'=>$procedencias,
                'colores' => $colores,
                'tipousos' => $tipouso,
                'clasevehiculos' => $clasevehiculo,
                'identificacionoficial'=>$identificacionoficial,
                'identificacionoficial1'=>$identificacionoficial,
                'factura'=>$factura,
                'factura1'=>$factura,
                'documentosCirculacion' => $documentosCirculacion,
                'map'=>$map,
            ));
        }
    }

    public function getRegistrosVrr($placa, $serie, $motor){
        $registros = DB::table('vehiculo')
                    ->join('rel_vehiculo_carpeta','rel_vehiculo_carpeta.idVehiculo','=','vehiculo.id')
                    ->join('cat_estado', 'cat_estado.id', '=', 'vehiculo.idEstado')
                    ->join('cat_submarca','cat_submarca.id','=','vehiculo.idSubmarca')
                    ->join('cat_marca', 'cat_marca.id', '=', 'cat_submarca.idMarca')
                    ->join('cat_color','cat_color.id', '=','vehiculo.idColor')
                    ->join('cat_tipo_vehiculo','cat_tipo_vehiculo.id','=','vehiculo.idTipoVehiculo')
                    ->join('cat_clase_vehiculo','cat_clase_vehiculo.id','=','cat_tipo_vehiculo.idClaseVehiculo')
                    ->join('cat_tipo_uso','cat_tipo_uso.id','=','vehiculo.idTipoUso')
                    ->join('cat_procedencia','cat_procedencia.id','=','vehiculo.idProcedencia')
                    ->join('cat_aseguradora','cat_aseguradora.id','=','vehiculo.idAseguradora')
                    ->where( array('placas' => $placa, 'numSerie' => $placa,'numMotor' => $motor ))
                    ->orwhere(array('placas' => $placa ))
                    ->orwhere(array('numSerie' => $serie))
                    ->orwhere(array('numMotor' => $motor ))
                    ->where(array('tipoDenuncia' => 2))
                    ->select('vehiculo.*','rel_vehiculo_carpeta.numCarpeta','cat_marca.nombre as marca',
                    'cat_submarca.nombre as submarca', 'cat_submarca.idMarca','cat_color.nombre as color',
                    'cat_tipo_vehiculo.nombre as tipoVehiculo','cat_clase_vehiculo.nombre as claseVehiculo',
                    'cat_clase_vehiculo.id as idClaseVehiculo','cat_tipo_uso.nombre as tipoUSo',
                    'cat_procedencia.nombre as procedencia','cat_aseguradora.nombre as aseguradora'
                         )
                    ->get();
        return response()->json($registros);
    }

    public function get_subclase($clasevehiculo)
    {
        $tipovehiculo = CatTipoVehiculo::where(array('idClaseVehiculo'=>$clasevehiculo))->orderBy('nombre','ASC')->get();
        $info=array(
            'status'=>"correcto",
            'estado'=>$tipovehiculo,
        );
        return response()->json($info);
    }

    public function getVehRecuperados ($idEncrypt) {

      $informe = ToolsTrait::sessionSistema($idEncrypt);

      $idCarpeta = $informe['id'];

        $vehiculo = DB::table('vehiculo')
            ->join('cat_estado', 'cat_estado.id', '=', 'vehiculo.idEstado')
            ->join('cat_submarca','cat_submarca.id','=','vehiculo.idSubmarca')
            ->join('cat_marca', 'cat_marca.id', '=', 'cat_submarca.idMarca')
            ->join('cat_color','cat_color.id', '=','vehiculo.idColor')
            ->join('cat_tipo_vehiculo','cat_tipo_vehiculo.id','=','vehiculo.idTipoVehiculo')
            ->join('cat_clase_vehiculo','cat_clase_vehiculo.id','=','cat_tipo_vehiculo.idClaseVehiculo')
            ->join('cat_tipo_uso','cat_tipo_uso.id','=','vehiculo.idTipoUso')
            ->join('cat_procedencia','cat_procedencia.id','=','vehiculo.idProcedencia')
            ->join('cat_aseguradora','cat_aseguradora.id','=','vehiculo.idAseguradora')
            ->join('ubicacion_vehiculo','ubicacion_vehiculo.idVehiculo','=','vehiculo.id')
            ->join('cat_deposito','cat_deposito.id','=','ubicacion_vehiculo.idDeposito')
            ->join('rel_vehiculo_carpeta', 'rel_vehiculo_carpeta.idVehiculo','=','vehiculo.id')
            ->select('vehiculo.*','cat_marca.nombre as marca','cat_submarca.nombre as submarca', 'cat_submarca.idMarca','cat_color.nombre as color',
            'cat_tipo_vehiculo.nombre as tipoVehiculo','cat_clase_vehiculo.nombre as claseVehiculo','cat_clase_vehiculo.id as idClaseVehiculo','cat_tipo_uso.nombre as tipoUSo',
            'cat_procedencia.nombre as procedencia','cat_aseguradora.nombre as aseguradora', 'cat_deposito.nombre as deposito','cat_deposito.id as idDeposito',
            'cat_estado.nombre as entidadPlaca',
            DB::raw('(CASE
                           WHEN vehiculo.placa_extranjera = 1 THEN CONCAT(placas,"-","Extranjera")
                           WHEN vehiculo.placa_extranjera = 0 THEN CONCAT(placas,"-","Nacional")
                           END) AS placas'),
                'vehiculo.placas as placas2')
            ->where(array('vehiculo.status'=>2, 'ubicacion_vehiculo.status' =>2,'idCarpeta'=> $idCarpeta))->get();

        return response()->json($vehiculo);
    }

    public function subirImagenes(Request $request){
      $imgs = $request->input('pictureWEBcam');
      $idVEhicle = $request->input('identyCar');
      if(is_array($imgs)){
        foreach ($imgs as $img) {
          $test= ToolsTrait::save_image($img,14,$idVEhicle,2,0);
        }
      }
      $mensaje = ($test)? 'Imagenes cargadas correctamente' : 'Problema con la carga.. intente de nuevo';
      return response()->json($mensaje, 200);
    }

    public function getImagenRec($idVehiculo){
        $doctosVeh = DB::table('rel_docs_veh_cat')
            ->join('subcategoria_documento','subcategoria_documento.id','=','rel_docs_veh_cat.idSubCategoriaDocumento')
            ->select('rel_docs_veh_cat.destino', 'rel_docs_veh_cat.idSubCategoriaDocumento as idDocto', 'subcategoria_documento.nombre')
            ->where('idVehiculo','=',$idVehiculo)
            ->where ('idSubCategoriaDocumento','=',14)
            ->get();
        $info=array(
            'estado' => $doctosVeh,
            'status' => 201,
        );
        return response()->json($info);
    }

    public function save_recuperado_bus ($idVehiculo, $idCarpeta){
        $vehiculo = DB::table('vehiculo')
            ->join('vehiculos_recuperados', 'vehiculos_recuperados.idVeh', '=', 'vehiculo.id')
            ->join('ubicacion_vehiculo', 'ubicacion_vehiculo.idVehiculo', '=', 'vehiculo.id')
            ->select('vehiculo.id as idVeh', 'placas as placa', 'folioDoctoCirc as permiso', 'numSerie as serie', 'numMotor as motor', 'senasPartic as senas', 'idDeposito as id_deposito',
            'id_entidad_rec as id_entidad_recupera', 'calle_rec', 'numext_rec', 'numint_rec', 'colonia_rec', 'id_municipio_rec', 'cp_rec', 'observacion as referencia_rec', 'idColor as id_color', 'fecha_rec', 'hora_rec',
            'llave_externa')
            ->where(array('vehiculo.id'=>$idVehiculo))->first();

        $datosCarpeta = UipjTrait::get_datos_uipj_recuperado($idCarpeta);
        //$datosCarpeta = UatTrait::get_datos_uat_recuperado(3);

        //dd($vehiculo);
        BusTrait::conversion_bus_recuperados($datosCarpeta, $vehiculo,1);

    }

    public function validacionRecuperados($idVeh){
        $estatusBus = DB::table('vehiculo_recuperado_bus')
        ->where('idVehiculo','=',$idVeh)
        ->where('status_muestra','=',0)
        ->select('busFGE','busCNS','busCNSKey')
        ->get();
        return $estatusBus;
    }

    public function validacionRecuperadosData($idVeh){

        $data = DB::table('resultado_operacion_error')
        ->join('vehiculo_recuperado_bus','vehiculo_recuperado_bus.LLAVE_EXTERNA','=','resultado_operacion_error.LLAVE' )
        ->join('vehiculo','vehiculo.id','=','vehiculo_recuperado_bus.idVehiculo')
        ->where('vehiculo.id','=',$idVeh)
        //->where('vehiculo_recuperado_bus.status_muestra','=',0)
        ->select('resultado_operacion_error.DESC_ERROR','resultado_operacion_error.LLAVE')
        ->get();

        $detail = DB::table('vehiculos_recuperados')
                  ->join('vehiculo','vehiculo.id','=','vehiculos_recuperados.idVeh')
                  ->join('ubicacion_vehiculo','ubicacion_vehiculo.idVehiculo','=','vehiculo.id')
                  ->where(array('idVeh'=>$idVeh))->get();

        $info = array(
          'error' => $data,
          'detalle' => $detail
        );
        //dd($info);
        return $info;
    }
}
