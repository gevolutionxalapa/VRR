<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UbicacionVehiculo;
use DB;
class EstadisticasController extends Controller
{
    //

    public function index(){
    	$config = array();
		$config['center'] = 'Veracruz';
		$config['zoom'] = 7;
		$config['places'] = TRUE;
		$config['placesLocation'] = '19.976312,-96.7279684';
		$config['placesRadius'] = 400;
		$config['zoomControl'] = false; 
		$config['scaleControl'] = true; 

	    app('map')->initialize($config);

	    // set up the marker ready for positioning
	    // once we know the users location
	    $marker = array();
		$marker['position'] = '19.976312,-96.7279684';
		//$marker['draggable'] = true;
		//$marker['ondragend'] = '$("#latMap").val(event.latLng.lat()); $("#lonMap").val(event.latLng.lng());';
		// $marker['ondragend'] = 'alert(\'You just dropped me at: \' + event.latLng.lat() + \', \' + event.latLng.lng());';
		
	    app('map')->add_marker($marker);

	    $map = app('map')->create_map();
		return view('estadisticas.index', array(
			'map'=>$map,			
		));
      //return view('estadisticas.index');

    }

    public function vehiculosRobados(){
    	$robados = UbicacionVehiculo::where(array('status'=>1))->get();
    	return response()->json($robados);
    }

    public function vehiculosRecuperados(){
    	$robados = UbicacionVehiculo::where(array('status'=>2))->get();
    	return response()->json($robados);
    }






    
}
