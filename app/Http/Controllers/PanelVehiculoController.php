<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//Call Models
use App\Models\Vehiculo;
use App\Models\TemporalVehiculo;
use App\Models\CatEstado;
use App\Models\CatMarca;
use App\Models\CatSubMarca;
use App\Models\CatAseguradora;
use App\Models\CatProcedencia;
use App\Models\CatColor;
use App\Models\Persona;
use App\Models\DispositivoInfo;
use App\Models\CatTipoVehiculo;
use App\Models\CatTipoUso;
use App\Models\CatClaseVehiculo;
use App\Models\RelDocsVehCat;
use App\Models\RelVehiculoCarpeta;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Bitacora;
use App\Traits\ToolsTrait;
use App\Models\CatSubCategoriaDocumento as SubCateDoctos;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Models\CatDocumentoCirculacion;
use App\Models\UbicacionVehiculo;



class PanelVehiculoController extends Controller

{
    use ToolsTrait;


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function registroVehiculos()
    {
        $estados = CatEstado::all();
        $marcas = CatMarca::all();
        $aseguradoras = CatAseguradora::all();
        $procedencias = CatProcedencia::all(); 
        $colores = CatColor::all();
        $identificacionoficial = SubCateDoctos::where('idCategoriaDocumento','=','1')->get();
        $factura = SubCateDoctos::where('idCategoriaDocumento','=','2')->get();
        $tipouso = CatTipoUso::all();
        $clasevehiculo = CatClaseVehiculo::all();
        $documentosCirculacion = CatDocumentoCirculacion::all();



        return view('denuncia.listas.registroVehiculos', array(
            'estados'=>$estados,
            'marcas'=>$marcas,
            'aseguradoras' => $aseguradoras,
            'procedencias'=>$procedencias,
            'colores' => $colores,
            'tipouso' => $tipouso,
            'clasevehiculo' => $clasevehiculo,
            'identificacionoficial'=>$identificacionoficial,
            'identificacionoficial1'=>$identificacionoficial,
            'factura'=>$factura,
            'factura1'=>$factura,
            'documentosCirculacion' => $documentosCirculacion,
        ));
   }




     public function showDenuncia()
   {

        $estados = CatEstado::all();
        $marcas = CatMarca::all();
        $aseguradoras = CatAseguradora::all();
        $procedencias = CatProcedencia::all();
        $colores = CatColor::all();
        $identificacionoficial = SubCateDoctos::where('idCategoriaDocumento','=','1')->get();
        $factura = SubCateDoctos::where('idCategoriaDocumento','=','2')->get();
        $tipouso = CatTipoUso::all();
        $clasevehiculo = CatClaseVehiculo::all();
        $documentosCirculacion = CatDocumentoCirculacion::all();

        //Configuracion para google maps
        $config = array();
		$config['center'] = 'Veracruz';
		$config['zoom'] = 7;
		$config['places'] = TRUE;
		$config['placesLocation'] = '19.976312,-96.7279684';
		$config['placesRadius'] = 400;
		$config['zoomControl'] = false;
		$config['scaleControl'] = true;

        app('map')->initialize($config);

	    $marker = array();
		$marker['position'] = '19.976312,-96.7279684';
		$marker['draggable'] = true;
		$marker['ondragend'] = '$("#latMap").val(event.latLng.lat()); $("#lonMap").val(event.latLng.lng());';

	    app('map')->add_marker($marker);

	    $map = app('map')->create_map();

        return view('denuncia.showDenuncia', array(
            'estados'=>$estados,
            'marcas'=>$marcas,
            'aseguradoras' => $aseguradoras,
            'procedencias'=>$procedencias,
            'colores' => $colores,
            'tipouso' => $tipouso,
            'clasevehiculo' => $clasevehiculo,
            'identificacionoficial'=>$identificacionoficial,
            'identificacionoficial1'=>$identificacionoficial,
            'factura'=>$factura,
            'factura1'=>$factura,
            'documentosCirculacion' => $documentosCirculacion,
            'map' => $map
        ));

    }





    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function get_subclase($clasevehiculo)
    {
        $tipovehiculo = CatTipoVehiculo::where(array('idClaseVehiculo'=>$clasevehiculo))->get();
        $info=array(
            'status'=>"correcto",
            'estado'=>$tipovehiculo,
        );
        return response()->json($info);

    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   
    public function saveValidacion(Request $request)
    {
        //dd($request->all());
         $dateInvalid = $request->elementosInvalidos;
         
         $porciones = explode(',', $dateInvalid);
         //dd(count($porciones));
         $vehiculo_id = $request->input('id');
         if($dateInvalid!=null){
            $folio = ($request->input('folioDoctoCirc') == null) ? 'NA': $request->input('folioDoctoCirc');
            $observacion = ($request->input('comentario_data') == null) ? 'NA': $request->input('comentario_data');
            $invalInsert= array();
         //dd($invalInsert);
            foreach ($porciones as $elemento) {
                $invalInsert[$elemento] = ($request->input($elemento) != null)?$request->input($elemento):0;   
            }
            //dd($invalInsert);
            $vehiculo = TemporalVehiculo::create([
                'id' => $vehiculo_id,
                'placas' => (array_key_exists('placa', $invalInsert))?$invalInsert['placa']:0,
                'idEstado' => (array_key_exists('idEstados', $invalInsert))?$invalInsert['idEstados']:0,
                'idSubmarca' => (array_key_exists('idSubmarcas', $invalInsert))?$invalInsert['idSubmarcas']:0,
                'modelo' => (array_key_exists('modelos', $invalInsert))?$invalInsert['modelos']:0,
                'idColor' => (array_key_exists('idColors', $invalInsert))?$invalInsert['idColors']:0,
                'numSerie' => (array_key_exists('numSeries', $invalInsert))?$invalInsert['numSeries']:0,
                'numMotor' => (array_key_exists('motors', $invalInsert))?$invalInsert['motors']:0,
                'senasPartic' => (array_key_exists('senasPartic2', $invalInsert))?$invalInsert['senasPartic2']:0,
                'nrpv' => (array_key_exists('nrpv2', $invalInsert))?$invalInsert['nrpv2']:0,
                'folioDoctoCirc' => $folio,
                'idProcedencia' => (array_key_exists('idProcedencia2', $invalInsert))?$invalInsert['idProcedencia2']:0,
                'idAseguradora' => (array_key_exists('idAseguradora2', $invalInsert))?$invalInsert['idAseguradora2']:0,
                'idTipoVehiculo' => (array_key_exists('idSubclase2', $invalInsert))?$invalInsert['idSubclase2']:0,
                'idTipoUso' => (array_key_exists('idUso2', $invalInsert))?$invalInsert['idUso2']:0,
                'idDocumentoCirculacion' =>1, //$request->input('idDocCirculacion'),
                'registradoPor' => $request->input('validaEntidad'),
                'observacion' => $observacion,
            ]);

            if($request->input('validaEntidad')==1){
                $status_validacion = Vehiculo::where('id','=',$vehiculo_id)
                ->update([
                    'status_policia' => 3,
                ]);
            }else{
                $status_validacion = Vehiculo::where('id','=',$vehiculo_id)
                ->update([
                    'status_enlace' => 3,
                ]);
            }


           
         }else{
            if($request->input('validaEntidad')==1){
                $status_validacion = Vehiculo::where('id','=',$vehiculo_id)
                ->update([
                    'status_policia' => 2,
                ]);
            }else{
                $status_validacion = Vehiculo::where('id','=',$vehiculo_id)
                ->update([
                    'status_enlace' => 2,
                ]);
            }
        }
         

          // if($request->input('validaEntidad')==1){
          //   $status_validacion = Vehiculo::where('id','=',$vehiculo_id)
          //       ->update([
          //           'status_policia' => 
          //       ])
          // } else{

          // }

        //dd('exito');
         return response()->json('La revisión fue exitosa', 200);
    }
    /* Funcion qu utiliza el datable para cargar datos */
    public function getRegistros()
    {
      //$registros = Vehiculo::all();

      $registros = DB::table('vehiculo')
                ->join('cat_estado', 'cat_estado.id', '=', 'vehiculo.idEstado')
                ->join('cat_submarca','cat_submarca.id','=','vehiculo.idSubmarca')
                ->join('cat_marca', 'cat_marca.id', '=', 'cat_submarca.idMarca')
                ->join('cat_color','cat_color.id', '=','vehiculo.idColor')
                ->join('cat_tipo_vehiculo','cat_tipo_vehiculo.id','=','vehiculo.idTipoVehiculo')
                ->join('cat_clase_vehiculo','cat_clase_vehiculo.id','=','cat_tipo_vehiculo.idClaseVehiculo')
                ->join('cat_tipo_uso','cat_tipo_uso.id','=','vehiculo.idTipoUso')
                ->join('cat_procedencia','cat_procedencia.id','=','vehiculo.idProcedencia')
                ->join('cat_aseguradora','cat_aseguradora.id','=','vehiculo.idAseguradora')
                ->select('vehiculo.*','cat_marca.nombre as marca','cat_submarca.nombre as submarca', 'cat_submarca.idMarca','cat_color.nombre as color','cat_tipo_vehiculo.nombre as tipoVehiculo','cat_clase_vehiculo.nombre as claseVehiculo','cat_clase_vehiculo.id as idClaseVehiculo','cat_tipo_uso.nombre as tipoUSo','cat_procedencia.nombre as procedencia','cat_aseguradora.nombre as aseguradora')
               ->where(array('tipoDenuncia'=>2))->get();


      return response()->json($registros, 200);
    }

    public function getDoctosVeh($idVehiculo){
            $doctosVeh = DB::table('rel_docs_veh_cat')
                    ->join('subcategoria_documento','subcategoria_documento.id','=','rel_docs_veh_cat.idSubCategoriaDocumento')
                    ->select('rel_docs_veh_cat.destino', 'rel_docs_veh_cat.idSubCategoriaDocumento as idDocto', 'subcategoria_documento.nombre')
                    ->where('idVehiculo','=',$idVehiculo)
                    ->where ('idSubCategoriaDocumento','<=',8)
                    ->get();


            /*$doctosVeh = RelDocsVehCat::select('rel_docs_veh_cat.destino', 'rel_docs_veh_cat.idSubCategoriaDocumento as idDocto')
                    ->where('idVehiculo','=',$idVehiculo)
                    ->get();
            */

            /*$rutas = array(
                array(
                'uno' => 'docs/images/thumbnails/tibet-1.jpg',
                '2' => 'docs/images/thumbnails/tibet-2.jpg',
                '3' => 'docs/images/thumbnails/tibet-2.jpg',
            )
            );*/

        $info=array(
            'estado' => $doctosVeh,
            'status' => 201,
        );
        return response()->json($info);
    }

    public function getDilidencia($idVehiculo){
        $docDiligencia= RelDocsVehCat::where('idVehiculo','=',$idVehiculo)
                        ->where(array('idSubCategoriaDocumento' => 13))
                        ->get();

        $info=array(
            'estado' => $docDiligencia,
            'status' => 201,
        );
        return response()->json($info);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //dd($request->all());
        // $datos = array(
        //     'id' => '12' ,
        //     'placas' => 'DGF6768',
        //     'modelo' => '2016',
        //     'nrpv' => 'PRUEBA',
        //     'permiso' => '545487',
        //     'numSerie' => '846568',
        //     'numMotor' => '7897878',
        //     'senasPartic'=> 'USHHDF',
        //     'procedencia' => '1',
        //     'marca' => '60',
        //     'submarca' => '5377',
        //     'color' => '25',
        //     'tipo' => '10',
        //     'clase' => '7',
        //     'tipoUso' => '14',
        //     'aseguradora' => '5'
        // );
        //dd($datos);
        $folio = ($request->input('folio') == null) ? '': $request->input('folio');

        $vehiculo = DB::table('vehiculo')
                ->where('id',$request->input('id'))
                ->update([
                    'placas' => $request->input('placa'),
                    'modelo' => $request->input('modelos'),
                    'nrpv' => $request->input('nrpv2'),
                    'folioDoctoCirc' => $folio,
                    'numSerie' => $request->input('numSeries'),
                    'numMotor' => $request->input('motors'),
                    'senasPartic' => $request->input('senasPartic2'),
                    'idProcedencia' => $request->input('idProcedencia2'),
                    'idSubmarca' => $request->input('idSubmarcas'),
                    'idColor' => $request->input('idColors'),
                    'idTipoVehiculo' => $request->input('idSubclase2'),
                    'idTipoUso' => $request->input('idUso2'),
                    'idAseguradora' => $request->input('idAseguradora2'),
                    'idDocumentoCirculacion' => $request->input('idDocCirculacion')

                ]);

            Bitacora::create(['idUsuario' => Auth::user()->id, 'tabla' => 'vehiculo', 'accion' => 'update', 'descripcion' => 'Se ha modificado el registro de robo de un vehículo.', 'idFilaAccion' => $request->input('id')]);


        return response()->json('El registro se actualiz&oacute; correctamente', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function get_vehiculo_uipj()
    {
        $vehiculo = array
        (
            array(
                'id' => 1,
                'numCarpeta' => 'UIPJ/DXI/1/1/2018',
                'idTipifDelito' => 2,
                'status' => 'INVOLUCRADO',
                'placas' => 'QWE123',
                'idEstado' => 10,
                'idSubmarca' => 14332,
                'modelo' => 2018,
                'nrpv' => 454787845478,
                'idColor' => 11,
                'permiso' => 'Sin informacion',
                'numSerie' => '12345678901234567',
                'numMotor' => '1231231231',
                'idTipoVehiculo' => 10,
                'idTipoUso' => 10,
                'senasPartic' => 'No tiene',
                'idProcedencia' => 2,
                'idAseguradora' => 25
            ),
            array(
                'id' => 2,
                'numCarpeta' => 'UIPJ/DV/1/1/2018',
                'idTipifDelito' => 3,
                'status' => 'INVOLUCRADO',
                'placas' => 'QWE123',
                'idEstado' => 10,
                'idSubmarca' => 14000,
                'modelo' => 2016,
                'nrpv' => 123456789012,
                'idColor' => 11,
                'permiso' => 'Sin informacion',
                'numSerie' => '11111111111111111',
                'numMotor' => '1111111111',
                'idTipoVehiculo' => 2,
                'idTipoUso' => 5,
                'senasPartic' => 'Faros delanteros rotos',
                'idProcedencia' => 3,
                'idAseguradora' => 15
            ),
            array(
                'id' => 3,
                'numCarpeta' => 'UIPJ/DXV/1/1/2018',
                'idTipifDelito' => 1,
                'status' => 'INVOLUCRADO',
                'placas' => 'MRF123',
                'idEstado' => 10,
                'idSubmarca' => 10000,
                'modelo' => 1998,
                'nrpv' => 121212121212,
                'idColor' => 11,
                'permiso' => 'Licencia',
                'numSerie' => '89089089089089089',
                'numMotor' => '1231231231',
                'idTipoVehiculo' => 2,
                'idTipoUso' => 5,
                'senasPartic' => 'Abolladura en la defensa',
                'idProcedencia' => 6,
                'idAseguradora' => 12
            ),
            array(
                'id' => 4,
                'numCarpeta' => 'UIPJ/DXII/1/1/2018',
                'idTipifDelito' => 2,
                'status' => 'INVOLUCRADO',
                'placas' => 'FG56876',
                'idEstado' => 7,
                'idSubmarca' => 14300,
                'modelo' => 2018,
                'nrpv' => 2006,
                'idColor' => 11,
                'permiso' => 'Sin informacion',
                'numSerie' => '56784534124321456',
                'numMotor' => '8976452316',
                'idTipoVehiculo' => 4,
                'idTipoUso' => 9,
                'senasPartic' => 'Sin placas',
                'idProcedencia' => 7,
                'idAseguradora' => 4
            ),
            array(
                'id' => 5,
                'numCarpeta' => 'UIPJ/DIII/1/1/2018',
                'idTipifDelito' => 2,
                'status' => 'INVOLUCRADO',
                'placas' => 'MRF432',
                'idEstado' => 25,
                'idSubmarca' => 10332,
                'modelo' => 2016,
                'nrpv' => 454787845478,
                'idColor' => 11,
                'permiso' => 'Sin informacion',
                'numSerie' => '78945678945678965',
                'numMotor' => '5555555555',
                'idTipoVehiculo' => 4,
                'idTipoUso' => 5,
                'senasPartic' => 'Le falta una llanta',
                'idProcedencia' => 7,
                'idAseguradora' => 12
            )

        );
        return response()->json($vehiculo, 200);
    }

    public function diligenciaDocs(Request $request){
      $img1 = $request->file('diligenciaFormaro');
      $vehiculo_id= $request->input('iddiligencia');
      $tipoArchivo = 13;
      $mensajes ="";
      $act = 'cargada';
      $Rename = str_replace('-','',str_replace(':','',str_replace(' ', '', Carbon::now('America/Monterrey'))));
      $fileName = $Rename.'.'.$img1->getClientOriginalExtension();
      $carpetaStore = 'diligencia/';
      $pathd1 = DB::table('rel_docs_veh_cat')->where(array('idVehiculo'=>$vehiculo_id, 'idSubCategoriaDocumento' => $tipoArchivo))->first();
      if($pathd1){
        $act = 'actualizo';
        Storage::disk('local')->delete($pathd1->destino);
        DB::table('rel_docs_veh_cat')->where(array('idVehiculo'=>$vehiculo_id, 'idSubCategoriaDocumento' => $tipoArchivo))->delete();
      }
      $registro1 = new RelDocsVehCat();
      $registro1->destino = $carpetaStore.$fileName;
      $registro1->idVehiculo = $vehiculo_id;
      $registro1->idImagen =0;
      $registro1->idSubCategoriaDocumento = $tipoArchivo;//this we be will change, when the document is selected
      if($registro1->save()){
        $mensajes = array(
          'msj'=>'Imagen '.$act.' correctamente',
          'status' => 'success'
        );
        $path1 = $img1->storeAs($carpetaStore, $fileName);
      }else{
        $mensajes = array(
          'msj'=>'Problema con la carga de imagenes',
          'status' => 'error'
        );

      }
      return response()->json($mensajes, 200);
    }

    public function existeDiligencia ($idVehiculo) {
      $docDiligencia= RelDocsVehCat::where('idVehiculo','=',$idVehiculo)
                      ->where(array('idSubCategoriaDocumento' => 13))
                      ->get();
                    //dd($docDiligencia);
      if(count($docDiligencia) > 0){
        return 1;
      } else {
        return 0;
      }
    }


    public function showPanel()
    {
        $estados = CatEstado::all();
        $marcas = CatMarca::all();
        $aseguradoras = CatAseguradora::all();
        $procedencias = CatProcedencia::all();
        $colores = CatColor::all();
        $identificacionoficial = SubCateDoctos::where('idCategoriaDocumento','=','1')->get();
        $factura = SubCateDoctos::where('idCategoriaDocumento','=','2')->get();
        $tipouso = CatTipoUso::all();
        $clasevehiculo = CatClaseVehiculo::all();
        $documentosCirculacion = CatDocumentoCirculacion::all();

        //Configuracion para google maps
        $config = array();
        $config['center'] = 'Veracruz';
        $config['zoom'] = 7;
        $config['places'] = TRUE;
        $config['placesLocation'] = '19.976312,-96.7279684';
        $config['placesRadius'] = 400;
        $config['zoomControl'] = false;
        $config['scaleControl'] = true;

        app('map')->initialize($config);

        $marker = array();
        $marker['position'] = '19.976312,-96.7279684';
        $marker['draggable'] = true;
        $marker['ondragend'] = '$("#latMap").val(event.latLng.lat()); $("#lonMap").val(event.latLng.lng());';

        app('map')->add_marker($marker);

        $map = app('map')->create_map();

     

        return view('panel.showPanel', array(
            'estados'=>$estados,
            'marcas'=>$marcas,
            'aseguradoras' => $aseguradoras,
            'procedencias'=>$procedencias,
            'colores' => $colores,
            'tipouso' => $tipouso,
            'clasevehiculo' => $clasevehiculo,
            'identificacionoficial'=>$identificacionoficial,
            'identificacionoficial1'=>$identificacionoficial,
            'factura'=>$factura,
            'factura1'=>$factura,
            'documentosCirculacion' => $documentosCirculacion,
            'map' => $map
        ));

    }

    public function notificacion_panel() {
        $registros = DB::table('temporal_vehiculo')
            ->where('visto','=',0)
            ->join('cat_submarca','cat_submarca.id','=','temporal_vehiculo.idsubmarca')
            ->join('cat_tipo_vehiculo','cat_tipo_vehiculo.id','=','temporal_vehiculo.idTipoVehiculo')
            ->select('temporal_vehiculo.*', 'cat_submarca.idMarca', 'cat_tipo_vehiculo.idClaseVehiculo')
            ->first();

        //dd($registros);
        if($registros != null) {
            return response()->json($registros, 200);
        } else {
            return 0;
        }
        
    }

    public function notificacion_uppanel($id) {
        $vehiculo = DB::table('temporal_vehiculo')
            ->where('id',$id)
            ->update([
                'visto' => 1
            ]);
        
    }

    public function notificacion_robados() {
      // $registros = DB::table('vehiculo')
      // ->where('status_token','=',1)
      // ->join('cat_submarca','cat_submarca.id','=','temporal_vehiculo.idsubmarca')
      // ->join('cat_tipo_vehiculo','cat_tipo_vehiculo.id','=','temporal_vehiculo.idTipoVehiculo')
      // ->select('temporal_vehiculo.*', 'cat_submarca.idMarca', 'cat_tipo_vehiculo.idClaseVehiculo')
      // ->first();


      $registros = DB::table('vehiculo')
      ->join('cat_submarca','cat_submarca.id','=','vehiculo.idsubmarca')
      ->join('cat_tipo_vehiculo','cat_tipo_vehiculo.id','=','vehiculo.idTipoVehiculo')
      ->where('status_token','=',1)
      ->select('vehiculo.*', 'cat_submarca.idMarca', 'cat_tipo_vehiculo.idClaseVehiculo')
      ->first();

      if($registros != null) {
        return response()->json($registros, 200);
      } else {
        return 0;
      }

    }

    public function notificacion_uprobados($id) {
        $vehiculo = DB::table('vehiculo')
            ->where('id',$id)
            ->update([
                'status_token' => 0
            ]);
        
    }

    public function statusRevision($idVeh,$entidad){
        $idEntidad = ($entidad==1)?'status_policia':'status_enlace';
        $vehiculo = DB::table('vehiculo')
                ->where('id',$idVeh)
                ->update([
                    $idEntidad => 1
                ]);
        return response()->json('Se actualizo correctamente', 200);
    }

}
