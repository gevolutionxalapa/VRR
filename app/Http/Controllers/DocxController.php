<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
class DocxController extends Controller
{
    public function __construct(){
        Carbon::setLocale('es');
    }
    function getDiligencia(Request $request){
        //dd($request->all());
        $fechaHoy = new Carbon();
        $mesLetra = $this->getMesLetra($fechaHoy->month);
        $fechaCompleta = mb_strtoupper($fechaHoy->day." DE ".$mesLetra." DE ".$fechaHoy->year);
       
		
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('templates/FormatoDiligencia.docx');
        $distrito = 'X';
        $nombreUIPJ = $request['investigacion']['nomCompleto'];
        $numFiscal = $request['investigacion']['numFiscal'];
        $fiscal = 'VIGESIMO QUINTO';
        $municipio = $request['robo']['municipio'];
        $numOficio = '2';
        $nombre = $request['acusacion']['denunciante'][0]['nombre'];
        $aPaterno= $request['acusacion']['denunciante'][0]['primerAp'];
        $aMaterno = $request['acusacion']['denunciante'][0]['segundoAp'];
        $nombreDenunciante = $nombre.' '.$aPaterno.' '.$aMaterno;
        $calle = $request['acusacion']['denunciante'][0]['calle'];
        $num = $request['acusacion']['denunciante'][0]['numExterno'];
        $colonia = $request['acusacion']['denunciante'][0]['colonia'];
        $telefono = $request['acusacion']['denunciante'][0]['telefono'];
        $descripcionHechos = $request['robo']['descripcionHechos'];
        $nombreFiscal = $request['investigacion']['nomFiscal'];

        $templateProcessor->setValue('distrito', $distrito);
        $templateProcessor->setValue('nombreUIPJ', $nombreUIPJ);
        $templateProcessor->setValue('numFiscal', $numFiscal);
        $templateProcessor->setValue('fiscal', $fiscal);
        $templateProcessor->setValue('municipio', $municipio);
        $templateProcessor->setValue('numOficio', $numOficio);
        $templateProcessor->setValue('anio', $fechaHoy->year);
        $templateProcessor->setValue('nombreDenunciante',$nombreDenunciante);
        $templateProcessor->setValue('calle', $calle);
        $templateProcessor->setValue('num', $num);
        $templateProcessor->setValue('colonia', $colonia);
        $templateProcessor->setValue('numTelefono',$telefono);
        $templateProcessor->setValue('descripcionHechos', $descripcionHechos);
        $templateProcessor->setValue('fecha', $fechaCompleta);
        $templateProcessor->setValue('nombreFiscal', $nombreFiscal);

		//$templateProcessor->saveAs('../storage/app/public/Diligencia_'.$numFiscal.'.docx');
		//return response()->json('Diligencia_'.$numFiscal.'.docx', 200);
		$templateProcessor->saveAs('../storage/app/public/Diligencia_1.docx');
		return response()->json('Diligencia_1.docx', 200);

    }

    public static function getDistritoLetra($numDistrito){
		switch ($numDistrito) {
        	case 'I':
        		$distritoLetra = "PRIMER";
        		break;
        	case 'II':
        		$distritoLetra = "SEGUNDO";
        		break;
        	case 'III':
        		$distritoLetra = "TERCER";
        		break;
        	case 'IV':
        		$distritoLetra = "CUARTO";
        		break;
        	case 'V':
        		$distritoLetra = "QUINTO";
        		break;
        	case 'VI':
        		$distritoLetra = "SEXTO";
        		break;
        	case 'VII':
        		$distritoLetra = "SEPTIMO";
        		break;
        	case 'VIII':
        		$distritoLetra = "OCTAVO";
        		break;
        	case 'IX':
        		$distritoLetra = "NOVENO";
        		break;
        	case 'X':
        		$distritoLetra = "DECIMO";
        		break;
        	case 'XI':
        		$distritoLetra = "DECIMOPRIMER";
        		break;
        	case 'XII':
        		$distritoLetra = "DECIMOSEGUNDO";
        		break;
        	default:
        		$distritoLetra = "PRIMER";
        		break;
        }
        return $distritoLetra;
    }
    
    public static function getMesLetra($numMes){
		switch ($numMes) {
        	case '1':
        		$mesLetra = "Enero";
        		break;
        	case '2':
        		$mesLetra = "Febrero";
        		break;
        	case '3':
        		$mesLetra = "Marzo";
        		break;
        	case '4':
        		$mesLetra = "Abril";
        		break;
        	case '5':
        		$mesLetra = "Mayo";
        		break;
        	case '6':
        		$mesLetra = "Junio";
        		break;
        	case '7':
        		$mesLetra = "Julio";
        		break;
        	case '8':
        		$mesLetra = "Agosto";
        		break;
        	case '9':
        		$mesLetra = "Septiembre";
        		break;
        	case '10':
        		$mesLetra = "Octubre";
        		break;
        	case '11':
        		$mesLetra = "Noviembre";
        		break;
        	case '12':
        		$mesLetra = "Diciembre";
        		break;
        	default:
        		$mesLetra = "ENERO";
        		break;
        }
        return $mesLetra;
	}
}
