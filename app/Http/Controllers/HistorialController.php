<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB; 

class HistorialController extends Controller
{
    public function index()
    {
      return view('historial.index');
    }

    public function getRegistros(){
    	$vehiculos = DB::table('vehiculo')
    		->join('rel_vehiculo_carpeta','rel_vehiculo_carpeta.idVehiculo','=','vehiculo.id')
            ->join('cat_estado', 'cat_estado.id', '=', 'vehiculo.idEstado')
            ->join('cat_submarca','cat_submarca.id','=','vehiculo.idSubmarca')
            ->join('cat_marca', 'cat_marca.id', '=', 'cat_submarca.idMarca')
            ->join('cat_color','cat_color.id', '=','vehiculo.idColor')
            ->join('cat_tipo_vehiculo','cat_tipo_vehiculo.id','=','vehiculo.idTipoVehiculo')
            ->join('cat_clase_vehiculo','cat_clase_vehiculo.id','=','cat_tipo_vehiculo.idClaseVehiculo')
            ->join('cat_tipo_uso','cat_tipo_uso.id','=','vehiculo.idTipoUso')
            ->join('cat_procedencia','cat_procedencia.id','=','vehiculo.idProcedencia')
            ->join('cat_aseguradora','cat_aseguradora.id','=','vehiculo.idAseguradora')
            ->select('vehiculo.*','cat_marca.nombre as marca','cat_submarca.nombre as submarca', 'cat_submarca.idMarca','cat_color.nombre as color','cat_tipo_vehiculo.nombre as tipoVehiculo','cat_clase_vehiculo.nombre as claseVehiculo','cat_clase_vehiculo.id as idClaseVehiculo','cat_tipo_uso.nombre as tipoUSo','cat_procedencia.nombre as procedencia','cat_aseguradora.nombre as aseguradora','rel_vehiculo_carpeta.numCarpeta',
            	DB::raw('(CASE 
            				WHEN vehiculo.status = 1 THEN "Robado" 
            				WHEN vehiculo.status = 2 THEN "Recuperado"
            				WHEN vehiculo.status = 3 THEN "Entregado"
            				END) AS estatus')
        )
            ->where(array('tipoDenuncia'=>2))
            ->get();
            //dd($vehiculos);
           return response()->json($vehiculos, 200);
    }
}
