<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//Call Models
use App\Models\Vehiculo;
use App\Models\CatEstado;
use App\Models\CatMarca;
use App\Models\CatSubMarca;
use App\Models\CatAseguradora;
use App\Models\CatProcedencia;
use App\Models\CatColor;
use App\Models\Persona;
use App\Models\DispositivoInfo;
use App\Models\CatTipoVehiculo;
use App\Models\CatTipoUso;
use App\Models\CatClaseVehiculo;
use App\Models\RelDocsVehCat;
use App\Models\RelVehiculoCarpeta;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Bitacora;
use App\Traits\ToolsTrait;
use App\Traits\UipjTrait;
use App\Traits\UatTrait;
use App\Traits\BusTrait;
use App\Models\CatSubCategoriaDocumento as SubCateDoctos;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Models\CatDocumentoCirculacion;
use App\Models\UbicacionVehiculo;
use App\Models\VehRobadoBus;
use Illuminate\Support\Facades\Crypt;


class RegVehiculoController extends Controller

{
    use ToolsTrait, BusTrait;
    use UipjTrait;

    private $numCarpetaSistema;

    public function getToken($token){
        $busToken = Vehiculo::where(array('token'=>$token))->count();
        if ($busToken!=0 || $busToken!=" "){
            $status_token = Vehiculo::where(array('token'=>$token))->value('status_token');
            $status_den = Vehiculo::where(array('token'=>$token))->value('tipoDenuncia');
            if($status_token==1){
                $vehiculo = DB::table('vehiculo')
                ->join('cat_estado', 'cat_estado.id', '=', 'vehiculo.idEstado')
                ->join('cat_submarca','cat_submarca.id','=','vehiculo.idSubmarca')
                ->join('cat_marca', 'cat_marca.id', '=', 'cat_submarca.idMarca')
                ->join('cat_color','cat_color.id', '=','vehiculo.idColor')
                ->join('cat_tipo_vehiculo','cat_tipo_vehiculo.id','=','vehiculo.idTipoVehiculo')
                ->join('cat_clase_vehiculo','cat_clase_vehiculo.id','=','cat_tipo_vehiculo.idClaseVehiculo')
                ->join('cat_tipo_uso','cat_tipo_uso.id','=','vehiculo.idTipoUso')
                ->join('cat_procedencia','cat_procedencia.id','=','vehiculo.idProcedencia')
                ->join('cat_aseguradora','cat_aseguradora.id','=','vehiculo.idAseguradora')
                ->join('ubicacion_vehiculo','ubicacion_vehiculo.idVehiculo','=','vehiculo.id')
                ->select('vehiculo.*','cat_marca.nombre as marca','cat_submarca.nombre as submarca', 'cat_submarca.idMarca','cat_color.nombre as color','cat_tipo_vehiculo.nombre as tipoVehiculo','cat_clase_vehiculo.nombre as claseVehiculo','cat_clase_vehiculo.id as idClaseVehiculo','cat_tipo_uso.nombre as tipoUSo','cat_procedencia.nombre as procedencia','cat_aseguradora.nombre as aseguradora', 'latitud', 'longitud')
                ->where(array('token'=>$token))->get();

                $doctos =DB::table('rel_docs_veh_cat')
                ->join('subcategoria_documento','subcategoria_documento.id','=','rel_docs_veh_cat.idSubCategoriaDocumento')
                ->select('rel_docs_veh_cat.*','subcategoria_documento.nombre')
                ->where('idVehiculo','=',$vehiculo[0]->id)->get();
                $info=array(
                    'status'=>"correcto",
                    'codigo'=>201,
                    'estado'=>$vehiculo,
                    'doctos'=>$doctos,
                );
            }else{
              if($status_token==3){

                  $info=array(
                      'status'=>"El folio ya ha sido procesado",
                      'codigo'=>421,
                      'estado'=>$status_token,
                  );
              } else{
                $info=array(
                    'status'=>"El folio ha expirado, favor de reactivar.",
                    'codigo'=>421,
                    'estado'=>$status_token,
                );
            }

        }
      }

        else{
            $info=array(
                'status'=>"Folio no encontrado",
                'codigo'=>421,
                'estado'=>$busToken,
            );
        }
        return response()->json($info);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function registroVehiculos($idtab=1)
    {
        $estados = CatEstado::all();
        $marcas = CatMarca::all();
        $aseguradoras = CatAseguradora::all();
        $procedencias = CatProcedencia::all();
        $colores = CatColor::all();
        $identificacionoficial = SubCateDoctos::where('idCategoriaDocumento','=','1')->get();
        $factura = SubCateDoctos::where('idCategoriaDocumento','=','2')->get();
        $tipouso = CatTipoUso::all();
        $clasevehiculo = CatClaseVehiculo::all();
        $documentosCirculacion = CatDocumentoCirculacion::all();

        return view('denuncia.listas.registroVehiculos', array(
            'estados'=>$estados,
            'marcas'=>$marcas,
            'aseguradoras' => $aseguradoras,
            'procedencias'=>$procedencias,
            'colores' => $colores,
            'tipouso' => $tipouso,
            'clasevehiculo' => $clasevehiculo,
            'identificacionoficial'=>$identificacionoficial,
            'identificacionoficial1'=>$identificacionoficial,
            'factura'=>$factura,
            'factura1'=>$factura,
            'documentosCirculacion' => $documentosCirculacion,
            'idtab' =>$idtab

        ));
    }

    public function showDenuncia(Request $request,$idcarpeta=0, $idtab=0)
    {
        // $codificacion = base64_encode($request->identyCarpt);
        //dd($idtab);
        $informe = ToolsTrait::sessionSistema($request->identyCarpt);
        // dd($informe);
        if($informe['access']=="true"){
          $denunciantes = DB::connection('uipj')->table('extra_denunciante')
                        ->join('variables_persona', 'variables_persona.id', '=', 'extra_denunciante.idVariablesPersona')
                        ->join('persona', 'persona.id', '=', 'variables_persona.idPersona')
                        ->where(array('variables_persona.idCarpeta'=>$informe['id'],'extra_denunciante.esDesaparecido'=>0))
                        ->select('variables_persona.id',DB::raw('CONCAT(persona.nombres," ",ifnull(persona.primerAp," ")," ",ifnull(persona.segundoAp," ")) as nombre'))
                        ->pluck('id','nombre');
          $numCarpetaSistema=$informe['idenCarpeta'];
          $estados = CatEstado::all();
          $marcas = CatMarca::all();
          $aseguradoras = CatAseguradora::all();
          $procedencias = CatProcedencia::all();
          $colores = CatColor::all();
          $identificacionoficial = SubCateDoctos::where('idCategoriaDocumento','=','1')->get();
          $factura = SubCateDoctos::where('idCategoriaDocumento','=','2')->get();
          $tipouso = CatTipoUso::all();
          $clasevehiculo = CatClaseVehiculo::all();
          $documentosCirculacion = CatDocumentoCirculacion::all();

          //Configuracion para google maps
          $config = array();
          $config['center'] = 'Veracruz';
          $config['zoom'] = 7;
          $config['places'] = TRUE;
          $config['placesLocation'] = '19.976312,-96.7279684';
          $config['placesRadius'] = 400;
          $config['zoomControl'] = false;
          $config['scaleControl'] = true;

          app('map')->initialize($config);

          $marker = array();
          $marker['position'] = '19.976312,-96.7279684';
          $marker['draggable'] = true;
          $marker['ondragend'] = '$("#latMap").val(event.latLng.lat()); $("#lonMap").val(event.latLng.lng());';

          app('map')->add_marker($marker);

          $map = app('map')->create_map();

          return view('denuncia.showDenuncia', array(
              'propietarios'=>$denunciantes,
              'estados'=>$estados,
              'marcas'=>$marcas,
              'aseguradoras' => $aseguradoras,
              'procedencias'=>$procedencias,
              'colores' => $colores,
              'tipouso' => $tipouso,
              'clasevehiculo' => $clasevehiculo,
              'identificacionoficial'=>$identificacionoficial,
              'identificacionoficial1'=>$identificacionoficial,
              'factura'=>$factura,
              'factura1'=>$factura,
              'documentosCirculacion' => $documentosCirculacion,
              'map' => $map,
              'idcarpeta'=> $informe['id'],
              'idtab' =>$idtab
          ));
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function get_subclase($clasevehiculo)
    {
        $tipovehiculo = CatTipoVehiculo::where(array('idClaseVehiculo'=>$clasevehiculo))->get();
        $info=array(
            'status'=>"correcto",
            'estado'=>$tipovehiculo,
        );
        return response()->json($info);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getCoincidencia($placa, $serie, $motor, $nrpv,$token=null){

        $placaCoin = Vehiculo::where('placas','=',$placa)
            ->when($token!=null, function ($query) use ($token) {
                return $query->where('token','!=', $token);
            })
            ->count();
        $serieCoin = Vehiculo::where('numSerie','=',$serie)
            ->when($token!=null, function ($query) use ($token) {
                return $query->where('token','!=', $token);
            })
            ->count();
        $motorCoin = Vehiculo::where('numMotor','=',$motor)
            ->when($token!=null, function ($query) use ($token) {
                return $query->where('token','!=' ,$token);
            })
            ->count();
        $nrpvCoin = Vehiculo::where('nrpv','=',$nrpv)
            ->when($token!=null, function ($query) use ($token) {
                return $query->where('token', '!=', $token);
            })
            ->count();

        $sumaCoin = $placaCoin + $serieCoin + $motorCoin + $nrpvCoin;

        if($sumaCoin>0){
            $result=array(
                'placas'=>$placaCoin,
                'numSerie'=>$serieCoin,
                'numMotor'=>$motorCoin,
                'nrpv'=>$nrpvCoin,
            );
            $status=201;
        }else{
            $result=array(
                'placas'=>0,
                'numSerie'=>0,
                'numMotor'=>0,
                'nrpv'=>0,
            );
            $status=421;
        }

        $info=array(
            'status'=>"correcto",
            'estado'=>$result,
            'status'=>$status,
        );

        return response()->json($info);

    }

    public function getRegisterCoi($placa, $serie, $motor,$tipoCons,$token=null,$nrpv=null){
        $registros = DB::table('vehiculo')
            ->join('rel_vehiculo_carpeta','rel_vehiculo_carpeta.idVehiculo','=','vehiculo.id')
            ->when($tipoCons==1, function ($query) use ($placa, $serie, $motor) {
                return $query->where(array('placas' => $placa,'numSerie'=>$serie,'numMotor'=>$motor));
            })
            ->when($tipoCons==2, function ($query) use ($placa, $serie, $motor) {
                $query->where('placas','=', $placa);
                $query->orwhere('numSerie','=', $serie);
                $query->orwhere('numMotor','=', $motor);
                return $query;
            })
            ->when($token!=null, function ($query) use ($token) {
                return $query->where('token','!=', $token);
            })
            ->where('vehiculo.status','=',2)
            ->select('vehiculo.*','rel_vehiculo_carpeta.numCarpeta')
            ->get();

        return response()->json($registros);
    }

    public function saveDenuncia(Request $request)
    {

       // dd($request->input('idClaseVehiculo'));
      //$codificacion = base64_encode($request->input('idSecurity'));
      $informe = ToolsTrait::sessionSistema($request->input('idSecurity'));

      if($informe['access']=="true"){

        $getCarpeta = UipjTrait::relVehCarpeta($informe['id']);


        //Se establecen valores por defe'cto
        $vehiculo_id = 0;
        $folio = ($request->input('folioDoctoCirc') == null) ? '': $request->input('folioDoctoCirc');
        $placa = ($request->input('placa') == null) ? '0000000': $request->input('placa');
        $token = ($request->input('token') == null) ? '': $request->input('token');
        $placa_extranjera = ($request->input('idDocumentoCirculacion') == 5) ? 1: 0;
        $nrpv = ($request->input('nrpv') == null) ? '': $request->input('nrpv');
        $senasPartic = ($request->input('senasPartic') == null) ? '': $request->input('senasPartic');
        $numeroeconomico = ($request->input('numeroeconomico') == null) ? '': $request->input('numeroeconomico');

        $update = false;

        $datos_porcentaje= array(
            'placa',
            'permiso' ,
            'idMarca',
            'idSubmarca' ,
            'modelo'  ,
            'idColor',
            'numSerie',
            'motor' ,
            'nrpv' ,
            'idClaseVehiculo',
            'idSubclase',
            'idUso',
            'idAseguradora',
            'senasPartic',
            'idProcedencia'
        );

        //dd($datos_porcentaje);
        $aciertos=0;
        for($i=0; $i<count($datos_porcentaje);$i++){
            ($request->input($datos_porcentaje[$i])?$aciertos++:$aciertos);
        }
        $porcentaje = ($aciertos*100)/(count($datos_porcentaje)-1);

        //dd($porcentaje);

        $datos_comparacion = array(
          'placas' => $placa,
          'idSubmarca' => $request->input('idSubmarca'),
          'modelo' => $request->input('modelo'),
          'idColor' => $request->input('idColor'),
          'numSerie' => $request->input('numSerie'),
          'numMotor' => $request->input('motor'),
          'nrpv' => $nrpv,
          'numeroeconomico' => $numeroeconomico,
          'idTipoVehiculo' => $request->input('idSubclase'),
          'idTipoUso' => $request->input('idUso')
        );

        $datos_vehiculo = array (
          'placa_extranjera' => $placa_extranjera,
          'placas' => $placa,
          'idEstado' => $request->input('idEstado'),
          // 'idMunicipio' => $request->input('idMunicipio'),
          // 'idEstadoPlaca' => $request->input('idEstado'),
          // 'idMunicipio' => 33,
          'idSubmarca' => $request->input('idSubmarca'),
          'modelo' => $request->input('modelo'),
          'idColor' => $request->input('idColor'),
          'numSerie' => $request->input('numSerie'),
          'numMotor' => $request->input('motor'),
          'senasPartic' => $senasPartic,
          'numeroeconomico' => $numeroeconomico,
          'nrpv' => $nrpv,
          'folioDoctoCirc' => $folio,
          'idProcedencia' => $request->input('idProcedencia'),
          'idAseguradora' => $request->input('idAseguradora'),
          'idTipoVehiculo' => $request->input('idSubclase'),
          'idTipoUso' => $request->input('idUso'),
          'idDocumentoCirculacion' => $request->input('idDocumentoCirculacion'),
          'porcentaje' => $porcentaje,
          'tipoDenuncia' => 2,
        );

        //dd($datos_vehiculo);

        if ($token != '') {
          $exist_vehiculo = DB::table('vehiculo')->where(array('token'=>$token))->first();
          $datos_comparacion['token'] = $token;
        } else {
          $exist_vehiculo = null;
        }

        $getVehiculo = DB:: table('vehiculo')->where($datos_comparacion)->count();


        if($getVehiculo>0 && $token == ''){
          $info = array(
            'msj' => 'Los datos ya estan registrados',
            'status' => 419
          );
        }else{
              //si se ingreso un token se actulizan los datos del vehiculo, sino se agrega como robado
              if($exist_vehiculo != null) {
                $update = true;
                $vehiculo_id = $exist_vehiculo->id;
                $datos_vehiculo['status_token'] = 3;
                $vehiculo = DB::table('vehiculo')
                              ->where('id',$vehiculo_id)
                              ->update($datos_vehiculo);
                $ubicacion = DB::table('ubicacion_vehiculo')
                              ->where('id',$vehiculo_id)
                              ->update([
                                        'latitud' => $request->input('latMap'),
                                        'longitud' => $request->input('lonMap'),
                                        'status' => 1 ,
                                        'idDeposito' => 5 ,
                                      ]);
              } else {
                $vehiculo = Vehiculo::create($datos_vehiculo);
                $vehiculo_id = $vehiculo->id;
                $ubicacion = UbicacionVehiculo::create([
                                                        'latitud' => $request->input('latMap'),
                                                        'longitud' => $request->input('lonMap'),
                                                        'status' => 1 ,
                                                        'idDeposito' => 5 ,
                                                        'idVehiculo' => $vehiculo_id
                                                      ]);
              }
              // Guardar documentos cuando se agregan desde el formulario
              if($request->input('optCaptura')=='1'){
                if($request->file('ID_OFICIAL')!=null){
                  $archivo = $request->file('ID_OFICIAL');
                  $test = ToolsTrait::save_image($archivo, $request->input('listaiden1'), $vehiculo_id, 1);
                }
                if($request->file('ID_FACTURA')!=null){
                  $archivo = $request->file('ID_FACTURA');
                  $test = ToolsTrait::save_image($archivo, $request->input('listaiden3'), $vehiculo_id, 1);
                }
              }
              // Guardar documentos tomados desde la webcam
              if($request->input('optCaptura')=='2'){
                  if($request->input('cam_ident')){
                      $archivo = $request->input('cam_ident');
                      $test = ToolsTrait::save_image($archivo, $request->input('listaiden1'), $vehiculo_id, 2);
                  }
                  if($request->input('cam_fact')){
                      $archivo = $request->input('cam_fact');
                      $test = ToolsTrait::save_image($archivo, $request->input('listaiden3'), $vehiculo_id, 2);
                  }
             }
              // dd($getCarpeta);
              $carpeta = RelVehiculoCarpeta::create([
                  'numCarpeta' => $getCarpeta->numCarpeta,
                  'idCarpeta' => $getCarpeta->idCarpeta,
                  'idVehiculo' => $vehiculo_id,
                  'numFiscal' => $getCarpeta->idFiscal,
              ]);
              //Agregar datos al bus
              // dd($informe['id']);
              $datosCarpeta = UipjTrait::get_datos_uipj($informe['id']);
              // dd($datosCarpeta);
              //--comentado
              //$datosCarpeta = UatTrait::get_datos_uat(1);

              $datos_vehiculo['idMarca']  = DB::table('cat_submarca')->where(array('id'=>$datos_vehiculo['idSubmarca']))->first()->idMarca;
              $datos_vehiculo['idClaseVehiculo']  = DB::table('cat_tipo_vehiculo')->where(array('id'=>$datos_vehiculo['idTipoVehiculo']))->first()->idClaseVehiculo;
              $datos_vehiculo['idVeh'] = $vehiculo_id;
              // dd($datosCarpeta->sexo_den);
              BusTrait::conversion_bus($datosCarpeta, $datos_vehiculo);

              // if($request->input('vinVal')==0){
              //     Bitacora::create(['idUsuario' => Auth::user()->id, 'tabla' => 'vehiculo', 'accion' => 'insert', 'descripcion' => 'La serie del registro no es valida.', 'idFilaAccion' => $vehiculo_id]);
              // }
              // dd(Auth::user()->id);
              Bitacora::create(['idUsuario' => Auth::user()->id, 'tabla' => 'vehiculo', 'accion' => 'insert', 'descripcion' => 'Se ha realizado registro de robo de vehículo.', 'idFilaAccion' => $vehiculo_id]);
              $info = array(
                  'msj' => 'Denuncia creada exitosamente',
                  'status' => 200
              );
          }
          return response()->json($info);

        }

    }

    public function updateDoctos(Request $request)
    {

        //dd($request->input('listaiden2'));

        $idVeh = ($request->input('id2') == null) ? '': $request->input('id2');
        // Guardar documentos cuando se agregan desde el formulario
        if($request->input('optCaptura1')=='1'){

            if($request->file('ID_OFICIAL1')!=null){

                $archivo = $request->file('ID_OFICIAL1');
                //dd($archivo);
                $test = ToolsTrait::save_image($archivo, $request->input('listaiden2'), $idVeh, 1);
            }
            if($request->file('ID_FACTURA1')!=null){
                $archivo = $request->file('ID_FACTURA1');
                $test = ToolsTrait::save_image($archivo, $request->input('listaiden4'), $idVeh, 1);
            }

        }
        // Guardar documentos tomados desde la webcam
        if($request->input('optCaptura1')=='2'){
             if($request->input('cam_ident1')){
                //dd("entra archivos");
                $archivo = $request->input('cam_ident1');
                $test = ToolsTrait::save_image($archivo, $request->input('listaiden2'), $idVeh, 2);
            }
            if($request->input('cam_fact1')){
                //dd("entra archivos");
                $archivo = $request->input('cam_fact1');
                $test = ToolsTrait::save_image($archivo, $request->input('listaiden4'), $idVeh, 2);
            }
        }

       //      $carpeta = RelVehiculoCarpeta::create([
       //          'numCarpeta' => $getCarpeta->numCarpeta,
       //          'idCarpeta' => $getCarpeta->id,
       //          'idVehiculo' => $vehiculo_id,
       //          'numFiscal' => $getCarpeta->idFiscal,
       //      ]);

       //      //Agregar datos al bus
       //      $datosCarpeta = UipjTrait::get_datos_uipj(1);
       //      //$datosCarpeta = UatTrait::get_datos_uat(1);
       //      $datos_vehiculo['idMarca']  = DB::table('cat_submarca')->where(array('id'=>$datos_vehiculo['idSubmarca']))->first()->idMarca;
       //      $datos_vehiculo['idClaseVehiculo']  = DB::table('cat_tipo_vehiculo')->where(array('id'=>$datos_vehiculo['idTipoVehiculo']))->first()->idClaseVehiculo;
       //      $datos_vehiculo['idVeh'] = $vehiculo_id;
       //      BusTrait::conversion_bus($datosCarpeta['datosCarpetaRobo'], $datosCarpeta['denunciante'], $datos_vehiculo);

       //      if($request->input('vinVal')==0){
       //          Bitacora::create(['idUsuario' => Auth::user()->id, 'tabla' => 'vehiculo', 'accion' => 'insert', 'descripcion' => 'La serie del registro no es valida.', 'idFilaAccion' => $vehiculo_id]);
       //      }


       //      Bitacora::create(['idUsuario' => Auth::user()->id, 'tabla' => 'vehiculo', 'accion' => 'insert', 'descripcion' => 'Se ha realizado registro de robo de vehículo.', 'idFilaAccion' => $vehiculo_id]);
       //      $info = array(
       //          'msj' => 'Denuncia creada exitosamente',
       //          'status' => 200
       //      );
       //  }


       return response()->json('Denuncia creada exitosamente', 200);

       // dd($idVeh);
        //return response()->json($request);
    }

    public function guardarPorcentaje($idVeh, $porcentaje){
        $updatePorcentaje = DB::table('vehiculo')->where(array('id'=>$idVeh))->update(['porcentaje'=>$porcentaje]);

        return response()->json('Se actualizo porcentaje', 200);
    }


    /* Funcion qu utiliza el datable para cargar datos */
    public function getRegistros($idEncrypt)
   {
       $informe = ToolsTrait::sessionSistema($idEncrypt);
       $idCarpeta = $informe['id'];

       $registros = DB::table('vehiculo')
           ->join('cat_estado', 'cat_estado.id', '=', 'vehiculo.idEstado')
           ->join('cat_submarca','cat_submarca.id','=','vehiculo.idSubmarca')
           ->join('cat_marca', 'cat_marca.id', '=', 'cat_submarca.idMarca')
           ->join('cat_color','cat_color.id', '=','vehiculo.idColor')
           ->join('cat_tipo_vehiculo','cat_tipo_vehiculo.id','=','vehiculo.idTipoVehiculo')
           ->join('cat_clase_vehiculo','cat_clase_vehiculo.id','=','cat_tipo_vehiculo.idClaseVehiculo')
           ->join('cat_tipo_uso','cat_tipo_uso.id','=','vehiculo.idTipoUso')
           ->join('cat_procedencia','cat_procedencia.id','=','vehiculo.idProcedencia')
           ->join('cat_aseguradora','cat_aseguradora.id','=','vehiculo.idAseguradora')
           ->join('vehiculo_robado_bus','vehiculo_robado_bus.idVehiculo','=','vehiculo.id')
           ->join('rel_vehiculo_carpeta', 'rel_vehiculo_carpeta.idVehiculo','=','vehiculo.id')
           ->select('vehiculo.*','cat_marca.nombre as marca','cat_submarca.nombre as submarca', 'cat_submarca.idMarca',
           'cat_color.nombre as color','cat_tipo_vehiculo.nombre as tipoVehiculo','cat_clase_vehiculo.nombre as claseVehiculo','cat_clase_vehiculo.id as idClaseVehiculo',
           'cat_tipo_uso.nombre as tipoUSo','cat_procedencia.nombre as procedencia','cat_aseguradora.nombre as aseguradora', 'cat_estado.nombre as entidadPlaca',
           DB::raw('(CASE
                          WHEN vehiculo.placa_extranjera = 1 THEN CONCAT(placas,"-","Extranjera")
                          WHEN vehiculo.placa_extranjera = 0 THEN CONCAT(placas,"-","Nacional")
                          END) AS placas'),
           // DB::raw('(CASE
           //                WHEN vehiculo.placa_extranjera = 0 THEN CONCAT(placas,"-","Nacional")
           //                END) AS placasNacional')
               'vehiculo.placas as placas2','vehiculo_robado_bus.busFGE','vehiculo_robado_bus.busCNS','vehiculo_robado_bus.busCNSKey')
           ->where(array('tipoDenuncia'=>2, 'vehiculo.status'=>1, 'idCarpeta'=> $idCarpeta))
           ->get();
           //dd($registros);
       return response()->json($registros, 200);
   }
    public function getDoctosVeh($idVehiculo){
        $doctosVeh = DB::table('rel_docs_veh_cat')
            ->join('subcategoria_documento','subcategoria_documento.id','=','rel_docs_veh_cat.idSubCategoriaDocumento')
            ->select('rel_docs_veh_cat.destino', 'rel_docs_veh_cat.idSubCategoriaDocumento as idDocto', 'subcategoria_documento.nombre')
            ->where('idVehiculo','=',$idVehiculo)
            ->where ('idSubCategoriaDocumento','<=',8)
            ->get();

        $info=array(
            'estado' => $doctosVeh,
            'status' => 201,
        );
        return response()->json($info);
    }

    public function getDilidencia($idVehiculo){
        $docDiligencia= RelDocsVehCat::where('idVehiculo','=',$idVehiculo)
            ->where(array('idSubCategoriaDocumento' => 13))
            ->get();

        $info=array(
            'estado' => $docDiligencia,
            'status' => 201,
        );
        return response()->json($info);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
    $folio = ($request->input('folio') == null) ? '': $request->input('folio');
		$placa_extranjera = ($request->input('idDocumentoCirculacion') == 5) ? 1: 0;
    $nrpv = ($request->input('nrpv2') == null) ? '': $request->input('nrpv2');
    $senasPartic = ($request->input('senasPartic2') == null) ? '': $request->input('senasPartic2');



        $vehiculo = DB::table('vehiculo')
            ->where('id',$request->input('id'))
            ->update([
                'placa_extranjera' => $placa_extranjera,
                'placas' => $request->input('placa'),
                'modelo' => $request->input('modelos'),
                'nrpv' => $nrpv,
                'numeroeconomico' => $request->input('numeroeconomico2'),
                'folioDoctoCirc' => $folio,
                'numSerie' => $request->input('numSeries'),
                'numMotor' => $request->input('motors'),
                'senasPartic' => $senasPartic,
                'idProcedencia' => $request->input('idProcedencia2'),
                'idSubmarca' => $request->input('idSubmarcas'),
                'idColor' => $request->input('idColors'),
                'idTipoVehiculo' => $request->input('idSubclase2'),
                'idTipoUso' => $request->input('idUso2'),
                'idAseguradora' => $request->input('idAseguradora2'),
                'idDocumentoCirculacion' => $request->input('idDocCirculacion'),
                'status_token' => 1
            ]);

        Bitacora::create(['idUsuario' => Auth::user()->id, 'tabla' => 'vehiculo', 'accion' => 'update', 'descripcion' => 'Se ha modificado el registro de robo de un vehículo.', 'idFilaAccion' => $request->input('id')]);

        return response()->json('El registro se actualiz&oacute; correctamente', 200);
    }

    public function actualizarValidacion(Request $request){
        //dd($request->all());
        $llave = $request->input('llaveExterna');
        $idVehiculo = $request->input('id1');
        $folio = ($request->input('folio1') == null) ? '': $request->input('folio1');
		    $placa_extranjera = ($request->input('idDocCirculacion1') == 5) ? 1: 0;

        $datos_vehiculo = array (
            'placa_extranjera' => $placa_extranjera,
            'placas' => $request->input('idplaca1'),
            'idSubmarca' => $request->input('idSubmarca1'),
            'modelo' => $request->input('modelo1'),
            'idColor' => $request->input('idColor1'),
            'numSerie' => $request->input('numSerie1'),
            'numMotor' => $request->input('motor1'),
            'senasPartic' => $request->input('senasPartic1'),
            'nrpv' => $request->input('nrpv1'),
            'folioDoctoCirc' => $folio,
            'idProcedencia' => $request->input('idProcedencia1'),
            'idAseguradora' => $request->input('idAseguradora1'),
            'idTipoVehiculo' => $request->input('idSubclase1'),
            'idTipoUso' => $request->input('idUso1'),
            'idDocumentoCirculacion' => $request->input('idDocCirculacion1'),

        );

        VehRobadoBus::where('idVehiculo','=',$idVehiculo)
            ->update(['status_muestra'=>1]);





        $vehiculo = DB::table('vehiculo')
            ->where('id','=',$idVehiculo)
            ->update($datos_vehiculo);

        //Insertar en BUS
        $datos_uipj = UipjTrait::get_datos_uipj(2);
        $datos_vehiculo['idMarca']  = DB::table('cat_submarca')->where(array('id'=>$datos_vehiculo['idSubmarca']))->first()->idMarca;
        $datos_vehiculo['idClaseVehiculo']  = DB::table('cat_tipo_vehiculo')->where(array('id'=>$datos_vehiculo['idTipoVehiculo']))->first()->idClaseVehiculo;
        $datos_vehiculo['idVeh'] = $idVehiculo;
        BusTrait::conversion_bus($datos_uipj['datosUipj'], $datos_uipj['denunciante'], $datos_vehiculo,$llave,2);


        //DB::table('temporal_vehiculo')->where('id', '=',$idVehiculo )->delete();

        // Bitacora::create(['idUsuario' => Auth::user()->id, 'tabla' => 'vehiculo', 'accion' => 'update', 'descripcion' => 'Se ha modificado el registro de robo de un vehículo.', 'idFilaAccion' => $request->input('id')]);
        Bitacora::create(['idUsuario' => Auth::user()->id, 'tabla' => 'vehiculo', 'accion' => 'update', 'descripcion' => 'Se ha modificado el registro de robo de un vehículo de acuerdo al bus.', 'idFilaAccion' => $idVehiculo]);

        return response()->json('El registro se actualiz&oacute; correctamente', 200);
    }

    public function diligenciaDocs(Request $request){
        $img1 = $request->file('diligenciaFormaro');
        $vehiculo_id= $request->input('iddiligencia');
        $tipoArchivo = 13;
        $mensajes ="";
        $act = 'cargada';
        $Rename = str_replace('-','',str_replace(':','',str_replace(' ', '', Carbon::now('America/Monterrey'))));
        $fileName = $Rename.'.'.$img1->getClientOriginalExtension();
        $carpetaStore = 'diligencia/';
        $pathd1 = DB::table('rel_docs_veh_cat')->where(array('idVehiculo'=>$vehiculo_id, 'idSubCategoriaDocumento' => $tipoArchivo))->first();

        $vehiculo = DB::table('vehiculo')
            ->where('id','=',$vehiculo_id)
            ->update([
                'status_diligencia' => 1
            ]);

        if($pathd1){
            $act = 'actualizo';
            Storage::disk('local')->delete($pathd1->destino);
            DB::table('rel_docs_veh_cat')->where(array('idVehiculo'=>$vehiculo_id, 'idSubCategoriaDocumento' => $tipoArchivo))->delete();
        }

        $registro1 = new RelDocsVehCat();
        $registro1->destino = $carpetaStore.$fileName;
        $registro1->idVehiculo = $vehiculo_id;
        $registro1->idImagen =0;
        $registro1->idSubCategoriaDocumento = $tipoArchivo;

        if($registro1->save()){
            $mensajes = array(
                'msj'=>'Imagen '.$act.' correctamente',
                'status' => 'success'
            );
            $path1 = $img1->storeAs($carpetaStore, $fileName);
        }else{
            $mensajes = array(
                'msj'=>'Problema con la carga de imagenes',
                'status' => 'error'
            );
        }
        return response()->json($mensajes, 200);
    }

    public function existeDiligencia ($idVehiculo) {
        $docDiligencia = RelDocsVehCat::where('idVehiculo','=',$idVehiculo)
            ->where(array('idSubCategoriaDocumento' => 13))
            ->get();
        if(count($docDiligencia) > 0){
            return 1;
        } else {
            return 0;
        }
    }


    public function showPanel()
    {
        return view('panel.showPanel');
    }

    public function validacionRobadosV($idVeh){
        $estatusBus = ToolsTrait::validacionRobados($idVeh);
        // $estatusBus = VehRobadoBus::where('idVehiculo','=',$idVeh)
        //             ->select('busFGE','busCNS','busCNSKey')
        //             ->get();

         return $estatusBus;
    }

    public function validacionRobadosVdata($idVeh){
        $dataValidacion = ToolsTrait::validacionRobadosData($idVeh);
        return $dataValidacion;
    }
    /*
    public function pruebaDatos($id){
        $datos_uipj = UipjTrait::get_datos_uipj($id);
        dd($datos_uipj);
    }
    */

    public function validarRegistro(){

    }

}
