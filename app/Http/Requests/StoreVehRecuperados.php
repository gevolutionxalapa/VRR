<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreVehRecuperados extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'placa' => 'required',
          'idDocumentoCirculacion' => 'required',
          'idEstado' => 'required',
          'idMunicipio' => 'required',
          'idMarca' => 'required',
          'idSubmarca' => 'required',
          'modelo' => 'required',
          'idColor' => 'required',
          'idnumserie' => 'required',
          // 'idnrpv' => 'required',
          'idClaseVehiculo' => 'required',
          'idSubclase' => 'required',
          'idUso' => 'required',
          'idProcedencia' => 'required',
          'idAseguradora' => 'required',
          'idResguardo' => 'required',
          'idnummotor' => 'required',



        ];
    }
}
