<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use SimpleSoftwareIO\QrCode\BaconQrCodeGenerator;

class MailPredenuncia extends Mailable
{
    use Queueable, SerializesModels;

    
    public $token;
    public $codigoQR;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(String $token, String $codigoQR)
    {
        $this->token = $token;
        $this->codigoQR = $codigoQR;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.mailPredenuncia')
        ->from('vehiculos@fiscaliaveracruz.gob.mx','FGE-Vehiculos')
        ->subject('Prueba envio PDF');
    }
}
