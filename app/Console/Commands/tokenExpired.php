<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Carbon\Carbon;

class tokenExpired extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:token';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Eliminar token después de 72hrs. de inactividad';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = Carbon::now();
        $data = DB::table('vehiculo')->where('status_token','!=',3)->where(DB::raw('DATE_ADD(created_at,INTERVAL 3 DAY)'),'<=',$date)->delete();
        //echo $data;
    }
}
