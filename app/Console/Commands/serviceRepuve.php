<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Vehiculo;
use App\Traits\ToolsTrait;

class serviceRepuve extends Command
{
    use ToolsTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:repuve';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verificación automática de datos en web service repuve';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $registros = Vehiculo::where('status_repuve','==',0)->get();
        $registros;
        foreach ($registros as $vehic) {            
            echo $vehic['placas'];
            //lanzar evento de repube
            $comprobo = ToolsTrait::comprobacionRepuve($vehic['placas'],$vehic['numSerie']);
            echo $comprobo;
            if(isset($comprobo)){
                echo "funciona";
                //fue encontrado sin reporte de robo
                //Vehiculo::where('id','=',$vehic['id'])->update(['status_repuve'=>1]);
                //fue encontrado con reporte de robo
                //Vehiculo::where('id','=',$vehic['id'])->update(['status_repuve'=>2]);                
            }
            
        }
        
    }
}
