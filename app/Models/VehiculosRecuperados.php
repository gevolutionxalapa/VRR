<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VehiculosRecuperados extends Model
{
    protected $table = 'vehiculos_recuperados';

    protected $fillable = [
        'id', 'idVeh', 'calle_rec', 'numext_rec', 'numint_rec', 'colonia_rec', 'id_municipio_rec', 'id_entidad_rec',  'cp_rec', 'fecha_rec', 'hora_rec',
    ];



    public function estado()
    {
        return $this->belongsTo('App\Models\CatEstado');
    }

    public function municipio()
    {
        return $this->belongsTo('App\Models\CatMunicipio');
    }


}
