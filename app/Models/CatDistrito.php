<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatDistrito extends Model
{
    protected $table = 'cat_distrito';
    
    protected $fillable = [
        'id', 'nombre', 'idFiscalia',
    ];

    public function fiscalia(){
        return $this->belongsTo('App\Models\CatFiscalia');
    }
}
