<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bitacora extends Model
{
	public $table='bitacora';
    public $fillable = [
       'id',
       'idUsuario',
       'tabla',
       'accion',
       'descripcion',
       'idFilaAccion'
   ];



public function user(){
       return $this->belongsTo('App\User');
   }
}
