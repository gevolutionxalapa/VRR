<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatUnidad extends Model
{
    protected $table = 'cat_unidad';

    protected $fillable = [
        'id', 'nombre', 'longitud', 'latitud', 'calle', 'numero', 'colonia', 'idCp'
    ];
}
