<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DispositivoInfo extends Model
{
    protected $table = 'dispositivo_info';

    protected $filable = ['navegador', 'ip', 'so', 'latitud', 'longitud'];

    public function vehiculo()
	{
		return $this->hasMany('App\Models\Vehiculo.php');
	}


}
