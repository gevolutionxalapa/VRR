<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VehEntregadosBus extends Model
{
    protected $table = 'vehiculo_entregado_bus';

    protected $fillable = [
        'id', 'idVeh', 'SERIE_ALTERADA', 'MOTOR_ALTERADO', 'CALLE_ENTREGA', 'NUMEXT_ENTREGA', 'NUMINT_ENTREGA', 'COLONIA_ENTREGA',  
        'ID_MUNICIPIO_ENTREGA', 'ID_ENTIDAD_ENTREGA', 'CP_ENTREGA','REFERENCIA_ENTREGA', 'INSPECCION', 'FECHA_ENTREGA', 'HORA_ENTREGA', 
        'SERIE', 'MOTOR', 'MONTO_VEHICULO', 'FACTURA_VEHICULO', 'FECHA_FACTURA', 'CRED_ELECT_PROP', 'PASAPORTE_PRO','CEDULA_PROF_PROP',
        'COMPROB_DOMIC_PROP','PERSONA_ENTREGA','llave_externa',
    ];

    
}
