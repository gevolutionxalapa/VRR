<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RelVehiculoCarpeta extends Model
{
    public $table = 'rel_vehiculo_carpeta';

    public $fillable = [
    	'numCarpeta',
        'idCarpeta',
    	'idVehiculo',
        'status',
        'numFiscal',
        'idUnidad',
        'nivel',
        'tokenSesion'
    ];

    public function vehiculo()
    {
        return $this->hasMany('App\Models\Vehiculo');
    }


}
