<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatClaseVehiculo extends Model
{
    protected $table = 'cat_clase_vehiculo';

    protected $fillable = [
        'id', 'nombre',
    ];

    public function clasevehiculo(){
    	return $this->hasMany('App\Models\CatClaseVehiculo');
    }

    public function vehiculos(){
    	return $this->hasMany('App\Models\Vehiculo');
    }
}
