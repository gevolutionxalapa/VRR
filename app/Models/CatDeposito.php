<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatDeposito extends Model
{
    protected $table = 'cat_deposito';

    protected $fillable = [
        'id', 'nombre',
    ];

    public function ubicacion_vehiculo(){
        return $this->hasMany('App\Models\UbicacionVehiculo');
    }


    public static function deposito($id){
        return CatDeposito::select('id', 'nombre')->orderBy('nombre', 'ASC')->get();
    }
}
