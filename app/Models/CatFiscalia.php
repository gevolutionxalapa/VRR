<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatFiscalia extends Model
{
    protected $table = 'cat_fiscalia';

    protected $fillable = [
        'id', 'nombre',
    ];

    public function distritos(){
        return $this->hasMany('App\Models\CatDistrito');
    }
}
