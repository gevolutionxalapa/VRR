<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatTipoUso extends Model
{
    protected $table = 'cat_tipo_uso';

     protected $fillable = [
        'id', 'nombre',  
    ];

    public function uso(){
    	return $this->hasMany('App\Models\CatTipoUso');
    }

    public function vehiculos(){
    	return $this->hasMany('App\Models\Vehiculo');
    }
}
