<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatCategoriaDocumento extends Model
{
    protected $table = 'categoria_documento';

    protected $fillable = [
        'id', 'nombre',
    ];

    public function subcatdoctos(){
    	return $this->hasMany('App\Models\CatSubCategoriaDocumento');
    }

}
