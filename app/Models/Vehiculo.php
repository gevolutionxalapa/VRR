<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vehiculo extends Model
{
    protected $table = 'vehiculo';

    protected $fillable = [
        'id','placa_extranjera', 'placas',
        'status', 'modelo', 'nrpv', 'permiso',
         'numSerie', 'numMotor',  'senasPartic','numeroeconomico',
          'token', 'folioDoctoCirc','telefono','idSubmarca',
          'tipoDenuncia', 'idColor', 'idTipoVehiculo',
           'idTipoUso', 'idProcedencia', 'idAseguradora',
           'idEstado', 'idMunicipio', 'idLocalidad', 'idDocumentoCirculacion','observacion',
        'llave_externa', 'status_repuve','porcentaje','propietario_asc'
    ];

    /*
    public function tipifDelito()
    {
        return $this->belongsTo('App\Models\TipifDelito');
    }
    */

    public function estado()
    {
        return $this->belongsTo('App\Models\CatEstado');
    }

    public function submarca()
    {
        return $this->belongsTo('App\Models\CatSubmarca');
    }

    public function color()
    {
        return $this->belongsTo('App\Models\CatColor');
    }

    public function tipoVehiculo()
    {
        return $this->belongsTo('App\Models\CatTipoVehiculo');
    }

    public function tipoUso()
    {
        return $this->belongsTo('App\Models\CatTipoUso');
    }

    public function procedencia()
    {
        return $this->belongsTo('App\Models\CatProcedencia');
    }

    public function aseguradora()
    {
        return $this->belongsTo('App\Models\CatAseguradora');
    }
     public function carpeta(){
        return $this->belongsTo('App\Models\Carpeta');
    }

    /*public function doctos(){
        return $this->belongToMany('App\Models\RelDocsVehCat');
    }*/
}
