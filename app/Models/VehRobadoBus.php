<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VehRobadoBus extends Model
{
    protected $table = 'vehiculo_robado_bus';

    protected $fillable = [
        'id_bus','id_alterna','id_fuente','nrpv','averiguacion','fecha_averigua','hora_averigua','agencia_mp','agente_mp','id_modalidad','responsables','victimas','peculiaridades','senas_delicuente','vestimenta','comportamiento','id_puesto','fecha_robo','hora_robo','calle_robo','num_ext_robo','num_int_robo','colonia_robo','id_municipio_robo','id_entidad_robo','cp_robo','referencia_robo','id_tipo_lugar','id_delito','id_arma_asoc','id_marca_asoc','id_submarca_asoc','nombre_dec','paterno_den','materno_den','rfc_den','curp_den','licencia_den','pasaporte_den','sexo_den','calle_den','numext_dom_den','numint_dom_den','colonia_den','id_municipio_den','id_identidad_den','cp_den','telefono_den','correo_den','placa','permiso','id_entidad_placa','id_marca','id_submarca','modelo','id_color','serie','motor','id_clase_vehiculo','id_tipo_vehiculo','id_tipo_uso','senas','id_procedencia','id_aseguradora','MOVIMIENTO','fiscalia','id','robo','fechaact','idVehiculo','busFGE','busCNS','busCNSKey','fechaenv','horaenv','fecharesp','horaresp','llave_externa','placa_extranjero','status_muestra',
    ];
}
