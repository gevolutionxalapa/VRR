<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatDocumentoCirculacion extends Model
{
    protected $table = 'cat_documentos_circulacion';

    protected $fillable = [
        'id', 'nombre'
    ];

    public function vehiculos(){
      return $this->hasMany('App/Models/Vehiculo');
    }
}
