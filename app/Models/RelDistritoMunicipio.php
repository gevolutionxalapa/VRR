<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RelDistritoMunicipio extends Model
{
    protected $table = 'rel_distrito_municipio';

    protected $fillable = [
        'idDistrito', 'idUnidad',
    ];
}
