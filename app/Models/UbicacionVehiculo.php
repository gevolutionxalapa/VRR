<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UbicacionVehiculo extends Model
{
    public $table = 'ubicacion_vehiculo';

    public $fillable = [
    	'latitud',
        'longitud',
    	'idVehiculo',
        'status',
        'idDeposito'
    ];

    public function vehiculo()
    {
        return $this->hasMany('App\Models\Vehiculo');
    }

    public function deposito()
    {
        return $this->belongsTo('App\Models\CatDeposito');
    }
}
