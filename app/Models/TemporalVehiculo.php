<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TemporalVehiculo extends Model
{
    //
    protected $table = 'temporal_vehiculo';

    protected $fillable = [
        'id', 'placas', 'status', 'modelo', 'nrpv', 'permiso', 'numSerie', 'numMotor',  'senasPartic', 'token', 'folioDoctoCirc','idSubmarca', 'tipoDenuncia', 'idColor', 'idTipoVehiculo', 'idTipoUso', 'idProcedencia', 'idAseguradora', 'idEstado', 'idMunicipio', 'idLocalidad', 'idDocumentoCirculacion','observacion','registradoPor',
    ];
}
