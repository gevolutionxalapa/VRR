<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatTipoVehiculo extends Model
{
    protected $table = 'cat_tipo_vehiculo';

     protected $fillable = [
        'id', 'idClaseVehiculo', 'nombre',
    ];

    public function vehiculos(){
    	return $this->hasMany('App\Models\Vehiculo');
    }
     public function clasevehiculo(){
        return $this->belongsTo('App\Models\CatClaseVehiculo');
    }

    public static function tipovehiculo($id){
        return CatTipoVehiculo::select('id', 'nombre')->where('idClaseVehiculo', '=', $id)->orderBy('nombre', 'ASC')->get();
    }


}
