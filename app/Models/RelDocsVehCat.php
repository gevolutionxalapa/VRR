<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RelDocsVehCat extends Model
{
  protected $table = 'rel_docs_veh_cat';
  protected $fillable = [
      'id', 'idSubCategoriaDocumento', 'destino', 'idVehiculo'
  ];

  /*public function vehiculo(){
      return $this->belongsToMany('App\Models\Vehiculo');
  }
   public function SubCategoria(){
      return $this->belongsToMany('App\Models\CatSubCategoriaDocumento');
  }*/
}
