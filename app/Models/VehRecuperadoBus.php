<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VehRecuperadosBus extends Model
{
    protected $table = 'vehiculo_recuperado_bus';

    protected $fillable = [
        'id_bus','id_alterna','id_fuente','documento', 'fecha_docto','hora_docto','agencia_docto','agente_docto','placa',
        'permiso','motor','serie','senas','id_entidad_recupera','id_deposito','calle_rec','numext_rec',
        'numint_rec','colonia_rec','id_municipio_rec','cp_rec','referencia_rec','id_color','fecha_rec',
        'hora_rec','llave_externa','idVehiculo','busFGE','busCNS','busCNSKey',
        'fechaenv','horaenv','fecharesp','horaresp','idbus','fiscalia','fechaact', 'averiguacion','id'
    ];
}
