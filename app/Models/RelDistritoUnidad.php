<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RelDistritoUnidad extends Model
{
    protected $table = 'rel_distrito_unidad';

    protected $fillable = [
        'idDistrito', 'idMunicipio', 'idEstado'
    ];
}
