<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatSubCategoriaDocumento extends Model
{
   protected $table = 'subcategoria_documento';

    protected $fillable = [
        'id', 'nombre', 'idcatdocumento', 'nombre'
    ];

    public function catdocto(){
    	return $this->belongTo('App\Models\CatCategoriaDocumento');
    }

    public static function SubCateDoctos($id){
        return CatSubCategoriaDocumento::select('id', 'nombre')->where('idcatdocumento', '=', $id)->orderBy('nombre', 'ASC')->get();
    }

/*
    public function doctos(){
    	return $this->belongTo('App\Models\RelDocsVehCat');
    }*/
}
