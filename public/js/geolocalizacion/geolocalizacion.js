function get_position(lat, long) {
    var nav = null;
    var position = null;

    function requestPosition() {
        if (nav == null) {
            nav = window.navigator;
        }

        var geoloc = nav.geolocation;
        if (geoloc != null) {
            position = geoloc.getCurrentPosition(successCallback, errorCallback);
        }
    }

    function successCallback(pos) {
        //position = { latitud: pos.coords.latitude, longitud: pos.coords.longitude };
        // $('#' + lat).val(pos.coords.latitude);
        // $('#' + long).val(pos.coords.longitude);
        $('#lat').val(pos.coords.latitude);
        $('#long').val(pos.coords.longitude);
        console.log('Infor dispositivo');
        console.log(position);
    }

    function errorCallback(error) {
        // Check for known errors
        switch (error.code) {
            case error.PERMISSION_DENIED:
                strMessage = "Access to your location is turned off. " +
                    "Change your settings to turn it back on.";
                break;
            case error.POSITION_UNAVAILABLE:
                strMessage = "Data from location services is " +
                    "currently unavailable.";
                break;
            case error.TIMEOUT:
                strMessage = "Location could not be determined " +
                    "within a specified timeout period.";
                break;
            default:
                break;
        }
    }

    requestPosition();

}
