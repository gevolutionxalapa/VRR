function cargarUbicacionToken(latitud, longitud) {


    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(latitud, longitud),
        draggable: true,
        map: map
    });

    $('#latMap').val(marker.getPosition().lat());
    $('#lonMap').val(marker.getPosition().lng());

    marker.addListener('dragend', function() {
        $('#latMap').val(marker.getPosition().lat());
        $('#lonMap').val(marker.getPosition().lng());
    });

    map.addListener('click', function(e) {
        placeMarkerAndPanTo(e.latLng, map, marker);
    });

    function placeMarkerAndPanTo(latLng, map, marker) {
        if (marker) {
            marker.setPosition(latLng);
        } else {
            marker = new google.maps.Marker({
                position: latLng,
                map: map
            });
        }
        map.panTo(latLng);
        $('#latMap').val(marker.getPosition().lat());
        $('#lonMap').val(marker.getPosition().lng());
    }
}