$("#selector :input").change(function(event) {
    //console.log(this); // points to the clicked input button
    var valor = $(event.target).val();
    if (valor == "archivo") {
        $('#cam_ide').attr('disabled', true);
        $('#cam_fac').attr('disabled', true);
        $(".div1,#complemento_asc").show();
        $(".div2").hide();
        $('#DOC1').prop('required', true);
        $('#DOC5').prop('required', true);
        $('#cam_ide').removeAttr('required');
        //$("#listaiden_op1,#listaiden_op2").prop('required',true);
        $('#tipe_load').val(1);
        $('#mini_ife, #mini_fac').val("");
        $('#ife_mini, #fact_mini').html("");
        $("#listaiden_op1").val('').trigger('change');
        $("#listaiden_op2").val('').trigger('change');
    } else if (valor == "camara") {
        $('#DOC1').css('display', 'none');
        $('#DOC5').css('display', 'none');
        $(".div1").hide();
        $(".div2,#complemento_asc").show();
        $('#cam_ide').prop('required', true);
        //$("#listaiden_op1,#listaiden_op2").prop('required',true);
        $('#tipe_load').val(2);
        $("#listaiden_op1").val('').trigger('change');
        $("#listaiden_op2").val('').trigger('change');
        $('#DOC1').removeAttr('required');
        $('#DOC5').removeAttr('required');

    }else {
        toastr.error("No ha ingresado Nombre","Aviso!");
    }
});
 


// $(document).ready(function() {
//     $("input[type=radio]").click(function(event){
//         var valor = $(event.target).val();
//         if(valor =="archivo"){
//             $("#div1").show();
//             $("#div2").hide();
//         } else if (valor == "camara") {
//             $("#div1").hide();
//             $("#div2").show();
//         } else {
//             // Otra cosa
//         }
//     });
// });
