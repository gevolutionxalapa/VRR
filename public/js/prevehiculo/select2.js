 $(document).ready(function() {
   function setCurrency(currency) {
     if (!currency.id) {
       return currency.text;
     }
     var $currency = $('<span class="glyphicon glyphicon-' + currency.element
       .value + '">' + currency.text + '</span>');
     return $currency;
   };
   $(".templatingSelect2").select2({
     placeholder: "SELECCIONE UNA ENTIDAD FEDERATIVA", //placeholder
     templateResult: setCurrency,
     templateSelection: setCurrency
   });
   $(".templatingSelect3").select2({
     placeholder: "SELECCIONE LA MARCA", //placeholder
     templateResult: setCurrency,
     templateSelection: setCurrency
   });
   $(".idSubmarca").select2({
     placeholder: "SELECCIONE LA SUBMARCA", //placeholder
     templateResult: setCurrency,
     templateSelection: setCurrency
   });
   $(".templatingSelect5").select2({
     placeholder: "SELECCIONE UN COLOR", //placeholder
     templateResult: setCurrency,
     templateSelection: setCurrency
   });
   $(".templatingSelect6").select2({
     placeholder: "SELECCIONE UNA ASEGURADORA", //placeholder
     templateResult: setCurrency,
     templateSelection: setCurrency
   });
   $(".templatingSelect10").select2({
     placeholder: "SELECCIONE UN TIPO DE DOCUMENTO", //placeholder
     templateResult: setCurrency,
     templateSelection: setCurrency
   });
   $(".templatingSelect11").select2({
     placeholder: "SELECCIONE UN MUNICIPIO", //placeholder
     templateResult: setCurrency,
     templateSelection: setCurrency
   });
     $(".templatingSelect12").select2({
      placeholder: "SELECCIONE EL DEPÓSITO DE RESGUARDO ", //placeholder
      templateResult: setCurrency,
      templateSelection: setCurrency
    });
    $(".templatingSelect14").select2({
      placeholder: "SELECCIONE CODIGO POSTAL", //placeholder
      templateResult: setCurrency,
      templateSelection: setCurrency
    });
    $(".templatingSelect15").select2({
      placeholder: "SELECCIONE LA CALLE", //placeholder
      templateResult: setCurrency,
      templateSelection: setCurrency
    });
    $(".templatingSelect16").select2({
      placeholder: "SELECCIONE LA COLONIA", //placeholder
      templateResult: setCurrency,
      templateSelection: setCurrency
    });
         $(".estado").select2({
    placeholder: "SELECCIONE UNA ENTIDAD FEDERATIVA", //placeholder
    templateResult: setCurrency,
    templateSelection: setCurrency
  });
                 $(".municipio").select2({
    placeholder: "SELECCIONE UN MUNICIPIO", //placeholder
    templateResult: setCurrency,
    templateSelection: setCurrency
  });
                 
 })
