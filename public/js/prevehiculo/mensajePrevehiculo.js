swal({
  title: "¿Esta seguro de que quiere continuar?",
  text: "Para continuar da clic en un botón",
  type: "warning",
  showCancelButton: true,
  confirmButtonClass: "btn-danger",
  confirmButtonText: "Continuar",
  cancelButtonText: "Cancelar",
  closeOnConfirm: false,
  closeOnCancel: false
},
function(isConfirm) {
  if (isConfirm) {
    swal("Deleted!", "Denuncia registrada correctamente", "success");
  } else {
    swal("Cancelled", "No se registro la denuncia)", "error");
  }
});
