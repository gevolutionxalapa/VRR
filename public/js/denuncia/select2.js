$(document).ready(function() {
    function setCurrency(currency) {
        if (!currency.id) { return currency.text; }
        var $currency = $('<span class="glyphicon glyphicon-' + currency.element.value + '">' + currency.text + '</span>');
        return $currency;
    };
    $(".templatingSelect2").select2({
        placeholder: "SELECCIONE UNA ENTIDAD FEDERATIVA", //placeholder
        templateResult: setCurrency,
        templateSelection: setCurrency,
    });
    $(".templatingSelect3").select2({
        placeholder: "SELECCIONE LA MARCA", //placeholder
        templateResult: setCurrency,
        templateSelection: setCurrency
    });
    $(".idSubmarca").select2({
        placeholder: "SELECCIONE LA SUBMARCA", //placeholder
        templateResult: setCurrency,
        templateSelection: setCurrency
    });
    $(".templatingSelect5").select2({
        placeholder: "SELECCIONE UN COLOR", //placeholder
        templateResult: setCurrency,
        templateSelection: setCurrency
    });
    $(".templatingSelect6").select2({
        placeholder: "SELECCIONE ASEGURADORA", //placeholder
        templateResult: setCurrency,
        templateSelection: setCurrency
    });
    $(".templatingSelectPropietario").select2({
        placeholder: "SELECCIONE PROPIETARIO", //placeholder
        templateResult: setCurrency,
        templateSelection: setCurrency
    });
    $(".templatingSelect7").select2({
        placeholder: "SELECCIONE EL TIPO DE VEHICULO", //placeholder
        templateResult: setCurrency,
        templateSelection: setCurrency
    });

    $(".templatingSelect8").select2({
        placeholder: "SELECCIONE EL TIPO DE USO", //placeholder
        templateResult: setCurrency,
        templateSelection: setCurrency
    });
    $(".templatingSelect9").select2({
        placeholder: "SELECCIONE LA PROCEDENCIA", //placeholder
        templateResult: setCurrency,
        templateSelection: setCurrency
    });
    $(".templatingSelect19").select2({
        placeholder: "SELECCIONE COMPROBANTE DE DOMICILIO", //placeholder
        templateResult: setCurrency,
        templateSelection: setCurrency
    });
    $(".templatingSelect17").select2({
        placeholder: "SELECCIONE UNA IDENTIFICACIÓN OFICIAL", //placeholder
        templateResult: setCurrency,
        templateSelection: setCurrency
    });
    $(".templatingSelect18").select2({
        placeholder: "SELECCIONE UNA OPCIÓN", //placeholder
        templateResult: setCurrency,
        templateSelection: setCurrency
    });
    $(".templatingSelect20").select2({
        placeholder: "SELECCIONE UNA AUTORIDAD", //placeholder
        templateResult: setCurrency,
        templateSelection: setCurrency
    });
    $(".templatingSelect21").select2({
        placeholder: "SELECCIONE UN PROPIETARIO O UN REPRESENTANTE", //placeholder
        templateResult: setCurrency,
        templateSelection: setCurrency
    });
    $(".templatingSelect1").select2({
        placeholder: "SELECCIONE EL SUBTIPO DE VEHICULO", //placeholder
        templateResult: setCurrency,
        templateSelection: setCurrency
    });
    $(".templatingSelect10").select2({
        placeholder: "SELECCIONE UN TIPO DE DOCUMENTO", //placeholder
        templateResult: setCurrency,
        templateSelection: setCurrency
    });
    $(".templatingSelect11").select2({
        placeholder: "SELECCIONE UN MUNICIPIO", //placeholder
        templateResult: setCurrency,
        templateSelection: setCurrency
    });
    $(".templatingSelect12").select2({
        placeholder: "SELECCIONE EL DEPÓSITO DE RESGUARDO", //placeholder
        templateResult: setCurrency,
        templateSelection: setCurrency
    });
    $(".templatingSelect13").select2({
        placeholder: "SELECCIONE EL TIPO DE ENTIDAD", //placeholder
        templateResult: setCurrency,
        templateSelection: setCurrency
    });
    $(".templatingSelect14").select2({
        placeholder: "SELECCIONE CODIGO POSTAL", //placeholder
        templateResult: setCurrency,
        templateSelection: setCurrency
    });
    $(".templatingSelect15").select2({
        placeholder: "SELECCIONE LA CALLE", //placeholder
        templateResult: setCurrency,
        templateSelection: setCurrency
    });
    $(".templatingSelect16").select2({
        placeholder: "SELECCIONE LA COLONIA", //placeholder
        templateResult: setCurrency,
        templateSelection: setCurrency
    });
    $(".listaiden1").select2({
        placeholder: "SELECCIONE UNA OPCIÓN", //placeholder
        templateResult: setCurrency,
        templateSelection: setCurrency
    });
    $(".listaiden3").select2({
        placeholder: "SELECCIONE UNA OPCIÓN", //placeholder
        templateResult: setCurrency,
        templateSelection: setCurrency
    });
    $(".estado").select2({
        placeholder: "SELECCIONE UNA ENTIDAD FEDERATIVA", //placeholder
        templateResult: setCurrency,
        templateSelection: setCurrency
    });
    $(".municipio").select2({
        placeholder: "SELECCIONE UN MUNICIPIO", //placeholder
        templateResult: setCurrency,
        templateSelection: setCurrency
    });
    $(".idSubmarcas").select2({
        placeholder: "SELECCIONE UNA SUBMARCA", //placeholder
        templateResult: setCurrency,
        templateSelection: setCurrency
    });
})