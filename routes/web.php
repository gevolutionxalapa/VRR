<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','PreVehiculoController@showIntro')->name('welcome');

/**/Route::get('guia', function () {
    return view('preVehiculo.guia');
})->name('guia');



//* Rutas con Prevehiculos
//Route::get('ver-predenuncia', 'PreVehiculoController@showPredenuncia')->name('showPredenuncia.vehiculo');
Route::get('ver-preregistro', 'PreVehiculoController@showPredenuncia')->name('showPredenuncia.vehiculo');
Route::post('guardar-predenuncia', 'PreVehiculoController@savePredenuncia')->name('savePredenuncia.vehiculo');
Route::get('ver-guia', 'PreVehiculoController@showGuia')->name('showGuia');
//Route::post('carga-guia', 'PreVehiculoController@recibirDatos')->name('cargar-guia');
Route::post('pre-registro', 'PreVehiculoController@recibirDatos')->name('cargar-guia');
Route::get('getsubmarca/{marca}', 'PreVehiculoController@get_submarca')->name('submarca');
Route::get('getmunicipio/{estado}', 'PreVehiculoController@get_municipio')->name('municipio');
Route::get('ver-intro', 'PreVehiculoController@showIntro')->name('showIntro');
Route::post('ver-mapa', 'PreVehiculoController@showMap')->name('showMap');
Route::get('ver-mapa', 'PreVehiculoController@showMap')->name('showMap');

Route::get('enviar-mail/{correo?}/{folio?}/{idEstado?}/{idMunicipio?}', 'PreVehiculoController@mailPredenuncia')->name('sendMail');
//Rutas Persona
Route::get('ver-persona', 'PersonaController@showPersona')->name('showPersona');
Route::post('cargar-persona', 'PersonaController@savePersona')->name('savePersona');

// Rutas de Carperta
Route::get('ver-carpeta', 'CarpetaController@showCarpeta')->name('ver');
Route::get('turnado-fiscal/{numFiscal}/{numFiscalNew}', 'CarpetaController@turnadoFiscal')->name('turnadoFiscal');

Route::middleware(['auth'])->group(function () {
    //Rutas Denuncia
    //Route::get('ver-denuncia', 'RegVehiculoController@showDenuncia')->name('showDenuncia');
    Route::get('ver-registro', 'RegVehiculoController@showDenuncia',['middleware'=>'role:editor'])->name('showDenuncia');
    Route::get('cargar-token/{token?}', 'RegVehiculoController@getToken')->name('getToken');
    Route::post('guardar-denuncia', 'RegVehiculoController@saveDenuncia')->name('saveDenuncia.vehiculo');
    Route::get('getsubclase/{clasevehiculo}','RegVehiculoController@get_subclase')->name('subclase');
    Route::get('existeDiligencia/{idVehiculo}','RegVehiculoController@existeDiligencia')->name('existeDiligencia');
    Route::get('existenImagenes/{idVehiculo}','VehRecuperadosController@existenImagenes')->name('existenImagenes');
    Route::post('updateDoctos','RegVehiculoController@updateDoctos')->name('updateDoctos');
    Route::get('getregistros/{idCarpeta?}','RegVehiculoController@getRegistros')->name('getregistros');


    //Vehiculos recuperados
    Route::get('ver-vehrecuperados', 'VehRecuperadosController@showVehRecuperados')->name('showVehRecuperados');
    Route::post('guardar_recuperados', 'VehRecuperadosController@saveRecuperados')->name('saveRecuperados');
    Route::get('get-vehrecuperados', 'VehRecuperadosController@getVehRecuperados')->name('getVehRecuperados');
    Route::post('saveImage', 'VehRecuperadosController@saveImage')->name('saveImage');
    Route::get('deleteImage/{idVeh?}/{idImagen?}', 'VehRecuperadosController@deleteImage')->name('deleteImage');

    // rutas para interconexión
    Route::get('ver-registro/{identyCarpt?}/{idtab?}', 'RegVehiculoController@showDenuncia')->name('showDenunciaConect');
    Route::get('ver-vehrecuperados/{identyCarpt?}', 'VehRecuperadosController@showVehRecuperados')->name('showVehRecuperadosConect');
    Route::get('test_carpeta/{identyCarpt?}', 'VehRecuperadosController@test_carpeta')->name('test_carpeta');
});


//Obtener coincidencias VRR y UIJP
//Vehiculos Robados
Route::get('get_robados/{placa?}/{serie?}/{motor?}/{token?}', 'CoincidenciasController@getVehRobados')->name('get_robados');
Route::get('get_countRobados/{placa?}/{serie?}/{motor?}/{token?}', 'CoincidenciasController@getCountVehRobados')->name('get_countRobados');
//Vehiculos Involucrados
Route::get('get_involucrados/{placa?}/{serie?}/{motor?}', 'CoincidenciasController@getVehInvolucrados')->name('get_involucrados');
Route::get('get_countInvolucrados/{placa?}/{serie?}/{motor?}/{nrpv?}', 'CoincidenciasController@getCountVehInvolucrados')->name('get_countInvolucrados');
//Vehiculos Recuperados
Route::get('get_recuperados/{placa?}/{serie?}/{motor?}/{token?}', 'CoincidenciasController@getVehRecuperados')->name('get_recuperados');
Route::get('get_countRecuperados/{placa?}/{serie?}/{motor?}/{token?}', 'CoincidenciasController@getCountVehRecuperados')->name('get_countRecuperados');
//Prueba llave para tabla bus


// Route::get('get_vrr/{placa?}/{serie?}/{motor?}/{nrpv?}/{token?}', 'RegVehiculoController@getCoincidencia')->name('getCoincidencia');

// Route::get('coincidencia_vrr/{placa?}/{serie?}/{motor?}/{nrpv?}/{token?}', 'RegVehiculoController@getCoincidencia')->name('getCoincidencia');
// Route::get('registros_vrr/{placa?}/{serie?}/{motor?}/{tipoCons?}/{token?}', 'RegVehiculoController@getRegisterCoi')->name('registros_vrr');

// Route::get('coincidencia_uipj/{placa?}/{serie?}/{motor?}/{nrpv?}/{token?}', 'RegVehiculoController@getCoincidenciaUIPJ')->name('getCoincidenciauipj');
// // Route::get('registros_uipj/{placa?}/{serie?}/{motor?}/{tipoCons?}', 'RegVehiculoController@getRegistrosUipj')->name('registros_uipj');
// Route::get('verificacion_vrr/{placa?}/{serie?}/{motor?}', 'VehRecuperadosController@getRegistrosVrr')->name('verificacion_vrr');
// Route::get('verificacion_carpetas/{placa?}/{serie?}/{motor?}', 'VehRecuperadosController@getRegistrosUipj')->name('verificacion_carpetas');




Route::get('getvehiculouipj','RegVehiculoController@get_vehiculo_uipj')->name('getvehiculouipj');
Route::post('actualizar-registro','RegVehiculoController@update')->name('updateRegistro');
Route::post('actualizar-validacion','RegVehiculoController@actualizarValidacion')->name('actualizarValidacion');

//Api rest for niv
Route::get('/verificacion-niv/{num_serie}','ExamenController@niv')->name('niv');
Route::get('/get_position/{idMunicipio?}','ExamenController@get_position')->name('get_position');

Route::get('getMockup/{idCarpeta?}','MockUpController@Mockup')->name('getMockup');
Route::get('getDB/{idCarpeta}','MockUpController@getDB')->name('getDB');
//Generador de codigo QR
Route::get('qr/{msj}/{size?}/{tipo?}/{idEstado?}/{idMunicipio?}', 'PreVehiculoController@qr')->name('qr');
//Routuas pdf
Route::get('descargable-registros/{folio?}/{idEstado?}/{idMunicipio?}','PdfController@getIndex')->name('generar-pdf');
Route::get('descargable-reportevehiculos/{idVeh}','PdfController@reporte')->name('generar-reportevehiculos');
Route::get('descargable-reportealtavehiculo/{idVeh}','PdfController@reporteAltaVehiculo')->name('generar-reportealtavehiculo');
Route::post('descargable-reportecoincidencias','PdfController@reporteCoincidencias')->name('generar-reporteCoincidencias');


//Panel de validación
Route::get('ver-panelv', 'PanelVehiculoController@showPanel')->name('showPanel');
Route::post('saveValidacion', 'PanelVehiculoController@saveValidacion')->name('saveValidacion');
Route::get('statusRevision/{idVeh?}/{entidad?}', 'PanelVehiculoController@statusRevision')->name('statusRevision');



//Datatables

Route::get('ver-registros', 'RegVehiculoController@registroVehiculos')->name('registroVehiculos');
Route::get('ver-registrosrecuperados', 'VehRecuperadosController@registroVehiculosRecuperados')->name('registroVehiculosRecuperados');

Route::get('getDoctos/{idVeh}', 'RegVehiculoController@getDoctosVeh')->name('getDoctosVeh');
Route::get('getImagenRec/{idVeh}', 'VehRecuperadosController@getImagenRec')->name('getImagenRec');
Route::get('storage/doctos/{filename}', function ($filename)
{
    $path = storage_path('public/doctos/' . $filename);
    //echo $path;
    if (!File::exists($path)) {
        abort(404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
})->name('getImg');

Route::get('validacionRobados/{idVeh?}', 'RegVehiculoController@validacionRobadosV')->name('validacionRobados');

Route::get('validacionRobadosData/{idVeh?}', 'RegVehiculoController@validacionRobadosVdata')->name('validacionRobadosData');

//RECUPERADOS BUS

Route::get('validacionRecuperados/{idVeh?}', 'VehRecuperadosController@validacionRecuperados')->name('validacionRecuperados');
Route::get('validacionRecuperadosData/{idVeh?}', 'VehRecuperadosController@validacionRecuperadosData')->name('validacionRecuperadosData');
Route::post('validacionBus-recuperados','VehRecuperadosController@validacionRecuperadosBus')->name('validacionRecuperadosBus');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Bitacora
Route::get('bitacora', 'BitacoraController@index')->name('bitacora');
Route::get('api/bitacora', 'BitacoraController@apiBitacora')->name('api.bitacora');

//Docs en word
Route::post('formato-diligencia', 'DocxController@getDiligencia')->name('formato-diligencia');
Route::get('getDilidencia/{idVeh}', 'RegVehiculoController@getDilidencia')->name('getDilidencia');

Route::post('cargar-doc-diligencia', 'RegVehiculoController@diligenciaDocs')->name('docUpDiligencia');


//regis verhiculos recuperador

Route::post('load-web-cam','VehRecuperadosController@subirImagenes')->name('webcam-recuperados-load');



//estadisticas
Route::get('estadistica-maps','EstadisticasController@index')->name('estadisca-gral');
Route::get('estadistica-robados','EstadisticasController@vehiculosRobados')->name('registros-robo');
Route::get('estadistica-recuperados','EstadisticasController@vehiculosRecuperados')->name('registros-recuperados');

//Carpeta
//Route::get('get_carpeta', 'RegVehiculoController@getcarpeta');

//Notificaciones beta
Route::get('notificacion_panel','PanelVehiculoController@notificacion_panel')->name('notificacion_panel');
Route::get('notificacion_uppanel/{id}','PanelVehiculoController@notificacion_uppanel')->name('notificacion_uppanel');
Route::get('notificacion_robados','PanelVehiculoController@notificacion_robados')->name('notificacion_robados');
Route::get('notificacion_uprobados/{id}','PanelVehiculoController@notificacion_uprobados')->name('notificacion_uprobados');

//Recovered vehicles

 Route::get('vehiculos-entregados','VehEntregadoController@index')->name('index-entregados');
 Route::get('ver-vehentregados','VehEntregadoController@showVehEntregados')->name('showVehEntregados');
 Route::post('save-vehentregado','VehEntregadoController@saveVehEntregado')->name('saveVehEntregado');
 Route::get('getvehentregados','VehEntregadoController@getVehEntregados')->name('getVehEntregados');
 Route::get('persona_entrega/{persona?}/{idCarpeta?}','VehEntregadoController@getPersonas')->name('persona_entrega');
 Route::get('validacionEntregados/{idVeh?}', 'VehEntregadoController@validacionEntregados')->name('validacionEntregados');
 Route::get('validacionEntregadosData/{idVeh?}', 'VehEntregadoController@validacionEntregadosData')->name('validacionEntregadosData');
 Route::post('actualizarValidacionEntregados/', 'VehEntregadoController@actualizarValidacionEntregados')->name('actualizarValidacionEntregados');


//Ruta de prueba
//Route::get('pruebaDatos/{id}', 'VehRecuperadosController@save_recuperado_bus')->name('pruebaDatos');

/*------------ Actulizar fotoPerfil -------------------------------*/
        Route::post('actulizar-foto', 'UserController@updateFoto')->name('update.foto');

//Sesiones

Route::get('session_uat','SessionController@sessionUAT')->name('uat');
Route::get('errorlogin', 'RegVehiculoController@errorlogin')->name('error.login');

//Rutas para el Historial
Route::get('historial-vrr','HistorialController@index')->name('index-historial');
Route::get('historial-preregistro','HistorialPreregistroController@index')->name('historial-preregistro');
Route::get('getpreregistros','HistorialPreregistroController@getPreregistros')->name('getpreregistros');
Route::get('getHistorial','HistorialController@getRegistros')->name('getHistorial');

//Rutas para el servicio SOAP (END POINT)

Route::get('consult-soap-service/{placa}/{serie}','SoapController@consultaRepuve')->name('consult-soap-service');
Route::get('consult-soap-sat/{rfc_emisor}/{rfc_receptor}/{importe}/{uuid}','SoapController@consultaSAT')->name('consult-soap-sat');
//Route::get('consult-soap-service/{placa}/{serie}','HistorialController@getRegistros')->name('consult-soap-service');
