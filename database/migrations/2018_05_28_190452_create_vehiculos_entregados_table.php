<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiculosEntregadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehiculos_entregados', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idVeh')->unsigned();
            $table->integer('SERIE_ALTERADA')->default(0);
            $table->integer('MOTOR_ALTERADO')->default(0);
            $table->string('CALLE_ENTREGA',80);
            $table->string('NUMEXT_ENTREGA',20);
            $table->string('NUMINT_ENTREGA',20);
            $table->string('COLONIA_ENTREGA',40);
            $table->integer('ID_MUNICIPIO_ENTREGA')->unsigned();
            $table->integer('ID_ENTIDAD_ENTREGA')->unsigned();
            $table->string('CP_ENTREGA',10);
            $table->string('REFERENCIA_ENTREGA',80);
            $table->integer('INSPECCION')->default(0);
            $table->date('FECHA_ENTREGA');
            $table->time('HORA_ENTREGA')->nullable();
            $table->string('SERIE',20);
            $table->string('MOTOR',20);
            $table->integer('MONTO_VEHICULO');
            $table->integer('ID_FUENTE_ENTREGA')->default(0);
            $table->string('FACTURA_VEHICULO',20);
            $table->date('FECHA_FACTURA');
            // $table->string('CRED_ELECT_PROP',20)->default('');
            // $table->string('PASAPORTE_PROP',20)->default('');
            // $table->string('CEDULA_PROF_PROP',20)->default('');
            // $table->string('COMPROB_DOMIC_PROP',20);
            $table->integer('PERSONA_ENTREGA');
            $table->timestamps();

            $table->foreign('idVeh')->references('id')->on('vehiculo')->onDelete('cascade');
            $table->foreign('ID_MUNICIPIO_ENTREGA')->references('id')->on('cat_municipio')->onDelete('cascade');
            $table->foreign('ID_ENTIDAD_ENTREGA')->references('id')->on('cat_estado')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehiculos_entregados');
    }
}
