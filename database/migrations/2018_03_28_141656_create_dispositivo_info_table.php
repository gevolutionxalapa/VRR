<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDispositivoInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dispositivo_info', function (Blueprint $table) {
            $table->increments('id');
            $table->string('navegador', 200);
            $table->ipAddress('ip', 15);
            $table->string('equipo', 50);
            $table->string('so', 100);
            $table->decimal('latitud', 17, 15);
            $table->decimal('longitud', 17, 15);
            $table->integer('idVehiculo')->unsigned();

            $table->foreign('idVehiculo')->references('id')->on('vehiculo')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dispositivo_info');
    }
}
