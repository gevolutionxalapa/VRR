<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelDocsVehCat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('rel_docs_veh_cat', function (Blueprint $table) {
        $table->string('destino',500);
        // $table->integer('idDocumentos')->unsigned();
        $table->integer('idVehiculo')->unsigned();
        $table->integer('idSubCategoriaDocumento')->unsigned();
        $table->integer('idImagen')->default(0);
        $table->timestamps();
        // $table->foreign('idDocumentos')->references('id')->on('cat_documentos')->onDelete('cascade');
        $table->foreign('idVehiculo')->references('id')->on('vehiculo')->onDelete('cascade');
        $table->foreign('idSubCategoriaDocumento')->references('id')->on('subcategoria_documento')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::dropIfExists('rel_docs_veh_cat');
    }
}
