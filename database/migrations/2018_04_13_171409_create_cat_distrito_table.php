<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatDistritoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_distrito', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',100)->unique();
            $table->integer('idFiscalia')->unsigned();
            $table->timestamps();

            $table->foreign('idFiscalia')->references('id')->on('cat_fiscalia')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_distrito');
    }
}
