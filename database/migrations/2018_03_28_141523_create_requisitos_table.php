<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequisitosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requisitos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('factura', 150);
            $table->string('tenencia', 150);
            $table->string('ordenamientoVehicular', 150);
            $table->string('tarjetaCirculacion', 150);
            $table->string('pedimientoImportacion', 150);
            $table->string('identificacionAnverso', 150);
            $table->string('identificacionReverso', 150);
            $table->string('tituloPropiedadAnverso', 150);
            $table->string('tituloPropiedadReverso', 150);
            $table->integer('idVehiculo')->unsigned();

            $table->foreign('idVehiculo')->references('id')->on('vehiculo')->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requisitos');
    }
}
