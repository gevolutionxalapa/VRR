<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubcategoriaDocumento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('subcategoria_documento', function (Blueprint $table) {
          $table->increments('id');
          $table->string('nombre', 50)->unique();
          $table->string('almacenamiento', 550);
          $table->string('descripción', 250);
          $table->integer('idCategoriaDocumento')->unsigned();
          $table->foreign('idCategoriaDocumento')->references('id')->on('categoria_documento')->onDelete('cascade');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subcategoria_documento');
    }
}
