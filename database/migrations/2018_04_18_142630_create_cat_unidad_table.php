<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatUnidadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_unidad', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',100)->unique();
            $table->string('longitud',15)->default('NA');
            $table->string('latitud',15)->default('NA');
            //$table->integer('idMunicipio')->unsigned();
            //$table->integer('idEstado')->unsigned();
            $table->string('calle',200)->default('NA');
            $table->string('numero',10)->default('NA');
            $table->string('colonia',100)->default('NA');
            $table->integer('idCp')->default(0);

            //$table->foreign('idMunicipio')->references('id')->on('cat_municipio')->onDelete('cascade');
            //$table->foreign('idEstado')->references('id')->on('cat_estado')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_unidad');
    }
}
