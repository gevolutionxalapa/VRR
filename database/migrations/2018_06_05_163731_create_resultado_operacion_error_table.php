<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultadoOperacionErrorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resultado_operacion_error', function (Blueprint $table) {
          //$table->increments('id');
            $table->integer('ID_ALTERNA');
            $table->integer('ESTADO_EMISOR');
            $table->string('LLAVE');
            $table->string('TIPO_OPERACION',10);
            $table->string('TIPO_INFORMACION',20);
            $table->string('COD_ERROR',50);
            $table->string('DESC_ERROR',300);  
            $table->integer('ID');
            $table->integer('ESTATUS');  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resultado_operacion_error');
    }
}
