<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiculoRobadoBus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('vehiculo_robado_bus', function (Blueprint $table) {
          $table->increments('id_bus');
          $table->integer('id_alterna')->unsigned()->default(0);
          $table->integer('id_fuente')->unsigned()->default(0);
          $table->string('nrpv', 20)->nullable();
          $table->string('averiguacion',30)->default("");
          $table->dateTime('fecha_averigua');
          $table->string('hora_averigua', 5)->default("");
          $table->string('agencia_mp', 40)->default("");
          $table->string('agente_mp', 60)->default("");
          $table->integer('id_modalidad')->unsigned()->default(0);
          $table->integer('responsables')->unsigned()->default(0);
          $table->integer('victimas')->unsigned()->default(0);
          $table->string('peculiaridades',50)->default("");
          $table->string('senas_delincuente',50)->default("");
          $table->string('vestimenta',50)->default("");
          $table->string('comportamiento',50)->default("");
          $table->integer('id_puesto')->unsigned()->default(0);
          $table->dateTime('fecha_robo');
          $table->string('hora_robo',8)->default("");
          $table->string('calle_robo',80)->default("");
          $table->string('num_ext_robo',20)->default("");
          $table->string('num_int_robo',20)->default("");
          $table->string('colonia_robo',60)->default("");
          $table->integer('id_municipio_robo')->unsigned()->default(0);
          $table->integer('id_entidad_robo')->unsigned()->default(0);
          $table->string('cp_robo',10)->default("");
          $table->string('referencia_robo', 80)->default("");
          $table->integer('id_tipo_lugar')->unsigned()->default(0);
          $table->integer('id_delito')->unsigned()->default(0);
          $table->integer('id_arma_asoc')->unsigned()->default(0);
          $table->integer('id_marca_asoc')->unsigned()->default(0);
          $table->integer('id_submarca_asoc')->unsigned()->default(0);
          $table->string('nombre_den', 40)->default("");
          $table->string('paterno_den', 20)->default("");
          $table->string('materno_den', 20)->default("");
          $table->string('rfc_den', 15)->default("");
          $table->string('curp_den', 18)->default("");
          $table->string('licencia_den', 15)->default("");
          $table->string('pasaporte_den', 15)->default("");
          $table->integer('sexo_den')->unsigned()->default(0);
          $table->string('calle_den',80)->default("");
          $table->string('numext_dom_den',20)->default("");
          $table->string('numint_dom_den',20)->default("");
          $table->string('colonia_den',60)->default("");
          $table->integer('id_municipio_den')->unsigned()->default(0);
          $table->integer('id_entidad_den')->unsigned()->default(0);
          $table->string('cp_den',10)->default("");
          $table->string('telefono_den',20)->default("");
          $table->string('correo_den',40)->default("");
          $table->string('placa',10)->default("");
          $table->string('permiso',10)->default("");
          $table->integer('id_entidad_placa')->unsigned()->default(0);
          $table->integer('id_marca')->unsigned()->default(0);
          $table->integer('id_submarca')->unsigned()->default(0);
          $table->integer('modelo')->unsigned()->default(0);
          $table->integer('id_color')->unsigned()->default(0);
          $table->string('serie', 20)->default("");
          $table->string('motor',20)->default("");
          $table->integer('id_clase_vehiculo')->unsigned();
          $table->integer('id_tipo_vehiculo')->unsigned();
          $table->integer('id_tipo_uso')->unsigned();
          $table->string('senas', 100)->nullable();
          $table->integer('id_procedencia')->unsigned();
          $table->integer('id_aseguradora')->unsigned();


          $table->string('MOVIMIENTO', 6)->default("");
          $table->string('fiscalia',50)->default("");
          $table->integer('id')->unsigned()->default(0);
          $table->string('robo')->default("");
          $table->dateTime('fechaact')->default(DB::raw('CURRENT_TIMESTAMP'));
          $table->integer('idVehiculo')->unsigned()->default(0);
          $table->string('busFGE')->default("");
          $table->string('busCNS')->default("");
          $table->string('busCNSKey')->default("");
          $table->dateTime('fechaenv')->default(date("1900-01-01"));
          $table->string('horaenv')->default("");
          $table->dateTime('fecharesp')->default(date("1900-01-01"));
          $table->string('horaresp')->default("");
          $table->string('llave_externa')->default("");
          $table->string('placa_extranjera', 150)->default("");
          $table->integer('status_muestra')->default(0);
          $table->timestamps();
          $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        // Schema::dropIfExists('vehiculo_robado_bus');
    }
}
