<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiculosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehiculo', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('placa_extranjera')->default(0);
            $table->string('placas', 10)->default('');
            $table->integer('modelo');
            $table->string('nrpv', 20)->default("");
            $table->string('permiso', 10)->default("");
            $table->string('numSerie', 20)->default("");
            $table->string('numMotor', 20)->default("");
            $table->string('senasPartic', 100)->default("");
            $table->string('token', 6)->default('');
            $table->boolean('status_token', 6)->default(1);
            $table->string('folioDoctoCirc',10)->default('');
            $table->integer('status')->unsigned()->default(1);
            $table->integer('status_repuve')->unsigned()->default(0);
            $table->integer('tipoDenuncia')->unsigned()->default(1);
            $table->integer('idSubmarca')->unsigned()->default(24403);
            $table->integer('idColor')->unsigned()->default(99);
            $table->integer('idTipoVehiculo')->unsigned()->default(999);
            $table->integer('idTipoUso')->unsigned()->default(99);
            $table->integer('idProcedencia')->unsigned()->default(4);
            $table->integer('idAseguradora')->unsigned()->default(25);
            $table->integer('idEstado')->unsigned()->default(33);
            // $table->integer('idEstadoPlaca')->unsigned()->default(33);
            $table->integer('idMunicipio')->unsigned()->default(2496);
            $table->integer('idLocalidad')->unsigned()->default(670);
            $table->integer('idDocumentoCirculacion')->unsigned()->default(1);
            $table->integer('status_policia')->unsigned()->default(0);
            $table->integer('status_enlace')->unsigned()->default(0);
            $table->integer('status_diligencia')->unsigned()->default(0);
            $table->string('observacion', 150)->default("");
            $table->string('telefono', 12)->default("");
            $table->string('numeroeconomico', 20)->default("");
            $table->integer('porcentaje')->default(0);
            $table->integer('propietario_asc')->default(3);



            $table->foreign('status')->references('id')->on('status_vehiculo')->onDelete('cascade');
            $table->foreign('tipoDenuncia')->references('id')->on('tipo_denuncia')->onDelete('cascade');
            $table->foreign('idSubmarca')->references('id')->on('cat_submarca')->onDelete('cascade');
            $table->foreign('idColor')->references('id')->on('cat_color')->onDelete('cascade');
            $table->foreign('idTipoVehiculo')->references('id')->on('cat_tipo_vehiculo')->onDelete('cascade');
            $table->foreign('idTipoUso')->references('id')->on('cat_tipo_uso')->onDelete('cascade');
            $table->foreign('idProcedencia')->references('id')->on('cat_procedencia')->onDelete('cascade');
            $table->foreign('idAseguradora')->references('id')->on('cat_aseguradora')->onDelete('cascade');
            $table->foreign('idEstado')->references('id')->on('cat_estado')->onDelete('cascade');
            $table->foreign('idMunicipio')->references('id')->on('cat_municipio')->onDelete('cascade');
            $table->foreign('idLocalidad')->references('id')->on('cat_localidad')->onDelete('cascade');


            $table->string('llave_externa')->default("");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehiculos');
    }
}
