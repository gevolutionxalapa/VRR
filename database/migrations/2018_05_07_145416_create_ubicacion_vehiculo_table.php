<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUbicacionVehiculoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ubicacion_vehiculo', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('latitud', 17, 15);
            $table->decimal('longitud', 17, 15);
            $table->integer('status');
            $table->integer('idDeposito')->unsigned();
            $table->integer('idVehiculo')->unsigned();
            $table->foreign('idVehiculo')->references('id')->on('vehiculo')->onDelete('cascade');
            $table->foreign('idDeposito')->references('id')->on('cat_deposito')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ubicacion_vehiculo');
    }
}
