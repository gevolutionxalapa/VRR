<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiculoRecuperadoBus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('vehiculo_recuperado_bus', function (Blueprint $table) {
          $table->increments('id_bus');
          $table->integer('ID_ALTERNA')->unsigned()->default(0);
          $table->integer('ID_FUENTE')->unsigned()->default(0);
          $table->string('DOCUMENTO', 20)->default("");
          $table->dateTime('FECHA_DOCTO');
          $table->string('HORA_DOCTO', 5)->default("");
          $table->string('AGENCIA_DOCTO', 40)->default("");
          $table->string('AGENTE_DOCTO', 60)->default("");
          $table->string('PLACA', 10)->default("");
          $table->string('PERMISO', 10)->default("");
          $table->string('SERIE', 20)->default("");
          $table->string('MOTOR', 20)->default("");
          $table->string('SENAS',100)->default("");
          $table->integer('ID_ENTIDAD_RECUPERA')->unsigned();
          $table->integer('ID_DEPOSITO')->unsigned();
          $table->string('CALLE_REC',80)->default("");
          $table->string('NUMEXT_REC',20)->default("");
          $table->string('NUMINT_REC',20)->default("");
          $table->string('COLONIA_REC',40)->default("");
          $table->integer('ID_MUNICIPIO_REC')->unsigned();
          //$table->string('NOM_MUNICIPIO_REC',40)->default("");
          $table->string('CP_REC',10)->default("");
          $table->string('REFERENCIA_REC',80)->default("");
          $table->integer('ID_COLOR')->unsigned();
          //$table->string('NOMBRE_COLOR',30)->default("");
          $table->dateTime('FECHA_REC');
          $table->string('HORA_REC', 5)->default("");

          $table->string('LLAVE_EXTERNA')->default("");
          $table->integer('IDVEHICULO')->unsigned();
          $table->string('BUSFGE')->default("");
          $table->string('BUSCNS', 9)->default("");
          $table->string('BUSCNSKEY')->default("");
          $table->dateTime('fechaenv')->default(date("1900-01-01"));
          $table->string('horaenv', 50)->default("");
          $table->dateTime('fecharesp')->default(date("1900-01-01"));
          $table->string('horaresp', 50)->default("");
          $table->integer('IDBUS')->unsigned();
          $table->string('FISCALIA', 6)->default("");
          $table->dateTime('FECHAACT')->default(date("1900-01-01"));
          $table->string('AVERIGUACION',50)->default("");
          $table->integer('id')->unsigned()->default(0);
          $table->integer('status_muestra')->default(0);
          $table->timestamps();
          $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('vehiculo_recuperado_bus');
    }
}
