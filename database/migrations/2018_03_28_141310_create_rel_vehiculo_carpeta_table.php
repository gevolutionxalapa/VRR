<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelVehiculoCarpetaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rel_vehiculo_carpeta', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numCarpeta',30);
            $table->integer('idCarpeta');
            $table->integer('idVehiculo')->unsigned();
            $table->integer('status')->default(1);
            $table->integer('numFiscal')->default(0);
            $table->integer('idUnidad')->default(0);
            $table->integer('nivel')->default(0);
            $table->string('tokenSesion')->default('NA');
            $table->foreign('idVehiculo')->references('id')->on('vehiculo')->onCascade('delete');
            //$table->foreign('idDelito')->references('id')->on('cat_delito');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rel_vehiculo_carpeta');
    }
}
