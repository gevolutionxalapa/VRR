<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiculoRecuperado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehiculos_recuperados', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idVeh')->unsigned();
            $table->string('calle_rec',80);
            $table->string('numext_rec',20);
            $table->string('numint_rec',20)->nullable();
            $table->string('colonia_rec',40);
            $table->integer('id_municipio_rec')->unsigned();
            $table->integer('id_entidad_rec')->unsigned();
            $table->string('cp_rec',10);
            $table->date('fecha_rec');
            $table->time('hora_rec')->nullable();
            $table->timestamps();

            $table->foreign('idVeh')->references('id')->on('vehiculo')->onDelete('cascade');
            $table->foreign('id_municipio_rec')->references('id')->on('cat_municipio')->onDelete('cascade');
            $table->foreign('id_entidad_rec')->references('id')->on('cat_estado')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
