<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemporalVehiculoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temporal_vehiculo', function (Blueprint $table) {
            $table->integer('id')->default(0);
            $table->string('placas', 9)->default('');
            $table->integer('modelo')->default(0);
            $table->string('nrpv', 50)->default('');
            $table->string('permiso', 50)->default('');
            $table->string('numSerie', 17)->default('');
            $table->string('numMotor', 50)->default('');
            $table->string('senasPartic', 150)->default('');
            $table->string('token', 6)->default('');
            $table->boolean('status_token', 6)->default(0);
            $table->string('folioDoctoCirc',50)->default('');
            $table->integer('status')->unsigned()->default(1);
            $table->integer('tipoDenuncia')->unsigned()->default(0);
            $table->integer('idSubmarca')->unsigned()->default(0);
            $table->integer('idColor')->unsigned()->default(0);
            $table->integer('idTipoVehiculo')->unsigned()->default(0);
            $table->integer('idTipoUso')->unsigned()->default(0);
            $table->integer('idProcedencia')->unsigned()->default(0);
            $table->integer('idAseguradora')->unsigned()->default(0);
            $table->integer('idEstado')->unsigned()->default(0);
            $table->integer('idMunicipio')->unsigned()->default(0);
            $table->integer('idLocalidad')->unsigned()->default(0);
            $table->integer('idDocumentoCirculacion')->unsigned()->default(0);
            $table->string('observacion', 150)->default("");
            $table->integer('registradoPor')->default(0);
			$table->integer('visto')->default(0);


            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temporal_vehiculo');
    }
}
