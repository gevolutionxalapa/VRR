<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email',50)->unique();
            $table->string('password');
            $table->string('name', 50);
            /*$table->integer('idUnidad')->unsigned();
            
            $table->string('apePaterno', 50);
            $table->string('apeMaterno', 50);
            $table->integer('numFiscal');
            $table->enum('nivel', ['1', '2', '3', '4', '5'])->default('1');
            $table->integer('idMunicipio');
            $table->integer('idRegion');
            $table->integer('idDistrito');*/
            
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
