<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultadoOperacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resultado_operacion', function (Blueprint $table) {
            //$table->increments('id');
            $table->integer('ID_ALTERNA');
            $table->integer('ESTADO_EMISOR');
            $table->string('LLAVE',40);
            $table->string('TIPO_OPERACION',10);
            $table->string('TIPO_INFORMACION',20);
            $table->integer('EXPEDIENTE_PM');
            $table->string('FECHA_ACTUALIZACION',10);
            $table->string('HORA',10);
            $table->integer('ID');
            $table->integer('ESTATUS');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resultado_operacion');
    }
}
