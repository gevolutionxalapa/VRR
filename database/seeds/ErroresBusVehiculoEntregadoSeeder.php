<?php

use Illuminate\Database\Seeder;

class ErroresBusVehiculoEntregadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          DB::table('errores_bus_vehiculo_entregado')->insert([
            ['clave'=>  15,'descripcion' => 'EL VALOR ENTIDAD Y MUNICIPIO EN CE NO SON CORRESPONDIENTES CON EL CATALOGO.'],
            ['clave'=>  16,'descripcion' => 'EL VALOR FECHA_ENTREGA EN CE NO PUEDE SER MENOR A 01-01-1900.'],
            ['clave'=>  17,'descripcion' => 'EL VALOR FECHA_FACTURA EN CE NO PUEDE SER MENOR A 01-01-1900.'],
            ['clave'=>  18,'descripcion' => 'EL VALOR FECHA_ENTREGA EN CE NO PUEDE SER MAYOR A LA FECHA ACTUAL.'],
            ['clave'=>  19,'descripcion' => 'EL VALOR FECHA_FACTURA EN CE NO PUEDE SER MAYOR A LA FECHA ACTUAL.'],
            ['clave'=>  20,'descripcion' => 'EL VALOR FECHA DE ENTREGA EN CE NO PUEDE SER MAYOR A LA FECHA DEL ROBO.'],
            ['clave'=>  21,'descripcion' => 'EL VALOR FECHA DE ENTREGA EN CE NO PUEDE SER MAYOR A LA FECHA DE RECUPERACION.'],
            ['clave'=>  22,'descripcion' => 'EL VALOR FECHA DE FACTURA EN CE NO PUEDE SER MAYOR A LA FECHA DEL ROBO.'],
            ['clave'=>  23,'descripcion' => 'EL VALOR FECHA DE FACTURA EN CE NO PUEDE SER MAYOR A LA FECHA DE RECUPERACION.']
          ]);
    }
}
