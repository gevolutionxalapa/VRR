<?php

use Illuminate\Database\Seeder;

class DistritoSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cat_distrito')->insert([
        	['id' => 1, 'nombre' => 'Distrito I-Pánuco', 'idFiscalia' => '1'],
        	['id' => 2, 'nombre' => 'Distrito II-Ozuluama', 'idFiscalia' => '1'],
        	['id' => 3, 'nombre' => 'Distrito III-Tantoyuca', 'idFiscalia' => '1'],
        	['id' => 4, 'nombre' => 'Distrito IV-Huayacocotla','idFiscalia' => '1'],
        	['id' => 5, 'nombre' => 'Distrito V-Chicontepec','idFiscalia' => '1'],
        	['id' => 6, 'nombre' => 'Distrito VI-Tuxpan','idFiscalia' => '2'],
        	['id' => 7, 'nombre' => 'Distrito VII-Poza Rica de Hidalgo','idFiscalia' => '2'],
        	['id' => 8, 'nombre' => 'Distrito VIII-Papantla','idFiscalia' => '2'],
        	['id' => 9, 'nombre' => 'Distrito IX-Misantla','idFiscalia' => '3'],
        	['id' => 10, 'nombre' => 'Distrito X-Jalacingo','idFiscalia' => '3'],
        	['id' => 11, 'nombre' => 'Distrito XI-Xalapa','idFiscalia' => '3'],
        	['id' => 12, 'nombre' => 'Distrito XII-Coatepec','idFiscalia' => '3'],
        	['id' => 13, 'nombre' => 'Distrito XIII-Huatusco','idFiscalia' => '4'],
        	['id' => 14, 'nombre' => 'Distrito XIV-Córdoba','idFiscalia' => '4'],
        	['id' => 15, 'nombre' => 'Distrito XV-Orizaba','idFiscalia' => '4'],
        	['id' => 16, 'nombre' => 'Distrito XVI-Zongolica','idFiscalia' => '4'],
        	['id' => 17, 'nombre' => 'Distrito XVII-Veracruz','idFiscalia' => '5'],
        	['id' => 18, 'nombre' => 'Distrito XVIII-Cosamaloapan','idFiscalia' => '6'],
        	['id' => 19, 'nombre' => 'Distrito XIX-San Andrés Tuxtla','idFiscalia' => '6'],
        	['id' => 20, 'nombre' => 'Distrito XX-Acayucan','idFiscalia' => '7'],
        	['id' => 21, 'nombre' => 'Distrito XXI-Coatzacoalcos','idFiscalia' => '7']
        ]);
    }
}
