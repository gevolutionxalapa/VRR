<?php

use Illuminate\Database\Seeder;

class UnidadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cat_unidad')->insert([
        	['id' => 1, 'nombre' => 'UIPJ Panuco','longitud'=>'-98.182188','latitud'=>'22.050307','calle'=>'Francisco Colorado ','numero'=>'307','colonia'=>'Maza Gutierrez','idCp' => '93996'],
        	['id' => 2, 'nombre' => 'UIPJ Ozuluama','longitud'=>'-97.8478812','latitud'=>'21.6611265','calle'=>'Higueras S/N, Camino a la Comunidad El Laurel','numero'=>'S/N','colonia'=>'5 de mayo','idCp' => '92080'],
        	['id' => 3, 'nombre' => 'UIPJ Tantoyuca','longitud'=>'-98.2298589','latitud'=>'21.3461728','calle'=>'Gabino Barreda','numero'=>'206','colonia'=>'El Jagüey','idCp' => '92126'],
        	['id' => 4, 'nombre' => 'UIPJ Huayacocotla','longitud'=>'-98.4794309','latitud'=>'20.5372438','calle'=>'Corregidora, Esq. Con Corregidora','numero'=>'S/N', 'colonia'=>'Zona Centro','idCp' => '92600'],
        	['id' => 5, 'nombre' => 'UIPJ Chicontepec','longitud'=>'-98.174690','latitud'=>'20.972454','calle'=>'Avenida López Mateos','numero'=>'58','colonia'=>'Zona Centro','idCp' => ' 92700'],
        	['id' => 6, 'nombre' => 'UIPJ Tuxpan','longitud'=>'-97.3709431','latitud'=>'20.9428908','calle'=>'Emiliano Zapata Esq Maples Arce' ,'numero'=>'1','colonia'=>'Adolfo Ruiz Cortines','idCp' => '92880'],
        	['id' => 7, 'nombre' => 'UIPJ Papantla','longitud'=>'-97.3342234','latitud'=>'20.4555001','calle'=>'Francisco I. Madero  esquina Bugambilias','numero'=>'730','colonia'=>'Centro','idCp' => '93400'],
        	['id' => 8, 'nombre' => 'UIPJ Poza Rica','longitud'=>'-97.4707771','latitud'=>'20.5311883','callee'=>'Boulevard Lázaro Cárdenas','numero'=>'800','colonia'=>'Morelos','idCp' => '93340'],
        	['id' => 9, 'nombre' => 'UIPJ Xalapa','longitud'=>'-96.876148','latitud'=>'19.507855','calle'=>'Circuito Guizar y Valencia','numero'=>'147','colonia'=>'Reserva Territorial','idCp' => '91096'],
        	['id' => 10, 'nombre' => 'UIPJ Jalancingo','longitud'=>'-97.301624','latitud'=>'19.801852','calle'=>'Carretera Federal Teziutlán-Perote','numero'=>' S/N ','colonia'=>'Cuartel Segundo','idCp' => '93660'],
        	['id' => 11, 'nombre' => 'UIPJ Misantla','longitud' => '-96.855680', 'latitud' => '19.943782', 'calle' => 'Camino real esquina Leona Vicario', 'numero' => '5', 'colonia' => 'Linda Vista','idCp'=> '93829'],
        	['id' => 12, 'nombre' => 'UIPJ Coatepec', 'longitud' => '-96.9513691', 'latitud' => '19.4595143', 'calle' => 'Ayuntamiento', 'numero' => '2', 'colonia' => 'Centro', 'idCp' => '91500'],
        	['id' => 13, 'nombre' => 'UIPJ Huatusco', 'longitud' => '-96.9678715', 'latitud' => '19.1507987', 'calle' => 'Avenida 1 Poniente entre calles 6 y 8', 'numero' => '824', 'colonia' => 'Centro', 'idCp' => '94100'],
        	['id' => 14, 'nombre' => 'UIPJ Córdoba', 'longitud' => '-96.9365753', 'latitud' => '18.8898538', 'calle' => 'Calle 9 esquina Avenida 9 y 7', 'numero' => '714', 'colonia' => 'Centro', 'idCp' => '94500'],
        	['id' => 15, 'nombre' => 'UIPJ Orizaba', 'longitud' => '-97.1027408', 'latitud' => '18.8511151', 'calle' => 'Norte 8 entre Oriente 3 y 5', 'numero' => '125-131', 'colonia' => 'Centro', 'idCp' => '94300'],
        	['id' => 16, 'nombre' => 'UIPJ Zongolica','longitud' => '-96.9982045', 'latitud' => '18.6670992', 'calle' => 'Calle Benito Juárez y Vicente Guerrero', 'numero' => '19', 'colonia' => 'Nuestro Señor del Recuerdo','idCp' => '93660'],
        	['id' => 17, 'nombre' => 'UIPJ Boca del Río','longitud'=>'-96.1159244','latitud'=>'19.1676436','calle'=>'Boulevard Adolfo Ruíz Cortines','numero'=>' 19 ','colonia'=>'Fraccionamiento Costa Verde','idCp' => '94299'],
        	['id' => 18, 'nombre' => 'UIPJ Veracruz','longitud'=>'-96.1690636','latitud'=>'19.1477889','calle'=>'Km. 8 Carretera Veracruz-Xalapa','numero'=>' S/N ','colonia'=>'Predio Rustico el Jobo','idCp' => '91963'],
        	['id' => 19, 'nombre' => 'UIPJ Cosamaloapan','longitud' => '-95.7969464', 'latitud' => '18.3644455', 'calle' => 'Venustiano Carranza', 'numero' => '214', 'colonia' => 'Centro','idCp' => '95400'],
        	['id' => 20, 'nombre' => 'UIPJ San Andres Tuxtla', 'longitud'=>'-95.2117732','latitud'=>'18.4473844', 'calle' =>'Independencia Esq con Callejón Virgilio Uribe', 'numero' => 'SN', 'colonia' => 'centro','idCp' => '95700'],
	        ['id' => 21, 'nombre' => 'UIPJ Coatzacoalcos','longitud' => '-94.4384301', 'latitud' => '18.1446594', 'calle' => 'Av. Veracruz', 'numero' => '223', 'colonia' => 'Petrolera','idCp' => '96500'],
        	['id' => 22, 'nombre' => 'UIPJ Acayucan','longitud' => '-94.913015', 'latitud' => '17.946736', 'calle' => 'Ignacio Zaragoza', 'numero' => '212', 'colonia' => 'Cruz verde','idCp' => '96079'],
			['id' => 23, 'nombre' => 'UIPJ Álamo (Subunidad)', 'longitud' => '-97.6809003', 'latitud' => '20.9200479', 'calle' => 'Abelardo L. Rodriguez', 'numero' => '18','colonia' => 'Adolfo López Mateo', 'idCp' => '92730'],
            ['id' => 24, 'nombre' => 'UIPJ Perote (Subunidad)','longitud'=>'-97.2476766','latitud'=>'19.5644853','calle'=>'Agustín Melgar Esq. Con Ponciano','numero'=>'S/N','colonia'=>'Niños Heroes', 'idCp' => '91275'],
            ['id' => 25, 'nombre' => 'UIPJ Martínez de la Torre (Subunidad)','longitud'=>'-97.0617290','latitud'=>'20.0619520','calle'=>'5 de Febrero No. 13 Esq. Luis G. Franco','numero'=>'S/N','colonia'=>'Constitución', 'idCp' => '0'],
            ['id' => 26, 'nombre' => 'UIPJ Tlapacoyan (Subunidad)','longitud'=>'-97.2165340','latitud'=>'19.9647710','calle'=>'Gutiérrez Zamora','numero'=>'104','colonia'=>'Centro', 'idCp' => '0'],
            ['id' => 27, 'nombre' => 'UIPJ Fortín de las Flores (Subunidad)','longitud'=>'-96.875428','latitud'=>'19.508299','calle'=>'','numero'=>'','colonia'=>'', 'idCp' => '0'],
            ['id' => 28, 'nombre' => 'UIPJ Paso del Macho (Subunidad)','longitud'=>'-96.875428','latitud'=>'19.508299','calle'=>'','numero'=>'','colonia'=>'', 'idCp' => '0'],
            ['id' => 29, 'nombre' => 'UIPJ Cd. Mendoza (Subunidad)','longitud'=>'-96.875428','latitud'=>'19.508299','calle'=>'','numero'=>'','colonia'=>'', 'idCp' => '0'],  
            ['id' => 30, 'nombre' => 'UIPJ Las Choapas (Subunidad)','longitud'=>'-94.0959061','latitud'=>'17.9157908','calle'=>'Boulevard México','numero'=>'101','colonia'=>'México', 'idCp' => '96980'],    
            ['id' => 31, 'nombre' => 'UIPJ Minatitlán (Subunidad)','longitud'=>'-94.5573519','latitud'=>'17.9923559','calle'=>'Guillermo Prieto','numero'=>'1','colonia'=>'Santa Clara', 'idCp' => '96730'],  
            ['id' => 32, 'nombre' => 'UIPJ Nanchital (Subunidad)','longitud'=>'-94.4082408','latitud'=>'18.0542682','calle'=>'San Javier','numero'=>'1','colonia'=>'Guadalupe Tepeyac', 'idCp' => '96360'], 
            ['id' => 33, 'nombre' => 'UIPJ Cd. Isla (Subunidad)','longitud'=>'-94.4082408','latitud'=>'18.0542682','calle'=>'Raúl Sandoval Esq. Nicolás bravo','numero'=>'1','colonia'=>'Guadalupe Tepeyac', 'idCp' => '96360'], 
        	['id' => 34, 'nombre' => 'UIPJ Playa Vicente(Subunidad)','longitud' => '-95.8125331', 'latitud' => '17.8254751', 'calle' => 'Calle de la Rosa', 'numero' => 'SN', 'colonia' => ' Emiliano Zapata','idCp' => '95600'],
        	['id' => 35, 'nombre' => 'UIPJ Boca del Rio (Subunidad)','longitud' => '-96.1157912', 'latitud' => '19.1676532', 'calle' => 'Boulevard Adolfo Ruíz Cortine', 'numero' => '19', 'colonia' => 'Fraccionamiento Costa Verde','idCp' => '94299'],
        	['id' => 36, 'nombre' => 'UIPJ Tatahuicapan (Subunidad)','longitud' => '-94.7606018', 'latitud' => '18.2455629', 'calle' => 'Emiliano Zapata', 'numero' => 'SN', 'colonia' => 'Centro','idCp' => '95940'],
        	['id' => 37, 'nombre' => 'UIPJ Cosoleacaque (Subunidad)','longitud' => '-94.6365512', 'latitud' => '17.9974747', 'calle' => 'Héroes de Totoapan', 'numero' => '1 Altos', 'colonia' => 'Barrio Primero','idCp' => '96353'],
        	['id' => 38, 'nombre' => 'UIPJ Jesús Carranza (Subunidad)','longitud' => '-95.0296701', 'latitud' => '17.4356839', 'calle' => 'Aldama', 'numero' => '115', 'colonia' => 'Centro','idCp' => '96950'],
        	['id' => 39, 'nombre' => 'UIPJ Cardel (Subunidad)','longitud' => '-96.3788834', 'latitud' => '19.3667787', 'calle' => 'Dr. Juan Martínez', 'numero' => '72', 'colonia' => 'Centro','idCp' => '91680'],
        	['id' => 40, 'nombre' => 'Tezonapa','longitud' => '-96.6871915', 'latitud' => '18.6015308', 'calle' => 'Morelos', 'numero' => 'SN', 'colonia' => ' Las Flores','idCp' => '95096'],
        	['id' => 41, 'nombre' => 'UIPJ Tihuatlan','longitud' => '-97.5373465', 'latitud' => '20.7208383', 'calle' => 'Cuauhtémoc', 'numero' => '36', 'colonia' => 'Centro','idCp' => '92900'],
        	['id' => 42, 'nombre' => 'UIPJ Alvarado','longitud' => '-95.7583932', 'latitud' => '18.7670882', 'calle' => 'Hidalgo', 'numero' => '100', 'colonia' => 'Centro','idCp' => '95270'],
        	['id' => 43, 'nombre' => 'UIPJ Tres Valles','longitud' => '-96.1338859', 'latitud' => '18.237547', 'calle' => 'Benito Juarez', 'numero' => '408', 'colonia' => 'Centro','idCp' => '95300'],
        	['id' => 44, 'nombre' => 'Fiscalía Coordinadora Especializada (Familia, Mujeres , Niñas y Niños y de Trata) XALAPA','longitud' => '-96.9291648', 'latitud' => '19.5312089', 'calle' => 'Avila Camacho ', 'numero' => '191', 'colonia' => 'Ferrer Guardia','idCp' => '91020'],
            ['id' => 45, 'nombre' => 'UIPJ Tempoal (Subunidad)','longitud' => '-98.3927988', 'latitud' => '21.5210972', 'calle' => 'Independencia esq Libertad', 'numero' => 'S/N', 'colonia' => 'Zona Centro','idCp' => '92060']
        ]);
       
    }
}
