<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        //$this->call(DelitoSeeder::class);
        $this->call(ClaseVehiculoSeeder::class);
        $this->call(ColorSeeder::class);
        $this->call(MarcaSeeder::class);
        $this->call(AseguradoraSeeder::class);
        $this->call(ProcedenciaSeeder::class);
        $this->call(TipoUsoSeeder::class);
        $this->call(TipoVehiculoSeeder::class);
        $this->call(SubmarcaSeeder::class);
        $this->call(Submarca2Seeder::class);
        $this->call(TipoDenunciaSeeder::class);
        $this->call(StatusVehiculoSeeder::class);
        $this->call(DocumentosCirculacionSeeder::class);
        // $this->call(DocumentosSeeder::class);
        $this->call(CategoriaDocumentoSeeder::class);
        $this->call(SubcategoriaDocumentoSeeder::class);
        $this->call(DepositoSeeder::class);
        $this->call(ErrorBusMandamientosSeeder::class);
        $this->call(ErroresBusVehiculoEntregadoSeeder::class);
        $this->call(ErroresBusVehiculoRecuperadoSeeder::class);




        //Carga de las localidades
        $this->call(EstadoSeeder::class);
        $this->call(MunicipioSeeder::class);
        $this->call(LocalidadesCDMXSeeder::class);
        $this->call(LocalidadesChiapasSeeder::class);
        $this->call(LocalidadesHidalgoSeeder::class);
        $this->call(LocalidadesOaxacaSeeder::class);
        $this->call(LocalidadesPueblaSeeder::class);
        $this->call(LocalidadesSanLuisPotosiSeeder::class);
        $this->call(LocalidadesTabascoSeeder::class);
        $this->call(LocalidadesTamaulipasSeeder::class);
        $this->call(LocalidadesVeracruzSeeder::class);
        $this->call(FiscaliaSeeder::class);
        $this->call(DistritoSeed::class);
        $this->call(RelDistritoMunicipio::class);
        $this->call(UnidadSeeder::class);
        $this->call(RelDistritoUnidad::class);
        $this->call(VehiculosSeeder::class);
        $this->call(RelVehCarpSeeder::class);
        $this->call(VehiculoRobadoBusSeeder::class);
        $this->call(VehiculoRecuperadoBusSeeder::class);
        $this->call(VehiculoEntregadoBusSeeder::class);
        $this->call(VehiculosRecuperadosSeeder::class);
        $this->call(VehiculosEntregadosSeeder::class);
        //$this->call(RelDocsVehCatSeeder::class);
        $this->call(UbicacionVehiculoSeeder::class);

    }
}
