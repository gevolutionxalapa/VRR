<?php

use Illuminate\Database\Seeder;

class StatusVehiculoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status_vehiculo')->insert([
            ['id' => 1,'nombre' => 'Robado'],
            ['id' => 2,'nombre' => 'Recuperado'],
            ['id' => 3,'nombre' => 'Entregado']
        ]);
    }
}
