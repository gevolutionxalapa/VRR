<?php

use Illuminate\Database\Seeder;

class SubcategoriaDocumentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('subcategoria_documento')->insert([
          ['id'=>  1,
            'nombre' => 'Credencial de elector',
            'almacenamiento' => 'identificacion/credencialelector/completa/',
            'descripción' => 'Documento oficial IFE, INE',
            'idCategoriaDocumento'=>1
          ],
          ['id'=>  2,
            'nombre' => 'Pasaporte',
            'almacenamiento' => 'identificacion/pasaporte/completa/',
            'descripción' => 'Documento de viaje expedido a los mexicanos para acreditar identidad en el extranjero',
            'idCategoriaDocumento'=>1
          ],
          ['id'=>  3,
            'nombre' => 'Cartilla militar',
            'almacenamiento' => 'identificacion/cartillamilitar/completa/',
            'descripción' => 'Documento de identificación militar',
            'idCategoriaDocumento'=>1
          ],
          ['id'=>  4,
            'nombre' => 'Licencia de conducir',
            'almacenamiento' => 'identificacion/licenciaconducir/completa/',
            'descripción' => 'Documento de identificación licencia de conducir',
            'idCategoriaDocumento'=>1
          ],
          ['id'=>  5,
            'nombre' => 'Cédula profesional',
            'almacenamiento' => 'identificacion/cedulaprofesional/completa/',
            'descripción' => 'Documento de identificación licencia de cedula profesional',
            'idCategoriaDocumento'=>1
          ],
          ['id'=>  6,
            'nombre' => 'Factura',
            'almacenamiento' => 'acreditacion-propiedad/factura/completa/',
            'descripción' => 'Documento comprobatorio de que eres el propietario de un vehículo',
            'idCategoriaDocumento'=>2
          ],
          ['id'=>  7,
             'nombre' => 'Carta factura (Menos a 60 dias)',
             'almacenamiento' => 'acreditacion-propiedad/carta-factura/completa/',
             'descripción' => 'Documento asociado a la “tenencia” o uso de vehículos, que cubre impuestos',
             'idCategoriaDocumento'=>2
           ],
           ['id'=>  8,
             'nombre' => 'Comodato de la factura',
             'almacenamiento' => 'acreditacion-propiedad/comodato/completa/',
             'descripción' => 'Documento asociado a la “tenencia” o uso de vehículos, que cubre impuestos',
             'idCategoriaDocumento'=>2
           ],
           ['id'=>  9,
             'nombre' => 'Placa',
             'almacenamiento' => 'documentos-circulacion/placa/completa/',
             'descripción' => 'Documento asociado a la “tenencia” o uso de vehículos, que cubre impuestos',
             'idCategoriaDocumento'=>3
           ],
           ['id'=>  10,
             'nombre' => 'Permiso de circulación',
             'almacenamiento' => 'documentos-circulacion/permisos-circulacion/completa/',
             'descripción' => 'Documento asociado a el impuesto que deben pagar anualmente todos los dueños de vehículos motorizados y que va en beneficio de las municipalidades',
             'idCategoriaDocumento'=>3
           ],
           ['id'=>  11,
             'nombre' => 'Constancia de empadronamiento',
             'almacenamiento' => 'documentos-circulacion/empadronamiento/completa/',
             'descripción' => 'Es el documento que se autoriza a los propietarios de establecimientos el desempeño de su actividad comercial, industrial o de servicios.',
             'idCategoriaDocumento'=>3
           ],
           ['id'=>  12,
             'nombre' => 'Acta de estravio de placas',
             'almacenamiento' => 'documentos-circulacion/acta-estravio/completa/',
             'descripción' => 'Documento asociado a la “tenencia” o uso de vehículos, que cubre impuestos',
             'idCategoriaDocumento'=>3
           ],
           ['id'=>  13,
             'nombre' => 'Formato Diligencia',
             'almacenamiento' => 'formatos/diligencia/',
             'descripción' => 'Documento asociado a la “tenencia” o uso de vehículos, que cubre impuestos',
             'idCategoriaDocumento'=>4
           ],
           ['id'=>  14,
             'nombre' => 'Imagen del vehiculo recuperado',
             'almacenamiento' => 'fotografias/vehiculo-recuperado/',
             'descripción' => 'Imagenes asociadas al estado visual de un vehiculo recuperado',
             'idCategoriaDocumento'=>5
           ],
      ]);
    }
}
