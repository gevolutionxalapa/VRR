<?php

use Illuminate\Database\Seeder;

class CategoriaDocumentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('categoria_documento')->insert([
          ['id'=>  1,'nombre' => 'Identificación','descripcion' => 'Documentos que permiten definir la identidad del usuario'],
          ['id'=>  2,'nombre' => 'Acreditación de propiedad','descripcion' => 'Documentos que permiten identificar la propiedad del vehiculo'],
          ['id'=>  3,'nombre' => 'Documentos de circulación','descripcion' => 'Documentos que saber identificar los permisos de circulación'],
          ['id'=>  4,'nombre' => 'Formatos','descripcion' => 'Documentos asociados a un formato'],
          ['id'=>  5,'nombre' => 'Vehiculos Recuperados','descripcion' => 'Fotografias asociadas a un vehiculo recuperado']
      ]);
    }
}
