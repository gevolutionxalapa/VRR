<?php

use Illuminate\Database\Seeder;

class FiscaliaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cat_fiscalia')->insert([
        	['id' => 1, 'nombre' => 'Fiscalía Regional Zona Norte-Tantoyuca'],
        	['id' => 2, 'nombre' => 'Fiscalía Regional Zona Norte-Tuxpan'],
        	['id' => 3, 'nombre' => 'Fiscalía Regional Zona Centro-Xalapa'],
        	['id' => 4, 'nombre' => 'Fiscalía Regional Zona Centro-Córdoba'],
        	['id' => 5, 'nombre' => 'Fiscalía Regional Zona Centro-Veracruz'],
        	['id' => 6, 'nombre' => 'Fiscalía Regional Zona Centro-Cosamaloapan'],
        	['id' => 7, 'nombre' => 'Fiscalía Regional Zona Sur-Coatzacoalcos']
        ]);
    }
}
