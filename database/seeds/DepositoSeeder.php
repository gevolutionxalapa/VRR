<?php

use Illuminate\Database\Seeder;

class DepositoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cat_deposito')->insert([
        	['id' => 1, 'nombre' => 'PLATAFORMA VEHICULAR DEL PUEBLITO'],
        	['id' => 2, 'nombre' => 'PLATAFORMA VEHICULAR COATEPEC'],
        	['id' => 3, 'nombre' => 'DEPOSITO OFICIAL VERACRUZ'],
        	['id' => 4, 'nombre' => 'PLATAFORMA VEHICULAR OFICIAL DE LA PGJ- XALAPA 2000'],
        	['id' => 5, 'nombre' => 'Otro']
        ]);
    }
}
