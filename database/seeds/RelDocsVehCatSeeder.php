<?php

use Illuminate\Database\Seeder;

class RelDocsVehCatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rel_docs_veh_cat')->insert([
          ['destino'=>'identificacion/credencialelector/completa/ine.jpg','idvehiculo'=>1,'idSubCategoriaDocumento' => 1, 'idImagen' => 0],
          ['destino'=>'acreditacion-propiedad/factura/completa/facturadeventa.jpg','idvehiculo'=>1,'idSubCategoriaDocumento' => 6, 'idImagen' => 0],

          ['destino'=>'identificacion/credencialelector/completa/ine3.jpg','idvehiculo'=>2,'idSubCategoriaDocumento' => 1, 'idImagen' => 0],
          ['destino'=>'acreditacion-propiedad/factura/completa/facturadeventa.jpg','idvehiculo'=>2,'idSubCategoriaDocumento' => 6, 'idImagen' => 0],


          ['destino'=>'identificacion/credencialelector/completa/ine2.jpg','idvehiculo'=>3,'idSubCategoriaDocumento' => 1, 'idImagen' => 0],
          ['destino'=>'acreditacion-propiedad/factura/completa/facturadeventa.jpg','idvehiculo'=>3,'idSubCategoriaDocumento' =>6, 'idImagen' => 0],

          ['destino'=>'identificacion/credencialelector/completa/ine4.jpg','idvehiculo'=>4,'idSubCategoriaDocumento' => 1, 'idImagen' => 0],
          ['destino'=>'acreditacion-propiedad/factura/completa/facturadeventa.jpg','idvehiculo'=>4,'idSubCategoriaDocumento' => 6, 'idImagen' => 0],


          ['destino'=>'identificacion/pasaporte/completa/pasaporte.jpg','idvehiculo'=>5,'idSubCategoriaDocumento' => 2, 'idImagen' => 0],
          ['destino'=>'acreditacion-propiedad/factura/completa/facturadeventa.jpg','idvehiculo'=>5,'idSubCategoriaDocumento' => 6, 'idImagen' => 0],

        





        ]);
    }
}
