<?php

use Illuminate\Database\Seeder;

class RelDistritoUnidad extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('rel_distrito_unidad')->insert([
         	['idDistrito' => '1', 'idUnidad' => '1'],
         	['idDistrito' => '2', 'idUnidad' => '2'],
         	['idDistrito' => '3', 'idUnidad' => '3'],
            ['idDistrito' => '3', 'idUnidad' => '45'],
         	['idDistrito' => '4', 'idUnidad' => '4'],
         	['idDistrito' => '5', 'idUnidad' => '5'],
         	['idDistrito' => '6', 'idUnidad' => '6'],
         	['idDistrito' => '6', 'idUnidad' => '23'],
            ['idDistrito' => '6', 'idUnidad' => '41'],
         	['idDistrito' => '7', 'idUnidad' => '8'],
            ['idDistrito' => '7', 'idUnidad' => '41'],
         	['idDistrito' => '8', 'idUnidad' => '7'],
         	['idDistrito' => '9', 'idUnidad' => '11'],
            ['idDistrito' => '9', 'idUnidad' => '25'],
         	['idDistrito' => '10', 'idUnidad' => '10'],
            ['idDistrito' => '10', 'idUnidad' => '24'],
            ['idDistrito' => '10', 'idUnidad' => '25'],
            ['idDistrito' => '10', 'idUnidad' => '26'],
         	['idDistrito' => '11', 'idUnidad' => '9'],
         	['idDistrito' => '12', 'idUnidad' => '12'],
         	['idDistrito' => '13', 'idUnidad' => '13'],
         	['idDistrito' => '14', 'idUnidad' => '14'],
         	['idDistrito' => '14', 'idUnidad' => '27'],
         	['idDistrito' => '14', 'idUnidad' => '28'],
         	['idDistrito' => '14', 'idUnidad' => '40'],
         	['idDistrito' => '15', 'idUnidad' => '15'],
            ['idDistrito' => '15', 'idUnidad' => '29'],
         	['idDistrito' => '16', 'idUnidad' => '16'],
         	['idDistrito' => '17', 'idUnidad' => '18'],
         	['idDistrito' => '17', 'idUnidad' => '17'],
         	['idDistrito' => '17', 'idUnidad' => '39'],
         	['idDistrito' => '17', 'idUnidad' => '42'],
         	['idDistrito' => '18', 'idUnidad' => '19'],
         	['idDistrito' => '18', 'idUnidad' => '34'],
         	['idDistrito' => '18', 'idUnidad' => '43'],
         	['idDistrito' => '19', 'idUnidad' => '20'],
            ['idDistrito' => '19', 'idUnidad' => '33'],
         	['idDistrito' => '20', 'idUnidad' => '22'],
         	['idDistrito' => '20', 'idUnidad' => '38'],
         	['idDistrito' => '21', 'idUnidad' => '21'],
         	['idDistrito' => '21', 'idUnidad' => '30'],
         	['idDistrito' => '21', 'idUnidad' => '31'],
         	['idDistrito' => '21', 'idUnidad' => '32'],
         	['idDistrito' => '21', 'idUnidad' => '36'],
         	['idDistrito' => '21', 'idUnidad' => '37']

         ]);
    }
}
