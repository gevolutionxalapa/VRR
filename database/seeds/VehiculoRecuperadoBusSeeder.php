<?php

use Illuminate\Database\Seeder;

class VehiculoRecuperadoBusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vehiculo_recuperado_bus')->insert([
          ['id'=>1,'idBus'=>1,'idVehiculo'=>1,'ID_ALTERNA'=>0,'placa'=>'VD01603','calle_rec'=>'Constitución','numext_rec'=>1995,'numint_rec'=>'','colonia_rec'=>'Emiliano zapata','ID_ENTIDAD_RECUPERA'=>30,'id_municipio_rec'=>186,'cp_rec'=>'91500',
          'fecha_rec'=>'2013-08-25 19:00:02','fecha_docto'=>'2000-10-03 19:00:02','permiso'=>'SIN INFO','serie'=>'8AFRR5DP4G6393146','motor'=>'8009908012','senas'=>'Rayones laterales','busFGE'=>1,'busCNS'=>1,'busCNSKey'=>0,'llave_externa'=>'30-UIPJ/DXVII/VER1/22/1/2018-1','id_deposito'=>3,'id_color'=>24],

          ['id'=>2,'idBus'=>2,'idVehiculo'=>2,'ID_ALTERNA'=>0,'placa'=>'MXU3785','calle_rec'=>'Ingenio azucarero','numext_rec'=>16,'numint_rec'=>'91b','colonia_rec'=>'Bugambilias','ID_ENTIDAD_RECUPERA'=>30,'id_municipio_rec'=>100,'cp_rec'=>'91608',
          'fecha_rec'=>'2018-02-10 12:44:02','fecha_docto'=>'2010-08-20 19:00:02','permiso'=>'SIN INFO','serie'=>'JM3KE2D74G0719069','motor'=>'9968997001','senas'=>'Parabrisas con cuarteaduras','busFGE'=>0,'busCNS'=>0,'busCNSKey'=>0,'llave_externa'=>'30-UIPJ/DXVII/VER1/22/1/2018-2','id_deposito'=>3,'id_color'=>19],

          ['id'=>3,'idBus'=>3,'idVehiculo'=>3,'ID_ALTERNA'=>0,'placa'=>'1XEZ178','calle_rec'=>'Heriberto Jara','numext_rec'=>86,'numint_rec'=>'11','colonia_rec'=>'Miguel Aleman','ID_ENTIDAD_RECUPERA'=>30,'id_municipio_rec'=>87,'cp_rec'=>'91169',
          'fecha_rec'=>'2016-08-10 12:44:02','fecha_docto'=>'2009-12-11 19:00:02','permiso'=>'SIN INFO','serie'=>'3N1CD15S2WK018182','motor'=>'9898098087','senas'=>'Llantas pintadas de color blanco sin rines','busFGE'=>0,'busCNS'=>2,'busCNSKey'=>0,'llave_externa'=>'30-UIPJ/DXVII/VER1/22/1/2018-3','id_deposito'=>3,'id_color'=>17],

          ['id'=>4,'idBus'=>4,'idVehiculo'=>4,'ID_ALTERNA'=>1,'placa'=>'1XEZ179','permiso'=>'SIN INFO','calle_rec'=>'Manlio Fabio Altamirano','numext_rec'=>5,'numint_rec'=>'10','colonia_rec'=>'Guizar y valencia','ID_ENTIDAD_RECUPERA'=>30,'id_municipio_rec'=>175,'cp_rec'=>'91670',
          'fecha_rec'=>'2018-05-20 12:44:02','fecha_docto'=>'2013-09-30 19:00:02','fecha_docto'=>'2013-08-25 19:00:02','serie'=>'ME1KG0440C2021555','motor'=>'7856009050','senas'=>'Sin espejo lateral derecho','serie'=>'8AFRR5DP4G6393146','motor'=>'8009908012','senas'=>'Rayones laterales','busFGE'=>1,'busCNS'=>1,'busCNSKey'=>0,'llave_externa'=>'30-UIPJ/DXVII/VER1/22/1/2018-4','id_deposito'=>3,'id_color'=>3],

          ['id'=>5,'idBus'=>5,'idVehiculo'=>5,'ID_ALTERNA'=>1,'permiso'=>'SIN INFO','placa'=>'YKU9866','permiso'=>'SIN INFO','calle_rec'=>'Loma de flores','numext_rec'=>7,'numint_rec'=>'','colonia_rec'=>'Sumidero','ID_ENTIDAD_RECUPERA'=>30,'id_municipio_rec'=>23,'cp_rec'=>'91098',
          'fecha_rec'=>'2018-05-12 17:50:02','fecha_docto'=>'2018-06-25 19:00:02','serie'=>'8AFRR5DP4G6393146','motor'=>'8009908012','senas'=>'Rayones laterales','busFGE'=>0,'busCNS'=>3,'busCNSKey'=>0,'llave_externa'=>'30-UIPJ/DXVII/VER1/22/1/2018-5','id_deposito'=>3,'id_color'=>3],


        ]);
    }
}
