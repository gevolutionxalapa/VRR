<?php

use Illuminate\Database\Seeder;

class VehiculosRecuperadosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vehiculos_recuperados')->insert([

          ['id'=>1,'idVeh'=>1,'calle_rec'=>'Constitución','numext_rec'=>1995,'numint_rec'=>'','colonia_rec'=>'Emiliano zapata','id_entidad_rec'=>30,'id_municipio_rec'=>186,'cp_rec'=>'91500','fecha_rec'=>'2013-08-25 19:00:02'],

          ['id'=>2,'idVeh'=>2,'calle_rec'=>'Ingenio azucarero','numext_rec'=>16,'numint_rec'=>'91b','colonia_rec'=>'Bugambilias','id_entidad_rec'=>30,'id_municipio_rec'=>100,'cp_rec'=>'91608','fecha_rec'=>'2018-02-10 12:44:02'],

          ['id'=>3,'idVeh'=>3,'calle_rec'=>'Heriberto Jara','numext_rec'=>86,'numint_rec'=>'11','colonia_rec'=>'Miguel Aleman','id_entidad_rec'=>30,'id_municipio_rec'=>87,'cp_rec'=>'91169','fecha_rec'=>'2016-08-10 12:44:02'],

          ['id'=>4,'idVeh'=>4,'calle_rec'=>'Manlio Fabio Altamirano','numext_rec'=>5,'numint_rec'=>'10','colonia_rec'=>'Guizar y valencia','id_entidad_rec'=>30,'id_municipio_rec'=>175,'cp_rec'=>'91670','fecha_rec'=>'2018-05-20 12:44:02'],

          ['id'=>5,'idVeh'=>5,'calle_rec'=>'Loma de flores','numext_rec'=>7,'numint_rec'=>'','colonia_rec'=>'Sumidero','id_entidad_rec'=>30,'id_municipio_rec'=>23,'cp_rec'=>'91098','fecha_rec'=>'2018-05-12 17:50:02'],





        ]);
    }
}
