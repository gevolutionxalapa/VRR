<?php

use Illuminate\Database\Seeder;

class UbicacionVehiculoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ubicacion_vehiculo')->insert([
          	['latitud'=>20.822460000000000, 'longitud'=>-97.475340000000000, 'status'=> 2, 'idDEposito' => 5, 'idVehiculo' => 1],
          	['latitud'=>19.211910000000000, 'longitud'=>-97.417140000000000, 'status'=> 2, 'idDEposito' => 5, 'idVehiculo' => 2],
          	['latitud'=>22.090000000000000, 'longitud'=>-97.063240000000000, 'status'=> 2, 'idDEposito' => 5, 'idVehiculo' => 3],
          	['latitud'=>19.207050000000000, 'longitud'=>-97.887140000000000, 'status'=> 2, 'idDEposito' => 5, 'idVehiculo' => 4],
        	  ['latitud'=>19.231660000000000, 'longitud'=>-97.787140000000000, 'status'=> 2, 'idDEposito' => 5, 'idVehiculo' => 5],
            ['latitud'=> 20.952460000000000, 'longitud'=> -97.417140000000000,'status'=> 1,'idDEposito'=> 1,'idVehiculo' => 6],
            ['latitud'=> 21.343100000000000, 'longitud'=> -98.235620000000000,'status'=> 1,'idDEposito'=> 2,'idVehiculo' => 7],
            ['latitud'=> 22.036000000000000, 'longitud'=> -98.209530000000000,'status'=> 1,'idDEposito'=> 3,'idVehiculo' => 8],
            ['latitud'=> 19.823550000000000, 'longitud'=> -97.355340000000000,'status'=> 1,'idDEposito'=> 4,'idVehiculo' => 9],
            ['latitud'=> 20.071410000000000, 'longitud'=> -97.053220000000000,'status'=> 1,'idDEposito'=> 5,'idVehiculo' => 10],
            ['latitud'=> 19.932040000000000, 'longitud'=> -96.844480000000000,'status'=> 1,'idDEposito'=> 1,'idVehiculo' => 11],
            ['latitud'=> 19.196600000000000, 'longitud'=> -96.147100000000000,'status'=> 1,'idDEposito'=> 2,'idVehiculo' => 12],
            ['latitud'=> 19.202070000000000, 'longitud'=> -96.160150000000000,'status'=> 1,'idDEposito'=> 3,'idVehiculo' => 13],
            ['latitud'=> 19.200570000000000, 'longitud'=> -96.140840000000000,'status'=> 1,'idDEposito'=> 4,'idVehiculo' => 14],
            ['latitud'=> 19.207910000000000, 'longitud'=> -96.149980000000000,'status'=> 1,'idDEposito'=> 5,'idVehiculo' => 15],
            ['latitud'=> 19.197050000000000, 'longitud'=> -96.136420000000000,'status'=> 1,'idDEposito'=> 1,'idVehiculo' => 16],
            ['latitud'=> 19.191660000000000, 'longitud'=> -96.140370000000000,'status'=> 1,'idDEposito'=> 2,'idVehiculo' => 17],
            ['latitud'=> 19.192750000000000, 'longitud'=> -96.135220000000000,'status'=> 1,'idDEposito'=> 3,'idVehiculo' => 18],
            ['latitud'=> 19.190240000000000, 'longitud'=> -96.129850000000000,'status'=> 1,'idDEposito'=> 4,'idVehiculo' => 19],
            ['latitud'=> 19.199840000000000, 'longitud'=> -96.135000000000000,'status'=> 1,'idDEposito'=> 5,'idVehiculo' => 20],
            ['latitud'=> 19.186670000000000, 'longitud'=> -96.145860000000000,'status'=> 1,'idDEposito'=> 1,'idVehiculo' => 21],
            ['latitud'=> 19.186270000000000, 'longitud'=> -96.135260000000000,'status'=> 1,'idDEposito'=> 2,'idVehiculo' => 22],
            ['latitud'=> 19.177710000000000, 'longitud'=> -96.142770000000000,'status'=> 1,'idDEposito'=> 3,'idVehiculo' => 23],
            ['latitud'=> 19.174880000000000, 'longitud'=> -96.126850000000000,'status'=> 1,'idDEposito'=> 4,'idVehiculo' => 24],
            ['latitud'=> 19.161540000000000, 'longitud'=> -96.140240000000000,'status'=> 1,'idDEposito'=> 5,'idVehiculo' => 25],
            ['latitud'=> 19.154160000000000, 'longitud'=> -96.125220000000000,'status'=> 1,'idDEposito'=> 1,'idVehiculo' => 26],
            ['latitud'=> 19.155700000000000, 'longitud'=> -96.132940000000000,'status'=> 1,'idDEposito'=> 2,'idVehiculo' => 27],
            ['latitud'=> 19.159510000000000, 'longitud'=> -96.132210000000000,'status'=> 1,'idDEposito'=> 3,'idVehiculo' => 28],
            ['latitud'=> 19.161660000000000, 'longitud'=> -96.123410000000000,'status'=> 1,'idDEposito'=> 4,'idVehiculo' => 29],
            ['latitud'=> 19.152300000000000, 'longitud'=> -96.114100000000000,'status'=> 1,'idDEposito'=> 5,'idVehiculo' => 30],
            ['latitud'=> 19.138230000000000, 'longitud'=> -96.128350000000000,'status'=> 1,'idDEposito'=> 1,'idVehiculo' => 31],
            ['latitud'=> 19.131620000000000, 'longitud'=> -96.116290000000000,'status'=> 1,'idDEposito'=> 2,'idVehiculo' => 32],
            ['latitud'=> 19.141270000000000, 'longitud'=> -96.118050000000000,'status'=> 1,'idDEposito'=> 3,'idVehiculo' => 33],
            ['latitud'=> 18.775860000000000, 'longitud'=> -95.769620000000000,'status'=> 1,'idDEposito'=> 4,'idVehiculo' => 34],
            ['latitud'=> 18.782410000000000, 'longitud'=> -95.770310000000000,'status'=> 1,'idDEposito'=> 5,'idVehiculo' => 35],
            ['latitud'=> 18.773710000000000, 'longitud'=> -95.756830000000000,'status'=> 1,'idDEposito'=> 1,'idVehiculo' => 36],
            ['latitud'=> 18.619810000000000, 'longitud'=> -95.664090000000000,'status'=> 1,'idDEposito'=> 2,'idVehiculo' => 37],
            ['latitud'=> 18.612850000000000, 'longitud'=> -95.653580000000000,'status'=> 1,'idDEposito'=> 3,'idVehiculo' => 38],
            ['latitud'=> 18.630790000000000, 'longitud'=> -95.524700000000000,'status'=> 1,'idDEposito'=> 4,'idVehiculo' => 39],
            ['latitud'=> 18.622170000000000, 'longitud'=> -95.521230000000000,'status'=> 1,'idDEposito'=> 5,'idVehiculo' => 40],
            ['latitud'=> 18.632580000000000, 'longitud'=> -95.510800000000000,'status'=> 1,'idDEposito'=> 1,'idVehiculo' => 41],
            ['latitud'=> 18.634490000000000, 'longitud'=> -95.505780000000000,'status'=> 1,'idDEposito'=> 2,'idVehiculo' => 42],
            ['latitud'=> 18.143070000000000, 'longitud'=> -94.472550000000000,'status'=> 1,'idDEposito'=> 3,'idVehiculo' => 43],
            ['latitud'=> 18.138340000000000, 'longitud'=> -94.446450000000000,'status'=> 1,'idDEposito'=> 4,'idVehiculo' => 44],
            ['latitud'=> 18.133040000000000, 'longitud'=> -94.463280000000000,'status'=> 1,'idDEposito'=> 5,'idVehiculo' => 45],
            ['latitud'=> 18.129130000000000, 'longitud'=> -94.437530000000000,'status'=> 1,'idDEposito'=> 1,'idVehiculo' => 46],
            ['latitud'=> 18.123330000000000, 'longitud'=> -94.461900000000000,'status'=> 1,'idDEposito'=> 2,'idVehiculo' => 47],
            ['latitud'=> 17.999630000000000, 'longitud'=> -94.560010000000000,'status'=> 1,'idDEposito'=> 3,'idVehiculo' => 48],
            ['latitud'=> 17.988520000000000, 'longitud'=> -94.555890000000000,'status'=> 1,'idDEposito'=> 4,'idVehiculo' => 49],
            ['latitud'=> 18.006890000000000, 'longitud'=> -94.528590000000000,'status'=> 1,'idDEposito'=> 5,'idVehiculo' => 50],
            ['latitud'=> 17.993830000000000, 'longitud'=> -94.593830000000000,'status'=> 1,'idDEposito'=> 1,'idVehiculo' => 51],
            ['latitud'=> 18.460390000000000, 'longitud'=> -95.303990000000000,'status'=> 1,'idDEposito'=> 2,'idVehiculo' => 52],
            ['latitud'=> 18.449320000000000, 'longitud'=> -95.213690000000000,'status'=> 1,'idDEposito'=> 3,'idVehiculo' => 53],
            ['latitud'=> 18.423590000000000, 'longitud'=> -95.117220000000000,'status'=> 1,'idDEposito'=> 4,'idVehiculo' => 54],
            ['latitud'=> 18.341490000000000, 'longitud'=> -95.830990000000000,'status'=> 1,'idDEposito'=> 5,'idVehiculo' => 55],
            ['latitud'=> 18.901080000000000, 'longitud'=> -96.979060000000000,'status'=> 1,'idDEposito'=> 1,'idVehiculo' => 56],
            ['latitud'=> 18.781510000000000, 'longitud'=> -96.459960000000000,'status'=> 1,'idDEposito'=> 2,'idVehiculo' => 57],
            ['latitud'=> 18.630620000000000, 'longitud'=> -96.251220000000000,'status'=> 1,'idDEposito'=> 3,'idVehiculo' => 58],
            ['latitud'=> 18.986810000000000, 'longitud'=> -96.718130000000000,'status'=> 1,'idDEposito'=> 4,'idVehiculo' => 59],
            ['latitud'=> 19.059520000000000, 'longitud'=> -96.437980000000000,'status'=> 1,'idDEposito'=> 5,'idVehiculo' => 60],
            ['latitud'=> 19.139970000000000, 'longitud'=> -96.987300000000000,'status'=> 1,'idDEposito'=> 1,'idVehiculo' => 61],
            ['latitud'=> 19.557200000000000, 'longitud'=> -97.245480000000000,'status'=> 1,'idDEposito'=> 2,'idVehiculo' => 62],
            ['latitud'=> 19.500250000000000, 'longitud'=> -96.591790000000000,'status'=> 1,'idDEposito'=> 3,'idVehiculo' => 63],
            ['latitud'=> 19.375930000000000, 'longitud'=> -96.372070000000000,'status'=> 1,'idDEposito'=> 4,'idVehiculo' => 64],
            ['latitud'=> 19.784790000000000, 'longitud'=> -97.256460000000000,'status'=> 1,'idDEposito'=> 5,'idVehiculo' => 65],
            ['latitud'=> 20.177140000000000, 'longitud'=> -96.893920000000000,'status'=> 1,'idDEposito'=> 1,'idVehiculo' => 66],
            ['latitud'=> 20.024960000000000, 'longitud'=> -96.652220000000000,'status'=> 1,'idDEposito'=> 2,'idVehiculo' => 67],
            ['latitud'=> 20.473330000000000, 'longitud'=> -97.072440000000000,'status'=> 1,'idDEposito'=> 3,'idVehiculo' => 68],
            ['latitud'=> 21.025540000000000, 'longitud'=> -98.344110000000000,'status'=> 1,'idDEposito'=> 4,'idVehiculo' => 69],
            ['latitud'=> 20.922960000000000, 'longitud'=> -98.212280000000000,'status'=> 1,'idDEposito'=> 5,'idVehiculo' => 70],
            ['latitud'=> 20.922960000000000, 'longitud'=> -97.695920000000000,'status'=> 1,'idDEposito'=> 1,'idVehiculo' => 71],
            ['latitud'=> 20.699600000000000, 'longitud'=> -97.498160000000000,'status'=> 1,'idDEposito'=> 2,'idVehiculo' => 72],
            ['latitud'=> 21.345660000000000, 'longitud'=> -97.679440000000000,'status'=> 1,'idDEposito'=> 3,'idVehiculo' => 73],
            ['latitud'=> 19.965600000000000, 'longitud'=> -97.204280000000000,'status'=> 1,'idDEposito'=> 4,'idVehiculo' => 74],
            ['latitud'=> 20.449210000000000, 'longitud'=> -97.324010000000000,'status'=> 1,'idDEposito'=> 5,'idVehiculo' => 75],
            ['latitud'=> 20.449210000000000, 'longitud'=> -97.324010000000000,'status'=> 1,'idDEposito'=> 3,'idVehiculo' => 76],
            ['latitud'=> 20.449210000000000, 'longitud'=> -97.324010000000000,'status'=> 1,'idDEposito'=> 5,'idVehiculo' => 77],
            ['latitud'=> 20.449210000000000, 'longitud'=> -97.324010000000000,'status'=> 1,'idDEposito'=> 5,'idVehiculo' => 78],
            ['latitud'=> 20.449210000000000, 'longitud'=> -97.324010000000000,'status'=> 1,'idDEposito'=> 5,'idVehiculo' => 79],
            ['latitud'=> 20.449210000000000, 'longitud'=> -97.324010000000000,'status'=> 1,'idDEposito'=> 5,'idVehiculo' => 80],
            ['latitud'=> 20.449210000000000, 'longitud'=> -97.324010000000000,'status'=> 1,'idDEposito'=> 5,'idVehiculo' => 81],
            ['latitud'=> 20.449210000000000, 'longitud'=> -97.324010000000000,'status'=> 1,'idDEposito'=> 5,'idVehiculo' => 82],
            ['latitud'=> 20.449210000000000, 'longitud'=> -97.324010000000000,'status'=> 1,'idDEposito'=> 4,'idVehiculo' => 83],
            ['latitud'=> 20.449210000000000, 'longitud'=> -97.324010000000000,'status'=> 1,'idDEposito'=> 5,'idVehiculo' => 84],
            ['latitud'=> 20.449210000000000, 'longitud'=> -97.324010000000000,'status'=> 1,'idDEposito'=> 4,'idVehiculo' => 85],
            ['latitud'=> 20.449210000000000, 'longitud'=> -96.324010000000000,'status'=> 1,'idDEposito'=> 5,'idVehiculo' => 86],
            ['latitud'=> 20.449210000000000, 'longitud'=> -97.324010000000000,'status'=> 1,'idDEposito'=> 3,'idVehiculo' => 87],
            ['latitud'=> 20.449210000000000, 'longitud'=> -96.324010670000000,'status'=> 1,'idDEposito'=> 5,'idVehiculo' => 88],
            ['latitud'=> 20.449210000000000, 'longitud'=> -97.124010000000000,'status'=> 1,'idDEposito'=> 5,'idVehiculo' => 89],
            ['latitud'=> 20.449210000000000, 'longitud'=> -97.324010000000000,'status'=> 1,'idDEposito'=> 3,'idVehiculo' => 90],


        ]);
    }
}
