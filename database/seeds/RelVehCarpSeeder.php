<?php

use Illuminate\Database\Seeder;

class RelVehCarpSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rel_vehiculo_carpeta')->insert([
        	['id'=>1,'numCarpeta'=>'UIPJ/DXVII/VER1/22/1/2018','idCarpeta'=>2,'idVehiculo'=>86,'status'=>1,'numFiscal'=>1,'idUnidad'=>0,'nivel'=>0],
        	['id'=>2,'numCarpeta'=>'UIPJ/DXVII/VER1/22/1/2018','idCarpeta'=>2,'idVehiculo'=>87,'status'=>1,'numFiscal'=>1,'idUnidad'=>0,'nivel'=>0],
        	['id'=>3,'numCarpeta'=>'UIPJ/DXVII/VER1/22/1/2018','idCarpeta'=>2,'idVehiculo'=>88,'status'=>1,'numFiscal'=>1,'idUnidad'=>0,'nivel'=>0],
        	['id'=>4,'numCarpeta'=>'UIPJ/DXVII/VER1/22/1/2018','idCarpeta'=>2,'idVehiculo'=>89,'status'=>1,'numFiscal'=>1,'idUnidad'=>0,'nivel'=>0],
        	['id'=>5,'numCarpeta'=>'UIPJ/DXVII/VER1/22/1/2018','idCarpeta'=>2,'idVehiculo'=>90,'status'=>1,'numFiscal'=>1,'idUnidad'=>0,'nivel'=>0],
          ['id'=>6,'numCarpeta'=>'UIPJ/DXVII/VER1/22/1/2018','idCarpeta'=>1,'idVehiculo'=>1,'status'=>1,'numFiscal'=>1,'idUnidad'=>0,'nivel'=>0],
        	['id'=>7,'numCarpeta'=>'UIPJ/DXVII/VER1/22/1/2018','idCarpeta'=>2,'idVehiculo'=>2,'status'=>1,'numFiscal'=>1,'idUnidad'=>0,'nivel'=>0],
        	['id'=>8,'numCarpeta'=>'UIPJ/DXVII/VER1/22/1/2018','idCarpeta'=>2,'idVehiculo'=>3,'status'=>1,'numFiscal'=>1,'idUnidad'=>0,'nivel'=>0],
        	['id'=>9,'numCarpeta'=>'UIPJ/DXVII/VER1/22/1/2018','idCarpeta'=>2,'idVehiculo'=>4,'status'=>1,'numFiscal'=>1,'idUnidad'=>0,'nivel'=>0],
        	['id'=>10,'numCarpeta'=>'UIPJ/DXVII/VER1/22/1/2018','idCarpeta'=>2,'idVehiculo'=>5,'status'=>1,'numFiscal'=>1,'idUnidad'=>0,'nivel'=>0],
          ['id'=>11,'numCarpeta'=>'UIPJ/DXVII/VER1/22/1/2018','idCarpeta'=>2,'idVehiculo'=>81,'status'=>1,'numFiscal'=>1,'idUnidad'=>0,'nivel'=>0],
        	['id'=>12,'numCarpeta'=>'UIPJ/DXVII/VER1/22/1/2018','idCarpeta'=>2,'idVehiculo'=>82,'status'=>1,'numFiscal'=>1,'idUnidad'=>0,'nivel'=>0],
        	['id'=>13,'numCarpeta'=>'UIPJ/DXVII/VER1/22/1/2018','idCarpeta'=>2,'idVehiculo'=>83,'status'=>1,'numFiscal'=>1,'idUnidad'=>0,'nivel'=>0],
        	['id'=>14,'numCarpeta'=>'UIPJ/DXVII/VER1/22/1/2018','idCarpeta'=>2,'idVehiculo'=>84,'status'=>1,'numFiscal'=>1,'idUnidad'=>0,'nivel'=>0],
        	['id'=>15,'numCarpeta'=>'UIPJ/DXVII/VER1/22/1/2018','idCarpeta'=>2,'idVehiculo'=>85,'status'=>1,'numFiscal'=>1,'idUnidad'=>0,'nivel'=>0],
        ]);
    }
}
