<?php

use Illuminate\Database\Seeder;

class DocumentosCirculacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cat_documentos_circulacion')->insert([
            ['id'=>  1,'nombre' => 'PLACA NACIONAL'],
            ['id'=>  2,'nombre' => 'PERMISO DE CIRCULACIÓN'],
            ['id'=>  3,'nombre' => 'CONSTANCIA DE EMPADRONAMIENTO'],
            ['id'=>  4,'nombre' => 'ACTA DE EXTRAVÍO DE PLACAS'],
            ['id'=>  5,'nombre' => 'PLACA EXTRANJERA'],
        ]);
    }
}
