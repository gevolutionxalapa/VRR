<?php

use Illuminate\Database\Seeder;

class TipoDenunciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_denuncia')->insert([
            ['id'=>  1,'nombre' => 'Pre-registro'],
            ['id'=>  2,'nombre' => 'Registro']
        ]);
    }
}
