<?php

use Illuminate\Database\Seeder;

class ErroresBusVehiculoRecuperadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('errores_bus_vehiculo_recuperado')->insert([
        ['clave'=>  15,'descripcion' => 'EL VALOR ENTIDAD Y MUNICIPIO EN CR NO SON CORRESPONDIENTES CON EL CATALOGO.'],
        ['clave'=>  16,'descripcion' => 'EL VALOR FECHA_REC EN CR NO PUEDE SER MENOR A 01-01-1900.'],
        ['clave'=>  17,'descripcion' => 'EL VALOR FECHA_DOCTO EN CR NO PUEDE SER MENOR A 01-01-1900.'],
        ['clave'=>  18,'descripcion' => 'EL VALOR FECHA_REC EN CR NO PUEDE SER MAYOR A LA FECHA ACTUAL.'],
        ['clave'=>  19,'descripcion' => 'EL VALOR FECHA_DOCTO EN CR NO PUEDE SER MAYOR A LA FECHA ACTUAL.'],
        ['clave'=>  20,'descripcion' => 'EL VALOR FECHA DE RECUPERACION EN CR NO PUEDE SER MAYOR A LA FECHA DEL ROBO.'],
        ['clave'=>  21,'descripcion' => 'EL VALOR FECHA DE RECUPERACION EN CR NO PUEDE SER MAYOR A LA FECHA DEL DOCUMENTO.'],
        ['clave'=>  22,'descripcion' => 'DEBE INGRESAR UN VALOR PARA PLACA O SERIE EN CR.']
      ]);
    }
}
